#ifndef PLUGINS_H
#define PLUGINS_H

class Plugin
{
public:
  Plugin();
  virtual ~Plugin();

  virtual void init(){};
};

#endif
