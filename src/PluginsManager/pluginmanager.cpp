#include "pluginmanager.h"
#include "plugins.h"

#include <kapp.h>
#include <kstddirs.h>
#include <qdir.h>
#include <qmessagebox.h>

#include <dlfcn.h>


PluginManager* PluginManager::instance = 0L;

PluginManager::PluginManager()
{
  instance = this;
  data = new QList<Plugin>;
  data->setAutoDelete( true );

  QString error;
  QStringList alld = KGlobal::dirs()->resourceDirs("lib");

  for ( QStringList::Iterator it = alld.begin(); it != alld.end(); ++it )
  {
  QDir dir( *it );
  if( dir.cd( "kdestudio/plugins" ) ){
    dir.setFilter( QDir::Files | QDir::Readable );
    dir.setNameFilter( "*.so" );
    QStringList foundPlugins = dir.entryList();

    for ( QStringList::Iterator it = foundPlugins.begin(); it != foundPlugins.end(); ++it )
    {
      QString path = dir.absPath()+"/"+(*it);
      void* Handle = registerPlugin( path, error );
      if ( Handle ){
        Plugin* (*registerFunc)();
        registerFunc = (Plugin*(*)(void)) dlsym( Handle, "Register" );
        Plugin* plg = registerFunc();
        data->append( plg );
      } else {
        QMessageBox::critical( 0L, "Plugin error", error );
      }
    }
  }
  }
}

PluginManager::~PluginManager()
{
  debug("delete manager");
  delete data;
}

void* PluginManager::registerPlugin( QString path, QString& error )
{
  void* Handle = dlopen( path.data() , RTLD_GLOBAL | RTLD_NOW );
  if( !Handle )
  {
    error.sprintf( "Unable to open plugin %s\n%s", path.data(), dlerror() );
  } else {
    Plugin* (*registerFunc)();
    registerFunc = (Plugin*(*)(void)) dlsym( Handle, "Register" );
    if(!registerFunc)
    {
      error.sprintf( "Unable to init plugin %s\n%s", path.data(), dlerror() );
      dlclose( Handle );
      Handle = 0;
    }
  }
  return Handle;
}

void PluginManager::initPlugins()
{
  for ( Plugin* plg = data->first(); plg != 0L; plg = data->next() )
  {
    plg->init();
  }
}
