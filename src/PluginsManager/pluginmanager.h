#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#define PLUGIN PluginManager::instance

#include <qstring.h>
#include <qlist.h>
class Plugin;

class PluginManager
{
public:
  PluginManager();
  ~PluginManager();

  void initPlugins();

  static PluginManager* instance;

private:
  void* registerPlugin( QString path, QString& error );
  QList<Plugin>* data;
};

#endif


