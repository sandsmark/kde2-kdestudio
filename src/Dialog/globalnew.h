#ifndef GLOBALNEW_H
#define GLOBALNEW_H

#include <qdialog.h>

class KDockTabCtl;
class WorkspaceListView;

class QWidget;
class QString;
class QLabel;
class QLineEdit;
class QPushButton;
class QGroupBox;
class QRadioButton;
class QButtonGroup;
class WorkspaceListView;

class GlobalNew : public QDialog
{ Q_OBJECT
public: 
	GlobalNew( QWidget * parent, const char* path = 0, int page = 0 , const char * name = 0 );
	~GlobalNew();

private:
  QPushButton *m_pOK;
  QPushButton *m_pCancel;
	
	KDockTabCtl *tab;
	QWidget *workspaceTab;
	QWidget *projectTab;
	QWidget *fileTab;

  void updateTrees();

  void slotCreateWorkspace();
  void createWorkspaceTab();
  QString projectDir;
  QLineEdit *m_pNameEdit;
  QLineEdit *m_pDirEdit;

  void slotCreateProject();
  void createProjectTab();
  QRadioButton* b1;
  QRadioButton* b2;
  QRadioButton* b3;
  QRadioButton* b4;
  QRadioButton* b5;
  WorkspaceListView* create_in_tree;
  QLineEdit* prName;

  void slotCreateFile();
  void createFileTab();
  WorkspaceListView* f_create_in_tree;
  WorkspaceListView* file_tree;
  QLineEdit* fname;
  QButtonGroup* f_buttonGroup;
  QRadioButton* b10;
  QRadioButton* b11;
  QRadioButton* b12;
  QRadioButton* b13;
  QRadioButton* b14;
  QRadioButton* b15;


private slots:
  void closeDialog();
  void slotDirSelect();
  void slotCreate();
};

#endif
