#ifndef DLGADDEXISTINGFILE_H
#define DLGADDEXISTINGFILE_H

#include <qdialog.h>

class QListBox;
class QPushButton;
class WorkspaceListView;

class DlgAddExistingFile : public QDialog
{ Q_OBJECT
public:
  DlgAddExistingFile( QWidget *parent, const QString& path );
  ~DlgAddExistingFile();

protected slots:
 void sourceButtonClicked();
 void slotOk();

private:
  WorkspaceListView* prj_tree;
  QListBox* source;
  QPushButton* cancelButton;
  QPushButton* sourceButton;
  QPushButton* okButton;
};

#endif
