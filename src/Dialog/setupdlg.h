#ifndef SETUPDLG_H
#define SETUPDLG_H

#include <qdialog.h>
class QButtonGroup;
class QWidget;
class QRadioButton;
class KColorButton;

class SetupDlg : public QDialog
{ Q_OBJECT
public:
	SetupDlg( QWidget * parent = 0L , const char*  = 0 );
	~SetupDlg();

private slots:
  void slotDone();

private:
	QButtonGroup* buttonGroup;
	QRadioButton* isOpenLast;
	QRadioButton* isShowWelcome;
	QRadioButton* isDoNothing;

  KColorButton* cout;
  KColorButton* pout;
  KColorButton* fpath;
  KColorButton* ftext;
  KColorButton* perror;
  KColorButton* fline;
  KColorButton* cerror;
};


#endif


