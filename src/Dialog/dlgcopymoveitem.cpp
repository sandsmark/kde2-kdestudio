#include "dlgcopymoveitem.h"
#include "workspacelistview.h"
#include "studio.h"
#include "studioview.h"
#include "seditwindow.h"
#include "workspace.h"

#include <qmessagebox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qdir.h>

#include <kapp.h>
#include <kconfig.h>

DlgCopyMoveItem::DlgCopyMoveItem(QWidget *parent, const QString& path, Workspace* w, Action action )
: QDialog( parent, 0, true )
{
  mode = path.isEmpty() ? doWorkspace : doFile;
  act = action;

  workspace = w;
  file = path;

  KConfig* config = kapp->config();
  config->setGroup("DlgCopyMoveItem");
  int dw = config->readNumEntry("Width",300);
  int dh = config->readNumEntry("Height",300);
  resize(dw,dh);
  move( (qApp->desktop()->width() - width())/2, (qApp->desktop()->height() - height())/2 );

  QGridLayout* grid = new QGridLayout( this );
  grid->setSpacing( 6 );
  grid->setMargin( 11 );

  okButton = new QPushButton( this );
  okButton->setDefault( true );

  grid->addWidget( okButton, 2, 2 );
  QSpacerItem* spacer = new QSpacerItem( 179, 20, QSizePolicy::Expanding, QSizePolicy::Fixed );
  grid->addItem( spacer, 2, 0 );

  QPushButton* cancelButton = new QPushButton( this );
  cancelButton->setText( "Cancel" );
  grid->addWidget( cancelButton, 2, 1 );

  QGridLayout* grid_2 = new QGridLayout;
  grid_2->setSpacing( 6 );
  grid_2->setMargin( 0 );

  newName = new QLineEdit( this );
  connect( newName, SIGNAL(textChanged(const QString&)), SLOT(slotNewName(const QString&)) );
  grid_2->addWidget( newName, 1, 1 );

  QLabel* TextLabel1 = new QLabel( this );
  TextLabel1->setText( "Old Name:" );
  grid_2->addWidget( TextLabel1, 0, 0 );

  QLabel* TextLabel2 = new QLabel( this );
  TextLabel2->setText( "New name:" );
  grid_2->addWidget( TextLabel2, 1, 0 );

  oldName = new QLineEdit( this );
  oldName->setEnabled( false );
  grid_2->addWidget( oldName, 0, 1 );

  grid->addMultiCellLayout( grid_2, 0, 0, 0, 2 );

  prj_tree = new WorkspaceListView( this );
  WORKSPACE->getTree( prj_tree, false, true );
  prj_tree->setCurrentWorkspaceName( w->getName() );
  prj_tree->setEnabled( action != Rename );
  connect( prj_tree, SIGNAL(selectWorkspace(const char*)), SLOT(slotSelectWorkspace(const char*)) );
  grid->addMultiCellWidget( prj_tree, 1, 1, 0, 2 );

  // signals and slots connections
  connect( cancelButton, SIGNAL(clicked()), SLOT(reject()) );
  connect( okButton, SIGNAL(clicked()), SLOT(slotOk()) );

  oldName->setText( mode==doFile ? QFileInfo(path).fileName() : w->getName() );
  newName->setText( mode==doFile ? QFileInfo(path).fileName() : w->getName() );
  newName->setFocus();
  newName->selectAll();

  updateAction();
}

DlgCopyMoveItem::~DlgCopyMoveItem()
{
  KConfig* config = kapp->config();
  config->setGroup("DlgCopyMoveItem");
  config->writeEntry("Width",width());
  config->writeEntry("Height",height());
  config->sync();
}

void DlgCopyMoveItem::slotSelectWorkspace( const char* )
{
  updateAction();
}

void DlgCopyMoveItem::slotNewName( const QString& )
{
  updateAction();
}

void DlgCopyMoveItem::updateAction()
{
  QString actname;
  Workspace* w = WORKSPACE->getWorkspaceFromName(prj_tree->getCurrentWorkspaceName());

  bool wc = w->getName() != workspace->getName();
  if ( mode == doWorkspace ) {
    wc = wc && w->getName() != workspace->getParentWorkspaceName();
  }

  bool nc = oldName->text() != newName->text();

  if ( mode == doWorkspace )
    if ( wc && w->getDir().find(workspace->getDir()) == 0 ) {
      okButton->setEnabled(false);
      WORKSPACE->freeSubWorkspace(w);
      return;
    }

  if ( !wc && !nc ) {
    if ( act == Copy )
      actname = "Copy";
    else
      if ( act == Move )
        actname = "Move";
      else
        actname = "Rename";

    setCaption( actname + (mode==doFile ? " file...":" forkspace...") );
    okButton->setText( actname );
    okButton->setEnabled(false);
  }

  if ( wc && !nc ) {
    if ( act == Copy )
      actname = "Copy";
    else
      actname = "Move";

    setCaption( actname + (mode==doFile ? " file...":" forkspace...") );
    okButton->setText( actname );
    okButton->setEnabled(true);
  }

  if ( !wc && nc ) {
    actname = "Rename";

    setCaption( actname + (mode==doFile ? " file...":" forkspace...") );
    okButton->setText( actname );
    okButton->setEnabled(true);
  }

  if ( wc && nc ) {
    if ( act == Copy )
      actname = "Copy and rename";
    else
      actname = "Move and rename";

    setCaption( actname + (mode==doFile ? " file...":" forkspace...") );
    okButton->setText( actname );
    okButton->setEnabled(true);
  }
  WORKSPACE->freeSubWorkspace(w);
}

void DlgCopyMoveItem::slotOk()
{
  Workspace* w = WORKSPACE->getWorkspaceFromName(prj_tree->getCurrentWorkspaceName());
  bool wc = w->getName() != workspace->getName();
  if ( mode == doWorkspace ) {
    wc = wc && w->getName() != workspace->getParentWorkspaceName();
  }
  bool nc = oldName->text() != newName->text();

  if ( mode == doWorkspace )
    if ( wc && w->getDir().find(workspace->getDir()) == 0 ) {
      WORKSPACE->freeSubWorkspace(w);
      return;
    }

  if ( mode == doWorkspace ) {
    if (wc) {
      workspace->changeParentWorkspace(w->getName());
    }
    if (nc) {
      workspace->rename(newName->text());
      if (workspace->error) {
        QMessageBox::critical( 0L, "Rename", workspace->errorString );
        WORKSPACE->freeSubWorkspace(w);
        return;
      }
    }
    WORKSPACE->needRebuild();
  } else {
    EDIT_WINDOW->extremalCloseFile(file);
    if (wc) { // copy file to another workspace
      QString newpath = w->copyAndAddFile(file);
      if (newpath.isEmpty()) {
        QMessageBox::critical( 0L, "Copy file", w->errorString );
        WORKSPACE->freeSubWorkspace(w);
        return;
      }
      if (nc) {
        QString renamepath = QFileInfo(newpath).dirPath() + "/" + newName->text();
        if (!QDir().rename(newpath,renamepath)) {
          QMessageBox::critical( 0L, "Rename file", QString("Error rename file:\n%1\nto\n%2").arg(newpath).arg(renamepath) );
          WORKSPACE->freeSubWorkspace(w);
          return;
        } else {
          w->removeFile(QFileInfo(newpath).fileName());
          w->copyAndAddFile(renamepath);
        }
      }
      if ( act == Move ) {
        workspace->removeFile(QFileInfo(file).fileName());
        if ( !QDir().remove(file) ) {
          QMessageBox::critical( 0L, "Move file", QString("Error in remove file:\n%1").arg(file) );
          WORKSPACE->freeSubWorkspace(w);
          return;
        }
      }
    } else {
      if (nc) {
        QString renamepath = QFileInfo(file).dirPath() + "/" + newName->text();
        if (!QDir().rename(file,renamepath)) {
          QMessageBox::critical( 0L, "Rename file", QString("Error rename file:\n%1\nto\n%2").arg(file).arg(renamepath) );
          WORKSPACE->freeSubWorkspace(w);
          return;
        } else {
          workspace->removeFile(QFileInfo(file).fileName());
          workspace->copyAndAddFile(renamepath);
        }
      }
    }
  }
  WORKSPACE->freeSubWorkspace(w);
  accept();
}
