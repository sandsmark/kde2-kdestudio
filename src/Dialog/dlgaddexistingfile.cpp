#include "dlgaddexistingfile.h"

#include <qlabel.h>
#include <qlistbox.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qpixmap.h>
#include <qdir.h>
#include <qfileinfo.h>
#include <qmessagebox.h>

#include <kapp.h>
#include <kconfig.h>

#include "workspacelistview.h"
#include "studio.h"
#include "studioview.h"
#include "openfile.h"
#include "workspace.h"

static const char* const image0_data[] = {
"16 16 5 1",
". c None",
"# c #000000",
"c c #808000",
"a c #ffff00",
"b c #ffffff",
"................",
".........###....",
"........#...#.#.",
".............##.",
".###........###.",
"#aba#######.....",
"#babababab#.....",
"#ababababa#.....",
"#baba###########",
"#aba#ccccccccc#.",
"#ba#ccccccccc#..",
"#a#ccccccccc#...",
"##ccccccccc#....",
"###########.....",
"................",
"................"};


DlgAddExistingFile::DlgAddExistingFile(QWidget *parent, const QString& path )
: QDialog( parent, 0, true )
{
  QPixmap image0( ( const char** ) image0_data );

  KConfig* config = kapp->config();
  config->setGroup("DlgAddExistingFile");
  int dw = config->readNumEntry("Width",300);
  int dh = config->readNumEntry("Height",200);
  resize(dw,dh);
  move( (qApp->desktop()->width() - width())/2, (qApp->desktop()->height() - height())/2 );

  setCaption("Add existing files to project");

  QGridLayout* grid = new QGridLayout( this );
  grid->setSpacing( 6 );
  grid->setMargin( 11 );

  okButton = new QPushButton( this );
  okButton->setText( "Ok" );
  okButton->setDefault( true );

  grid->addWidget( okButton, 2, 4 );

  QLabel* TextLabel1 = new QLabel( this );
  TextLabel1->setText( "Source file's:" );
  grid->addMultiCellWidget( TextLabel1, 0, 0, 0, 3 );

  prj_tree = new WorkspaceListView( this );
  WORKSPACE->getTree( prj_tree, false, true );
  prj_tree->setCurrentWorkspaceName( path );
  grid->addMultiCellWidget( prj_tree, 1, 1, 4, 6 );

  sourceButton = new QPushButton( this );
  sourceButton->setPixmap( image0 );
  grid->addWidget( sourceButton, 2, 0 );

  QLabel* TextLabel3 = new QLabel( this );
  TextLabel3->setText( "Select source file's" );
  grid->addWidget( TextLabel3, 2, 1 );

  QLabel* TextLabel2 = new QLabel( this );
  TextLabel2->setText( "Destination project:" );
  grid->addMultiCellWidget( TextLabel2, 0, 0, 4, 6 );

  source = new QListBox( this );
  grid->addMultiCellWidget( source, 1, 1, 0, 3 );

  cancelButton = new QPushButton( this );
  cancelButton->setText( "Cancel" );
  grid->addWidget( cancelButton, 2, 6 );

  QSpacerItem* spacer = new QSpacerItem( 72, 20, QSizePolicy::Expanding, QSizePolicy::Fixed );
  grid->addItem( spacer, 2, 5 );
  QSpacerItem* spacer_2 = new QSpacerItem( 72, 20, QSizePolicy::Expanding, QSizePolicy::Fixed );
  grid->addItem( spacer_2, 2, 2 );
  QSpacerItem* spacer_3 = new QSpacerItem( 72, 20, QSizePolicy::Expanding, QSizePolicy::Fixed );
  grid->addItem( spacer_3, 2, 3 );

  // signals and slots connections
  connect( cancelButton, SIGNAL(clicked()), SLOT(reject()) );
  connect( sourceButton,SIGNAL(clicked()),SLOT(sourceButtonClicked()) );
  connect( okButton,SIGNAL(clicked()),SLOT(slotOk()) );

  // tab order
  setTabOrder( source, okButton );
  setTabOrder( okButton, prj_tree );
  setTabOrder( prj_tree, sourceButton );
  setTabOrder( sourceButton, cancelButton );
}

DlgAddExistingFile::~DlgAddExistingFile()
{
  KConfig* config = kapp->config();
  config->setGroup("DlgAddExistingFile");
  config->writeEntry("Width",width());
  config->writeEntry("Height",height());
  config->sync();
}

void DlgAddExistingFile::sourceButtonClicked()
{
  QStringList files( SFileDialog::getOpenFileNames( QString::null, "All ( * )", "Source File(s)...", "sourceFiles") );
  source->clear();
  if(!files.isEmpty()){
    for(uint i = 0; i < files.count(); i++){
      QString str = files[i];
      source->insertItem(str);
    }
  }
  files.clear();
}

void DlgAddExistingFile::slotOk()
{
	Workspace* w = 0L;
  w = WORKSPACE->getWorkspaceFromName( prj_tree->getCurrentWorkspaceName() );

  WORKSPACE->enableAutoUpdate( false );

  for ( uint k = 0; k < source->count(); k++ ){
     QFileInfo file_info( source->text(k) );
    if ( file_info.exists() ){
      if (w->copyAndAddFile( file_info.filePath() ).isEmpty()) {
        QMessageBox::critical( 0L, "Copy file", w->errorString );
      }
    } else {
      QMessageBox::critical( 0L, "Copy file", QString("File not found: %1").arg(file_info.filePath()) );
		}
  }
  WORKSPACE->enableAutoUpdate( true );
	
	WORKSPACE->freeSubWorkspace(w);
  accept();
}
