#include "setupdlg.h"

#include <kapp.h>
#include <kcolorbtn.h>
#include <kconfig.h>

#include <qbuttongroup.h>
#include <qlabel.h>
#include <qpixmap.h>
#include <qradiobutton.h>
#include <qpushbutton.h>

SetupDlg::SetupDlg( QWidget * parent, const char * name )
:QDialog( parent, name, true, 0)
{
  setCaption( "KDE Studio Setup" );

	buttonGroup = new QButtonGroup( this );
	buttonGroup->setGeometry( 10, 10, 300, 120 );
	buttonGroup->setExclusive( true );
  buttonGroup->setTitle( "Startup options" );

	isOpenLast = new QRadioButton( this );
	isOpenLast->setGeometry( 20, 40, 260, 20 );
	isOpenLast->setText( "Open last saved workspace" );
	isOpenLast->setChecked( false );

	isShowWelcome = new QRadioButton( this );
	isShowWelcome->setGeometry( 20, 70, 260, 20 );
	isShowWelcome->setText( "Show Welcome dialog" );

	isDoNothing = new QRadioButton( this );
	isDoNothing->setGeometry( 20, 100, 210, 20 );
	isDoNothing->setText( "Do nothing" );
	isDoNothing->setChecked( false );

	buttonGroup->insert( isDoNothing );
	buttonGroup->insert( isOpenLast );
	buttonGroup->insert( isShowWelcome );

/************************/
	QPushButton* cancel;
	cancel = new QPushButton( this );
	cancel->setGeometry( 232, 304, 80, 24 );
	cancel->setText( "Cancel" );
  connect( cancel, SIGNAL(clicked()), SLOT(reject()) );

	QPushButton* ok;
	ok = new QPushButton( this );
	ok->setGeometry( 144, 304, 80, 24 );
	ok->setText( "Ok" );
  connect( ok, SIGNAL(clicked()), SLOT(slotDone()) );
  ok->setDefault( true );
/************************/

  QGroupBox* GroupBox_1;
  GroupBox_1 = new QGroupBox( this );
  GroupBox_1->setGeometry( 10, 140, 150, 70 );
  GroupBox_1->setTitle( "Compiler process" );

  QGroupBox* GroupBox_2;
  GroupBox_2 = new QGroupBox( this);
  GroupBox_2->setGeometry( 10, 220, 150, 70 );
  GroupBox_2->setTitle( "Application process" );

  QGroupBox* GroupBox_3;
  GroupBox_3 = new QGroupBox( this );
  GroupBox_3->setGeometry( 170, 140, 140, 150 );
  GroupBox_3->setTitle( "Find output" );

  QLabel* Label_1;
  Label_1 = new QLabel( this );
  Label_1->setGeometry( 24, 240, 40, 16 );
  Label_1->setText( "Error:" );

  QLabel* Label_2;
  Label_2 = new QLabel( this );
  Label_2->setGeometry( 24, 264, 40, 16 );
  Label_2->setText( "Output:" );

  QLabel* Label_4;
  Label_4 = new QLabel( this );
  Label_4->setGeometry( 176, 208, 120, 16 );
  Label_4->setText( "Line number:" );

  QLabel* Label_5;
  Label_5 = new QLabel( this );
  Label_5->setGeometry( 24, 184, 40, 16 );
  Label_5->setText( "Output:" );

  QLabel* Label_6;
  Label_6 = new QLabel( this );
  Label_6->setGeometry( 24, 160, 40, 16 );
  Label_6->setText( "Error:" );

  QLabel* Label_7;
  Label_7 = new QLabel( this );
  Label_7->setGeometry( 176, 168, 120, 16 );
  Label_7->setText( "File path:" );

  QLabel* Label_8;
  Label_8 = new QLabel( this );
  Label_8->setGeometry( 176, 248, 120, 16 );
  Label_8->setText( "Find text:" );

  cout = new KColorButton( this );
  cout->setGeometry( 80, 184, 72, 20 );

  cerror = new KColorButton( this );
  cerror->setGeometry( 80, 160, 72, 20 );

  pout = new KColorButton( this );
  pout->setGeometry( 80, 264, 72, 20 );

  perror = new KColorButton( this );
  perror->setGeometry( 80, 240, 72, 20 );

  fpath = new KColorButton( this );
  fpath->setGeometry( 176, 184, 128, 20 );

  fline = new KColorButton( this );
  fline->setGeometry( 176, 224, 128, 20 );

  ftext = new KColorButton( this );
  ftext->setGeometry( 176, 264, 128, 20 );

  kapp->config()->setGroup("ColorData");
  cout->setColor( kapp->config()->readColorEntry( "StdOut", &black ) );
  cerror->setColor( kapp->config()->readColorEntry( "StdError", &darkGreen ) );
  pout->setColor( kapp->config()->readColorEntry( "RunStdOut", &black ) );
  perror->setColor( kapp->config()->readColorEntry( "RunStdError", &darkGreen ) );
  fpath->setColor( kapp->config()->readColorEntry( "FindFilePath", &black ) );
  fline->setColor( kapp->config()->readColorEntry( "FindLineNumber", &red ) );
  ftext->setColor( kapp->config()->readColorEntry( "FindText", &blue ) );

  kapp->config()->setGroup("StartupData");
	isDoNothing->setChecked( kapp->config()->readBoolEntry( "DoNothing", false ) );
	isOpenLast->setChecked( kapp->config()->readBoolEntry( "OpenLast", false ) );
	isShowWelcome->setChecked( kapp->config()->readBoolEntry( "ShowWelcomeDialog", true ) );

	setFixedSize( 320,334 );
  move( (qApp->desktop()->width() - width())/2, (qApp->desktop()->height() - height())/2 );
}

SetupDlg::~SetupDlg()
{
}

void SetupDlg::slotDone()
{
  kapp->config()->setGroup("ColorData");

  kapp->config()->writeEntry( "StdOut", cout->color() );
  kapp->config()->writeEntry( "StdError", cerror->color() );
  kapp->config()->writeEntry( "RunStdOut", pout->color() );
  kapp->config()->writeEntry( "RunStdError", perror->color() );
  kapp->config()->writeEntry( "FindFilePath", fpath->color() );
  kapp->config()->writeEntry( "FindLineNumber", fline->color() );
  kapp->config()->writeEntry( "FindText", ftext->color() );

  kapp->config()->setGroup("StartupData");

  kapp->config()->writeEntry( "ShowWelcomeDialog", isShowWelcome->isChecked() );
  kapp->config()->writeEntry( "OpenLast", isOpenLast->isChecked() );
  kapp->config()->writeEntry( "DoNothing", isDoNothing->isChecked() );

  kapp->config()->sync();
  accept();
}

