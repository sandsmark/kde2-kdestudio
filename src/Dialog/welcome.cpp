#include "welcome.h"
#include "studio.h"

#include <kapp.h>
#include <kconfig.h>
#include <qpainter.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qframe.h>
#include <qstrlist.h>

#include "x2.xpm"
#include "x3.xpm"

Welcome::Welcome( QWidget * parent, const char * name )
:QDialog( parent, name, true, 0)
{
  setCaption( "KDE Studio Welcome" );

  x1pixmap = new QPixmap(Ic("x1.png"));
  x2pixmap = new QPixmap(x2);
  x3pixmap = new QPixmap(x3);

	buttonGroup = new QButtonGroup( this );
	buttonGroup->setGeometry( 10, 10, 300, 340 );
	buttonGroup->setExclusive( true );
/*
	QPushButton* cancel;
	cancel = new QPushButton( this );
	cancel->setGeometry( 210, 390, 100, 30 );
	cancel->setText( "Cancel" );
  connect( cancel, SIGNAL(clicked()), SLOT(reject()) );
*/
	QPushButton* ok;
	ok = new QPushButton( this );
	ok->setGeometry( 210, 390, 100, 30 );
	ok->setText( "Ok" );
  connect( ok, SIGNAL(clicked()), SLOT(slotDone()) );

	showNext = new QCheckBox( this );
	showNext->setGeometry( 10, 360, 230, 20 );
	showNext->setText( "Show welcome dialog on next start" );
	showNext->setChecked( true );
  connect( showNext, SIGNAL(clicked()), SLOT(slotShowNext()) );

	isCreateNew = new QRadioButton( this, "RadioButton_1" );
	isCreateNew->setGeometry( 80, 80, 210, 20 );
	isCreateNew->setText( "Create new workspace ..." );
	isCreateNew->setChecked( false );
	isCreateNew->setChecked( true );
  connect( isCreateNew, SIGNAL(clicked()), SLOT(slotOther()) );
  createNew = true;

	isOpenLast = new QRadioButton( this );
	isOpenLast->setGeometry( 80, 140, 210, 20 );
	isOpenLast->setText( "Open last saved workspace:" );
	isOpenLast->setChecked( false );
  connect( isOpenLast, SIGNAL(clicked()), SLOT(slotOther()) );

	isOpenList = new QRadioButton( this );
	isOpenList->setGeometry( 80, 200, 210, 20 );
	isOpenList->setText( "Open workspace" );
	isOpenList->setChecked( false );
  connect( isOpenList, SIGNAL(clicked()), SLOT(slotOpenList()) );

	isNo = new QRadioButton( this );
	isNo->setGeometry( 80, 20, 210, 20 );
	isNo->setText( "Do nothing" );
	isNo->setChecked( false );
	
	workspaceList = new QListBox( this );
	workspaceList->setGeometry( 20, 240, 280, 100 );
  workspaceList->setEnabled( false );
  connect( workspaceList, SIGNAL(selected(const QString&)), SLOT(slotListSelect(const QString&)) );

  QStrList fileList;
  kapp->config()->setGroup("MainData");
  kapp->config()->readListEntry( "resentProjectPath", fileList );

  if ( fileList.count() > 0 ){
    QString path = fileList.at(0);
    path = path.right( path.length() - path.findRev("/") - 1 );

    QLabel* lastPrj;
  	lastPrj = new QLabel( this );
  	lastPrj->setGeometry( 100, 155, 200, 20 );
  	lastPrj->setText( path );
  } else {
    isOpenLast->setEnabled( false );
  }

  if ( fileList.count() > 1 ){
    for ( uint k = 0; k < fileList.count(); k++ )
      workspaceList->insertItem( fileList.at(k) );
  } else {
    workspaceList->setEnabled( false );
    isOpenList->setEnabled( false );
  }

	buttonGroup->insert( isCreateNew );
	buttonGroup->insert( isOpenLast );
	buttonGroup->insert( isOpenList );
	buttonGroup->insert( isNo );

	setFixedSize( 320,430 );
  move( (qApp->desktop()->width() - width())/2, (qApp->desktop()->height() - height())/2 );
}

Welcome::~Welcome()
{
}

void Welcome::paintEvent( QPaintEvent * )
{
  QPainter paint;
  paint.begin( buttonGroup );
  paint.drawPixmap( 10, 60 , *x1pixmap );
  paint.drawPixmap( 10, 120, *x3pixmap );
  paint.drawPixmap( 10, 180, *x2pixmap );
  paint.end();
}

void Welcome::slotShowNext()
{
  kapp->config()->setGroup("StartupData");
  kapp->config()->writeEntry( "ShowWelcomeDialog", showNext->isChecked() );
  kapp->config()->sync();
}

void Welcome::slotOpenList()
{
  workspaceList->setEnabled( true );
  if ( workspaceList->currentItem() == -1 ) workspaceList->setCurrentItem( 0 );
}

void Welcome::slotOther()
{
  workspaceList->setEnabled( false );
}

void Welcome::slotListSelect( const QString& item )
{
  createNew = false;
  openWorkspace = item;
  accept();
}

void Welcome::slotDone()
{
  if ( isNo->isChecked() ) {
    createNew = false;
    reject();
  }

  if ( isCreateNew->isChecked() )
    createNew = true;

  if ( isOpenLast->isChecked() ){
    QStrList fileList;
    kapp->config()->setGroup("MainData");
    kapp->config()->readListEntry( "resentProjectPath", fileList );
    openWorkspace = fileList.at(0);
    createNew = false;
  }

  if ( isOpenList->isChecked() ){
    openWorkspace = workspaceList->text( workspaceList->currentItem() );
    createNew = false;
  }

  accept();
}

