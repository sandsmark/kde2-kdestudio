#ifndef DLGCOPYMOVEITEM_H
#define DLGCOPYMOVEITEM_H

#include <qdialog.h>
class QLineEdit;
class WorkspaceListView;
class Workspace;
class QPushButton;

class DlgCopyMoveItem : public QDialog
{ Q_OBJECT
public:

  enum Action {
    Copy,
    Move,
    Rename
  };

  DlgCopyMoveItem(QWidget *parent, const QString& path, Workspace*, Action );
  ~DlgCopyMoveItem();

protected slots:
  void slotOk();
  void slotSelectWorkspace( const char* );
  void slotNewName( const QString& );

protected:
  void updateAction();

  enum Mode {
    doFile,
    doWorkspace
  };
  Mode mode;
  Action act;

  Workspace* workspace;
  QString file;

  QLineEdit* newName;
  QLineEdit* oldName;
  WorkspaceListView* prj_tree;
  QPushButton* okButton;
};

#endif
