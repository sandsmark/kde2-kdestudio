#ifndef WELCOME_H
#define WELCOME_H


#include <qcheckbox.h>
#include <qdialog.h>
#include <qbuttongroup.h>
#include <qpixmap.h>
#include <qlistbox.h>
#include <qradiobutton.h>

class Welcome : public QDialog
{ Q_OBJECT
public:
	Welcome( QWidget * parent = 0L , const char*  = 0 );
	~Welcome();

  QString openWorkspace;
  bool createNew;

protected:
  virtual void paintEvent( QPaintEvent * );

private slots:
  void slotShowNext();
  void slotOpenList();
  void slotOther();
  void slotListSelect( const QString& );
  void slotDone();

private:
	QListBox* workspaceList;
	QCheckBox* showNext;
	QButtonGroup* buttonGroup;
	QRadioButton* isCreateNew;
	QRadioButton* isOpenLast;
	QRadioButton* isOpenList;
	QRadioButton* isNo;
  QPixmap* x1pixmap;
  QPixmap* x2pixmap;
  QPixmap* x3pixmap;
};


#endif


