#include "globalnew.h"
#include "studio.h"
#include "studioview.h"
#include "workspacelistview.h"
#include "workspace.h"
#include "workspacelistview.h"

#include <qlabel.h>
#include <qstring.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qradiobutton.h>
#include <qbuttongroup.h>
#include <qdir.h>
#include <qfiledialog.h>
#include <qmessagebox.h>
#include <qlayout.h>

#include <kapp.h>
#include <kconfig.h>

GlobalNew::GlobalNew( QWidget * parent, const char* path, int page, const char * name )
:QDialog( parent, name, true, 0)
{
	projectDir = WORKSPACE->getDir();

  setCaption( "Create new..." );

	QGridLayout* layout = new QGridLayout(this,0,0,3,2);
	tab = new KDockTabCtl(this);

	m_pOK = new QPushButton(this);
	m_pOK->setText("Create");
	m_pOK->setDefault(true);
	m_pOK->setAutoDefault(true);
	
	m_pCancel = new QPushButton(this);
	m_pCancel->setText("Close");

	connect( m_pOK, SIGNAL(clicked()), SLOT(slotCreate()) );
	connect( m_pCancel, SIGNAL(clicked()), SLOT(closeDialog()) );

  layout->addMultiCellWidget(tab,0,0,0,2);
  layout->addWidget(m_pCancel,1,1);
  layout->addWidget(m_pOK,1,2);

  layout->setColStretch(0,10);
  layout->setColStretch(1,1);
  layout->setColStretch(1,1);

  createWorkspaceTab();
  createProjectTab();
  createFileTab();

	tab->insertPage( workspaceTab, "Workspace", 0 );
	tab->insertPage( projectTab, "Project", 1 );
	tab->insertPage( fileTab, "File", 2 );

  if ( !WORKSPACE->Ok() ) {
    tab->setPageEnabled( fileTab, false );
    tab->setPageEnabled( projectTab, false );
  } else {
    if ( !WORKSPACE->haveSub() )
      tab->setPageEnabled( fileTab, false );
  }
  updateTrees();
  create_in_tree->setCurrentWorkspaceName( path );
  f_create_in_tree->setCurrentWorkspaceName( path );

  tab->setVisiblePage( page );

  KConfig* config = kapp->config();
  config->setGroup("GlobalNew");
  int dw = config->readNumEntry("Width",300);
  int dh = config->readNumEntry("Height",500);
  resize(dw,dh);
  move( (qApp->desktop()->width() - width())/2, (qApp->desktop()->height() - height())/2 );
}


void GlobalNew::updateTrees()
{
  if ( !WORKSPACE->Ok() )
    return;
  WORKSPACE->getTree( create_in_tree  , false, true );
  WORKSPACE->getTree( f_create_in_tree, true , true );
}

GlobalNew::~GlobalNew()
{
  KConfig* config = kapp->config();
  config->setGroup("GlobalNew");
  config->writeEntry("Width",width());
  config->writeEntry("Height",height());
  config->sync();
}

void GlobalNew::slotDirSelect()
{
  projectDir = QFileDialog::getExistingDirectory(projectDir,this,"Select Workspace Directory");
  m_pDirEdit->setText(projectDir);
}

void GlobalNew::slotCreateWorkspace()
{
  QString name = WORKSPACE->createNew(m_pDirEdit->text(), m_pNameEdit->text());
  if ( !WORKSPACE->error )
  {
    STUDIO_VIEW->readWorkspace( name );
    tab->setPageEnabled( projectTab, true );
    updateTrees();
  } else {
    QMessageBox::critical( 0L, "Create Workspace", WORKSPACE->errorString );
  }
}

void GlobalNew::createWorkspaceTab()
{
	workspaceTab = new QWidget(tab);
	QGridLayout* layout = new QGridLayout(workspaceTab,3,3,3,2);
	
	QLabel* m_pNameLabel = new QLabel(workspaceTab);
	m_pNameLabel->setText("Workspace name:");

	m_pNameEdit = new QLineEdit(workspaceTab);
	m_pNameEdit->setText("");

	QLabel* m_pDirLabel = new QLabel(workspaceTab);
	m_pDirLabel->setText("Workspace directory:");

	m_pDirEdit = new QLineEdit(workspaceTab);
	m_pDirEdit->setText( QDir::home().path() );

	QPushButton* m_pDirSelect = new QPushButton(workspaceTab);
	m_pDirSelect->setPixmap( Ic("open.xpm") );

	connect( m_pDirSelect, SIGNAL(clicked()), SLOT(slotDirSelect()) );

  layout->addWidget(m_pNameLabel,0,0);
  layout->addWidget(m_pDirLabel,1,0);

  layout->addMultiCellWidget(m_pNameEdit,0,0,1,2);
  layout->addWidget(m_pDirEdit,1,1);
  layout->addWidget(m_pDirSelect,1,2);
}

void GlobalNew::createProjectTab()
{
  projectTab = new QWidget(tab);
	QGridLayout* layout = new QGridLayout(projectTab,0,0,3,5);

  QButtonGroup* buttonGroup = new QButtonGroup( projectTab );
  buttonGroup->setTitle( "Project type:" );
	QVBoxLayout* bl = new QVBoxLayout(buttonGroup,5,2);

  b1 = new QRadioButton( buttonGroup );
  b1->setText( "group" );

  b2 = new QRadioButton( buttonGroup );
  b2->setText( "shared library" );

  b3 = new QRadioButton( buttonGroup );
  b3->setText( "static library" );

  b4 = new QRadioButton( buttonGroup );
  b4->setText( "executable" );
  b4->setChecked( true );

  b5 = new QRadioButton( buttonGroup );
  b5->setText( "data" );

  bl->addSpacing(10);
  bl->addWidget(b4);
  bl->addWidget(b1);
  bl->addWidget(b2);
  bl->addWidget(b3);
  bl->addWidget(b5);
  bl->addStretch(10);

  QLabel* caption = new QLabel( projectTab );
  caption->setText( "Name:" );

  prName = new QLineEdit( projectTab );
  prName->setText( "" );

  create_in_tree = new WorkspaceListView( projectTab );

  layout->addMultiCellWidget(buttonGroup,0,0,0,1);
  layout->addMultiCellWidget(create_in_tree,0,2,2,2);
  layout->addWidget(caption,2,0);
  layout->addWidget(prName,2,1);

  layout->setColStretch(2,3);
  layout->setRowStretch(1,10);
}

void GlobalNew::createFileTab()
{
	fileTab = new QWidget(tab);
	QGridLayout* layout = new QGridLayout(fileTab,0,0,3,5);

	fname = new QLineEdit( fileTab );
  fname->setText( "" );

	QLabel* caption = new QLabel( fileTab );
	caption->setText( "Name:" );

	f_create_in_tree = new WorkspaceListView( fileTab );

  f_buttonGroup = new QButtonGroup( fileTab );
  f_buttonGroup->setTitle( "File type:" );
	QVBoxLayout* bl = new QVBoxLayout(f_buttonGroup,5,2);

  b10 = new QRadioButton( f_buttonGroup );
  b10->setText( "CPP file" );

  b11 = new QRadioButton( f_buttonGroup );
  b11->setText( "Header file" );

  b12 = new QRadioButton( f_buttonGroup );
  b12->setText( "CPP/Header file's" );

  b13 = new QRadioButton( f_buttonGroup );
  b13->setText( "Main program" );
  b13->setChecked( true );

  b14 = new QRadioButton( f_buttonGroup );
  b14->setText( ".desktop file" );

  b15 = new QRadioButton( f_buttonGroup );
  b15->setText( "Any file" );

  bl->addSpacing(10);
  bl->addWidget(b13);
  bl->addWidget(b10);
  bl->addWidget(b11);
  bl->addWidget(b12);
  bl->addWidget(b15);
  bl->addWidget(b14);
  bl->addStretch(10);

  layout->addMultiCellWidget(f_buttonGroup,0,0,0,1);
  layout->addMultiCellWidget(f_create_in_tree,0,2,2,2);
  layout->addWidget(caption,2,0);
  layout->addWidget(fname,2,1);

  layout->setColStretch(2,3);
  layout->setRowStretch(1,10);
}

void GlobalNew::slotCreateProject()
{
  QString createIn = create_in_tree->getCurrentWorkspaceName();
  if ( createIn.isEmpty() ) createIn = WORKSPACE->getName();
  Workspace* w = WORKSPACE->getWorkspaceFromName( createIn );
  if ( w == 0L ){
    QMessageBox::critical( 0L, "Create project error", QString("Can find project :%1").arg(createIn) );
    return;
  }
  bool noer = true;

  if ( b5->isChecked() ) {
    noer = w->createSub( prName->text(), Workspace::DATA );
  }
  if ( b4->isChecked() ) {
    noer = w->createSub( prName->text(), Workspace::EXECUTABLE );
  }
  if ( b1->isChecked() ) {
    noer = w->createSub( prName->text(), Workspace::GROUP );
  }
  if ( b3->isChecked() ) {
    noer = w->createSub( prName->text(), Workspace::STATIC_LIB );
  }
  if ( b2->isChecked() ) {
    noer = w->createSub( prName->text(), Workspace::SHARED_LIB );
  }
  if ( noer ) {
    WORKSPACE->needRebuild();
    updateTrees();
    tab->setPageEnabled( fileTab, true );
  }
  else
  {
    QMessageBox::critical( 0L, "Create project error", WORKSPACE->errorString );
  }

  WORKSPACE->freeSubWorkspace( w );
}

void GlobalNew::slotCreateFile()
{
  QString f_createIn = f_create_in_tree->getCurrentWorkspaceName();

  if ( f_createIn.isEmpty() ) f_createIn = WORKSPACE->getName();

  Workspace* w = WORKSPACE->getWorkspaceFromName( f_createIn );
  if ( w == 0L ){
    QMessageBox::critical( 0L, "Create file error", QString("Can find project :%1").arg(f_createIn) );
    return;
  }

  bool noer = true;

  if ( b10->isChecked() ) {
      noer = w->createFile( fname->text(), Workspace::CPPFILE );
  }
  if ( b11->isChecked() ) {
      noer = w->createFile( fname->text(), Workspace::HFILE );
  }
  if ( b12->isChecked() ) {
      WORKSPACE->enableAutoUpdate( false );
      noer = w->createFile( fname->text(), Workspace::CPPFILE );
      noer = noer | w->createFile( fname->text(), Workspace::HFILE );
      WORKSPACE->enableAutoUpdate( true );
  }
  if ( b13->isChecked() ) {
      noer = WORKSPACE->createMainP( fname->text(), f_createIn );
  }

  if ( b14->isChecked() ) {
      noer = w->createFile( fname->text(), Workspace::KDELINKFILE );
  }

  if ( b15->isChecked() ) {
      noer = w->createFile( fname->text(), Workspace::ANYFILE );
  }

  if ( noer ) {
    updateTrees();
  } else {
    QMessageBox::critical( 0L, "Create file error", WORKSPACE->errorString );
  }

  WORKSPACE->freeSubWorkspace( w );
}

void GlobalNew::closeDialog()
{
  reject();
}

void GlobalNew::slotCreate()
{
  if ( tab->visiblePage() == workspaceTab )
    slotCreateWorkspace();

  if ( tab->visiblePage() == projectTab )
    slotCreateProject();

  if ( tab->visiblePage() == fileTab )
    slotCreateFile();
}
