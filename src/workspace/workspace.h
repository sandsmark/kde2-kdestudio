#ifndef WORKSPACE_H
#define WORKSPACE_H

#include <qobject.h>
#include <qdict.h>
#include <kprocess.h>

class MainWorkspace;
class KSimpleConfig;
class QString;
class QStrList;
class QListView;
class QListViewItem;
class WorkspaceListView;

typedef QDict<char> stdLibraryDict;
typedef QDictIterator<char> stdLibraryIterator;

struct sKeyValue
{
  QString key;
  QString value;
};
typedef QValueList<sKeyValue> sConfigValues;

struct sConfigData
{
  void clear()
  {
    includeData.clear();
    libPathData.clear();
    libData.clear();
  }
  sConfigValues includeData;
  sConfigValues libPathData;
  sConfigValues libData;
};


class Workspace: public QObject
{Q_OBJECT
friend class OptionsDlg;
friend class MainWorkspace;
public:
	Workspace( Workspace* parent = 0L, const char* fname = 0L );
	~Workspace();

  QString errorString;
  bool    error;

  // Project options for setProjectSimpleOptions and getProjectSimpleOptions functions
  // bool options
  #define WS_DEBUG_INFO          "DebugInfo"
  #define WS_OPTIMIZE            "Optimize"
  #define WS_SHOW_WARNING        "ShowWarnings"
  #define WS_REMOVE_SIMBOL_TABLE "RemoveSymbolTable"
  #define WS_BIN_INSTALL         "isBinInstallProcess"
  #define WS_DATA_INSTALL        "isDataInstallProcess"
  #define WS_LIB_INSTALL         "isLibInstallProcess"
  #define WS_HEADER_INSTALL      "isHeaderInstallProcess"

  // QString and const char*  options
  #define WS_DEF_RUN_PRJ     "DefRunPrj"
  #define WS_VERSION         "Version"
  #define WS_DESCRIPTIONS    "Memo"
  #define WS_ADD_COMPILE_OPT "AddCompileOptions"
  #define WS_ADD_LINK_OPT    "AddLinkOptions"
  #define WS_CONFIGUREARG    "ConfigureArguments"
  #define WS_TARGETNAME      "TargetName"

  // QStrList options
  #define WS_NOT_IN_INCLUDE_PATH   "NotInIncludePath"
  #define WS_ADD_TO_INCLUDE_PATH   "AddToIncludePath"
  #define WS_LIBRARY_SELF_NOT_USES "NotUsesSelfLibrary"
  #define WS_LIBRARY_USES_DEF      "DefaultUsesLibrary"
  #define WS_LIBRARY_USER          "UserLibrary"
  #define WS_GROUP                 "Group"
  #define WS_CONVENIENCE_LIBS      "ConvenienceList"
  #define WS_INSTALL_DATA          "InstallDataPath"
  #define WS_INSTALL_BIN           "InstallBinPath"
  #define WS_INSTALL_LIB           "InstallLibPath"
  #define WS_INSTALL_HEADER        "InstallHeaderPath"

  // Internal Use Only
  #define WS_LIBRARY_DEF           "DefaultLibrary"

  enum wType {
    GROUP             =0,
    EXECUTABLE        =1,
    SHARED_LIB        =2,
    STATIC_LIB        =3,
    MAIN              =7,
    DATA              =8,

    CPPFILE     = 100,
    HFILE       = 101,
    MAINP       = 102,
    MAINCPP     = 103,
    KTMAINWCPP  = 104,
    KTMAINWH    = 105,
    KDELINKFILE = 106,
    ANYFILE     = 107
   };


	QString copyAndAddFile( const char* path );
  bool createSub  ( const char* wname, wType type );
  bool createFile ( const char* fname, wType type );
  bool checkName  ( const char* wname, wType type );

  wType   getType();
	QString getName() { return name; }
	QString getTargetName();
	QString getDir() { return dir; }
	QString getConfigPath() { return configPath; }
	QString getParentWorkspaceName() { return parentName; }
  KSimpleConfig* getConfig() {return wConfig; }
  MainWorkspace* getMainWorkspace() { return mainWorkspace; }

  void changeParentWorkspace( const char* newParentWorkspaceName );
  void rename( const char* newName );
  void remove( bool reparse = true );
	bool removeFile( QString fileName );

  void setMemo( QString text );
	QString getMemo();

  void setProjectSimpleOptions( QString optionName, QStrList& list );
  void setProjectSimpleOptions( QString optionName, const char* text );
  void setProjectSimpleOptions( QString optionName, QString   text );
  void setProjectSimpleOptions( QString optionName, bool value );

  bool getProjectSimpleOptionsBool( QString optionName );
  void getProjectSimpleOptionsList( QString optionName, QStrList& list );
  QString getProjectSimpleOptions ( QString optionName );


  void getFiles( QStrList& list );
  void getAllSub( QStrList& list, bool withGroup = true );
  void getTree( WorkspaceListView *list, bool withFile = false, bool expand = false );

  void updateMakefileAm( bool recursive = false );

private:
  // IMPORTANT: You need to delete this workspace , call only freeSubWorkspace() for it;
  Workspace* getFromName( QString name );
  void temporaryFreeAllChild();

  bool unZip( QString fname );
  bool copyFile( QString path, QString fname );
  bool createDirectory( QString path );
  bool init( wType type );
  void getAllNoInstLibrary( QStrList& );
  void getAllLibrary( QStrList& );
  void getAllProjectName( QStrList& );
  void getAllFilePath( QStrList& );
  void setName( QString text, bool remove );
  void changeParent( Workspace* );
  QString processIncludeHeaders();

  KSimpleConfig *wConfig;
  MainWorkspace* mainWorkspace;
  QString configPath;
  QString name;
  QString parentName;
  QString dir;
  KShellProcess pr;

private slots:
  void slotReceivedStdout(KProcess*,char*,int);
};


class MainWorkspace : public Workspace
{Q_OBJECT
friend class Workspace;
public:
  MainWorkspace();
  ~MainWorkspace();

  void needRebuild();
  void optionsDlg( const char* path = 0 );

  void getTree( WorkspaceListView *list, bool withFile = false, bool expand = false );

  QString createNew( const char* path, const char* name);
  void    close();
  QString open( const char* path );
  bool    Ok(){ return !name.isEmpty(); }
  bool    haveSub();

  bool createMainP( const char* name, QString createIn );
  void updateMakefilesAm();

  void getStdLibraryDict( stdLibraryDict& dict );
  void getAllProjectNoInstLibrary( QStrList& list ){ getAllNoInstLibrary( list ); }
  void getAllProjectLibrary( QStrList& list ){ getAllLibrary( list ); }
  void getAllProjectNames( QStrList& list ){ getAllProjectName( list ); }
  void getAllProjectFilePath( QStrList& list ){ getAllFilePath( list ); }

  void getAllExecutablePath(  QStrList& list );

  // IMPORTANT: You need to delete this workspace , call only freeSubWorkspace() for it;
  Workspace* getWorkspaceFromName( QString wname ){ return getFromName( wname ); }

  QString getDefaultRunPath();
  void setDefaulRunPath( QString path );

  void enableAutoUpdate( bool update );

  void update(){ if ( autoUpdate ) emit updateFileTree(); }

  void freeSubWorkspace( Workspace* w );

  bool checkVersion();

signals:
  void reparsing();
  void closeMainWorkspace();
  void openMainWorkspace();
  void closeWorkspace( Workspace* );
  void updateFileTree();
  void updateUI();

  void insertFilesInWorkspace( QStrList& list );
  void removeFileFromWorkspace( const QString& );

private:
  bool autoUpdate;

  QString ktMainWclassName;
  QString ktMainWclassVar;
};

#endif
