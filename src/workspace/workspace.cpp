#include "studio.h"
#include "optionsdlg.h"
#include "workspace.h"
#include "workspacelistview.h"
#include "tools.h"
#include "seditwindow.h"
#include "kwmanager.h"

#include <ksimpleconfig.h>
#include <kstddirs.h>
#include <kprocess.h>
#include <kapp.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qregexp.h>
#include <qdir.h>
#include <qstrlist.h>
#include <qfileinfo.h>
#include <qtextstream.h>
#include <qmap.h>
#include <qmessagebox.h>

void Workspace::slotReceivedStdout(KProcess*,char* buffer,int buflen)
{
  QString str = QString().fromLatin1( buffer,buflen );
  errorString.append(str);
}

/*===================================================================*/

Workspace::Workspace( Workspace* parent, const char* fname )
{
  errorString = "";
  if ( parent ) {
    name = fname;
    parentName = parent->getName();
    configPath = parent->getConfigPath() + "/" + name;
    dir = parent->getDir() + name + "/";
    wConfig = parent->getConfig();
    mainWorkspace =  parent->getMainWorkspace();
  }

  connect( &pr,SIGNAL(receivedStdout(KProcess*,char*,int)),
           this,SLOT(slotReceivedStdout(KProcess*,char*,int)) );
  connect( &pr,SIGNAL(receivedStderr(KProcess*,char*,int)),
           this,SLOT(slotReceivedStdout(KProcess*,char*,int)) );

}

Workspace::~Workspace()
{
}

bool Workspace::createSub( const char* wname, wType type )
{
  if ( !checkName( wname, type ) ) return false;

  Workspace* gr = new Workspace( this, wname );
  if ( !gr->init(type) ){
    delete gr;
    return false;
  }

  QStrList list;
  wConfig->setGroup( configPath  );
  wConfig->readListEntry( "Group", list );
  // executable always compiling late ( first compiling library)
  // order to compile group may be change by project options dialog
  if ( type == EXECUTABLE )
    list.append( wname );
  else
    list.insert( 0, wname );
  wConfig->writeEntry( "Group", list );
  wConfig->sync();

  mainWorkspace->updateMakefilesAm();
  delete gr;
  mainWorkspace->update();

  emit mainWorkspace->updateUI();
  return true;
}

bool Workspace::init( wType type )
{
#define SET_COMPILE_OPTIONS\
      wConfig->writeEntry( WS_DEBUG_INFO, true );\
      wConfig->writeEntry( WS_OPTIMIZE, true );\
      wConfig->writeEntry( WS_SHOW_WARNING, true );\
      wConfig->writeEntry( WS_REMOVE_SIMBOL_TABLE, false );\

  if ( !createDirectory( dir ) ) return false;

  wConfig->setGroup( configPath );
  wConfig->writeEntry( "Type", (int)type );
  switch ( type ){
    case DATA:
      wConfig->writeEntry( WS_DATA_INSTALL, false );
      wConfig->writeEntry( WS_INSTALL_DATA, "" );
      break;
    case GROUP:
      break;
    case EXECUTABLE:{
      SET_COMPILE_OPTIONS
      wConfig->writeEntry( WS_HEADER_INSTALL, false );
      wConfig->writeEntry( WS_INSTALL_HEADER, "" );
      wConfig->writeEntry( WS_BIN_INSTALL, true );
      wConfig->writeEntry( WS_INSTALL_BIN, "" );
      QStrList deflib;
      deflib.append("LIB_KDEUI");
      deflib.append("LIB_KDECORE");
      deflib.append("LIB_QT");
      deflib.append("LIB_X11");
      wConfig->writeEntry( WS_LIBRARY_USES_DEF, deflib );
      // set DefRunPrj name
      wConfig->setGroup( mainWorkspace->configPath );
      if ( wConfig->readEntry(WS_DEF_RUN_PRJ).isEmpty() )
        wConfig->writeEntry( WS_DEF_RUN_PRJ, name );
      break;
    }
    case SHARED_LIB:
      SET_COMPILE_OPTIONS
      wConfig->writeEntry( WS_LIB_INSTALL, true );
      wConfig->writeEntry( WS_INSTALL_LIB, "" );
      wConfig->writeEntry( WS_HEADER_INSTALL, false );
      wConfig->writeEntry( WS_INSTALL_HEADER, "" );
      break;
    case STATIC_LIB:
      SET_COMPILE_OPTIONS
      wConfig->writeEntry( WS_HEADER_INSTALL, false );
      wConfig->writeEntry( WS_INSTALL_HEADER, "" );
      break;
    case MAIN:
      break;
    default: break;
  }

  wConfig->sync();
  return true;
}

MainWorkspace::MainWorkspace() :Workspace()
{
  autoUpdate = true;
  wConfig = 0L;
  name = "";
  configPath = "";
  dir = "";
  mainWorkspace =  this;
}

MainWorkspace::~MainWorkspace()
{
}

void MainWorkspace::optionsDlg( const char* path )
{
  OptionsDlg::showDialog( this, path );
}


QString MainWorkspace::createNew( const char* path, const char* name)
{
  QString w = "kde_workspace";

	QString p(path);
	if ( p.right(1) != "/") p.append("/");
	QDir rootDir(p);
  if ( !rootDir.exists() )
  {
    errorString = "Directory " + p + " not exists";
    error = true;
    return "";
  }
  QString dir = p + QString(name);

  if ( !createDirectory( dir ) ) return "";

  QString tpath = KGlobal::dirs()->findResource("data", "studio/templates/kde_workspace.tar.gz");
  if ( tpath == QString::null ){
    debug("ERROR - Cannot fin template directory $[KDEDIR]/apps/share/kdestudio/template");
    debug("ERROR - or template file kde_workspace.tar.gz");
    return "";
  }

  if ( !copyFile( QFileInfo(tpath).dirPath(), w + ".tar.gz" ) )
  {
    rootDir.rmdir( dir );
    return "";
  }

  if ( !unZip( w ) )
  {
    rootDir.rmdir( dir );
    return "";
  }

  QString pname = dir + "/" + QString(name) + ".studio";
  KSimpleConfig *wr = new KSimpleConfig( pname );

  wr->setGroup( "Main" );
  wr->writeEntry( "Name", name );

  wr->setGroup( name );
  wr->writeEntry( "Type", (int)MAIN );
  wr->writeEntry( WS_DEF_RUN_PRJ, "" );

  QStrList list;
  wr->setGroup( name  );
  wr->readListEntry( "File", list );
  list.append( "configure.in.in" );
  wr->writeEntry( "File", list );
  wr->sync();
	
  error = false;
	return pname;
}

QString MainWorkspace::getDefaultRunPath()
{
  if ( !Ok() ) return "";

  wConfig->setGroup( configPath );
  QString prjName = wConfig->readEntry( WS_DEF_RUN_PRJ );
  if ( prjName.isEmpty() ){
    return "";
  }

  Workspace* temp = getWorkspaceFromName(prjName);
  if ( temp == 0L ) return "";
  QString path = temp->dir + temp->getTargetName();
  freeSubWorkspace( temp );
  return path;
}

void MainWorkspace::setDefaulRunPath( QString path )
{
  if ( path.isEmpty() ){
    wConfig->setGroup( configPath );
    wConfig->writeEntry( WS_DEF_RUN_PRJ, "" );
    wConfig->sync();
    return;
  }

  QString prjName = path.right( path.length() - path.findRev("/") -1 );
  Workspace* temp = getWorkspaceFromName(prjName);
  if ( temp != 0L ){
    wConfig->setGroup( configPath );
    wConfig->writeEntry( WS_DEF_RUN_PRJ, prjName );
    wConfig->sync();
    freeSubWorkspace( temp );
  }
}

void MainWorkspace::getStdLibraryDict( stdLibraryDict& dict )
{
  dict.clear();

  QFile f(getDir()+"config.status");
  if ( !f.open( IO_ReadOnly ) ) {
    return;
  }

  uint size = f.size();
  char* buffer = new char[ size + 1 ];
  f.readBlock( buffer, size );
  f.close();
  buffer[ size ] = 0;

  QString text = QString::fromLatin1( buffer, size );
  delete[] buffer;

  sConfigData data;
  data.clear();

  while (!text.isEmpty()) {
    int k = text.find("\n");
    if (k==-1)
      k = text.length();
    QString s = text.left(k);
    text.remove(0,k+1);
    if ( s.find("s%@") == 0 ){
      sKeyValue kv;

      s.remove(0,3);
      int sep = s.find("@%");
      kv.key = s.left(sep);
      s.remove(0,sep+2);
      kv.value = s.left(s.find("%g"));

      if ( kv.value.find("-I") == 0 )
        data.includeData << kv;

      if ( kv.value.find("-L") == 0 )
        data.libPathData << kv;

      if ( kv.value.find("-l") == 0 )
        data.libData << kv;
    }
  }

  QStrList libDef;
  QString str;

  sConfigValues datas = data.libData;
  sConfigValues::Iterator it;
  for( it = datas.begin(); it != datas.end(); ++it ){
    QString value = (*it).value.replace( QRegExp("$"), "");
    str = (*it).key + " = " + value;
    libDef.append(str);
  }

  int k = 0; //for store sorting
  for( str = libDef.first();str != 0;str = libDef.next() ){
    QString alias = str.left( str.find("=") ).stripWhiteSpace();
    QString* lib = new QString( str.remove(0, str.find("=") + 1).stripWhiteSpace() );
    //for store sorting
    lib->prepend( QString().setNum(k) );
    if ( k < 10 ) lib->prepend("0");
    k++;
    dict.insert( alias, lib->data() );
  }
}


void MainWorkspace::getAllExecutablePath(  QStrList& list )
{
  QStrList names;
  getAllProjectNames( names );
  for ( uint k=0; k < names.count(); k++ ){
    Workspace* temp = getWorkspaceFromName( names.at(k) );
    if ( temp->getType() == EXECUTABLE ){
      list.append( temp->dir + temp->name );
    }
    freeSubWorkspace( temp );
  }
}

void MainWorkspace::close()
{
  emit closeMainWorkspace();
  name = "";
  dir = "";

  if (wConfig)
    delete wConfig;

  emit updateUI();
}

void MainWorkspace::enableAutoUpdate( bool update )
{
  if ( autoUpdate != update && update ) emit updateFileTree();
  autoUpdate = update;
}

QString MainWorkspace::open( const char* path )
{
  close();
  autoUpdate = true;
  wConfig = new KSimpleConfig( path );
  wConfig->setGroup( "Main" );
  name = wConfig->readEntry( "Name" );
  if ( Ok() ){
    if ( true/*checkVersion()*/ ){
      configPath = name;
      dir = QString( path ).left( QString( path ).findRev("/") + 1 );

      update();
      emit updateUI();
      emit openMainWorkspace();
    } else {
      name.truncate(0);
    }
  }
  return name;
}

bool MainWorkspace::checkVersion()
{
  wConfig->setGroup( "Main" );
  if ( wConfig->readEntry("Version","1").toInt() > 2 ){
    QMessageBox::warning(0L,"Cannot open this version of project", "Need project version <= 2", "Ok");
    return false;
  }
  if ( wConfig->readEntry("Varsion","1").toInt() < 2 ){
    QString text = "Convert project version?\nIt will be add/update directory ./admin\nand remove all data from configure.in";
    int t = QMessageBox::warning(0L,"Old project version", text, "Ok","Cancel");
    if (t==1)
      return false;


    wConfig->setGroup("Main");
    wConfig->writeEntry("Version","2");
  }
  return true;
}


// unzip fname.tar.gz in current directory and delete this file
bool Workspace::unZip(QString fname /* without ".tar.gz" */)
{
  pr.clearArguments();
  pr << "gunzip" << fname + ".tar.gz";
  pr.start( KProcess::Block, KProcess::AllOutput );
  if ( pr.exitStatus() != 0 )
  {
    error = true;
    return false;
  }

  pr.clearArguments();
  pr << "tar" << "xf" << fname + ".tar";
  pr.start( KProcess::Block, KProcess::AllOutput );
  if ( pr.exitStatus() != 0 )
  {
    error = true;
    return false;
  }

  pr.clearArguments();
  pr << "rm" << fname + ".tar";
  pr.start( KProcess::Block, KProcess::AllOutput );
  if ( pr.exitStatus() != 0 )
  {
    error = true;
    return false;
  }
  return true;
}

// copy file( fname ) from path to currentdir
bool Workspace::copyFile(QString path, QString fname)
{
	if ( path.right(1) != "/") path.append("/");
  pr.clearArguments();
  pr << "cp" << QString("'%1'").arg(path + fname) << QString("'%1'").arg(fname);
  pr.start( KProcess::Block, KProcess::AllOutput );
  if ( pr.exitStatus() != 0 )
  {
    error = true;
    return false;
  }
  return true;
}

QString Workspace::copyAndAddFile( const char*  path )
{
	QFileInfo info(path);
	if ( !QDir().setCurrent( dir ) ){
    errorString = "Cannot set current dir to:" + dir;
    error = true;
    return "";
	}
  if ( info.dirPath() + "/" != getDir() )
    if ( !copyFile( info.dirPath(), info.fileName()) ) {
      errorString = QString("Cannot copy file to project dir. File: %1\n%2").arg(path).arg(errorString);
      error = true;
      return "";
  	}

	QStrList list;
  wConfig->setGroup( configPath  );
  wConfig->readListEntry( "File", list );
  list.append( info.fileName() );
  wConfig->writeEntry( "File", list );
  wConfig->sync();

  updateMakefileAm();
  mainWorkspace->update();
  QString fname = dir + info.fileName();
  QStrList pl;
  pl.append(fname);
  emit mainWorkspace->insertFilesInWorkspace( pl );
  return fname;
}

bool Workspace::createDirectory( QString path )
{
  if ( !QDir::root().mkdir( path ) )
  {
    errorString = " mkdir(" + dir + ") failure";
    error = true;
    return false;
  }
  QDir::root().setCurrent( path );
  return true;
}

void MainWorkspace::getTree( WorkspaceListView *list, bool withFile, bool expand )
{
  list->blockSignals ( true );

  list->saveData();
  list->clear();
  list->insertWorkspaceItem( this );
  Workspace::getTree( list, withFile, expand );

  list->blockSignals ( true );
  list->restoreData();
  list->blockSignals ( false );
}

void Workspace::getTree( WorkspaceListView *list, bool withFile, bool expand )
{
  if ( withFile ){
    QStrList files;
    QString str;
    wConfig->setGroup( configPath );
    wConfig->readListEntry( "File", files );
    for ( str = files.first();str != 0;str = files.next() )
      list->insertFileItem( this, str );
  }

  QStrList sub;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", sub );
  for ( uint k = 0; k < sub.count(); k++ )
  {
    Workspace* ws = new Workspace( this, sub.at(k) );
    list->insertWorkspaceItem( ws, this );
    ws->getTree( list, withFile, expand );
    delete ws;
  }
  list->setOpen( this, ( this ==mainWorkspace ) ? true:expand );
}

void Workspace::getFiles( QStrList& list )
{
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "File", list );
}

void Workspace::getAllSub( QStrList& list, bool withGroup )
{
  if ( mainWorkspace != this )
  {
    QString iname = configPath; iname.remove( 0, iname.find("/")+1 );
    if ( !((getType() == GROUP) && (!withGroup)) )list.append( iname );
  }

  QStrList sub;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", sub );
  for ( uint k = 0; k < sub.count(); k++ )
  {
    Workspace* ws = new Workspace( this, sub.at(k) );
    ws->getAllSub( list, withGroup );
    delete ws;
  }
}

Workspace::wType Workspace::getType()
{
  wConfig->setGroup( configPath );
  return (Workspace::wType)(wConfig->readNumEntry( "Type" ));
}

bool MainWorkspace::haveSub()
{
  QStrList subdir_list;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", subdir_list );
  if ( subdir_list.count() > 0 ) return true;
  return false;
}

QString Workspace::processIncludeHeaders()
{
  bool install = getProjectSimpleOptionsBool( WS_HEADER_INSTALL );
  QString path = getProjectSimpleOptions( WS_INSTALL_HEADER );

  if ( !install ) return QString("\n");

  QString data = "\ninclude_HEADERS = ";
  QStrList files;
  getFiles( files );
  for ( QString f = files.first(); f != 0L; f = files.next() ){
    if (isHeaderFile(f) ){
      data += "\\\n\t\t";
      data += f;
    }
  }
  if ( !path.isEmpty() ){
    data += "\nincludedir = ";
    data += path + "\n";
  }
  return data;
}

void Workspace::updateMakefileAm( bool recursive )
{
  if ( recursive ){
    QStrList sub;
    wConfig->setGroup( configPath );
    wConfig->readListEntry( "Group", sub );
    for ( uint k = 0; k < sub.count(); k++ )
    {
      Workspace* ws = new Workspace( this, sub.at(k) );
      ws->updateMakefileAm( true );
      delete ws;
    }
  }

  QString abs_filename = dir + "/Makefile.am";
  QFile file(abs_filename);
  QTextStream stream(&file);
  QStrList subdirs, files;
  QString str;
  QString extra_dist;

  QStrList oldFile;
  if ( file.open(IO_ReadOnly) ){
    bool studioPart = false;
    while ( !stream.eof() ){
      QString line = stream.readLine();
      if ( line == "#1########################################################!@#$" )
        studioPart = true;
      if ( !studioPart ){
        oldFile.append( line );
      }
      if ( line == "#2########################################################!@#$" )
        studioPart = false;
    }
  }
  file.close();

  if ( !file.open(IO_WriteOnly) ) return;

  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", subdirs );

/******************************************************************************/
  stream << "#1########################################################!@#$\n";
  stream << "####   Makefile.am generated automatically by KDEStudio   ####\n";
  stream << "#### WARNING! All changes made in this part will be lost! ####\n";
  stream << "##############################################################\n";

/******************************************************************************/
	if ( !subdirs.isEmpty() ){
    stream << "SUBDIRS = ";
    for ( str = subdirs.first();str != 0;str = subdirs.next() )
      stream << str << " ";
  }
/******************************************************************************/
  QString tname = "";
  QStrList prjLib, prjNotUsesLib, userLib, defLib;
  QString prjSharedLibStr, prjStaticLibStr, userLibStr, defLibStr, ldflagStr;
  stdLibraryDict stdDict;

  switch ( getType() )
  {
/******************************************************************************/
    case EXECUTABLE:
    {
      // bin install options
      bool isInstall = getProjectSimpleOptionsBool( WS_BIN_INSTALL );
      QString install_path = getProjectSimpleOptions( WS_INSTALL_BIN );
      if ( isInstall ){
        stream << "\nbin_PROGRAMS = " << getTargetName() << "\n";
        if ( !install_path.isEmpty() ){
          stream << "\nbindir = " << install_path << "\n";
        }
      } else {
        stream << "\nnoinst_PROGRAMS = " << getTargetName() << "\n";
      }
      stream << processIncludeHeaders() << "\n";

      tname = getTargetName() + "_";
      stream << tname << "LDFLAGS = $(all_libraries)\n";

      getProjectSimpleOptionsList( WS_LIBRARY_USES_DEF, defLib );

      mainWorkspace->getStdLibraryDict(stdDict);
      for ( uint i = 0; i < defLib.count(); i++){
        if ( stdDict.find(defLib.at(i)) ) {
          defLibStr += "\\\n\t\t";
          defLibStr += QString(stdDict.find( defLib.at(i) )).remove(0,2); // remove sorting number
        }
      }

      defLibStr.replace( QRegExp("([a-zA-Z0-9_\\.]*)"), "");
      defLibStr.stripWhiteSpace();

      // Project Libraries
      mainWorkspace->getAllProjectLibrary( prjLib );
      getProjectSimpleOptionsList( WS_LIBRARY_SELF_NOT_USES, prjNotUsesLib );
      QString ldFlags;

      // shared libs
      for ( QString libName = prjLib.first(); libName != 0L; libName = prjLib.next() ){
        if ( prjNotUsesLib.find(libName) == -1 ){
          Workspace* temp = mainWorkspace->getWorkspaceFromName(libName);
          if ( temp->getType() == SHARED_LIB && temp->getProjectSimpleOptionsBool(WS_LIB_INSTALL) ){

            prjSharedLibStr += "\\\n\t\t$(top_srcdir)/";
            QString ipath = temp->dir;
            ipath.remove( 0, mainWorkspace->dir.length() );
            prjSharedLibStr += ipath;
            prjSharedLibStr += "lib";
            prjSharedLibStr += libName;
            prjSharedLibStr += ".la";
          }
          mainWorkspace->freeSubWorkspace( temp );
        }
      }
      // static lib
      for ( QString libName = prjLib.first(); libName != 0L; libName = prjLib.next() ){
        if ( prjNotUsesLib.find(libName) == -1 ){
          Workspace* temp = mainWorkspace->getWorkspaceFromName(libName);
          if ( temp->getType() == STATIC_LIB ){
            QString ipath = temp->dir;
            ipath.remove( 0, mainWorkspace->dir.length() );
            ipath.prepend("\\\n\t\t$(top_srcdir)/");
            prjStaticLibStr += ipath;
            prjStaticLibStr += "lib";
            prjStaticLibStr += libName;
            prjStaticLibStr += ".a";
          }
          mainWorkspace->freeSubWorkspace( temp );
        }
      }

      // User Libraries
      getProjectSimpleOptionsList( WS_LIBRARY_USER, userLib );
      for ( QString item = userLib.first(); item != 0L; item = userLib.next() ){
        userLibStr += "\\\n\t\t-l";
        userLibStr += item;
      }
      stream << tname << "LDADD = " << prjStaticLibStr << prjSharedLibStr << defLibStr << userLibStr;

/******************************************************************************/
      if ( getProjectSimpleOptionsBool(WS_REMOVE_SIMBOL_TABLE) )
        ldflagStr = "-s";

      stream << "\nLDFLAGS = " << ldFlags << " " << ldflagStr << " " << getProjectSimpleOptions(WS_ADD_LINK_OPT).replace(QRegExp("\n")," ");

      break;
    }
/******************************************************************************/
/******************************************************************************/
    case SHARED_LIB:
    {
      // process install lib options
      bool isInstall = getProjectSimpleOptionsBool( WS_LIB_INSTALL );
      QString install_path = getProjectSimpleOptions( WS_INSTALL_LIB );
      if ( isInstall ){
        if ( !install_path.isEmpty() )
          stream << "\nlibdir = " << install_path;
        stream << "\nlib_LTLIBRARIES =";
      } else {
        stream << "\nnoinst_LTLIBRARIES =";
      }
      stream << "lib" << getTargetName() << ".la\n";
      stream << processIncludeHeaders() << "\n";

      tname = "lib" + getTargetName() + "_la_";
      if ( isInstall )
        stream << tname << "LDFLAGS = -version-info " << getProjectSimpleOptions(WS_VERSION) << "\n";

      // convenience libs ( noinst shared libs )
      QStrList noInstLibList;
      QString noInstLibStr = "";
      getProjectSimpleOptionsList( WS_CONVENIENCE_LIBS , noInstLibList );
      for ( QString libName = noInstLibList.first(); libName != 0L; libName = noInstLibList.next() ){
        Workspace* temp = mainWorkspace->getWorkspaceFromName(libName);
        noInstLibStr += "\\\n\t\t";
        QString ipath = temp->dir;
        ipath.remove( 0, mainWorkspace->dir.length() );
        ipath.prepend("$(top_srcdir)/");
        noInstLibStr += ipath + "lib" + libName + ".la";
        mainWorkspace->freeSubWorkspace( temp );
      }
      if ( !noInstLibStr.isEmpty() ){
        stream << tname << "LIBADD = " << noInstLibStr << "\n";
      }
      break;
    }
/******************************************************************************/
/******************************************************************************/
    case STATIC_LIB:
      stream << "\nnoinst_LIBRARIES = lib" << getTargetName() << ".a\n";
      stream << processIncludeHeaders() << "\n";
      tname = "lib" + getTargetName() + "_a_";
      break;
/******************************************************************************/
/******************************************************************************/
    case DATA:
    {
      // process install data path
      bool isInstall = getProjectSimpleOptionsBool( WS_DATA_INSTALL );
      QString dir =  getProjectSimpleOptions( WS_INSTALL_DATA );
      if ( isInstall ){
        if ( !dir.isEmpty() )
          stream << "\ndatadir = " << dir << "\n";
        stream << "\ndata_DATA = ";
      } else {
        stream << "\nnoinst_DATA = ";
      }

      files.clear();
      wConfig->setGroup( configPath );
      wConfig->readListEntry( "File", files );
    	if ( !files.isEmpty() ){
        for ( str = files.first();str != 0;str = files.next() )
          stream << str << " ";
      }
      break;
    }
    default:
      break;
  }

/******************************************************************************/
  if ( getType() == EXECUTABLE ||
       getType() == SHARED_LIB ||
       getType() == STATIC_LIB )
  {
    stream << "\nINCLUDES = $(all_includes)";

    QStrList list;
    getProjectSimpleOptionsList( WS_NOT_IN_INCLUDE_PATH, list );

    subdirs.clear();
    mainWorkspace->getAllProjectNames( subdirs );
    for ( uint k = 0; k < subdirs.count(); k++ )
    {
      if ( list.find(subdirs.at(k)) == -1 ){
        QString path = "\\\n\t\t-I$(top_srcdir)/";
        Workspace* w =  mainWorkspace->getWorkspaceFromName( subdirs.at(k) );
        QString s = w->configPath;
        mainWorkspace->freeSubWorkspace( w );
        s.remove( 0, mainWorkspace->configPath.length() + 1 );
        path += s;
        stream << path;
      }
    }
    list.clear();
    getProjectSimpleOptionsList( WS_ADD_TO_INCLUDE_PATH, list );
    for ( uint k = 0; k < list.count(); k++ )
    {
      stream << "\\\n\t\t-I" << list.at(k);
    }
  /******************************************************************************/
    QString cxxflags;
    if ( getProjectSimpleOptionsBool(WS_DEBUG_INFO  ) ) cxxflags += "-g ";
    if ( getProjectSimpleOptionsBool(WS_OPTIMIZE    ) ) cxxflags += "-O2 ";
    if ( getProjectSimpleOptionsBool(WS_SHOW_WARNING) ) cxxflags += "-Wall ";
    cxxflags.stripWhiteSpace();
    stream << "\nCXXFLAGS = " << cxxflags << getProjectSimpleOptions(WS_ADD_COMPILE_OPT).replace(QRegExp("\n")," ");
  /******************************************************************************/
    QString defs;
    defs = "-I. -I$(srcdir) -I$(top_srcdir)";
    stream << "\nDEFS = " << defs;
  /******************************************************************************/
    stream << "\n";
    stream << tname << "METASOURCES = AUTO\n";
  /******************************************************************************/
    files.clear();
    wConfig->setGroup( configPath );
    wConfig->readListEntry( "File", files );
    stream << tname << "SOURCES = ";
    for ( str = files.first();str != 0;str = files.next() ){
      extra_dist += "\\\n\t\t";
      extra_dist += str;
      if ( isSourceFile(str) )
        stream << "\\\n\t\t" << str;
    }
  }
  /******************************************************************************/
  files.clear();
  extra_dist = "";
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "File", files );
  for ( str = files.first();str != 0;str = files.next() ){
    extra_dist += "\\\n\t\t";
    extra_dist += str;
  }
  stream << "\nEXTRA_DIST = ";
  if ( this == mainWorkspace )
    stream << "admin/* Makefile.cvs " << name + ".studio ";
  stream << extra_dist;
/******************************************************************************/

  // install .kdelnk file
  if ( getType() == EXECUTABLE ){
    files.clear();
    wConfig->setGroup( configPath );
    wConfig->readListEntry( "File", files );
    if ( files.find(getTargetName() + ".kdelnk") != -1 ){
      stream << "\ndata_DATA = " << getTargetName() + ".kdelnk";
      stream << "\ndatadir = $(kde_appsdir)/Applications";
    }
  }

  stream << "\n";
  stream << "##############################################################\n";
  stream << "####                  END KDEStudio part                  ####\n";
  stream << "#2########################################################!@#$\n";

  for ( uint l=0; l<oldFile.count(); l++){
    stream << oldFile.at(l) << "\n";
  }
  file.close();
}

bool Workspace::createFile( const char* fname, wType type )
{
  QString fileName( fname );
  if ( fileName.isEmpty() || fileName.find('/') != -1 )
  {
    errorString = QString("File name invalid");
    return false;
  }

  bool noExt = fileName.find('/') == -1;

  switch ( type )
  {
    case CPPFILE:
    case MAINCPP:
    case KTMAINWCPP:
      if (noExt)
        fileName.append(".cpp");
      break;
    case HFILE:
    case KTMAINWH:
      if (noExt)
        fileName.append(".h");
      break;
    case KDELINKFILE:
      if (noExt)
        fileName = getTargetName() + ".desktop";
      break;
    default:
      break;
  }

  QStrList list;
  wConfig->setGroup( configPath  );
  wConfig->readListEntry( "File", list );
  if ( list.find(fileName) != -1 ) {
    errorString = QString("This name: ") + fname + ", already exist";
    return false;
  }

  QString abs_filename = dir + "/" + fileName;
  QFile file(abs_filename);
  QTextStream stream(&file);

  if ( !file.open(IO_WriteOnly) )
  {
    errorString = QString("File name invalid");
    return false;
  }

  QString className = mainWorkspace->ktMainWclassName;
  QString varName   = mainWorkspace->ktMainWclassVar;
  switch ( type )
  {
    case CPPFILE:
      stream << "#include \"" << fname << ".h\"\n";
      break;
    case HFILE:
      stream << "#ifndef " << QString(fname).upper() + "_H" << "\n";
      stream << "#define " << QString(fname).upper() + "_H" << "\n";
      stream << "\n";
      stream << "#ifdef HAVE_CONFIG_H\n";
      stream << "#include <config.h>\n";
      stream << "#endif\n";
      stream << "\n";
      stream << "\n";
      stream << "#endif\n";
      break;
    case MAINCPP:
      stream << "#include \"" << varName << ".h\"\n";
      stream << "\n";
      stream << "int main(int argc, char* argv[])\n";
      stream << "{\n";
      stream << "  KApplication app(argc,argv,\"" << varName << "\");\n";
      stream << "  " << className << "* " << varName << " = new " << className << "( \"" << varName << "\" );\n";
      stream << "  app.setMainWidget(" << varName << ");\n";
      stream << "  " << varName << "->show();\n";
      stream << "  return app.exec();\n";
      stream << "}\n";
      stream << "\n";
      break;
    case KTMAINWCPP:
      stream << "#include \"" << fname << ".h\"\n";
      stream << "\n";
      stream << className << "::" << className << "( const char *name )\n";
      stream << ": KTMainWindow( name )\n";
      stream << "{\n";
      stream << "}\n";
      stream << "\n";
      stream << className << "::~" << className << "()\n";
      stream << "{\n";
      stream << "}\n";
      stream << "\n";
      break;
    case KTMAINWH:
      stream << "#ifndef " << QString(fname).upper() + "_H" << "\n";
      stream << "#define " << QString(fname).upper() + "_H" << "\n";
      stream << "\n";
      stream << "#include <kapp.h>\n";
      stream << "#include <ktmainwindow.h>\n";
      stream << "\n";
      stream << "class " << className << " : public KTMainWindow\n";
      stream << "{Q_OBJECT\n";
      stream << "\n";
      stream << "public:\n";
      stream << "\t" << className << "( const char *name = 0L );\n";
      stream << "\t~" << className << "();\n";
      stream << "\n";
      stream << "protected:\n";
      stream << "\n";
      stream << "private:\n";
      stream << "\n";
      stream << "};\n";
      stream << "#endif\n";
      break;
    case KDELINKFILE:
      stream << "[Desktop Entry]\n";
      stream << "Name=" << fname << "\n";
      stream << "Exec=" << getTargetName() << "\n";
      stream << "Icon=" << "\n";
      stream << "DocPath=" << "\n";
      stream << "Comment=" << "\n";
      stream << "Terminal=0" << "\n";
      stream << "Type=Application" << "\n";
      break;
    default:
      break;
  }
  stream << "\n";
  file.close();

  list.append( fileName );
  wConfig->setGroup( configPath  );
  wConfig->writeEntry( "File", list );
  wConfig->sync();

  updateMakefileAm();
  mainWorkspace->update();
  return true;
}

bool MainWorkspace::createMainP( const char* fn, QString createIn )
{
  QString fname(fn);

  Workspace* w = getWorkspaceFromName( createIn );
  if ( w == 0L ) return false;
  ktMainWclassVar  = fname;
  ktMainWclassName = fname;
  ktMainWclassName = ktMainWclassName.left(1).upper() + \
                     ktMainWclassName.right( ktMainWclassName.length() - 1 ) + \
                     "App";

  bool e = w->createFile( "main", MAINCPP );
  if ( fname.find('.') != -1 )
    fname = fname.left(fname.find('.'));

  if ( e ) e = w->createFile(  fname , KTMAINWH );
  if ( e ) e = w->createFile(  fname , KTMAINWCPP );

  QStrList pList;
  pList.append( w->getDir() + "main.cpp" );
  pList.append( w->getDir() + fname + ".h" );
  pList.append( w->getDir() + fname + ".cpp" );
  emit insertFilesInWorkspace( pList );

  freeSubWorkspace( w );
  return e;
}

void MainWorkspace::updateMakefilesAm()
{
  updateMakefileAm();

  QStrList sub;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", sub );
  for ( uint k = 0; k < sub.count(); k++ )
  {
    Workspace* ws = new Workspace( this, sub.at(k) );
    ws->updateMakefileAm( true );
    delete ws;
  }
}

void MainWorkspace::freeSubWorkspace( Workspace* w )
{
  if ( w != this ) delete w;
}

void Workspace::changeParentWorkspace( const char* newParentWorkspaceName )
{
  if ( mainWorkspace == this ) return;

  mainWorkspace->enableAutoUpdate( false );

  temporaryFreeAllChild();

  // remove all project file and all subproject file from parser store
  QStrList fileList;
  getAllFilePath( fileList );
  for ( uint k = 0; k < fileList.count(); k++ )
    emit mainWorkspace->removeFileFromWorkspace( fileList.at(k) );

  Workspace* newParent = mainWorkspace->getWorkspaceFromName( newParentWorkspaceName );
  changeParent( newParent );
  mainWorkspace->freeSubWorkspace( newParent );

  // add all project file and all subproject file from parser store
  fileList.clear();
  getAllFilePath( fileList );
  emit mainWorkspace->insertFilesInWorkspace( fileList );

  mainWorkspace->enableAutoUpdate( true );
}

void Workspace::rename( const char* newName )
{
  error = false;
  if ( mainWorkspace == this ) return;

  Workspace* tw = mainWorkspace->getWorkspaceFromName(newName);
  if (tw) {
    //already exist
    error = true;
    errorString = "Workspace with this name already exist";
    mainWorkspace->freeSubWorkspace(tw);
    return;
  }

  mainWorkspace->enableAutoUpdate( false );

  temporaryFreeAllChild();

  // remove all project file and all subproject file from parser store
  QStrList fileList;
  getAllFilePath( fileList );
  for ( uint k = 0; k < fileList.count(); k++ )
    emit mainWorkspace->removeFileFromWorkspace( fileList.at(k) );

  setName( newName, false );

  // add all project file and all subproject file from parser store
  fileList.clear();
  getAllFilePath( fileList );
  emit mainWorkspace->insertFilesInWorkspace( fileList );

  mainWorkspace->enableAutoUpdate( true );
}

void Workspace::setName( QString text, bool remove )
{
  if ( mainWorkspace == this ) return;

  wConfig->setGroup( mainWorkspace->configPath );
  if ( wConfig->readEntry(WS_DEF_RUN_PRJ) == name ){
    if (!remove )
      wConfig->writeEntry( WS_DEF_RUN_PRJ, text );
    else {
      // search another default run project name
      QStrList nameList;
      mainWorkspace->getAllExecutablePath(  nameList );
      wConfig->setGroup( mainWorkspace->configPath );
      if ( nameList.isEmpty() ) // no more executables in workspace
        wConfig->writeEntry( WS_DEF_RUN_PRJ, "" );
      else {
        QString curname = nameList.at(0);
        if ( curname.right(1) == "/" ) curname.remove( curname.length() -1, 1 );
        curname = curname.remove( 0, curname.findRev("/") + 1 );
        wConfig->writeEntry( WS_DEF_RUN_PRJ, curname );
      }
    }
  }
  wConfig->sync();

  wConfig->setGroup( configPath );

  QString newDir = dir.left( dir.length()-1 );
  newDir = newDir.left( newDir.findRev("/") + 1 ) + text + "/";

  if ( remove ){
      pr.clearArguments();
      pr << "rm" << "-r" << "-f" << QString("'%1'").arg(dir);
      pr.start(KProcess::Block,KProcess::AllOutput);
  } else {
    if ( !(QDir().rename( dir, newDir )) ) {
      QMessageBox::critical( 0L, "Error", QString("Cannot rename directory\n%1\nto\n%2").arg(dir).arg(newDir) );
      return;
    }
  }

  QString newConfigPath = configPath.left( configPath.findRev("/") + 1 ) + text;

  if ( !remove ){
    /* change group name in child subproject
       from root/item1/item2/OLD_NAME/itemX
       to   root/item1/item2/NEW_NAME/itemX
       ... include this group name
       rewrite all subproject config data for it
    */

    /* STEP 1 : collect item need to update */
    QStringList groupList = wConfig->groupList();
    QStrList updateItem;
    for ( QStringList::Iterator listIt= groupList.begin(); listIt != groupList.end(); ++listIt ) {
      QString item = *listIt;
      if ( item.find( configPath+"/" ) == 0 || item  == configPath )
      {
        updateItem.append( item );
      }
    }

    /* STEP 2: dublicate this group to new group name ( newConfigPath ) */
    for ( QString item = updateItem.first(); item != 0L; item = updateItem.next() ){
      typedef QMap<QString, QString> GroupMap;
      GroupMap gm = wConfig->entryMap( item );
      GroupMap::Iterator it;

      wConfig->setGroup( item.replace(0, configPath.length() ,newConfigPath) );
      for ( it = gm.begin(); it != gm.end(); ++it ) {
        wConfig->writeEntry( it.key(), it.data() );
      }
    }

    /* STEP 3: remove old group */
    for ( QString item = updateItem.first(); item != 0L; item = updateItem.next() ){
      wConfig->deleteGroup( item );
    }
  }

  // update name in entry "Group" in parent project
  if ( mainWorkspace != this ){ // mainWorkspace not have parent project -> skip
    wConfig->setGroup( configPath.left(configPath.findRev("/")) );
    QStrList gr;
    gr.setAutoDelete( true );
    wConfig->readListEntry( "Group", gr );
    int k = gr.find( name );
    gr.remove( k );
    if ( !remove ) gr.insert( k, text );
    wConfig->writeEntry( "Group", gr );
    wConfig->sync();
  }

  // update key WS_NOT_IN_INCLUDE_PATH and WS_LIBRARY_SELF_NOT_USES and WS_CONVENIENCE_LIBS
  QStringList ugroupList = wConfig->groupList();
  for ( QStringList::Iterator listIt= ugroupList.begin(); listIt != ugroupList.end(); ++listIt ) {
    wConfig->setGroup( *listIt );
    QStrList include, library, convlibrary;
/*****************************************************************************/
    if ( wConfig->readListEntry( WS_NOT_IN_INCLUDE_PATH, include ) > 0 ){
      int k = include.find(name);
      if ( k != -1 ) {
        include.remove( k );
        if ( !remove ) include.insert( k, text );
      }
      wConfig->writeEntry( WS_NOT_IN_INCLUDE_PATH, include );
    }
/*****************************************************************************/
    if ( wConfig->readListEntry( WS_LIBRARY_SELF_NOT_USES, library ) > 0){
      int k = library.find(name);
      if ( k != -1 ) {
        library.remove( k );
        if ( !remove ) library.insert(k, text);
      }
      wConfig->writeEntry( WS_LIBRARY_SELF_NOT_USES, library );
    }
/*****************************************************************************/
    if ( wConfig->readListEntry( WS_CONVENIENCE_LIBS, convlibrary ) > 0){
      int k = convlibrary.find(name);
      if ( k != -1 ) {
        convlibrary.remove( k );
        if ( !remove ) convlibrary.insert(k, text);
      }
      wConfig->writeEntry( WS_CONVENIENCE_LIBS, convlibrary );
    }
/*****************************************************************************/
  }


  if ( remove ){
    wConfig->deleteGroup( configPath );
  } else {
    configPath = newConfigPath;
    dir        = newDir;
    name       = text;
  }
  wConfig->sync();
  mainWorkspace->updateMakefileAm( true );
}

void Workspace::changeParent( Workspace* parent )
{
  if ( mainWorkspace == this ) return;

  wConfig->setGroup( configPath );

  QString newDir = parent->dir + name + "/";
  if ( !(QDir().rename( dir, newDir )) ){ debug("cannot move project"); return; }

  QString newConfigPath = parent->configPath + "/" + name;

  /* change group name in child subproject
     from configPath/itemX
     to   newConfigPath/itemX
     ... include this group name
     rewrite all subproject config data for it
  */

  /* STEP 1 : collect item need to update */
    QStringList groupList = wConfig->groupList();
    QStrList updateItem;
    for ( QStringList::Iterator listIt= groupList.begin(); listIt != groupList.end(); ++listIt ) {
      QString item = *listIt;
      if ( item.find( configPath+"/" ) == 0 || item  == configPath )
      {
        updateItem.append( item );
      }
    }

  /* STEP 2: dublicate this group to new group name ( newConfigPath ) */
  for ( QString item = updateItem.first(); item != 0L; item = updateItem.next() ){
    typedef QMap<QString, QString> GroupMap;
    GroupMap gm = wConfig->entryMap( item );
    GroupMap::Iterator it;

    wConfig->setGroup( item.replace(0, configPath.length() ,newConfigPath) );
    for ( it = gm.begin(); it != gm.end(); ++it ) {
      wConfig->writeEntry( it.key(), it.data() );
    }
  }

  /* STEP 3: remove old group */
  for ( QString item = updateItem.first(); item != 0L; item = updateItem.next() ){
    wConfig->deleteGroup( item );
  }

  // update name in entry "Group" in old parent project
  wConfig->setGroup( configPath.left(configPath.findRev("/")) );
  QStrList grd;
  grd.setAutoDelete( true );
  wConfig->readListEntry( "Group", grd );
  grd.remove( grd.find( name ) );
  wConfig->writeEntry( "Group", grd );

  // update name in entry "Group" in parent project
  wConfig->setGroup( parent->configPath );
  QStrList gri;
  wConfig->readListEntry( "Group", gri );
  gri.append( name );
  wConfig->writeEntry( "Group", gri );

  configPath = newConfigPath;
  dir        = newDir;
  wConfig->sync();
  mainWorkspace->updateMakefileAm( true );
}

/////////////////////////////////////////////////////////////////////////////
void Workspace::setProjectSimpleOptions( QString optionName, QStrList& list )
{
  wConfig->setGroup( configPath );
  wConfig->writeEntry( optionName, list );
  wConfig->sync();
}

void Workspace::setProjectSimpleOptions( QString optionName, QString text )
{
  wConfig->setGroup( configPath );
  wConfig->writeEntry( optionName, text.replace( QRegExp("\n"), "\t" ) );
  wConfig->sync();
}

void Workspace::setProjectSimpleOptions( QString optionName, const char* text )
{
  wConfig->setGroup( configPath );
  wConfig->writeEntry( optionName, QString(text).replace( QRegExp("\n"), "\t" ) );
  wConfig->sync();
}

void Workspace::setProjectSimpleOptions( QString optionName, bool value )
{
  wConfig->setGroup( configPath );
  wConfig->writeEntry( optionName, value );
  wConfig->sync();
}

bool Workspace::getProjectSimpleOptionsBool( QString optionName )
{
  wConfig->setGroup( configPath );
  return wConfig->readBoolEntry( optionName );
}

void Workspace::getProjectSimpleOptionsList( QString optionName, QStrList& list )
{
  wConfig->setGroup( configPath );
  wConfig->readListEntry( optionName, list );
}

QString Workspace::getProjectSimpleOptions ( QString optionName )
{
  wConfig->setGroup( configPath );
  QString text =  wConfig->readEntry( optionName );
  text.replace( QRegExp("\t"), "\n" );
  return text;
}

void Workspace::getAllNoInstLibrary( QStrList& list )
{
  if ( getType() == SHARED_LIB && !getProjectSimpleOptionsBool(WS_LIB_INSTALL) )
    list.append( name );

  QStrList sub;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", sub );
  for ( uint k = 0; k < sub.count(); k++ )
  {
    Workspace* ws = new Workspace( this, sub.at( sub.count() - 1 - k) );
    ws->getAllNoInstLibrary( list );
    delete ws;
  }
}

void Workspace::getAllLibrary( QStrList& list )
{
  if ( getType() == SHARED_LIB ||  getType() == STATIC_LIB ) list.append( name );

  QStrList sub;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", sub );
  for ( uint k = 0; k < sub.count(); k++ )
  {
    Workspace* ws = new Workspace( this, sub.at( sub.count() - 1 - k) );
    ws->getAllLibrary( list );
    delete ws;
  }
}

void Workspace::getAllProjectName( QStrList& list )
{
  list.append( name );

  QStrList sub;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", sub );
  for ( uint k = 0; k < sub.count(); k++ )
  {
    Workspace* ws = new Workspace( this, sub.at(k) );
    ws->getAllProjectName( list );
    delete ws;
  }
}

void Workspace::getAllFilePath( QStrList& list )
{
	QStrList nlist;
  wConfig->setGroup( configPath  );
  wConfig->readListEntry( "File", nlist );
  for ( nlist.first(); nlist.current() != 0L; nlist.next() ){
    list.append( dir + nlist.current() );
  }

  QStrList sub;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", sub );
  for ( uint k = 0; k < sub.count(); k++ )
  {
    Workspace* ws = new Workspace( this, sub.at(k) );
    ws->getAllFilePath( list );
    delete ws;
  }
}

bool Workspace::checkName( const char* wname, wType type )
{
  if ( QString(wname).isEmpty() ) {
    errorString = QString("Name is empty");
    return false;
  }

  QStrList list;
  mainWorkspace->getAllProjectNames( list );
  if ( list.find(wname) != -1 ) {
    errorString = QString("This name: <") + wname + "> should be unuque";
    return false;
  }
  return true;
}

void Workspace::temporaryFreeAllChild()
{
  emit mainWorkspace->closeWorkspace( this );

  QStrList sub;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", sub );
  for ( uint k = 0; k < sub.count(); k++ )
  {
    Workspace* ws = new Workspace( this, sub.at(k) );
    ws->temporaryFreeAllChild();
    delete ws;
  }
}

void Workspace::remove( bool reparse )
{
  if ( mainWorkspace == this ) return; // root workspace can be remove;
  emit mainWorkspace->closeWorkspace( this );

  QStrList fileList;
  getFiles( fileList );
  for ( uint k = 0; k < fileList.count(); k++ )
    emit mainWorkspace->removeFileFromWorkspace( dir + fileList.at(k) );

  QStrList sub;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", sub );
  for ( uint k = 0; k < sub.count(); k++ )
  {
    Workspace* ws = new Workspace( this, sub.at(k) );
    ws->remove( false );
    delete ws;
  }

  setName( "remove", true );
  mainWorkspace->updateMakefilesAm();

  if ( reparse ) emit mainWorkspace->reparsing();
  mainWorkspace->update();
  emit mainWorkspace->updateUI();
}

bool Workspace::removeFile( QString fileName )
{
	QStrList list;
  wConfig->setGroup( configPath  );
  wConfig->readListEntry( "File", list );
	int i = list.find( fileName );
	if ( i != -1 ) list.remove(i); else return false;
  wConfig->writeEntry( "File", list );
  wConfig->sync();
  updateMakefileAm();

  emit mainWorkspace->removeFileFromWorkspace( dir + fileName );
  mainWorkspace->update();
	return true;
}

Workspace* Workspace::getFromName( QString wname )
{
  if ( wname.isEmpty() ) return 0L;

  if ( name == wname ) return this;

  QStrList sub;
  wConfig->setGroup( configPath );
  wConfig->readListEntry( "Group", sub );
  for ( uint k = 0; k < sub.count(); k++ )
  {
    Workspace* ws = new Workspace( this, sub.at(k) );
    Workspace* temp = ws->getFromName( wname );
    if ( temp != 0L ) return temp;
    delete ws;
  }
  return 0L;
}

void MainWorkspace::needRebuild()
{
  QDir().remove(getDir()+"Makefile");
}

QString Workspace::getTargetName()
{
  QString tname = getProjectSimpleOptions(WS_TARGETNAME);
  if (tname.isEmpty()) {
    tname = getName();
    setProjectSimpleOptions( WS_TARGETNAME, tname );
  }
  return tname;
}
