#ifndef dlg_compile_included
#define dlg_compile_included

#include <qwidget.h>
#include <qcheckbox.h>
#include <qmultilinedit.h>

class CheckListBox;

class dlg_compile : public QWidget
{Q_OBJECT

public:
    dlg_compile( QWidget* parent = NULL, const char* name = NULL );
    virtual ~dlg_compile();

    CheckListBox* incdir;
    QMultiLineEdit* addopt;
    QCheckBox* debug;
    QCheckBox* optimize;
    QCheckBox* warning;
};
#endif





