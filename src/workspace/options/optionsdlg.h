#ifndef OPTIONSDLG_H
#define OPTIONSDLG_H

#include <qdialog.h>

class QPushButton;
class MainWorkspace;
class WorkspaceListView;
class KDockTabCtl;
class dlg_compile;
class dlg_general;
class dlg_link;
class dlg_install;
class CheckListBox;
class QListBox;
class QCheckBox;
class QLineEdit;

class OptionsDlg : public QDialog
{Q_OBJECT
public:
  static void showDialog( MainWorkspace*, const char* path );

protected:
	OptionsDlg( MainWorkspace*, const char* path );
	~OptionsDlg();

private:
  MainWorkspace* w;

  CheckListBox* sortGroup;
  CheckListBox* convenList;
  WorkspaceListView* wList;
  KDockTabCtl* tab;

  dlg_general* general;
  dlg_compile* compile;
  dlg_link* link;
  dlg_install* install;

  QPushButton* reset;
  QPushButton* apply;
  QPushButton* close;

protected slots:
  void slotSelectPrj( const char* );
  void slotReset();
  void slotApply();
  void slotItemCheckChange( int, bool );
};

#endif

















