#ifndef DLG_INSTALL_H
#define DLG_INSTALL_H

#include <qwidget.h>
#include "install_dlg.h"

class Workspace;

class dlg_install : public QWidget
{Q_OBJECT
public:
  dlg_install( QWidget* parent );
  ~dlg_install(){};

  void setup( Workspace* );
  void done( Workspace* );

private:
  install_dlg* data;
  install_dlg* bin;
  install_dlg* lib;
  install_dlg* header;
};

#endif


