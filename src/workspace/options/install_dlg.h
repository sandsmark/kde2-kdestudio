#ifndef INSTALL_DLG_H
#define INSTALL_DLG_H

/*Include section - START*/
#include <qwidget.h>
class QButtonGroup;
class QLineEdit;
class QRadioButton;
/*section - END*/

/*Form Class declaration section - START*/
class install_dlg : public QWidget
{ Q_OBJECT
/*section - END*/

/*Public section - START*/
public:
	install_dlg( QWidget * parent=0, const char * name=0, WFlags f=0 );
	~install_dlg();
/*section - END*/
  void setType( const QString& );

  void setup( bool install, QString ipath );
  QString getInstallPath();
  bool isInstall();

protected slots:
/*Slots section - START*/
/*section - END*/
	void rb_Toggled(bool);

/*Private section - START*/
private:
	QButtonGroup* buttonGroup1;
	QLineEdit* path;
	QRadioButton* custom;
	QRadioButton* default_install;
	QRadioButton* notinstall;
/*section - END*/

};

#endif
