#ifndef dlg_general_included
#define dlg_general_included

#include <qmultilinedit.h>
#include <qlineedit.h>

class dlg_general : public QWidget
{ Q_OBJECT
public:
    dlg_general( QWidget* parent = NULL, const char* name = NULL );
    virtual ~dlg_general();

  QMultiLineEdit* prjdesc;
  QLineEdit* prjname;
  QLineEdit* tname;
  QLineEdit* prjver;
  QLineEdit* configarg;
};
#endif



