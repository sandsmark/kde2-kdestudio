#include "install_dlg.h"

/*Include section - START*/
#include <qbuttongroup.h>
#include <qlineedit.h>
#include <qradiobutton.h>
/*section - END*/

/*Constructor section - START*/
install_dlg::install_dlg( QWidget * parent, const char * name, WFlags f )
: QWidget( parent, name, f )
{
	#include "install_dlg.create"
/*section - END*/

}

/*Destructor section - START*/
install_dlg::~install_dlg()
{
/*section - END*/

}

void install_dlg::setType( const QString& t )
{
  buttonGroup1->setTitle( t + ": Install path" );
}

void install_dlg::rb_Toggled(bool)
{
  path->setEnabled( custom->isChecked() );
}

void install_dlg::setup( bool install, QString ipath )
{
  if ( install ){
    if ( ipath.isEmpty() ){
      default_install->setChecked( true );
    } else {
      custom->setChecked( true );
      path->setText( ipath );
    }
  } else {
    notinstall->setChecked( true );
  }
}

QString install_dlg::getInstallPath()
{
  if ( !custom->isChecked() ) return "";
  QString p = path->text();
  return p;
}

bool install_dlg::isInstall()
{
  return !notinstall->isChecked();
}

