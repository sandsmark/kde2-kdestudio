#include "optionsdlg.h"
#include <kapp.h>
#include <kdocktabctl.h>

#include <qpushbutton.h>
#include <qcheckbox.h>

#include "workspace.h"
#include "workspacelistview.h"
#include "checklistbox.h"

#include "dlg_compile.h"
#include "dlg_general.h"
#include "dlg_link.h"
#include "dlg_install.h"


OptionsDlg::OptionsDlg( MainWorkspace* mw, const char* path )
: QDialog( kapp->mainWidget(), "Project Options", true )
{
  w = mw;
  setCaption( "Project Options" );

  wList = new WorkspaceListView( this );
  wList->setGeometry( 0, 0, 190, 445 );
  w->getTree( wList, false, true );
  connect( wList, SIGNAL(selectWorkspace(const char*)), SLOT(slotSelectPrj(const char*)) );

  tab = new KDockTabCtl( this );
  compile = new dlg_compile( tab, "Compiler" );
  general = new dlg_general( tab, "General" );
  link = new dlg_link( tab, "Link" );
  install = new dlg_install( tab );
  connect( link->libStdAlias, SIGNAL(itemCheckChange(int,bool)), SLOT(slotItemCheckChange(int, bool)) );

  sortGroup = new CheckListBox( tab, "Group" );
  sortGroup->setHeaderCaption("Group order to compile:");
  sortGroup->allowChecked(false);
  sortGroup->allowEditing(false);

  convenList = new CheckListBox( tab, "convenList" );
  convenList->setHeaderCaption("Convenience shared libraries:");
  convenList->allowEditing(false);

  tab->insertPage( general, "General" );
  tab->insertPage( compile, "Compiler options" );
  tab->insertPage( link, "Linker options" );
  tab->insertPage( sortGroup , "Group options" );
  tab->insertPage( convenList , "Convenience" );
  tab->insertPage( install , "Install options" );

  tab->setGeometry( 200, 0, 540, 400 );

  reset = new QPushButton( this );
  reset->setText("Reset");
  reset->move( 200 , 410 );
  connect( reset, SIGNAL(clicked()), SLOT(slotReset()) );
  apply = new QPushButton( this );
  apply->setText("Apply");
  apply->move( 300 , 410 );
  connect( apply, SIGNAL(clicked()), SLOT(slotApply()) );
  close = new QPushButton( this );
  close->setText("Close");
  close->move( 400 , 410 );
  connect( close, SIGNAL(clicked()), SLOT(reject()) );

  setFixedSize( 740, 445 );

  if ( !path ) path = mw->getName();

  wList->setCurrentWorkspaceName( path );
  slotSelectPrj( path );

  move( (qApp->desktop()->width() - width())/2, (qApp->desktop()->height() - height())/2 );
}

OptionsDlg::~OptionsDlg()
{
}

void OptionsDlg::showDialog( MainWorkspace* mw , const char* path )
{
  OptionsDlg* dlg = new OptionsDlg( mw, path );
  dlg->exec();
}

void OptionsDlg::slotSelectPrj( const char* name )
{
  Workspace* ws = w->getWorkspaceFromName( name );

  general->configarg->setText("");
  general->configarg->setEnabled(false);

  general->tname->setText("");
  general->tname->setEnabled(false);

  switch ( ws->getType() ){
    case Workspace::MAIN:
      tab->setPageEnabled( install , false );
      tab->setPageEnabled( link , false );
      tab->setPageEnabled( compile , false );
      tab->setPageEnabled( convenList , false );

      general->configarg->setText( ws->getProjectSimpleOptions(WS_CONFIGUREARG) );
      general->configarg->setEnabled(true);
      break;
    case Workspace::EXECUTABLE:
      tab->setPageEnabled( install , true );
      tab->setPageEnabled( link , true );
      tab->setPageEnabled( compile , true );
      tab->setPageEnabled( convenList , false );
      general->tname->setEnabled(true);
      break;
    case Workspace::DATA:
      tab->setPageEnabled( install , true );
      tab->setPageEnabled( link , false );
      tab->setPageEnabled( compile , false );
      tab->setPageEnabled( convenList , false );
      break;
    case Workspace::GROUP:
      tab->setPageEnabled( install , false );
      tab->setPageEnabled( link , false );
      tab->setPageEnabled( compile , false );
      tab->setPageEnabled( convenList , false );
      break;
    case Workspace::SHARED_LIB:
      tab->setPageEnabled( install , true );
      tab->setPageEnabled( link , false );
      tab->setPageEnabled( compile , true );
      tab->setPageEnabled( convenList , true );
      general->tname->setEnabled(true);
      break;
    case Workspace::STATIC_LIB:
      tab->setPageEnabled( install , true );
      tab->setPageEnabled( link , false );
      tab->setPageEnabled( compile , true );
      tab->setPageEnabled( convenList , false );
      general->tname->setEnabled(true);
      break;
    default: break;
  }

  // Install Setting
  install->setup( ws );

  // General Setting ==================================================================
  general->prjname->setText( ws->getName() );
  general->tname->setText( ws->getTargetName() );
  general->prjver ->setText( ws->getProjectSimpleOptions(WS_VERSION) );
  general->prjdesc->setText( ws->getProjectSimpleOptions(WS_DESCRIPTIONS) );

  // Group Setting ==================================================================
  QStrList group; // list of group
  ws->getProjectSimpleOptionsList( WS_GROUP, group );
  sortGroup->clear();
  for ( uint k = 0;k<group.count(); k++ )
  {
    CheckListBoxItem* item = sortGroup->insertItem( group.at(k), false );
    item->enableDeleted( false );
  }

  // Compile Setting ==================================================================
  compile->incdir->clear();

  QStrList notInPath;
  ws->getProjectSimpleOptionsList( WS_NOT_IN_INCLUDE_PATH, notInPath );

  QStrList sub;
  ws->getMainWorkspace()->getAllProjectNames( sub );

  for ( uint k = 0; k < sub.count(); k++ )
  {
    CheckListBoxItem* item = compile->incdir->insertItem( sub.at(k) );
    item->enableDeleted( false );
    item->allowEdit( false );
    if ( notInPath.find(sub.at(k)) == -1 )
      item->setCheck( true );
  }

  QStrList addInPath;
  ws->getProjectSimpleOptionsList( WS_ADD_TO_INCLUDE_PATH, addInPath );
  for ( QString item = addInPath.first(); item != 0L; item = addInPath.next() ){
    compile->incdir->insertItem( item )->setCheck( true );
  }

  compile->debug->setChecked( ws->getProjectSimpleOptionsBool( WS_DEBUG_INFO ) );
  compile->optimize->setChecked( ws->getProjectSimpleOptionsBool( WS_OPTIMIZE ) );
  compile->warning->setChecked( ws->getProjectSimpleOptionsBool( WS_SHOW_WARNING ) );
  compile->addopt->setText( ws->getProjectSimpleOptions(WS_ADD_COMPILE_OPT) );

  // Link Setting ==================================================================
  link->libStdAlias->setUpdatesEnabled(false);

  link->lib->setUpdatesEnabled(false);

  link->libStdAlias->clear();
  link->lib->clear();

  link->removeallsimbol->setChecked( ws->getProjectSimpleOptionsBool( WS_REMOVE_SIMBOL_TABLE ) );
  link->addflag->setText( ws->getProjectSimpleOptions(WS_ADD_LINK_OPT) );

  stdLibraryDict stdLibDict;
  w->getStdLibraryDict( stdLibDict );

  // Default Libraries
  QStrList usesDefLib; // list of uses default library alias
  ws->getProjectSimpleOptionsList( WS_LIBRARY_USES_DEF, usesDefLib );

  stdLibraryIterator stdLibIterator( stdLibDict );
  stdLibIterator.toFirst();
  QStrList usesLib;
  for ( uint k = 0;k<stdLibIterator.count(); k++ ) link->libStdAlias->insertItem("(NULL)");
  while ( stdLibIterator.current() ) {
    QString alias = stdLibIterator.currentKey();
    QString lib = stdLibIterator.current();
    int k = lib.left(2).toInt();
    lib.remove(0, 2);
    CheckListBoxItem* item = link->libStdAlias->insertItem( alias, false, k );
    link->libStdAlias->removeItem( k + 1 );
    item->allowEdit( false );
    if ( usesDefLib.find(alias) != -1 ){
      item->setCheck( true );
    }
    ++stdLibIterator;
  }

  // Project Libraries
  QStrList prjLib;
  QStrList prjNotUsesLib;
  w->getAllProjectLibrary( prjLib );
  ws->getProjectSimpleOptionsList( WS_LIBRARY_SELF_NOT_USES, prjNotUsesLib );

  for ( QString libName = prjLib.first(); libName != 0L; libName = prjLib.next() ){
    CheckListBoxItem* item = link->lib->insertItem( libName );
    item->enableDeleted( false );
    item->allowEdit( false );
    if ( prjNotUsesLib.find(libName) == -1 ){
      item->setCheck( true );
    }
  }

  // User Libraries
  QStrList userLib;
  ws->getProjectSimpleOptionsList( WS_LIBRARY_USER, userLib );
  for ( QString item = userLib.first(); item != 0L; item = userLib.next() ){
    link->lib->insertItem( item )->setCheck( true );
  }

  link->libStdAlias->setUpdatesEnabled(true);
  link->libStdAlias->updateList();

  link->lib->setUpdatesEnabled(true);
  link->lib->updateList();

  // Convenience Setting ==================================================================
  QStrList convenLib; // list of uses noinst shared libs
  QStrList allConvenLib; // list of all project noinst shared libs
  ws->getProjectSimpleOptionsList( WS_CONVENIENCE_LIBS, convenLib );
  w->getAllProjectNoInstLibrary( allConvenLib );
  convenList->clear();
  for ( uint k = 0; k < allConvenLib.count(); k++ )
  {
    CheckListBoxItem* item = convenList->insertItem( allConvenLib.at(k), false );
    if ( convenLib.find(allConvenLib.at(k)) != -1 )
      item->setCheck( true );
    item->enableDeleted( false );
  }

  ws->mainWorkspace->freeSubWorkspace( ws );
}

void OptionsDlg::slotReset()
{
  QString wname = wList->getCurrentWorkspaceName();
  if ( !wname.isEmpty() ) slotSelectPrj( wname );
}

void OptionsDlg::slotApply()
{
  Workspace* ws = w->getWorkspaceFromName( wList->getCurrentWorkspaceName() );
  if ( ws == 0L ) return;

  // General Setting
  if ( general->configarg->isEnabled() )
    ws->setProjectSimpleOptions( WS_CONFIGUREARG, general->configarg->text() );

  if ( ws->getName() != general->prjname->text() )
    ws->rename( general->prjname->text() );

  ws->setProjectSimpleOptions( WS_DESCRIPTIONS, general->prjdesc->text() );
  ws->setProjectSimpleOptions( WS_VERSION, general->prjver->text() );
  ws->setProjectSimpleOptions( WS_TARGETNAME, general->tname->text() );

  // Convenience Setting
  if ( tab->isPageEnabled( convenList ) ){
    QStrList convLib; // list of group
    for ( uint k = 0; k < convenList->count(); k++ )
    {
      if ( convenList->item(k)->isChecked() )
        convLib.append( convenList->item(k)->text() );
    }
    ws->setProjectSimpleOptions( WS_CONVENIENCE_LIBS, convLib );
  }

  // Install Setting
  install->done( ws );

  // Group Setting
  if ( tab->isPageEnabled( sortGroup ) ){
    QStrList group; // list of group
    for ( uint k = 0;k<sortGroup->count(); k++ )
    {
      group.append( sortGroup->item(k)->text() );
    }
    ws->setProjectSimpleOptions( WS_GROUP, group );
  }

  if ( tab->isPageEnabled( compile ) ){
    // Compile Setting
    QStrList notInPath;
    QStrList addInPath;

    for ( uint k = 0; k < compile->incdir->count(); k++ ){
      CheckListBoxItem* item = compile->incdir->item(k);
      if ( item->isAllowEdit() && item->isChecked() ){
        addInPath.append( item->text() );
      }
      if ( !item->isAllowEdit() && !item->isChecked() ){
        notInPath.append( item->text() );
      }
    }
    ws->setProjectSimpleOptions( WS_ADD_TO_INCLUDE_PATH, addInPath );
    ws->setProjectSimpleOptions( WS_NOT_IN_INCLUDE_PATH, notInPath );

    ws->setProjectSimpleOptions( WS_DEBUG_INFO, compile->debug->isChecked() );
    ws->setProjectSimpleOptions( WS_OPTIMIZE, compile->optimize->isChecked() );
    ws->setProjectSimpleOptions( WS_SHOW_WARNING, compile->warning->isChecked() );
    ws->setProjectSimpleOptions( WS_ADD_COMPILE_OPT, compile->addopt->text() );
  }

  if ( tab->isPageEnabled( link ) ){
    // Link Setting
    ws->setProjectSimpleOptions( WS_REMOVE_SIMBOL_TABLE, link->removeallsimbol->isChecked() );
    ws->setProjectSimpleOptions( WS_ADD_LINK_OPT, link->addflag->text() );

    QStrList usesDefLib;
    for ( uint k = 0; k < link->libStdAlias->count(); k++ ){
      CheckListBoxItem* item = link->libStdAlias->item(k);
      if ( item->isChecked() ){
        usesDefLib.append( item->text() );
      }
    }
    ws->setProjectSimpleOptions( WS_LIBRARY_USES_DEF, usesDefLib );

    // User and Project Libraries
    QStrList userLib;
    QStrList notUsesSelfLib;
    for ( uint k = 0; k < link->lib->count(); k++ ){
      CheckListBoxItem* item = link->lib->item(k);
      if ( item->isAllowEdit() && item->isChecked() ){
        userLib.append( item->text() );
      }
      if ( !item->isAllowEdit() && !item->isChecked() ){
        notUsesSelfLib.append( item->text() );
      }
    }
    ws->setProjectSimpleOptions( WS_LIBRARY_USER, userLib );
    ws->setProjectSimpleOptions( WS_LIBRARY_SELF_NOT_USES, notUsesSelfLib );
  }

  ws->updateMakefileAm();

  // reload all data
  QString thisPath = ws->getName();
  ws->mainWorkspace->freeSubWorkspace( ws );
  w->getTree( wList, false, true );
  wList->setCurrentWorkspaceName( thisPath );
}

void OptionsDlg::slotItemCheckChange( int k, bool ch )
{
  stdLibraryDict stdLibDict;
  w->getStdLibraryDict( stdLibDict );
  QString alias = link->libStdAlias->item(k)->text();
  QString text = stdLibDict.find( alias );
  if ( ch ){
    while ( !text.isEmpty() && text.find("(") != -1 ){
      text.remove(0, text.find("(") + 1 );
      QString alias = text.left( text.find(")") );
      text.remove( 0, text.find(")") + 1 );
      for ( uint i = 0; i < link->libStdAlias->count(); i++ )
        if ( link->libStdAlias->item(i)->text() == alias )
          if ( !link->libStdAlias->item(i)->isChecked() )
            link->libStdAlias->item(i)->setCheck(true);
    }
  } else {
    alias.prepend("(").append(")");
    for ( uint i = 0; i < link->libStdAlias->count(); i++ ){
      QString itemLibs = stdLibDict.find( link->libStdAlias->item(i)->text() );
      if ( itemLibs.find( alias ) != -1 )
        link->libStdAlias->item(i)->setCheck(false);
    }
  }
}
