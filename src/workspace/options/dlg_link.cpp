#include "dlg_link.h"
#include "checklistbox.h"

#include <qlabel.h>

dlg_link::dlg_link( QWidget* parent, const char* name )
: QWidget( parent, name )
{
  libStdAlias = new CheckListBox( this );
  libStdAlias->showHeader( false );
  libStdAlias->setGeometry( 10, 40, 250, 210 );

  lib = new CheckListBox( this );
  lib->setGeometry( 270, 40, 240, 210 );
  lib->setHeaderCaption("Project & User Library:");

  QLabel* qtarch_Label_3;
  qtarch_Label_3 = new QLabel( this );
  qtarch_Label_3->setGeometry( 10, 10, 250, 18 );
  qtarch_Label_3->setText( "Library Alias:" );

  removeallsimbol = new QCheckBox( this );
  removeallsimbol->setGeometry( 10, 260, 450, 30 );
  removeallsimbol->setText( "remove all symbol table and relocation information from the executable" );

  QLabel* qtarch_Label_13;
  qtarch_Label_13 = new QLabel( this );
  qtarch_Label_13->setGeometry( 10, 290, 100, 30 );
  qtarch_Label_13->setText( "additional flags:" );

  addflag = new QMultiLineEdit( this );
  addflag->setGeometry( 110, 300, 410, 50 );

  resize( 530,360 );
}


dlg_link::~dlg_link()
{
}










