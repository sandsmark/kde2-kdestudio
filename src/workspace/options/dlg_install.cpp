#include "dlg_install.h"
#include <workspace.h>
#include <tools.h>

dlg_install::dlg_install( QWidget* parent )
:QWidget( parent )
{
  data   = new install_dlg( this );
  bin    = new install_dlg( this );
  lib    = new install_dlg( this );
  header = new install_dlg( this );

  bin->setType( "Executable" );
  lib->setType( "Library" );
  header->setType( "Headers" );
  data->setType( "Data" );

  QPoint p(0,0);
  QPoint add( 0, bin->height() + 3 );

  bin->move( p ); p += add;
  lib->move( p ); p += add;
  data->move( p ); p += add;
  header->move( p ); p += add;
}

void dlg_install::setup( Workspace* ws )
{
  data->setEnabled( ws->getType() ==  Workspace::DATA );
  bin->setEnabled( ws->getType() ==  Workspace::EXECUTABLE );
  lib->setEnabled( ws->getType() ==  Workspace::SHARED_LIB );
  header->setEnabled( ws->getType() ==  Workspace::EXECUTABLE || ws->getType() ==  Workspace::STATIC_LIB || ws->getType() ==  Workspace::SHARED_LIB );

  #define SET_PAGE( page, INSTALL, INSTALL_PATH )\
  if ( page->isEnabled() ){\
    bool install = ws->getProjectSimpleOptionsBool( INSTALL );\
    QString path = ws->getProjectSimpleOptions( INSTALL_PATH );\
    page->setup( install, path );\
  }

  SET_PAGE( data  , WS_DATA_INSTALL  , WS_INSTALL_DATA   );
  SET_PAGE( bin   , WS_BIN_INSTALL   , WS_INSTALL_BIN    );
  SET_PAGE( lib   , WS_LIB_INSTALL   , WS_INSTALL_LIB    );
  SET_PAGE( header, WS_HEADER_INSTALL, WS_INSTALL_HEADER );

  #undef SET_PAGE
}

void dlg_install::done( Workspace* ws )
{
  #define GET_PAGE( page, INSTALL, INSTALL_PATH )\
  if ( page->isEnabled() ){\
    bool install = page->isInstall();\
    QString path = page->getInstallPath();\
    ws->setProjectSimpleOptions( INSTALL, install );\
    ws->setProjectSimpleOptions( INSTALL_PATH, path );\
  }

  GET_PAGE( data  , WS_DATA_INSTALL  , WS_INSTALL_DATA   );
  GET_PAGE( bin   , WS_BIN_INSTALL   , WS_INSTALL_BIN    );
  GET_PAGE( lib   , WS_LIB_INSTALL   , WS_INSTALL_LIB    );
  GET_PAGE( header, WS_HEADER_INSTALL, WS_INSTALL_HEADER );

  #undef GET_PAGE
}