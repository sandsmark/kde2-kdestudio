#ifndef dlg_link_included
#define dlg_link_included

#include <qwidget.h>
#include <qcheckbox.h>
#include <qmultilinedit.h>

class CheckListBox;

class dlg_link : public QWidget
{Q_OBJECT
public:
    dlg_link( QWidget* parent = NULL, const char* name = NULL );
    virtual ~dlg_link();

    CheckListBox* libStdAlias;
    CheckListBox* lib;
    QCheckBox* removeallsimbol;
    QMultiLineEdit* addflag;

};
#endif







