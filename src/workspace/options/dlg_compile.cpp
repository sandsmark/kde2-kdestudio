#include "dlg_compile.h"
#include "checklistbox.h"
#include <qlabel.h>

dlg_compile::dlg_compile( QWidget* parent, const char* name )
: QWidget( parent, name )
{
	incdir = new CheckListBox( this );
  incdir->setHeaderCaption("Include directories");
	incdir->setGeometry( 10, 10, 230, 340 );

	QLabel* qtarch_Label_9;
	qtarch_Label_9 = new QLabel( this );
	qtarch_Label_9->setGeometry( 260, 140, 130, 18 );
	qtarch_Label_9->setText( "Additional options:" );

	addopt = new QMultiLineEdit( this );
	addopt->setGeometry( 260, 160, 260, 190 );

	debug = new QCheckBox( this );
	debug->setGeometry( 250, 40, 250, 20 );
	debug->setText( "generate debugging information" );

	optimize = new QCheckBox( this );
	optimize->setGeometry( 250, 10, 250, 20 );
	optimize->setText( "optimize" );

	warning = new QCheckBox( this );
	warning->setGeometry( 250, 70, 250, 20 );
	warning->setText( "show warnings" );

	resize( 530,360 );
}


dlg_compile::~dlg_compile()
{
}




