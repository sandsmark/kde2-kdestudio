#include "dlg_general.h"

#include <qlabel.h>

dlg_general::dlg_general( QWidget* parent, const char* name )
:	QWidget( parent, name )
{
	prjname = new QLineEdit( this );
	prjname->setGeometry( 130, 10, 150, 20 );

	QLabel* l1 = new QLabel( this );
	l1->setGeometry( 10, 10, 99, 18 );
	l1->setText( "Project Name:" );

	prjdesc = new QMultiLineEdit( this );
	prjdesc->setGeometry( 10, 120, 510, 230 );

	QLabel* l2 = new QLabel( this );
	l2->setGeometry( 10, 90, 92, 18 );
	l2->setText( "Descriptions:" );

	QLabel* l3 = new QLabel( this );
	l3->setGeometry( 390, 10, 58, 18 );
	l3->setText( "Version:" );

	prjver = new QLineEdit( this );
	prjver->setGeometry( 460, 10, 60, 20 );

	QLabel* l4 = new QLabel( this );
	l4->setGeometry( 10, 60, 99, 18 );
	l4->setText( "Configure options's:" );

	configarg = new QLineEdit( this );
	configarg->setGeometry( 130, 60, 390, 20 );

	QLabel* l5 = new QLabel( this );
	l5->setGeometry( 10, 35, 99, 18 );
	l5->setText( "Target Name:" );

	tname = new QLineEdit( this );
	tname->setGeometry( 130, 35, 150, 20 );
	
	resize( 530,360 );
}


dlg_general::~dlg_general()
{
}

