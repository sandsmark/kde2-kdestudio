#include <qheader.h>
#include <qfileinfo.h>
#include <kapp.h>
#include <kiconloader.h>

#include "workspacelistview.h"
#include "workspace.h"
#include "tools.h"

WorkspaceListView::WorkspaceListView(QWidget* parent, const char *name )
: QListView(parent,name)
{
  setFocusPolicy( NoFocus );
  setSorting(0);
  setRootIsDecorated(true);
  addColumn("");
  header()->hide();
  setFrameStyle( QFrame::Panel | QFrame::Sunken );

  connect( this, SIGNAL(clicked(QListViewItem*)), SLOT(itemSelect(QListViewItem *)) );
  connect( this, SIGNAL(doubleClicked(QListViewItem*)), SLOT(itemSelect(QListViewItem *)) );
  connect( this, SIGNAL(rightButtonPressed(QListViewItem*,const QPoint&,int)), SLOT(slotRightButtonPressed(QListViewItem*,const QPoint&,int)) );
}

WorkspaceListView::~WorkspaceListView()
{
}

void WorkspaceListView::itemSelect( QListViewItem* i )
{
  if ( i == 0L ) return;

  WorkspaceListViewItem* item = (WorkspaceListViewItem*)i;
  switch ( item->itemType ){
    case ITEM_WORKSPACE:
      emit selectWorkspace( item->workspaceName );
      break;
    default:
      emit selectFileName( item->text(0)  );
      emit selectFilePath( item->filePath );
  }
}

void WorkspaceListView::insertWorkspaceItem( Workspace* w, Workspace* parent )
{
  WorkspaceListViewItem* r;
  if ( parent == 0L )
    r = new WorkspaceListViewItem( this, w );
  else
    r = new WorkspaceListViewItem( data.find(parent->getName()), w );

  data.insert( w->getName(), r );
}

void WorkspaceListView::insertFileItem( Workspace* w, QString filename )
{
  WorkspaceListViewItem* r = 0L;
  WorkspaceListViewItem* p = 0L;

  p = data.find( w->getName() );
  r = new WorkspaceListViewItem( p, w, filename );

  data.insert( w->getName() + "::" + filename, r );
}

void WorkspaceListView::setOpen( Workspace* w, bool open )
{
  QListView::setOpen( data.find( w->getName() ), open );
}

QString WorkspaceListView::getCurrentWorkspaceName()
{
  WorkspaceListViewItem* item = (WorkspaceListViewItem*)currentItem();
  if ( item != 0L && item->itemType == ITEM_WORKSPACE ) return item->workspaceName;
  return QString::null;
}

QString WorkspaceListView::getCurrentWorkspaceNameForCurrentItem()
{
  WorkspaceListViewItem* item = (WorkspaceListViewItem*)currentItem();
  if ( item != 0L ) return item->workspaceName;
  return QString::null;
}

QString WorkspaceListView::getCurrentFilePath()
{
  WorkspaceListViewItem* item = (WorkspaceListViewItem*)currentItem();
  if ( item != 0L && item->itemType == ITEM_WORKSPACE_FILES  )
    return item->filePath;
  return QString::null;
}

QString WorkspaceListView::getCurrentFileName()
{
  WorkspaceListViewItem* item = (WorkspaceListViewItem*)currentItem();
  if ( item != 0L && item->itemType == ITEM_WORKSPACE_FILES )
    return item->text(0);
  return QString::null;
}

void WorkspaceListView::setCurrentWorkspaceName( QString name )
{
  if ( name.isEmpty() ){
    if ( firstChild() != 0L ){
      selectItem( (WorkspaceListViewItem*)firstChild() );
    }
    return;
  }

  WorkspaceListViewItem* item = data.find( name );
  if ( item == 0L ) item = (WorkspaceListViewItem*)firstChild();
  if ( item != 0L ){
    selectItem( item );
  }
}

void WorkspaceListView::setCurrentFileName( QString prjName, QString name )
{
  setCurrentWorkspaceName( prjName );

  if ( prjName.isEmpty() || name.isEmpty() ) return;

  WorkspaceListViewItem* item = data.find( prjName + "::" + name );
  if ( item != 0L ){
    selectItem( item );
  }
}
/*********************************************************************************************/

WorkspaceListViewItem::WorkspaceListViewItem( WorkspaceListView* parent, Workspace* item )
: QListViewItem( parent, item->getName() )
{
  setupWorkspace( item );
}

WorkspaceListViewItem::WorkspaceListViewItem( WorkspaceListViewItem* parent, Workspace* item )
: QListViewItem( parent, item->getName() )
{
  setupWorkspace( item );
}

void WorkspaceListViewItem::setupWorkspace( Workspace* item )
{
  workspaceName = item->getName();
  setPixmap( 0, getPixmapForWorkspaceType(item->getType()) );
  itemType = WorkspaceListView::ITEM_WORKSPACE;
}

WorkspaceListViewItem::WorkspaceListViewItem( WorkspaceListViewItem* parent, Workspace* item, QString name )
: QListViewItem( parent, name )
{
  workspaceName = item->getName();
  filePath = item->getDir() + name;
  itemType = WorkspaceListView::ITEM_WORKSPACE_FILES;
  setPixmap( 0, getPixmapForFileType( filePath ) );
}

WorkspaceListViewItem::~WorkspaceListViewItem()
{
}

int WorkspaceListViewItem::width(const QFontMetrics& fm, const QListView* lv, int ) const
{
  int w = -(fm.minLeftBearing()+fm.minRightBearing()) + fm.width(text(0)) + lv->itemMargin() * 2;
	w += (pixmap(0) == 0L) ? 0:pixmap(0)->width() + lv->itemMargin() + 3;
  return w;
}

void WorkspaceListViewItem::paintCell( QPainter * p, const QColorGroup & cg, int , int width, int align )
{
  if ( !p ) return;

  QListView *lv = listView();
  int r = lv->itemMargin();
  const QPixmap * icon = pixmap(0);

  p->fillRect( 0, 0, width, height(), cg.base() );

  int marg = lv->itemMargin();

  if ( isSelected() )
  {
    p->fillRect( r - marg, 0, width - r + marg, height(), QApplication::winStyleHighlightColor() );
    p->setPen( white );
  } else {
    p->setPen( cg.text() );
  }

  if ( icon != 0L ) {
    p->drawPixmap( r, (height()-icon->height())/2, *icon );
    r += icon->width() + listView()->itemMargin() + 3;
  }

  const char * t = text( 0 );
  if ( t && *t )
  {
    p->drawText( r, 0, width-marg-r, height(), align | AlignVCenter, t );
  }
}

void WorkspaceListView::slotRightButtonPressed( QListViewItem * i, const QPoint & pos, int )
{
  emit popupMenu( (WorkspaceListViewItem*)i, pos );
  /* fix selected on mouseMoveEvent - do MouseButtonRelease*/
  QMouseEvent br(QEvent::MouseButtonRelease,QPoint(0,0),RightButton,0);
  qApp->notify(viewport(),&br);
}

void WorkspaceListView::selectItem( WorkspaceListViewItem* item )
{
  QListViewItem* parent  = item->parent();
  while ( parent != 0L ){
    parent->setOpen( true );
    parent = parent->parent();
  }
  setSelected( item, true );
  setCurrentItem( item );
  ensureItemVisible( item );
}

void WorkspaceListView::saveData()
{
  sdata.clear();
  QDictIterator<WorkspaceListViewItem> it(data);
  while ( it.current() != 0L )
  {
    WorkspaceListViewItem* item = it.current();
    if ( item->isOpen() ) sdata.append( item->text(0) );
    ++it;
  }
  WorkspaceListViewItem* item = (WorkspaceListViewItem*)currentItem();
  if ( item == 0L ) sdataCurrentItem = "";
  else
    if ( item->itemType == ITEM_WORKSPACE )
      sdataCurrentItem = item->workspaceName;
    else
      sdataCurrentItem = item->workspaceName + "::" + item->text(0);
}

void WorkspaceListView::restoreData()
{
  for ( QString name = sdata.first(); name != 0L; name = sdata.next() )
  {
    WorkspaceListViewItem* item = data.find( name );
    if ( item != 0L ) item->setOpen( true );
  }
  if ( !sdataCurrentItem.isEmpty() ){
    WorkspaceListViewItem* item = data.find( sdataCurrentItem );
    if ( item != 0L ) selectItem( item );
  }
  sdata.clear();
  sdataCurrentItem = "";
}

bool WorkspaceListView::checkSelectedItemType( type t )
{
  WorkspaceListViewItem* item = (WorkspaceListViewItem*)currentItem();
  if ( item != 0L && item->itemType == t ) return true;
  return false;
}

QString WorkspaceListViewItem::key(int, bool) const
{
  WorkspaceListViewItem* i = (WorkspaceListViewItem*)this;
  return QString("%1%2").arg((int)i->itemType).arg(i->text(0));
}
