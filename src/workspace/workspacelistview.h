#ifndef WORKSPACELISTVIEW_H
#define WORKSPACELISTVIEW_H

#include <qwidget.h>
#include <qlistview.h>
#include <qstrlist.h>
#include <qdict.h>

class Workspace;
class WorkspaceListViewItem;

class WorkspaceListView : public QListView
{ Q_OBJECT
public: 
  enum type
  {
    ITEM_WORKSPACE,
    ITEM_WORKSPACE_FILES
  };

	WorkspaceListView( QWidget* parent=0, const char *name=0 );
	~WorkspaceListView();

  virtual void clear(){ data.clear(); QListView::clear(); }

  void insertWorkspaceItem( Workspace*, Workspace* parent = 0L );
  void insertFileItem( Workspace*, QString filename );
  void setOpen( Workspace*, bool open );

  void selectItem( WorkspaceListViewItem* );
  void setCurrentWorkspaceName( QString name );
  void setCurrentFileName( QString prjName, QString name );
  QString getCurrentWorkspaceName();
  QString getCurrentWorkspaceNameForCurrentItem();
  QString getCurrentFilePath();
  QString getCurrentFileName();

  bool checkSelectedItemType( type );

  void saveData();
  void restoreData();

private slots:
  void itemSelect( QListViewItem* i );
  void slotRightButtonPressed( QListViewItem *, const QPoint &, int );

signals:
  void selectWorkspace( const char* );
  void selectFileName ( const char* );
  void selectFilePath ( const char* );
  void popupMenu(WorkspaceListViewItem*, const QPoint &);

private:
  QDict<WorkspaceListViewItem> data;
  QStrList sdata;
  QString sdataCurrentItem;
};

class WorkspaceListViewItem : public QListViewItem
{
friend class WorkspaceListView;
public:
	WorkspaceListViewItem( WorkspaceListView* parent, Workspace* item );
	WorkspaceListViewItem( WorkspaceListViewItem* parent, Workspace* item );
	WorkspaceListViewItem( WorkspaceListViewItem* parent, Workspace* item, QString name );

	~WorkspaceListViewItem();

  virtual void paintCell( QPainter *, const QColorGroup & cg, int column, int width, int alignment );
  virtual int width ( const QFontMetrics &, const QListView *, int column ) const;

  void setupWorkspace( Workspace* );

  QString workspaceName;
  QString filePath;
  WorkspaceListView::type itemType;

  virtual QString key(int, bool) const;
};

#endif
