#include <studioview.h>
#include <pluginmanager.h>

#include <kapp.h>
#include <klocale.h>
#include <kcmdlineargs.h>
#include <kaboutdata.h>

static const char* description = "Integrated Development Enviropment";
static const char* version = "2.0";

int main(int argc, char* argv[])
{
  KAboutData aboutData("studio", I18N_NOOP("KDEStudio"),
                        version, description, KAboutData::License_GPL,
                        "(c) 1999-2000, theKompany.com",0,"www.thekompany.com");

  KCmdLineArgs::init( argc, argv, &aboutData );
  KApplication app;

  StudioView* studio = new StudioView;
  app.setMainWidget( studio );

  PluginManager* pmanager = new PluginManager();
  pmanager->initPlugins();
  studio->initApp();

  int i = app.exec();
  delete pmanager;

  return i;
}  
