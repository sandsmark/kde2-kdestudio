/****************************************************************************
** $Id: qwidgetstack.cpp,v 1.2 2000/06/08 22:49:51 msdina Exp $
**
** Implementation of QWidgetStack class
**
** Created : 980128
**
** Copyright (C) 1992-2000 Troll Tech AS.  All rights reserved.
**
** This file is part of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Troll Tech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** Licensees holding valid Qt Professional Edition licenses may use this
** file in accordance with the Qt Professional Edition License Agreement
** provided with the Qt Professional Edition.
**
** See http://www.troll.no/pricing.html or email sales@troll.no for
** information about the Professional Edition licensing, or see
** http://www.troll.no/qpl/ for QPL licensing information.
**
*****************************************************************************/

#include "qwidgetstack.h"

#include "qobjectlist.h"
#include "qobjectdict.h"


// NOT REVISED
/*! \class QWidgetStack qwidgetstack.h

  \brief The QWidgetStack class provides a stack of widgets, where the
  user can see only the top widget.

  \ingroup organizers

  The application programmer can move any widget to the top of the
  stack at any time using the slot raiseWidget(), and add or remove
  widgets using addWidget() and removeWidget().

  visibleWidget() is the \e get equivalent of raiseWidget(); it
  returns a pointer to the widget that is currently on the top of the
  stack.

  QWidgetStack also provides the ability to manipulate widgets through
  application-specfied integer IDs, and to translate from widget
  pointers to IDs using id() and from IDs to widget pointers using
  widget().  These numeric IDs have and unique (per QWidgetStack, not
  globally) and cannot be -1, but apart from that QWidgetStack does
  not attach any meaning to them.

  The default widget stack is frame-less and propagates its font and
  palette to all its children, but you can use the usual QFrame
  functions (like setFrameStyle()) to add a frame, and use
  setFontPropagation() and setPalettePropagation() to change the
  propagation style.

  Finally, QWidgetStack provides a signal, aboutToShow(), which is
  emitted just before a managed widget is shown.

  \sa QTabDialog QTabBar QFrame
*/


/*!  Constructs an empty widget stack. */

QWidgetStack::QWidgetStack( QWidget * parent, const char *name )
    : QFrame( parent, name )
{
    focusWidgets = new QPtrDict<QWidget>;
    dict = new QIntDict<QWidget>;
    topWidget = 0;
    setFontPropagation( AllChildren );
    setPalettePropagation( AllChildren );
}


/*! Destructs the object and frees any allocated resources. */

QWidgetStack::~QWidgetStack()
{
    delete focusWidgets;
    focusWidgets = 0;
    delete dict;
    dict = 0;
}


/*!  Adds \a w to this stack of widgets, with id \a id.

  If \a w is not a child of \c this, QWidgetStack moves it using
  reparent().
*/

void QWidgetStack::addWidget( QWidget * w, int id )
{
    if ( !w )
	return;

    dict->insert( id+1, w );

    w->hide();
    if ( w->parent() != this )
	w->reparent( this, 0, contentsRect().topLeft(), FALSE );
    w->setGeometry( contentsRect() );
}


/*!  Removes \a w from this stack of widgets.  Does not delete \a
  w. If \a w is the currently visible widget, no other widget is
  substituted. \sa visibleWidget() raiseWidget() */

void QWidgetStack::removeWidget( QWidget * w )
{
    if ( !w )
	return;
    int i = id( w );
    if ( i != -1 )
	dict->take( i+1 );
    if ( w == topWidget )
	topWidget = 0;
}


/*!  Raises \a id to the top of the widget stack. \sa visibleWidget() */

void QWidgetStack::raiseWidget( int id )
{
    if ( id == -1 )
	return;
    QWidget * w = dict->find( id+1 );
    if ( w )
	raiseWidget( w );
}


/*!  Raises \a w to the top of the widget stack. */

void QWidgetStack::raiseWidget( QWidget * w )
{
  if ( !w )
    return;

  if ( topWidget ){
    focusWidgets->remove( topWidget );
    QWidget* f = topWidget->focusWidget();
    if ( f ){
      focusWidgets->insert( topWidget, f );
    }
  }

  topWidget = w;

  if ( !isVisible() )
    return;

  emit aboutToShow( w );
  if ( receivers( SIGNAL(aboutToShow(int)) ) ) {
    int i = id( w );
    if ( i >= 0 )
    emit aboutToShow( i );
  }

  w->setGeometry( contentsRect() );
  w->raise();
  w->show();

  const QObjectList * c = children();
  QObjectListIt it( *c );
  QObject * o;

  while( (o=it.current()) != 0 ) {
    ++it;
    if ( o->isWidgetType() && o != w )
      ((QWidget *)o)->hide();
  }

  QObjectList* ol = w->queryList();
  bool focusfound = false;
  QWidget* f = focusWidgets->find(w);
  if ( f ){
    if ( ol->find(f) != -1 ){
      f->setFocus();
      focusfound = true;
    }
  }
  if (!focusfound) {
    for ( QObject* o = ol->first(); o; o = ol->next() ){
      if ( o->isWidgetType() ){
        f = (QWidget*)o;
        if ( f->focusPolicy() == QWidget::StrongFocus &&
             !f->focusProxy() &&
             f->isVisible() &&
             f->isEnabled()
           )
        {
          f->setFocus();
          break;
        }
      }
    }
  }
  delete ol;
}


/*! \reimp */

void QWidgetStack::frameChanged()
{
    QFrame::frameChanged();
    setChildGeometries();
}


/*! \reimp */

void QWidgetStack::setFrameRect( const QRect & r )
{
    QFrame::setFrameRect( r );
    setChildGeometries();
}


/*!  Fix up the children's geometries. */

void QWidgetStack::setChildGeometries()
{
    if ( topWidget )
	topWidget->setGeometry( contentsRect() );
}


/*! \reimp */
void QWidgetStack::show()
{
    //  Reimplemented in order to set the children's geometries
    //  appropriately.
    if ( !isVisible() && children() ) {
	setChildGeometries();

	const QObjectList * c = children();
	QObjectListIt it( *c );
	QObject * o;

	while( (o=it.current()) != 0 ) {
	    ++it;
	    if ( o->isWidgetType() )
		if ( o == topWidget )
		    ((QWidget *)o)->show();
		else
		    ((QWidget *)o)->hide();
	}
    }

    QFrame::show();
}


/*!  Returns a pointer to the widget with ID \a id.  If this widget
  stack does not manage a widget with ID \a id, this function returns
  0.

  \sa id() addWidget()
*/

QWidget * QWidgetStack::widget( int id ) const
{
    return id != -1 ? dict->find( id+1 ) : 0;
}


/*!  Returns the ID of the \a widget.  If \a widget is 0 or is not
  being managed by this widget stack, this function returns -1.

  \sa widget() addWidget()
*/

int QWidgetStack::id( QWidget * widget ) const
{
    if ( !widget || !dict )
	return -1;

    QIntDictIterator<QWidget> it( *dict );
    while ( it.current() && it.current() != widget )
	++it;
    return it.current() == widget ? it.currentKey()-1 : -1;
}


/*! Returns a pointer to the currently visible widget (the one on the
  top of the stack), or 0 if nothing is currently being shown.

  \sa aboutToShow() id() raiseWidget()
*/

QWidget * QWidgetStack::visibleWidget() const
{
    return topWidget;
}


/*! \fn void QWidgetStack::aboutToShow( int )

  This signal is emitted just before a managed widget is shown, if
  that managed widget has a non-zero ID.  The argument is the numeric
  ID of the widget.
*/


/*! \fn void QWidgetStack::aboutToShow( QWidget * )

  This signal is emitted just before a managed widget is shown.  The
  argument is a pointer to the widget.
*/


/*! \reimp */

void QWidgetStack::resizeEvent( QResizeEvent * e )
{
    QFrame::resizeEvent( e );
    setChildGeometries();
}


/*! \reimp */

QSize QWidgetStack::sizeHint() const
{
    constPolish();

    QSize size(0,0);
    if ( children() ) {
	const QObjectList * c = children();
	QObjectListIt it( *c );
	QObject * o;

	while( (o=it.current()) != 0 ) {
	    ++it;
	    if ( o->isWidgetType() ) {
		QWidget *w = (QWidget*)o;
		size = size.expandedTo( w->sizeHint() )
		       .expandedTo(w->minimumSize());
	    }
	}
    }
    if ( size.isNull() )
	return QSize(100,50); //### is this a sensible default???
    return QSize( size.width() + 2*frameWidth(), size.height() + 2*frameWidth() );
}


/*! \reimp */
QSize QWidgetStack::minimumSizeHint() const
{
    constPolish();

    QSize size(0,0);
    if ( children() ) {
	const QObjectList * c = children();
	QObjectListIt it( *c );
	QObject * o;

	while( (o=it.current()) != 0 ) {
	    ++it;
	    if ( o->isWidgetType() ) {
		    QWidget *w = (QWidget*)o;
		    size = size.expandedTo( w->minimumSizeHint())
					    .expandedTo(w->minimumSize());
	    }
	}
    }
    return QSize( size.width() + 2*frameWidth(), size.height() + 2*frameWidth() );
}

/*! \reimp  */
void QWidgetStack::childEvent( QChildEvent * e)
{
    if ( e->child()->isWidgetType() && e->removed() )
	removeWidget( (QWidget*) e->child() );
}
