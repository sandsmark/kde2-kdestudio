#include "history_action.h"
#include "tools.h"

#include <qfileinfo.h>
#include <qpopupmenu.h>
#include <kstringhandler.h>
#include <ktoolbar.h>

HistoryAction::HistoryAction( const QString& text, const QString& icon, int accel, QObject* parent, const char* name )
: KAction( text, icon, accel, parent, name )
{
  m_popup = 0;
}

HistoryAction::~HistoryAction()
{
  if ( m_popup )
    delete m_popup;
}

int HistoryAction::plug( QWidget *widget, int index )
{
  KToolBar *bar = (KToolBar *)widget;

  int id_ = KAction::getToolButtonID();
  bar->insertButton( icon(), id_, SIGNAL( clicked() ), this,
                     SLOT( slotActivated() ), isEnabled(), plainText(),
                     index );

  addContainer( bar, id_ );

  connect( bar, SIGNAL( destroyed() ), this, SLOT( slotDestroyed() ) );

  bar->setDelayedPopup( id_, popupMenu(), true );

  return containerCount() - 1;
}

void HistoryAction::unplug( QWidget *widget )
{
  KToolBar *bar = (KToolBar *)widget;

  int idx = findContainer( bar );

  if ( idx != -1 )
  {
    bar->removeItem( menuId( idx ) );
    removeContainer( idx );
  }
}

void HistoryAction::fillHistoryPopup( const HistoryEntryList& history,
                                          QPopupMenu * popup,
                                          bool onlyBack,
                                          bool onlyForward,
                                          uint startPos )
{
  QListIterator<HistoryEntry> it( history );
  if (onlyBack || onlyForward) {
    it += history.at(); // Jump to current item
    if ( !onlyForward )
      --it;
    else
      ++it; // And move off it
  } else
    if ( startPos )
      it += startPos; // Jump to specified start pos

  uint i = 0;
  while ( it.current() ) {
    QString text = QString("%1::%2").arg(QFileInfo(it.current()->filePath).fileName()).arg(it.current()->line+1);
    text = KStringHandler::csqueeze(text, 50);
    popup->insertItem( getPixmapForFileType(it.current()->filePath), text );
    if ( ++i > 10 )
        break;
    if ( !onlyForward )
      --it;
    else
      ++it;
  }
}

void HistoryAction::setEnabled( bool b )
{
  int len = containerCount();
  for ( int i = 0; i < len; i++ )
  {
    QWidget *w = container( i );
    if ( w->inherits( "KToolBar" ) )
      ((KToolBar *)w)->setItemEnabled( menuId( i ), b );
  }
  KAction::setEnabled( b );
}

void HistoryAction::setIconSet( const QIconSet& iconSet )
{
  int len = containerCount();
  for ( int i = 0; i < len; i++ )
  {
    QWidget *w = container( i );

    if ( w->inherits( "KToolBar" ) )
      ((KToolBar *)w)->setButtonPixmap( menuId( i ), iconSet.pixmap() );

  }
  KAction::setIconSet( iconSet );
}

QPopupMenu *HistoryAction::popupMenu()
{
  if ( m_popup )
    return m_popup;

  m_popup = new QPopupMenu();
  return m_popup;
}
