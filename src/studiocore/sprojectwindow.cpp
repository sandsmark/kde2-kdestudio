#include "sprojectwindow.h"
#include "saction.h"
#include "studio.h"
#include "seditwindow.h"
#include "studioview.h"
#include "globalnew.h"
#include "dlgaddexistingfile.h"
#include "dlgcopymoveitem.h"
#include "workspace.h"
#include "workspacelistview.h"

#include <ktoolbar.h>
#include <kpopupmenu.h>

#include <qdir.h>
#include <qstring.h>
#include <qlayout.h>
#include <qmessagebox.h>

SProjectWindow::SProjectWindow() : QObject( 0L, "ProjectWindow" )
{
  localCall = false;
  cutName = "";
}

SProjectWindow::~SProjectWindow()
{
}

void SProjectWindow::popUpProject( WorkspaceListViewItem* item , const QPoint & pos )
{
  if ( item == 0L ) return;

  PRJ_TREE->blockSignals( true );
  PRJ_TREE->selectItem( item );
  PRJ_TREE->blockSignals( false );

	KPopupMenu menu;
  localCall = true;
  connect( &menu, SIGNAL(destroyed()), SLOT(slotPopupDestroy()) );

  if ( item->itemType == WorkspaceListView::ITEM_WORKSPACE ){
    QString subPr = PRJ_TREE->getCurrentWorkspaceName();
    Workspace* w = WORKSPACE->getWorkspaceFromName( subPr );
    switch ( w->getType() ){
      case Workspace::DATA:
        menu.setTitle( subPr.prepend("Data: ") );
        break;
      case Workspace::GROUP:
        menu.setTitle( subPr.prepend("Group: ") );
        break;
      case Workspace::EXECUTABLE:
        menu.setTitle( subPr.prepend("Programm: ") );
        break;
      case Workspace::SHARED_LIB:{
        if ( w->getProjectSimpleOptionsBool(WS_LIB_INSTALL) ){
          menu.setTitle( subPr.prepend("Shared Library: ") );
        } else {
          menu.setTitle( subPr.prepend("Convenience Library: ") );
        }
        break; }
      case Workspace::STATIC_LIB:
        menu.setTitle( subPr.prepend("Static Library: ") );
        break;
      case Workspace::MAIN:
        menu.setTitle( subPr.prepend("Main Workspace: ") );
        break;
      default: break;
    }
    ACTION->setMenu( &menu, "newproject | addfile addnewfile |");
    ACTION->setMenu( &menu, "compile make_install | perl_automoc | make_uninstall make_clean |");
    switch ( w->getType() ){
      case Workspace::EXECUTABLE:
        ACTION->setMenu( &menu, "run |");
        break;
      case Workspace::MAIN:
        ACTION->setMenu( &menu, "make_distrib reconfigure make_distclean | Update_makefilesAm |");
        break;
      default: break;
    }
    if ( w->getType() != Workspace::MAIN )
      ACTION->setMenu( &menu, "moveitem renameitem | removeprj |");

    ACTION->setMenu( &menu, "showmakefile projectopt");
    WORKSPACE->freeSubWorkspace( w );
  }

  if ( item->itemType == WorkspaceListView::ITEM_WORKSPACE_FILES ){
    QString fname = PRJ_TREE->getCurrentFileName();
    menu.setTitle( fname.prepend("File: ") );
    ACTION->setMenu( &menu, "copyitem moveitem renameitem | deletefile removefile");
  }

  ACTION->changeAllMenuAccel( MAIN_ACCEL );
  menu.exec( pos );
}

void SProjectWindow::deleteFile()
{
  if ( !PRJ_TREE->checkSelectedItemType( WorkspaceListView::ITEM_WORKSPACE_FILES) ) return;

  QString filePath = PRJ_TREE->getCurrentFilePath();
  if ( filePath.isEmpty() ) return;

  if  ( QMessageBox::warning(0,"Deleting file", QString("File: ") + filePath + "\n\n"\
        "Do you really want to delete this file?\n"
        "   There is no way to restore it!",\
        "Ok", "Cancel") != 0 ) return;

  EDIT_WINDOW->extremalCloseFile( filePath );

	QDir().remove( filePath );

  Workspace* w = WORKSPACE->getWorkspaceFromName( PRJ_TREE->getCurrentWorkspaceNameForCurrentItem() );
	if ( w!= 0L ){
		w->removeFile( PRJ_TREE->getCurrentFileName() );
	  WORKSPACE->freeSubWorkspace( w );
	}
}

void SProjectWindow::removeFile()
{
  if ( !PRJ_TREE->checkSelectedItemType( WorkspaceListView::ITEM_WORKSPACE_FILES) ) return;

  QString filePath = PRJ_TREE->getCurrentFilePath();
  if ( filePath.isEmpty() ) return;

  if  ( QMessageBox::warning(0,"Remove file", QString("File: ") + filePath + "\n\n"\
        "Do you really want to remove this file from project?\n"
        "              It will remain on disk.",\
        "Ok", "Cancel") != 0 ) return;

  EDIT_WINDOW->extremalCloseFile( filePath );

  Workspace* w = WORKSPACE->getWorkspaceFromName( PRJ_TREE->getCurrentWorkspaceNameForCurrentItem() );
	if ( w!= 0L ){
		w->removeFile( PRJ_TREE->getCurrentFileName() );
	  WORKSPACE->freeSubWorkspace( w );
	}
}

void SProjectWindow::addExistingFile()
{
  DlgAddExistingFile* dlg = new DlgAddExistingFile( 0L , PRJ_TREE->getCurrentWorkspaceName() );
  dlg->exec();
  delete dlg;
}

void SProjectWindow::newFile()
{
  GlobalNew *global = new GlobalNew( 0L, PRJ_TREE->getCurrentWorkspaceName(), 2 );
  global->exec();
  delete global;	
}

void SProjectWindow::newProject()
{
  GlobalNew *global = new GlobalNew( 0L, PRJ_TREE->getCurrentWorkspaceName(), 1 );
  global->exec();
  delete global;	
}

void SProjectWindow::removeSubProject()
{
  Workspace* w = WORKSPACE->getWorkspaceFromName( PRJ_TREE->getCurrentWorkspaceName() );
  if ( w != 0L ){

    if  ( QMessageBox::warning(0,"Remove Project", QString("Project: ") + PRJ_TREE->getCurrentWorkspaceName() + "\n\n"\
          "Do you really want to remove this project?\n"
          "      There is no way to restore it!",\
          "Ok", "Cancel") != 0 ) return;

    w->remove();
    WORKSPACE->freeSubWorkspace( w );
    STATUS("Do reconfigure now for updating Makefile");
  }
}

void SProjectWindow::slotCloseFiles( Workspace* w )
{
  QStrList list;
  w->getFiles( list );
  for ( uint k = 0; k < list.count(); k++ )
    EDIT_WINDOW->extremalCloseFile( w->getDir() + list.at(k) );
}

void SProjectWindow::projectOptions()
{
  WORKSPACE->optionsDlg( getSelectedProjectName() );
  updateWorkspaceTreeFile();
}

void SProjectWindow::updateWorkspaceTreeFile()
{
  WORKSPACE->getTree( PRJ_TREE, true );
}

QString SProjectWindow::getSelectedProjectName()
{
  QString wname = PRJ_TREE->getCurrentWorkspaceName();
  if ( wname.isEmpty() || !localCall )
    wname = WORKSPACE->getName();
  return wname;
}

void SProjectWindow::slotCompile()
{
  STUDIO_VIEW->runMake( getSelectedProjectName(), "" );
}

void SProjectWindow::slotRun()
{
  slotCompile();
  STUDIO_VIEW->next_job = "run";
}

void SProjectWindow::slotPopupDestroy()
{
  localCall = false;  
}

void SProjectWindow::slotMakeInstall()
{
  STUDIO_VIEW->runMake( getSelectedProjectName(), "install" );
}

void SProjectWindow::slotMakeUnInstall()
{
  STUDIO_VIEW->runMake( getSelectedProjectName(), "uninstall" );
}

void SProjectWindow::slotMakeDistrib()
{
  STUDIO_VIEW->runMake( WORKSPACE->getName(), "dist" );
}

void SProjectWindow::slotMakeClean()
{
  STUDIO_VIEW->runMake( getSelectedProjectName(), "clean" );
}

void SProjectWindow::slotMakeDistClean()
{
  STUDIO_VIEW->runMake( getSelectedProjectName(), "distclean" );
}

void SProjectWindow::slotPerlAutomoc()
{
  Workspace* w = WORKSPACE->getWorkspaceFromName( getSelectedProjectName() );
  if ( w == WORKSPACE )
    STUDIO_VIEW->runPerl( "./admin/am_edit", "-v" );
  else
    STUDIO_VIEW->runPerl( "./admin/am_edit -v", w->getDir() + "Makefile.in" );
  WORKSPACE->freeSubWorkspace( w );
}

void SProjectWindow::slotMoveItem()
{
  Workspace* w = WORKSPACE->getWorkspaceFromName( PRJ_TREE->getCurrentWorkspaceNameForCurrentItem() );
  if (!w)
    return;

  DlgCopyMoveItem* dlg = new DlgCopyMoveItem( 0L, PRJ_TREE->getCurrentFilePath(), w, DlgCopyMoveItem::Move );
  dlg->exec();
  delete dlg;

  WORKSPACE->freeSubWorkspace( w );
}

void SProjectWindow::slotCopyItem()
{
  Workspace* w = WORKSPACE->getWorkspaceFromName( PRJ_TREE->getCurrentWorkspaceNameForCurrentItem() );
  if (!w)
    return;

  DlgCopyMoveItem* dlg = new DlgCopyMoveItem( 0L, PRJ_TREE->getCurrentFilePath(), w, DlgCopyMoveItem::Copy );
  dlg->exec();
  delete dlg;

  WORKSPACE->freeSubWorkspace( w );
}

void SProjectWindow::slotRenameItem()
{
  Workspace* w = WORKSPACE->getWorkspaceFromName( PRJ_TREE->getCurrentWorkspaceNameForCurrentItem() );
  if (!w)
    return;

  DlgCopyMoveItem* dlg = new DlgCopyMoveItem( 0L, PRJ_TREE->getCurrentFilePath(), w, DlgCopyMoveItem::Rename );
  dlg->exec();
  delete dlg;

  WORKSPACE->freeSubWorkspace( w );
}

void SProjectWindow::slotUpdateAllMakefileAm()
{
  WORKSPACE->updateMakefilesAm();
}

void SProjectWindow::slotShowMakefile()
{
  Workspace* w = 0L;
  w = WORKSPACE->getWorkspaceFromName( getSelectedProjectName() );
  EDIT_WINDOW->selectLine(w->getDir()+"Makefile.am",0);
  WORKSPACE->freeSubWorkspace( w );
}
