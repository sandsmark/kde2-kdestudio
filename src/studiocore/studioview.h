#ifndef STUDIOVIEW_H 
#define STUDIOVIEW_H 

#include <kapp.h>
#include <kprocess.h>
#include <kiconloader.h>

#include <qwidget.h>
#include <qmultilinedit.h>
#include <qstring.h>
#include <qstrlist.h>

#include <ksimpleconfig.h>

#include <qfileinfo.h>
#include <qfile.h>

#include "studio.h"

class GrepDialog;
class CErrorMessageParser;
class QListBoxItem;

class StudioView : public StudioApp
{ Q_OBJECT
friend class SProjectWindow;
public:
  StudioView();
  ~StudioView();

  void readWorkspace( QString name );
  void init();

  void runMake( QString wname = 0, QString arg = 0 );
  void runPerl( QString arg1, QString arg2 = 0 );

protected:
  CErrorMessageParser* error_parse;

public slots:
  void slotNewPrj();
  void slotOpenProject();
  void slotOpenFile();
  void slotGrepDlg();
  void reConfigure();

  void gotoNextError();
  void gotoPrevError();

private:
  void runProject();
  void runProgram( QString programPath );

  void addOutput( QString buffer, QListBox* listBox, QColor color );
  bool checkMakefileExist( QString wname, QString arg );

  int reconfig;
  QString reconfigWname;
  QString reconfigArg;

  QListBox *outf;
  QListBox *outr;
  QListBox *oute;
  QListBox *outc;
  QListBox* lastVisible;
  QString buffer_find;
  QString outBuffer;

  KShellProcess process;
  KShellProcess appl_process;
  KShellProcess find_process;

  QString make_cmd;
  QString next_job;
  QString make_start_dir;
  QString workspaceNameForRun;

  QColor old_color;
  bool ready;

  KDockWidget* d1;
  KDockWidget* d2;
  KDockWidget* d3;
  KDockWidget* d4;
  KDockWidget* d8;
	GrepDialog* grepdlg;

protected slots:
  void slotProcessExited ( KProcess* );
  void slotReceivedStdout( KProcess*, char*, int );
  void slotReceivedStderr( KProcess*, char*, int );

  void slotFindOut( KProcess*,char*,int );
  void slotFind( QListBoxItem* );
  void slotFindEnd( KProcess* );

  void slotRunExited( KProcess* );
  void slotRunStdout( KProcess*, char*, int );
  void slotRunStderr( KProcess*, char*, int );

  void slotGotoError( QListBoxItem* );
  void slotStop();
};

#endif
