#include "studioview.h"
#include "seditwindow.h"
#include "sprojectwindow.h"
#include "globalnew.h"
#include "seditwindow.h"
#include "sprojectwindow.h"
#include "serroritem.h"
#include "grepdialog.h"

#include "workspace.h"
#include "cerrormessageparser.h"
#include "workspacelistview.h"
#include "tools.h"
#include "openfile.h"

#include <qdir.h>
#include <qregexp.h>
#include <qfileinfo.h>
#include <kconfigbase.h>
#include <ktmainwindow.h>
#include <kiconloader.h>

#include <kwmanager.h>

StudioView::StudioView()
: StudioApp()
{
  KDockWidget* main = createDockWidget( "Editor", Ic("output.xpm"), this );
  setView( main );

  QPalette pal = palette();
  pal.setColor(QColorGroup::HighlightedText, pal.color(QPalette::Normal, QColorGroup::Text));
  pal.setColor(QColorGroup::Highlight,       pal.color(QPalette::Normal, QColorGroup::Mid));

  d8 = createDockWidget("Project Manager", BarIcon("project_manager"));
  PRJ_TREE = new WorkspaceListView( d8, "ProjectTree" );
  d8->setWidget( PRJ_TREE );

  d1 = createDockWidget("Find", BarIcon("find"));
  outf = new QListBox( d1, "Find");
  d1->setWidget( outf );
  outf->setPalette(pal);
  outf->setFocusPolicy( NoFocus );

  d2 = createDockWidget("Run", Ic("output.xpm"));
  outr = new QListBox( d2, "Run");
  d2->setWidget( outr );
  outr->setPalette(pal);
  outr->setFocusPolicy( NoFocus );

  d3 = createDockWidget("Run Error", Ic("output.xpm"));
  oute = new QListBox( d3, "Run Error");
  d3->setWidget( oute );
  oute->setPalette(pal);

  d4 = createDockWidget("Compile", BarIcon("make"));
  outc = new QListBox( d4, "Compile");
  d4->setWidget( outc );
  outc->setPalette(pal);
  outc->setFocusPolicy( NoFocus );

  lastVisible = 0L;
  reconfig = 0;

  grepdlg = new GrepDialog( topLevelWidget() );

  STUDIO_VIEW = this;
  initInternal();
  init();
}

StudioView::~StudioView()
{
  delete grepdlg;
}

void StudioView::init()
{
  debug("void StudioView::init()");
  KDockWidget* main = (KDockWidget*)QMainWindow::centralWidget();
  setMainDockWidget( main );
  main->setWidget( EDIT_WINDOW );

  d1->manualDock( getMainDockWidget(), KDockWidget::DockBottom );

  d2->manualDock( d1, KDockWidget::DockCenter );
  d3->manualDock( d1, KDockWidget::DockCenter );
  d4->manualDock( d1, KDockWidget::DockCenter );
  d8->manualDock( getMainDockWidget(), KDockWidget::DockLeft );

  connect( outc, SIGNAL(clicked(QListBoxItem*)), this, SLOT(slotGotoError(QListBoxItem*)) );
  connect( outf, SIGNAL(clicked(QListBoxItem*)), this, SLOT(slotFind(QListBoxItem*)));
/*********************************************************************/
  make_cmd = "make";
  ready = true;

  connect(&process,SIGNAL(receivedStdout(KProcess*,char*,int)),
          this,SLOT(slotReceivedStdout(KProcess*,char*,int)) );

  connect(&process,SIGNAL(receivedStderr(KProcess*,char*,int)),
          this,SLOT(slotReceivedStderr(KProcess*,char*,int)) );

  connect(&process,SIGNAL(processExited(KProcess*)),
          this,SLOT(slotProcessExited(KProcess*) )) ;

  connect(&appl_process,SIGNAL(receivedStdout(KProcess*,char*,int)),
          this,SLOT(slotRunStdout(KProcess*,char*,int)) );

  connect(&appl_process,SIGNAL(receivedStderr(KProcess*,char*,int)),
	        this,SLOT(slotRunStderr(KProcess*,char*,int)) );

  connect(&appl_process,SIGNAL(processExited(KProcess*)),
	        this,SLOT(slotRunExited(KProcess*) )) ;

  connect(&find_process,SIGNAL(receivedStdout(KProcess*,char*,int)),
          this,SLOT(slotFindOut(KProcess*,char*,int)) );

  connect(&find_process,SIGNAL(processExited(KProcess*)),
          this,SLOT(slotFindEnd(KProcess*)) );

  error_parse = new CErrorMessageParser();
}

void StudioView::slotOpenFile()
{
  QString str;
  str = SFileDialog::getOpenFileName( QString::null, "*", "Open File", "openAnyFile" );
  if (str.isEmpty()) return;

  EDIT_WINDOW->selectLine(str,1);
}

void StudioView::slotOpenProject()
{
  QString str;
  str = SFileDialog::getOpenFileName( QString::null, "*.studio", "Open Workspace", "openWorkspace" );
  if (str.isEmpty()) return;

  readWorkspace(str);
}

void StudioView::reConfigure()
{
  STATUS("Reconfigure ...");
  QDir::setCurrent(WORKSPACE->getDir());
  process.clearArguments();
  process << make_cmd << "-f" << "Makefile.cvs";
  next_job = "reconfigure";
  outc->clear();
  STUDIO->processRunUpdateUI();
  process.start(KProcess::NotifyOnExit,KProcess::AllOutput);
}

void StudioView::slotNewPrj()
{
	GlobalNew *global = new GlobalNew( this );
	global->exec();
  delete global;
}

bool StudioView::checkMakefileExist( QString wname, QString arg )
{
  // if make -f ....
  if ( arg.find("-f") != -1 ) return true;
  if ( QFileInfo(WORKSPACE->getDir()+ "Makefile").exists() ) return true;
  // no Makefile
  reconfigWname = wname;
  reconfigArg = arg;
  // create Makefile
  runMake( WORKSPACE->getName(), "-f Makefile.cvs" );
  return false;
}

void StudioView::runMake( QString wname, QString arg )
{
  if ( !checkMakefileExist( wname, arg ) ) return;

  if ( arg == "-f Makefile.cvs" ){
    reconfig = 1;
  }

  workspaceNameForRun = wname;

  Workspace* w = WORKSPACE->getWorkspaceFromName( wname );
  if ( w == 0L ){
    make_start_dir = WORKSPACE->getDir();
  } else {
    make_start_dir = w->getDir();
    WORKSPACE->freeSubWorkspace( w );
  }

	EDIT_WINDOW->slotSaveAll();
  STATUS(QString("Running make ") + arg + " in " + make_start_dir + "  ...");

  QDir::setCurrent( make_start_dir );

  process.clearArguments();
  process << make_cmd << arg;

  outc->blockSignals( true );
  outc->clear();
  outr->clear();
  oute->clear();
  error_parse->reset();
  EDIT_WINDOW->clearStepLine();

  STUDIO->processRunUpdateUI();
  process.start(KProcess::NotifyOnExit,KProcess::AllOutput);
}

void StudioView::slotReceivedStdout(KProcess*,char* buffer,int buflen)
{
  QString str = QString().fromLatin1( buffer,buflen );
  kapp->config()->setGroup("ColorData");
  QColor c = kapp->config()->readColorEntry( "StdOut", &black );
  addOutput( str, outc, c );
}

void StudioView::slotReceivedStderr(KProcess*,char* buffer,int buflen)
{
  QString str = QString().fromLatin1( buffer,buflen );
  kapp->config()->setGroup("ColorData");
  QColor c = kapp->config()->readColorEntry( "StdError", &darkGreen );
  addOutput( str, outc, c );
}

void StudioView::runProject()
{
    runProgram( WORKSPACE->getDefaultRunPath() );
}

void StudioView::runProgram( QString programPath )
{
  STATUS("Running program ...");
  appl_process.clearArguments();
  appl_process << programPath;

  STUDIO->processRunUpdateUI();
  appl_process.start(KProcess::NotifyOnExit,KProcess::All);
}

void StudioView::slotProcessExited(KProcess* proc)
{
  lastVisible = 0L;
  outBuffer = "";
  outc->blockSignals( false );
  STUDIO->processEndUpdateUI();

  STATUS("Ready");
  outc->insertItem("");

  /* Fill error_parser */
  QString makeoutput;
  for ( uint k = 0; k < outc->count()-1; k++ ){
    makeoutput += outc->text(k);
  }
  error_parse->toogleOn();
  error_parse->parseInMakeMode( &makeoutput, make_start_dir );
  /********************************************************/

  if (process.normalExit() && process.exitStatus() == 0) {
		outc->insertItem("===success===");
    outc->setBottomItem(outc->count()-1);

    outr->clear();
    oute->clear();
    QDir::setCurrent(WORKSPACE->getDir()); // only for core file

    if ( reconfig  == 1 ){ // after make Makefile.cvs
      reconfig = 2;
      QString ca = WORKSPACE->getProjectSimpleOptions(WS_CONFIGUREARG);
      STATUS(QString("Running ./configure ")+ca+" ...");
      QDir::setCurrent( WORKSPACE->getDir() );
      QDir().remove(WORKSPACE->getDir()+"config.cache");
      process.clearArguments();
      process << "./configure" << ca;
      STUDIO->processRunUpdateUI();
      process.start(KProcess::NotifyOnExit,KProcess::AllOutput);
      return;
    }

    if ( reconfig  == 2 ){ // after configure
      reconfig = 0;
      WORKSPACE->updateMakefilesAm();
      runMake( reconfigWname, reconfigArg );
      return;
    }

    if (next_job == "run"){
      next_job = "";
      runProject();
      return;
    }

    if (next_job == "reconfigure"){
      next_job = "";
      QString ca = WORKSPACE->getProjectSimpleOptions(WS_CONFIGUREARG);
      STATUS(QString("Running ./configure ")+ca+" ...");
      QDir::setCurrent( WORKSPACE->getDir() );
      QDir().remove(WORKSPACE->getDir()+"config.cache");
      process.clearArguments();
      process << "./configure" << ca;
      STUDIO->processRunUpdateUI();
      process.start(KProcess::NotifyOnExit,KProcess::AllOutput);
      next_job = "updateMakefileAm";
      return;
    }
    if (next_job == "updateMakefileAm"){
      next_job = "";
      WORKSPACE->updateMakefilesAm();
      return;
    }
  } else {
    next_job = "";
    reconfig = false;
		outc->insertItem("===exited with error(s)===");
    outc->setBottomItem(outc->count()-1);

    /* Goto first error line in editor */
    gotoNextError();
    /******************************************************************/
  }
}

void StudioView::slotRunStdout(KProcess*,char* buffer,int buflen)
{
  QString str = QString().fromLatin1( buffer,buflen );
  kapp->config()->setGroup("ColorData");
  QColor c = kapp->config()->readColorEntry( "RunStdOut", &black );
  addOutput( str, outr, c );
}

void StudioView::slotRunStderr(KProcess*,char* buffer,int buflen)
{
  QString str = QString().fromLatin1( buffer,buflen );
  kapp->config()->setGroup("ColorData");
  QColor c = kapp->config()->readColorEntry( "RunStdError", &darkGreen );
  addOutput( str, oute, c );
}

void StudioView::slotRunExited(KProcess*)
{
  outBuffer = "";
  lastVisible = 0L;
  STUDIO->processEndUpdateUI();
  STATUS("Ready");
  outr->insertItem("");
  oute->insertItem("");
  if ( appl_process.normalExit() && appl_process.exitStatus() == 0){
    outr->insertItem("===success===");
    oute->insertItem("===success===");
  } else {
    outr->insertItem("===exited with error(s)===");
    oute->insertItem("===exited with error(s)===");
  }
  outr->setBottomItem(outr->count()-1);
  oute->setBottomItem(oute->count()-1);
}

void StudioView::slotGrepDlg()
{
  if ( grepdlg->directoryString().isEmpty() )
  	grepdlg->setDirectory( WORKSPACE->getDir().isNull() ? QDir::homeDirPath():WORKSPACE->getDir());

	grepdlg->exec();
  if ( grepdlg->result() != QDialog::Accepted ) return;

	buffer_find="";
	outf->clear();
  find_process.clearArguments();
  makeWidgetDockVisible( outf );

  QString files;
  QStringList filelist = QStringList::split(",", grepdlg->filesString());
  if (!filelist.isEmpty())
    {
      QStringList::Iterator it(filelist.begin());
      files = "'" + (*it) + "'";
      ++it;
      for (; it != filelist.end(); ++it)
          files += " -o -name '" + (*it) + "'";
    }

  QString pattern = grepdlg->templateString();
  pattern.replace(QRegExp("%s"), grepdlg->patternString());
  pattern.replace(QRegExp("'"), "'\\''");

  QString filepattern = "`find '";
  filepattern += grepdlg->directoryString();
  filepattern += "'";
  if (!grepdlg->recursiveFlag())
      filepattern += " -maxdepth 1";
  filepattern += " -name ";
  filepattern += files;
  filepattern += "`";

  find_process << "grep";
  find_process << "-n";
  find_process << (QString("-e '") + pattern + "'");
  find_process << filepattern;
  find_process << "/dev/null";

  find_process.start(KProcess::NotifyOnExit, KProcess::Stdout);
  STUDIO->processRunUpdateUI();
}

void StudioView::slotFindOut(KProcess*,char* buffer,int buflen)
{
  QString str = QString().fromLatin1( buffer,buflen );
	buffer_find.append(str);
	int k = buffer_find.find("\n");
	while ( k != -1 )
	{
    QString part = buffer_find.left(k);
		buffer_find.remove(0, k+1);
		outf->insertItem(new SFindItem(part), -1);
		outf->setBottomItem(outf->count()-1);
		k = buffer_find.find("\n");
	}
}

void StudioView::slotFind( QListBoxItem* item )
{
  if ( !item ) return;

	int pos;
	QString filename, linenumber;
  QString str = item->text();

  if ( (pos = str.find(':')) != -1)
	{
		filename = str.left(pos);
		str = str.right(str.length()-1-pos);
		if ( (pos = str.find(':')) != -1)
		{
			linenumber = str.left(pos);
			EDIT_WINDOW->selectLine(filename, linenumber.toInt());
		}
	}
}

void StudioView::addOutput( QString buffer, QListBox* listBox, QColor color )
{
  if ( lastVisible != listBox ){
    lastVisible = listBox;
    makeWidgetDockVisible( listBox );
  }

  QString b = outBuffer;
  b.append( buffer );

  int k = b.find("\n");
  while ( k != -1 )
  {
    QString part = b.left(k+1);
    b.remove( 0, k+1 );

    if ( part.length() > 1023 ){
      part.truncate( 1023 );
      part.append("\n");
    }
    listBox->insertItem( new SErrorItem( part, color) );

    k = b.find("\n");
	}
  outBuffer=b;
  listBox->setBottomItem(listBox->count()-1);
  old_color = color;
}

void StudioView::readWorkspace(QString name)
{
  EDIT_WINDOW->slotCloseAll();

  WORKSPACE->open( name );
  if ( !WORKSPACE->Ok() ) return;

  QStrList fileList;
  kapp->config()->setGroup("MainData");
  kapp->config()->readListEntry( "resentProjectPath", fileList );
  int i  = fileList.find( name );
  if ( i != -1 ) fileList.remove( i );

  fileList.insert( 0, name );
  kapp->config()->writeEntry( "resentProjectPath", fileList );
  kapp->config()->sync();
}

void StudioView::slotGotoError( QListBoxItem* item )
{
  if ( !item ) return;
  TErrorMessageInfo info = error_parse->getInfo( outc->index(item) + 1 );
  QFileInfo finfo( info.filename );
  EDIT_WINDOW->selectLine( finfo.absFilePath(), info.errorline );
}

void StudioView::slotStop()
{
  process.kill();
  find_process.kill();
  appl_process.kill();
}

void StudioView::runPerl( QString arg1, QString arg2 )
{
  if ( arg2.isEmpty() ) arg2 = " ";
  STATUS(QString("Running perl ") + arg1 + " " + arg2 + "  ...");

  QDir::setCurrent( WORKSPACE->getDir() );

  process.clearArguments();
  process << "perl" << arg1 << arg2;

  outc->blockSignals( true );
  outc->clear();
  outr->clear();
  oute->clear();
  next_job = "";
  STUDIO->processRunUpdateUI();
  process.start(KProcess::NotifyOnExit,KProcess::AllOutput);
}

void StudioView::slotFindEnd( KProcess* )
{
  STUDIO->processEndUpdateUI();
}

void StudioView::gotoNextError()
{
  if ( error_parse->hasNext() ){
    TErrorMessageInfo info = error_parse->getNext();
    outc->setCurrentItem( info.makeoutputline - 1 );
    outc->centerCurrentItem();

    QFileInfo finfo( info.filename );
    EDIT_WINDOW->selectLine( finfo.absFilePath(), info.errorline );
  }
}

void StudioView::gotoPrevError()
{
  if ( error_parse->hasPrev() ){
    TErrorMessageInfo info = error_parse->getPrev();
    outc->setCurrentItem( info.makeoutputline - 1 );
    outc->centerCurrentItem();

    QFileInfo finfo( info.filename );
    EDIT_WINDOW->selectLine( finfo.absFilePath(), info.errorline );
  }
}
