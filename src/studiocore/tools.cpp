#include "studio.h"
#include "tools.h"

#include <kstddirs.h>
#include <klocale.h>
#include <kmimetype.h>
#include <kiconloader.h>
#include <kurl.h>

#include <qdir.h>
#include <qfileinfo.h>
#include <qpixmap.h>
#include <fcntl.h>
#include <unistd.h>

void splitPathToNameAndProjectName( QString path, QString& name, QString& projectName )
{
  name = "";
  projectName = "";
  int k = path.findRev("/");
  if ( k == -1 ) return;

  name = path.right( path.length() - k - 1 );
  projectName = path.left( k );
  k = projectName.findRev("/");
  if ( k != -1 )
    projectName = projectName.right( projectName.length() - k - 1 );
  QString mainPath = WORKSPACE->getDir();
  mainPath = mainPath.left( mainPath.length() -1 );
  mainPath = mainPath.right( mainPath.length() - mainPath.findRev("/") - 1 );
  if ( mainPath == projectName ) projectName = WORKSPACE->getName();
  return;
}

bool isSourceFile( QString file )
{
  QFileInfo i(file.data());
  QString ext = i.extension();
  return ( ext == "cpp" || ext == "cc" || ext == "C" || ext == "c" || ext == "ui" );
}

bool isHeaderFile( QString file )
{
  QFileInfo i(file);
  QString ext = i.extension();
  return ( ext == "h" || ext == "hpp" || ext == "H" );
}


QString getSwitchFile( QString file )
{
  QString switchFile;
  QFileInfo i(file);
  QString baseName = i.dirPath() + "/" + i.baseName();
  QString newName;

  if ( isSourceFile(file) ){
    newName = baseName + ".h";
    i.setFile( newName); if ( i.exists() ) return newName;

    newName = baseName + ".H";
    i.setFile( newName); if ( i.exists() ) return newName;

    newName = baseName + ".hpp";
    i.setFile( newName); if ( i.exists() ) return newName;
  }

  if ( isHeaderFile(file) ){
    newName = baseName + ".cpp";
    i.setFile( newName); if ( i.exists() ) return newName;

    newName = baseName + ".cc";
    i.setFile( newName); if ( i.exists() ) return newName;

    newName = baseName + ".C";
    i.setFile( newName); if ( i.exists() ) return newName;

    newName = baseName + ".c";
    i.setFile( newName); if ( i.exists() ) return newName;
  }
  return "";
}

QString getFileNameFromFilePath( QString path )
{
  QFileInfo i(path);
  return i.fileName();
}

QPixmap getPixmapForFileType( QString filePath )
{
  #include "p_cpp.xpm"
  #include "p_h.xpm"
  #include "file.xpm"
  #include "lsm.xpm"

  QString ext = QFileInfo(filePath).extension();
  if ( isSourceFile(filePath) ){
    return QPixmap( p_cpp );
  }

  if ( isHeaderFile(filePath) ){
    return QPixmap( p_h );
  }

  if ( ext == "kdelnk" ){
    return QPixmap( lsm );
  }

  if ( ext == "po" ) {
    QPixmap p = findFlagPixmap(QFileInfo(filePath).baseName());
    if (!p.isNull())
      return QPixmap(p);
  }

  return QPixmap(BarIcon(KMimeType::iconForURL(KURL(filePath))));
}

QPixmap getPixmapForWorkspaceType( Workspace::wType type )
{
  #include "folder.xpm"
  #include "folder_blue.xpm"
  #include "folder_green.xpm"
  #include "folder_yellow.xpm"
  #include "folder_cyan.xpm"

  switch ( type ){
    case Workspace::EXECUTABLE:
      return QPixmap( folder_cyan );
    case Workspace::SHARED_LIB:
      return QPixmap( folder_green );
    case Workspace::STATIC_LIB:
      return QPixmap( folder_yellow );
    case Workspace::DATA:
      return QPixmap( folder_blue );
    default:
      break;
  }
  return QPixmap( folder_xpm );
}

// return number of bytes read if ok, -1 if failed
int read_file(const char *name, char *buf, int max)
{	
    int fd = open(name, O_RDONLY);
    if(fd < 0) return -1;
    int r = read(fd, buf, max);
    if(r < 0) return -1;
    close(fd);
    return r;
}

QPixmap findFlagPixmap(QString code)
{
  int pos=code.length();
  QString flag = locate("locale", QString("l10n/%1/flag.png").arg(code.lower()));
  if (flag.isEmpty()) {
    pos = code.find("_");
    if (pos > 0)
      flag = locate("locale", QString("l10n/%1/flag.png").arg(code.mid(pos+1).lower()));
    else
      flag = locate("locale", QString("l10n/C/flag.png"));
  }

  if (flag.isEmpty())
    return QPixmap();

  QPixmap pm = QPixmap(flag);
  return pm;
}
