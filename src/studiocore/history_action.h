#ifndef HISTORY_ACTION_H
#define HISTORY_ACTION_H

#include <kaction.h>
#include <qlist.h>

class QPopupMenu;

struct HistoryEntry
{
  QString filePath;
  int line;
  int column;
};

typedef QList<HistoryEntry> HistoryEntryList;

class HistoryAction : public KAction
{ Q_OBJECT
public:
  HistoryAction( const QString& text, const QString& icon, int accel = 0, QObject* parent = 0, const char* name = 0 );
  virtual ~HistoryAction();

  virtual int plug( QWidget *widget, int index = -1 );
  virtual void unplug( QWidget *widget );

  static void fillHistoryPopup( const HistoryEntryList& history,
                                QPopupMenu * popup,
                                bool onlyBack = false,
                                bool onlyForward = false,
                                uint startPos = 0 );

  virtual void setEnabled( bool b );
  virtual void setIconSet( const QIconSet& iconSet );

  QPopupMenu *popupMenu();

signals:
  void activated( int );

private:
  QPopupMenu *m_popup;
};

#endif
