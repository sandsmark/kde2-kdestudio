#include "serroritem.h"

#include <qpainter.h>
#include <kapp.h>
#include <kconfig.h>

SErrorItem::SErrorItem( const char *text, QColor c )
:QListBoxText()
{
  color = c;
	setText( text );
}

SErrorItem::~SErrorItem()
{
}

void SErrorItem::paint( QPainter *p )
{
  QFontMetrics fm = p->fontMetrics();
  p->setPen(color);
  p->drawText( 5, fm.ascent() + fm.leading()/2, text() );
}

SFindItem::SFindItem( const char *text )
:QListBoxText()
{
  setText( text );

  kapp->config()->setGroup("ColorData");
  filePathColor = kapp->config()->readColorEntry( "FindFilePath", &Qt::black );
  lineNumberColor = kapp->config()->readColorEntry( "FindLineNumber", &Qt::red );
  findTextColor = kapp->config()->readColorEntry( "FindText", &Qt::blue );
}

SFindItem::~SFindItem()
{
}

void SFindItem::paint( QPainter *p )
{
  int x;
  QFontMetrics fm = p->fontMetrics();
  QString item(text());
  int k = item.find(':');
  if (k != -1) {
  QString fn = item.left(k);
  item = item.right(item.length()-1-k);
  int k = item.find(':');
  if (k != -1) {
    QString n = item.left(k);
    		item = item.right(item.length()-1-k);
				p->setPen( filePathColor );
				p->drawText( 3,  fm.ascent() + fm.leading()/2, fn.data() );
				x = fm.width( fn.data() );

				p->drawText( 3 + x,  fm.ascent() + fm.leading()/2, ":" );
				x += fm.width( ":" );

				p->setPen( lineNumberColor );
				p->drawText( 3 + x,  fm.ascent() + fm.leading()/2, n.data() );
				x += fm.width( n.data() );

				p->setPen( filePathColor );
				p->drawText( 3 + x,  fm.ascent() + fm.leading()/2, ":" );
				x += fm.width( ":" );

				if ( x < 200 ) x = 200;
				p->setPen( findTextColor );
				p->drawText( 3 + x,  fm.ascent() + fm.leading()/2, item.data() );
	  }
  }
}




