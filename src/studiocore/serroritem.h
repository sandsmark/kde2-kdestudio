#ifndef SERRORITEM_H
#define SERRORITEM_H

#include <qlistbox.h>

class SErrorItem : public QListBoxText
{
public: 
	SErrorItem( const char * = 0, QColor color = Qt::black );
	~SErrorItem();

protected:
  virtual void paint( QPainter * );

private:
  QColor color;
};

class SFindItem : public QListBoxText
{
public:
	SFindItem( const char * = 0 );
	~SFindItem();

protected:
  virtual void paint( QPainter * );

private:
  QColor filePathColor;
  QColor lineNumberColor;
  QColor findTextColor;
};

#endif



