#include "seditwindow.h"
#include "studio.h"
#include "studioview.h"
#include "saction.h"

#include "kwmanager.h"
#include "tools.h"
#include "workspace.h"

#include <kstddirs.h>
#include <qapp.h>
#include <qobjcoll.h>
#include <qsplitter.h>
#include <qmessagebox.h>
#include <qfileinfo.h>

#include <kdocktabctl.h>
#include <ktoolbar.h>
#include <kiconloader.h>

#define CHILD(parent,type,name) ((type*)qt_find_obj_child(parent,#type,name))

#define WRITE_CONNECT  \
        write->parentWidget()->setFocusProxy( write );\
        connect(write, SIGNAL(newUndo()),    SLOT(slotNewUndo()));   \
        connect(write, SIGNAL(newCurPos()),  SLOT(slotNewCurPos())); \
        connect(write, SIGNAL(newStatus()),  SLOT(slotNewStatus())); \
        connect(write, SIGNAL(newCaption()), SLOT(slotNewCaption()));\
        connect(write, SIGNAL(statusMsg(const char *)),  SLOT(slotStatusMsg(const char *))); \
        connect(write, SIGNAL(deleteLine(int)), SLOT(slotDeleteLine(int))); \
        write->clearFocus(); write->setFocus();


#define EDITOR STUDIO->currentEditor

SEditWindow::SEditWindow(QWidget * parent, const char * name)
: QWidget( parent, name )
{
  connect(WORKSPACE,SIGNAL(closeMainWorkspace()),SLOT(slotWorkspaceClose()));
  connect(WORKSPACE,SIGNAL(openMainWorkspace()),SLOT(slotWorkspaceOpen()));

  manager = new KWriteManager();
  connect(manager,SIGNAL(activate(KWrite*)),SLOT(kWriteActivate(KWrite*)));
  connect(manager,SIGNAL(deactivate(KWrite*)),SLOT(kWriteDeactivate(KWrite*)));
  
  tab = new KDockTabCtl(this, "Tab");
  connect(tab, SIGNAL(pageSelected(QWidget*)), this, SLOT(slotPageSelected(QWidget*)));

/**************************************************************************************************************************/
  popup = new QPopupMenu();
  connect(popup, SIGNAL(activated(int)), this, SLOT(slotPopup(int)));
  connect(popup, SIGNAL(aboutToShow()), this, SLOT(slotShowPopup()));
/**************************************************************************************************************************/
  item = new QStrList();
  item->setAutoDelete(true);

  stepLineWrite = 0L;
  EDITOR = 0L;
  extremalClose = false;

  m_bLockHistory = false;
  m_lstHistory.setAutoDelete( true );
}

void SEditWindow::init()
{
  m_paBack = new HistoryAction( "&Back", "back", CTRL+Key_Left, STUDIO_VIEW->actionCollection(), "back" );
  connect( m_paBack, SIGNAL( activated() ), SLOT( slotBack() ) );
  connect( m_paBack->popupMenu(), SIGNAL( aboutToShow() ), SLOT( slotBackAboutToShow() ) );
  connect( m_paBack->popupMenu(), SIGNAL( activated( int ) ), SLOT( slotBackActivated( int ) ) );

  m_paForward = new HistoryAction( "&Forward", "forward", CTRL+Key_Right, STUDIO_VIEW->actionCollection(), "forward" );
  connect( m_paForward, SIGNAL( activated() ), SLOT( slotForward() ) );
  connect( m_paForward->popupMenu(), SIGNAL( aboutToShow() ), SLOT( slotForwardAboutToShow() ) );
  connect( m_paForward->popupMenu(), SIGNAL( activated( int ) ), SLOT( slotForwardActivated( int ) ) );

  tools = new KToolBar(STUDIO_VIEW);
  tools->setFullSize( false );
  STUDIO_VIEW->addToolBar(tools);

  write_menu = ACTION->createQPopupMenu( "undo redo | cut copy paste | openfile save close | gotoline |" );
  manager->installRBPopup( write_menu );

  toolsMenu  = ACTION->createQPopupMenu( "editor_options editor_color editor_defaults editor_highlighting" );
  find_popup = ACTION->createQPopupMenu( "search search_repeat | replace" );
 
  m_paBack->plug(tools);
  m_paForward->plug(tools);

  ACTION->setToolbar( tools, "| close | cut copy paste undo redo | search filesearch | one_window split_h split_v | set_second |" );
  tools->insertButton( "options", 15, true, "Options");
  tools->setDelayedPopup(15,toolsMenu);

  ACTION->setDelayedPopup( tools, "set_second", popup );
  ACTION->setDelayedPopup( tools, "search", find_popup );

  m_paBack->setEnabled(false);
  m_paForward->setEnabled(false);

  disableAllAction();
}

SEditWindow::~SEditWindow()
{
  slotCloseAll();
  delete manager;
}

void SEditWindow::resizeEvent(QResizeEvent *evt)
{
 tab->resize(evt->size());
 QWidget::resizeEvent(evt);
}

void SEditWindow::selectLine( const QString& path, int line)
{
  if ( !viewAdd(path) )
    return;

  clearStepLine();
  EDITOR->setCursorPosition(line-1, 0);
  EDITOR->setStepLine( line-1 );
  stepLineWrite = EDITOR;

  slotPageSelected( tab->visiblePage() );
}

void SEditWindow::slotViewTreeListItem( const char* f )
{
  viewAdd(f);
}

bool SEditWindow::viewAdd( const QString& path )
{
  QFileInfo fi( path );
  if ( !fi.exists() || !fi.isFile() )
    return false;

  if (fi.extension()=="ui") {
    KShellProcess* dp = new KShellProcess();
    connect(dp,SIGNAL(processExited(KProcess*)),SLOT(slotUIProcessExited(KProcess*)));
    *dp << "$QTDIR/bin/designer" << path;
    dp->start();
    return false;
  }

  if (fi.extension()=="po") {
    KShellProcess* dp = new KShellProcess();
    connect(dp,SIGNAL(processExited(KProcess*)),SLOT(slotUIProcessExited(KProcess*)));
    *dp << "kbabel" << path;
    dp->start();
    return false;
  }

  if ( selectTabFromFileName( path ) )
    return true;

  QSplitter* split = new QSplitter(tab);
  split->setOpaqueResize(true);

  KWrite *write = manager->createKWrite( split, "kWrite1" );
  WRITE_CONNECT;
  
  tab->insertPage( split, fi.fileName() );
  tab->setPixmap( split , getPixmapForFileType( path ) );
  tab->setToolTip( split, path );

  item->append(path);
  write->loadFile( path );

  tab->setVisiblePage( split );

  emit EditorOpenFile( write, path );
  return true;
}

void SEditWindow::slotVform()
{
  QSplitter* split = (QSplitter*)( tab->visiblePage() );
  split->setOrientation(QSplitter::Horizontal);

  if ( CHILD(split, KWrite, "kWrite2") )
    return;

  KWrite *write = manager->createKWrite( split, "kWrite2", CHILD(split, KWrite, "kWrite1")->doc() );
  WRITE_CONNECT;

  QValueList<int> sizes;
  sizes.append(split->width()/2);
  sizes.append(split->width()/2);
  split->setSizes(sizes);

  write->show();
}

void SEditWindow::slotHform()
{
  QSplitter* split = (QSplitter*)( tab->visiblePage() );
  split->setOrientation(QSplitter::Vertical);
  if ( CHILD(split, KWrite, "kWrite2") != 0 ) return;

  KWrite *write = manager->createKWrite( split, "kWrite2", CHILD(split, KWrite, "kWrite1")->doc() );
  WRITE_CONNECT;

  QValueList<int> sizes;
  sizes.append(split->height()/2);
  sizes.append(split->height()/2);
  split->setSizes(sizes);

  write->show();
}

void SEditWindow::slotNform()
{
  QSplitter* split = (QSplitter*)( tab->visiblePage() );
  if ( CHILD(split, KWrite, "kWrite2") )
    delete CHILD(split, KWrite, "kWrite2");

  ((QWidget *)CHILD(split, KWrite, "kWrite1")->children()->getFirst())->setFocus();
}

void SEditWindow::slotCppH()
{
  QString path = CHILD(tab->visiblePage(), KWrite, "kWrite1")->fileName();
  path = getSwitchFile( path );

  if ( path.isEmpty () )
    return;

  int i = tab->visiblePageId();
  viewAdd(path);
  int k = tab->visiblePageId();
  KWrite *write = CHILD(tab->page(i), KWrite, "kWrite2");
  if ( !write )
    return;

  tab->setVisiblePage(i);

  QSplitter* split = (QSplitter*)( tab->visiblePage() );
  QValueList<int> sizes = split->sizes();

  delete write;

  write = manager->createKWrite( tab->page(i), "kWrite2", CHILD(tab->page(k), KWrite, "kWrite1")->doc() );

  split->setSizes(sizes);

  WRITE_CONNECT;
  write->show();
}

void SEditWindow::slotPopup( int itemId )
{
  if ( itemId < 0 ){
    slotCppH();
    return;
  }

  KWrite *write = CHILD(tab->visiblePage(), KWrite, "kWrite2");
  if (!write){
    selectTabFromFileName( item->at(itemId) );
    return;
  }

  QSplitter* split = (QSplitter*)( tab->visiblePage() );
  QValueList<int> sizes = split->sizes();

  delete write;

  write = manager->createKWrite( tab->visiblePage(), "kWrite2", findKWriteFromFileName( item->at(itemId) )->doc() );

  split->setSizes(sizes);

  WRITE_CONNECT;
  write->show();
}

void SEditWindow::slotPageSelected( QWidget* widget )
{
  // for setup EDITOR
  kWriteActivate( CHILD(widget,KWrite,"kWrite1") );
  enableAllAction();

  if ( !EDITOR->hasFocus() )
    EDITOR->setFocus();

  updateHistory();
}

void SEditWindow::extremalCloseFile( QString path )
{
  if ( selectTabFromFileName( path ) )
  {
    extremalClose = true;
    slotClose();
    extremalClose = false;
  }
}

void SEditWindow::slotClose()
{
  if (!EDITOR)
    return;

  KWrite *write = CHILD( tab->visiblePage(), KWrite, "kWrite1" );
  if ( !write ) return;

  QString path = write->fileName();

  emit EditorBeforeClosingFile( write, path );

  if (write->isModified()) {
    QString note = "File: ";
  	note.append( path );
  	note.append( " is modified ! Save changes ?" );

  	int t = 0;
    if (extremalClose)
      t = QMessageBox::warning(write, "File is modified...", note, "Save", "No");
    else
      t = QMessageBox::warning(write, "File is modified...", note, "Save", "No", "Cancel");

  	if ( t == 0 ){
      write->save();
      emit EditorSaveFile( write, path );
    }
  	if ( t == 2 ) return;
  }

  path = write->fileName();
  QString path2 = path;
  if ( write->isLastView() ) delete write->doc();

  // step line in this write ( only "kWrite1" ) do not to clearStepLine()
  if ( stepLineWrite == write )
    stepLineWrite = 0L;

  delete write;

  write = CHILD( tab->visiblePage(), KWrite, "kWrite2" );
  if (write)
    delete write;

  tab->removePage( tab->visiblePage() );
  item->remove(path);

  for ( QWidget* w = tab->getFirstPage(); w != 0L; w = tab->getNextPage( w ) ) {
    write = CHILD( w, KWrite, "kWrite2" );
    if ( write && write->fileName() == path2 ) {
        if ( write->isLastView() )
          delete write->doc();
        delete write;
        w->update();
    }
  }
  if (!tab->visiblePage()){
    EDITOR = 0L;
    disableAllAction();
    STUDIO_VIEW->setCaption("");
  }
}

KWrite* SEditWindow::getActiveEditor()
{
  return EDITOR;
}

void SEditWindow::slotNewCurPos()
{
  KWrite *write = (KWrite *)(sender());

  QString num;
  num.sprintf("Line: %d", write->currentLine() +1);
  STATUS_BAR->changeItem( num.data(), 1);
  num.sprintf("Col: %d", write->currentColumn() +1);
  STATUS_BAR->changeItem( num.data(), 2);
}

void SEditWindow::slotNewStatus()
{
  KWrite *write = (KWrite *)(sender());
  int config = write->config();
  STATUS_BAR->changeItem(config & cfOvr ? " OVR " : " INS ", 0);
  STATUS_BAR->changeItem(write->isModified() ? " Modified " : " ", 4);
  if ( write->isModified() ) {
    ACTION->setActionsEnabled( "save", true );
    tab->setTabTextColor( findTabIdForKWrite( write ) , red );
  } else {
    ACTION->setActionsEnabled( "save", false );
    tab->setTabTextColor( findTabIdForKWrite( write ), black );
  }
}

void SEditWindow::slotNewUndo()
{
  KWrite* write = (KWrite*)(sender());
  ACTION->setActionsEnabled( "undo", write->undoState() & 1 );
  ACTION->setActionsEnabled( "redo", write->undoState() & 2 );
}

void SEditWindow::slotNewCaption()
{
  KWrite* write = (KWrite*)(sender());
  QString path = write->fileName();
  path.prepend(" ");
  STUDIO_VIEW->setCaption( path );
}

void SEditWindow::slotStatusMsg(const char *status)
{
  STATUS_BAR->message(status,2000);
}

void SEditWindow::slotSaveAll()
{
  for ( QWidget* w = tab->getFirstPage(); w != 0L; w = tab->getNextPage( w ) )
  {
    KWrite* write = CHILD( w, KWrite, "kWrite1" );
    if ( write != 0L && write->isModified() ){
      write->save();

      QString path = write->fileName();
      emit EditorSaveFile( write, path );
    }
  }
}

void SEditWindow::slotSave()
{
  if (EDITOR){
    QString path = EDITOR->fileName();
    slotStatusMsg(QString("Saving file:")+path);
  	EDITOR->save();
    emit EditorSaveFile(EDITOR,path);
  }
}

void SEditWindow::slotCut()
{
  if (EDITOR)
    EDITOR->cut();
}

void SEditWindow::selectAll()
{
  if (EDITOR)
    EDITOR->selectAll();
}

void SEditWindow::slotCopy()
{
  if (EDITOR)
    EDITOR->copy();
}

void SEditWindow::slotPaste()
{
  if (EDITOR)
    EDITOR->paste();
}

void SEditWindow::slotUndo()
{
  if (EDITOR)
    EDITOR->undo();
}

void SEditWindow::slotRedo()
{
  if (EDITOR)
    EDITOR->redo();
}

void SEditWindow::slotSearch()
{
  if (EDITOR){
    updateHistory();
    EDITOR->search();
  }
}

void SEditWindow::slotRSearch()
{
  if (EDITOR){
    updateHistory();
    EDITOR->searchAgain();
  }
}

void SEditWindow::slotReplace()
{
  if (EDITOR){
    updateHistory();
    EDITOR->replace();
  }
}

void SEditWindow::slotCloseAll()
{
  while ( item->count() != 0 ) {
    tab->setVisiblePage( tab->getFirstPage() );
    slotClose();
  }
  EDITOR = 0L;
  disableAllAction();
}

void SEditWindow::disableAllAction()
{
  ACTION->setActionsEnabled("selectAll save save_all indent unindent undo redo cut copy paste close search search_repeat replace one_window split_h split_v set_second gotoline", false );
}

void SEditWindow::enableAllAction()
{
  ACTION->setActionsEnabled("selectAll save_all indent unindent undo redo cut copy paste close search search_repeat replace one_window split_h split_v set_second gotoline", true );
}

void SEditWindow::slotDeleteLine(int line )
{
  emit EditorBeforeDeletingLine( EDITOR, line );
}

KWrite* SEditWindow::findKWriteFromFileName( QString fileName )
{
  for ( QWidget* w = tab->getFirstPage(); w != 0L; w = tab->getNextPage( w ) ) {
    KWrite* write = CHILD( w, KWrite, "kWrite1" );
    if ( write !=0 ){
      QString path = write->fileName();
      if ( path == fileName )
        return write;
    }
  }
  return 0L;
}

bool SEditWindow::selectTabFromFileName( QString fileName )
{
  for ( QWidget* w = tab->getFirstPage(); w != 0L; w = tab->getNextPage( w ) ) {
    KWrite* write = CHILD( w, KWrite, "kWrite1" );
    if ( write !=0 ){
      QString path = write->fileName();
      if ( path == fileName ) {
        if ( tab->visiblePage() == w )
          slotPageSelected( w );
        else
          tab->setVisiblePage( w );
        return true;
      }
    }
  }
  return false;
}

bool SEditWindow::selectEditor( KWrite* editor )
{
  for ( QWidget* w = tab->getFirstPage(); w != 0L; w = tab->getNextPage( w ) ) {
    KWrite* write = CHILD( w, KWrite, "kWrite1" );
    if ( write == editor  ){
        if ( tab->visiblePage() == w )
          slotPageSelected( w );
        else
          tab->setVisiblePage( w );
        return true;
    }
  }
  return false;
}

int SEditWindow::findTabIdForKWrite( KWrite* editor )
{
  for ( QWidget* w = tab->getFirstPage(); w != 0L; w = tab->getNextPage( w ) ) {
    KWrite* write = CHILD( w, KWrite, "kWrite1" );
    if ( write == editor  ){
      return tab->id( w );
    }
  }
  return -1;
}

void SEditWindow::slotIndent()
{
  if (EDITOR)
    EDITOR->indent();
}

void SEditWindow::slotUnindent()
{
  if (EDITOR)
    EDITOR->unIndent();
}

void SEditWindow::slotGotoLine()
{
  if (EDITOR)
    EDITOR->gotoLine();
}

void SEditWindow::slotShowPopup()
{
  popup->clear();
  popup->insertItem("Switch header/source");
  popup->insertSeparator();
  for ( uint k = 0; k < item->count(); k++ ){
    popup->insertItem( getPixmapForFileType( item->at(k) ), getFileNameFromFilePath( item->at(k) ), k );
  }
}

void SEditWindow::updateHistory()
{
  if ( !EDITOR || QString(EDITOR->fileName()).isEmpty() || m_bLockHistory )
    return;

  HistoryEntry * current = m_lstHistory.current();

  HistoryEntry* newEntry = new HistoryEntry;
  newEntry->filePath = EDITOR->fileName();
  newEntry->line = EDITOR->currentLine();
  newEntry->column = EDITOR->currentColumn();

  if (current && current->filePath == newEntry->filePath && current->line == newEntry->line ) {
    delete newEntry;
    return;
  }

  if (current) {
    m_lstHistory.at( m_lstHistory.count() - 1 ); // go to last one
    for ( ; m_lstHistory.current() != current ; ) {
      m_lstHistory.removeLast();
    }
  }

  m_lstHistory.append(newEntry);

  m_paBack->setEnabled( m_lstHistory.at() > 0 );
  m_paForward->setEnabled( m_lstHistory.at() != ((int)m_lstHistory.count())-1 );
}

void SEditWindow::slotBackActivated( int id )
{
  go(-(m_paBack->popupMenu()->indexOf(id)+1));
}

void SEditWindow::slotBackAboutToShow()
{
  m_paBack->popupMenu()->clear();
  HistoryAction::fillHistoryPopup( m_lstHistory, m_paBack->popupMenu(), true, false );
}

void SEditWindow::slotBack()
{
  go(-1);
}

void SEditWindow::slotForwardActivated( int id )
{
  go(m_paForward->popupMenu()->indexOf(id)+1);
}

void SEditWindow::slotForwardAboutToShow()
{
  m_paForward->popupMenu()->clear();
  HistoryAction::fillHistoryPopup( m_lstHistory, m_paForward->popupMenu(), false, true );
}

void SEditWindow::slotForward()
{
  go(1);
}

void SEditWindow::go( int steps )
{
  updateHistory();

  int newPos = m_lstHistory.at() + steps;
  HistoryEntry* l = m_lstHistory.at( newPos );

  m_bLockHistory = true;
  if ( selectTabFromFileName(l->filePath) ) {
    EDITOR->setCursorPosition(l->line,l->column);
  }

  m_paBack->setEnabled( m_lstHistory.at() > 0 );
  m_paForward->setEnabled( m_lstHistory.at() != ((int)m_lstHistory.count())-1 );

  m_bLockHistory = false;
}

void SEditWindow::clearStepLine()
{
  if (stepLineWrite)
    stepLineWrite->clearStepLine();
}

void SEditWindow::kWriteActivate( KWrite* write )
{
  EDITOR = write;
}

void SEditWindow::kWriteDeactivate( KWrite* write )
{
}

void SEditWindow::slotWorkspaceClose()
{
  manager->setConfig(0L);
}

void SEditWindow::slotWorkspaceOpen()
{
  manager->setConfig(WORKSPACE->getConfig());
}

void SEditWindow::slotUIProcessExited(KProcess* pr)
{
  delete pr;
}
