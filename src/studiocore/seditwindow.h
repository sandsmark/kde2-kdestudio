#ifndef SEDITWINDOW_H
#define SEDITWINDOW_H

#include "history_action.h"

#include <qwidget.h>
#include <qstrlist.h>
#include <qlist.h>

class QResizeEvent;
class QSplitter;
class QPopupMenu;

class KToolBar;
class KProcess;

class KWrite;
class KWriteManager;
class KDockTabCtl;

class SEditWindow : public QWidget
{ Q_OBJECT
friend class StudioApp;
public:
  SEditWindow( QWidget * parent = 0, const char * name = 0 );
  ~SEditWindow();

  KWriteManager* manager;

  void init();

  void extremalCloseFile( QString );
  bool selectEditor( KWrite* editor );
  bool selectTabFromFileName( QString fileName );
  KWrite* findKWriteFromFileName( QString fileName );
  int  findTabIdForKWrite( KWrite* editor );
  void clearStepLine();

  QPopupMenu *write_menu;

public slots:
  void selectLine(const QString&, int );
  void slotViewTreeListItem( const char* f );
  void slotSaveAll();
  void slotCloseAll();

  void selectAll();
  void slotClose();
  void slotSave();
  void slotCut();
  void slotCopy();
  void slotPaste();
  void slotUndo();
  void slotRedo();
  void slotSearch();
  void slotRSearch();
  void slotReplace();

  void slotGotoLine();

protected:
  virtual void resizeEvent(QResizeEvent *evt);

protected slots:
  void kWriteActivate( KWrite* );
  void kWriteDeactivate( KWrite* );
  
  void slotVform();
  void slotHform();
  void slotNform();
  void slotPageSelected(QWidget*);
  void slotCppH();
  void slotPopup(int itemId);
  void slotShowPopup();

  void slotNewCurPos();
  void slotNewStatus();
  void slotNewUndo();
  void slotNewCaption();
  void slotStatusMsg(const char *);
  void slotIndent();
  void slotUnindent();

  void slotDeleteLine(int);
  
  void go( int );

  void slotBackActivated(int);
  void slotBackAboutToShow();
  void slotBack();

  void slotForwardActivated(int);
  void slotForwardAboutToShow();
  void slotForward();

  void slotWorkspaceClose();
  void slotWorkspaceOpen();

  void slotUIProcessExited(KProcess*);

public:
  void updateHistory();
  KWrite* getActiveEditor();

signals:
  void EditorBeforeDeletingLine( KWrite*, int );
  void EditorOpenFile( KWrite*, const QString& );
  void EditorSaveFile( KWrite*, const QString& );
  void EditorBeforeClosingFile( KWrite*, QString );

private:
  bool viewAdd( const QString& path );
  void disableAllAction();
  void enableAllAction();
  
  KWrite* stepLineWrite;
  KDockTabCtl* tab;
  QStrList* item;
  KToolBar* tools;
  QPopupMenu* popup;

  QPopupMenu* toolsMenu;
  QPopupMenu* find_popup;
  QStrList history_list;
  
  bool extremalClose;

  bool m_bLockHistory;
  HistoryEntryList m_lstHistory;
  HistoryAction* m_paBack;
  HistoryAction* m_paForward;
};

#endif
