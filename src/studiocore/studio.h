#ifndef STUDIO_H 
#define STUDIO_H 
 
#include <kapp.h>
#include <kdockwidget.h>
#include <ktoolbar.h>

class KAccel;
class KMenuBar;
class KStatusBar;
class QPopupMenu;
class QRect;

class MainWorkspace;
class STabCtl;
class SEditWindow;
class SProjectWindow;
class SAction;
class WorkspaceListView;
class KWrite;

class StudioView;

#define Ic(x)         StudioApp::Studio->loadIcon(x)

#define EDITOR_MANAGER   StudioApp::Studio->editWindow->manager
#define EDITOR_POPUPMENU StudioApp::Studio->editWindow->write_menu
#define STUDIO        StudioApp::Studio
#define STUDIO_VIEW   StudioApp::Studio->view
#define MENU_BAR      StudioApp::Studio->menu_bar
#define TOOL_BAR      StudioApp::Studio->tool_bar
#define STATUS_BAR    StudioApp::Studio->status_bar
#define EDIT_WINDOW   StudioApp::Studio->editWindow
#define PRJ_WINDOW    StudioApp::Studio->projectWindow
#define PRJ_TREE      StudioApp::Studio->tree
#define WORKSPACE     StudioApp::Studio->workspace
#define MAIN_ACCEL    StudioApp::Studio->accel
#define ACTION        StudioApp::Studio->action

#define STATUS(x)     StudioApp::Studio->status_bar->message(x)

class StudioApp : public KDockMainWindow
{ Q_OBJECT
public: 
  StudioApp();
  ~StudioApp();

  static StudioApp  *Studio;

  void initApp();

  void processRunUpdateUI();
  void processEndUpdateUI();

  QPixmap loadIcon( const QString& );

  StudioView *view;
  MainWorkspace *workspace;
  KMenuBar *menu_bar;
  KStatusBar *status_bar;
  KToolBar *tool_bar_0, *tool_bar_1;
  KToolBar *run_toolbar;
  SEditWindow *editWindow;
  SProjectWindow *projectWindow;
  WorkspaceListView *tree;
  SAction* action;

  KAccel *accel;

  KWrite* currentEditor;

protected:
  virtual bool queryClose();
  void initInternal();
  void initMenuBar();
  void initToolBars();
  void initStatusBar();
  void initView();
  void initActions();

protected slots:
  void slotWorkspaceUpdateUI();
  void slotAboutToShowRunMenu();
  void slotActivatedRunMenu(int);
  void slotAboutToShowOpenMenu();
  void slotActivatedOpenMenu(int);
  void slotSetup();
  void slotAccelSetup();
  void slotExit();
  void slotStatusBarPressed(int);

private:
  void setDefaultActionState();

  QPopupMenu *file_menu_recent;
  QPopupMenu *view_menu;
  QRect rMainGeom;
  QPopupMenu* runMenu;
  QPopupMenu* openMenu;
};
 
#endif
