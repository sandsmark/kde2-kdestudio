#ifndef SPROJECTWINDOW_H
#define SPROJECTWINDOW_H

#include <qobject.h>

class WorkspaceListViewItem;
class Workspace;

class SProjectWindow : public QObject
{Q_OBJECT

public:
  SProjectWindow();
 ~SProjectWindow();

public slots:
  void slotCloseFiles( Workspace* w );
  void updateWorkspaceTreeFile();

  void newProject();
  void newFile();
  void deleteFile();
  void removeFile();
  void addExistingFile();
  void removeSubProject();
  void projectOptions();
  void slotCompile();
  void slotRun();
  void slotMakeInstall();
  void slotMakeUnInstall();
  void slotMakeDistrib();
  void slotMakeClean();
  void slotMakeDistClean();
  void slotPerlAutomoc();

  void slotCopyItem();
  void slotMoveItem();
  void slotRenameItem();

  void slotUpdateAllMakefileAm();
  void slotShowMakefile();

private slots:
  void popUpProject(WorkspaceListViewItem*, const QPoint &);
  void slotPopupDestroy();

private:
  QString getSelectedProjectName();
  QString cutName;
  bool localCall;

};
#endif
