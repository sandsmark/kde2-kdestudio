#include "studio.h"
#include "setupdlg.h"
#include "studioview.h"
#include "sprojectwindow.h"
#include "seditwindow.h"
#include "saction.h"
#include "workspace.h"
#include "workspacelistview.h"
#include "welcome.h"

#include <khelpmenu.h>
#include <kiconloader.h>
#include <kaccel.h>
#include <kkeydialog.h>
#include <kstddirs.h>
#include <kmenubar.h>
#include <kwmanager.h>
          
StudioApp*   StudioApp::Studio = 0L;

StudioApp::StudioApp()
: KDockMainWindow(0L,"StudioApp")
{
  currentEditor = 0L;

  runMenu = new QPopupMenu( 0L, "runPopupMenu");
  openMenu = new QPopupMenu( 0L, "openPopupMenu");

  connect( runMenu, SIGNAL(aboutToShow()), this, SLOT(slotAboutToShowRunMenu()) );
  connect( runMenu, SIGNAL(activated(int)), this, SLOT(slotActivatedRunMenu(int)) );

  connect( openMenu, SIGNAL(aboutToShow()), this, SLOT(slotAboutToShowOpenMenu()) );
  connect( openMenu, SIGNAL(activated(int)), this, SLOT(slotActivatedOpenMenu(int)) );

  action = new SAction( "Global Action" );
  accel = new KAccel(this);
  accel->setConfigGroup("Accel Setting");

  Studio = this;

  setCaption("");
}

StudioApp::~StudioApp()
{
  delete EDIT_WINDOW;
}

void StudioApp::initInternal()
{
  initView();

  initActions();
  initMenuBar();

  initToolBars();

  EDIT_WINDOW->init();

  initStatusBar();

/*****************************************************************************************/

  connect( PRJ_TREE   , SIGNAL(popupMenu(WorkspaceListViewItem*, const QPoint &)),
           PRJ_WINDOW , SLOT(popUpProject(WorkspaceListViewItem*, const QPoint &)) );

  connect( PRJ_TREE   , SIGNAL(selectFilePath(const char *)),
           EDIT_WINDOW, SLOT(slotViewTreeListItem(const char *)) );

  connect( WORKSPACE  , SIGNAL(closeWorkspace(Workspace*)),
           PRJ_WINDOW , SLOT(slotCloseFiles(Workspace*)) );

  connect( WORKSPACE  , SIGNAL(updateFileTree()),
           PRJ_WINDOW , SLOT(updateWorkspaceTreeFile()) );

  connect( WORKSPACE  , SIGNAL(updateUI()),
           this       , SLOT(slotWorkspaceUpdateUI()) );
}

void StudioApp::initApp()
{
  view->readDockConfig();
  show();

  slotWorkspaceUpdateUI();

  ACTION->setAllAccel( accel );
  kapp->config()->setGroup("Accel Setting");
  accel->readSettings(kapp->config());

  ACTION->changeAllMenuAccel( accel );
  setDefaultActionState();

/*********************************************/
  // process startup options
  kapp->config()->setGroup("StartupData");
  if ( kapp->config()->readBoolEntry( "OpenLast", false ) )
  {
    QStrList fileList;
    kapp->config()->setGroup("MainData");
    kapp->config()->readListEntry( "resentProjectPath", fileList );
    if ( fileList.count() > 0 ){
      QString path = fileList.at(0);
      STUDIO_VIEW->readWorkspace( path );
    }
  }

  kapp->config()->setGroup("StartupData");
  if ( kapp->config()->readBoolEntry( "ShowWelcomeDialog", true ) )
  {
    Welcome* welcome = new Welcome();
    if ( welcome->exec() == QDialog::Accepted ){
      if ( welcome->createNew ){
        STUDIO_VIEW->slotNewPrj();
      } else {
        STUDIO_VIEW->readWorkspace( welcome->openWorkspace );
      }
    };
    delete welcome;
  }
  STATUS("Welcome to Studio");
}

void StudioApp::initMenuBar()
{
  menu_bar = menuBar();
  menuBar()->insertItem( "&File", ACTION->createQPopupMenu("newworkspace newproject | openworkspace openfile | save save_all close | exit") );
  menuBar()->insertItem( "&Edit", ACTION->createQPopupMenu("undo redo | cut copy paste selectAll | indent unindent") );
  menuBar()->insertItem( "&Search", ACTION->createQPopupMenu("search filesearch replace search_repeat | gotoline") );
  menuBar()->insertItem( "&View", view->dockHideShowMenu() );
  menuBar()->insertItem( "&Project", ACTION->createQPopupMenu("addfile addnewfile | compile run stop | make_install make_distrib | make_uninstall make_clean make_distclean | reconfigure perl_automoc | Update_makefilesAm | projectopt") );
  menuBar()->insertItem( "&Options", ACTION->createQPopupMenu("studio_setup accel_setup | editor_options editor_color editor_defaults editor_highlighting") );
  menuBar()->insertItem( "&Window", ACTION->createQPopupMenu("one_window split_h split_v") );

  menuBar()->insertItem( "&Help", helpMenu(QString::null,false) );
}

void StudioApp::initActions()
{
  // File Menu
  ACTION->addAction( "newworkspace", "&New Workspace", "new_workspace", view, SLOT(slotNewPrj()), 0, "Create new workspace" );
  ACTION->addAction( "newproject", "New &Project", "new_project", PRJ_WINDOW, SLOT(newProject()), 0, "Create new project" );
  ACTION->addAction( "openworkspace", "&Open Workspace...", "open_workspace", view, SLOT(slotOpenProject()), KStdAccel::Open, "Open workspace" );
  ACTION->addAction( "openfile", "Open Fil&e...", "fileopen", view, SLOT(slotOpenFile()), 0, "Open file" );
  ACTION->addAction( "save", "&Save", "filesave", EDIT_WINDOW, SLOT(slotSave()), KStdAccel::Save, "Save" );
  ACTION->addAction( "save_all", "Save &All", "filesaveall", EDIT_WINDOW, SLOT(slotSaveAll()), CTRL + SHIFT + Key_S, "Save all" );
  ACTION->addAction( "close", "C&lose", "fileclose", EDIT_WINDOW, SLOT(slotClose()), KStdAccel::Close, "Close" );
  ACTION->addAction( "exit", "E&xit", "exit", this, SLOT(slotExit()), 0, "Exit from KDEStudio" );

  // Edit Menu
  ACTION->addAction( "undo", "&Undo", "undo", EDIT_WINDOW, SLOT(slotUndo()), KStdAccel::Undo, "Undo" );
  ACTION->addAction( "redo", "&Redo", "redo", EDIT_WINDOW, SLOT(slotRedo()), KStdAccel::Redo, "Redo" );
  ACTION->addAction( "cut", "Cu&t", "editcut", EDIT_WINDOW, SLOT(slotCut()), KStdAccel::Cut, "Cut" );
  ACTION->addAction( "copy", "&Copy", "editcopy", EDIT_WINDOW, SLOT(slotCopy()), KStdAccel::Copy, "Copy" );
  ACTION->addAction( "paste", "&Paste", "editpaste", EDIT_WINDOW, SLOT(slotPaste()), KStdAccel::Paste, "Paste" );
  ACTION->addAction( "selectAll", "Se&lect All", "select_all", EDIT_WINDOW, SLOT(selectAll()), CTRL + Key_A, "Select All" );
  ACTION->addAction( "indent", "&Indent", "indent.xpm", EDIT_WINDOW, SLOT(slotIndent()), CTRL + Key_I, "Indent" );
  ACTION->addAction( "unindent", "Unin&dent", "unindent.xpm", EDIT_WINDOW, SLOT(slotUnindent()), CTRL + Key_U, "Unindent" );

  // Search Menu
  ACTION->addAction( "search", "&Find...", "find", EDIT_WINDOW, SLOT(slotSearch()), KStdAccel::Find, "Search" );
  ACTION->addAction( "filesearch", "Fin&d in Files...", "find_in_file", STUDIO_VIEW, SLOT(slotGrepDlg()), 0, "Search in files" );
  ACTION->addAction( "replace", "&Replace...", "replace", EDIT_WINDOW, SLOT(slotReplace()), KStdAccel::Replace, "Replace" );
  ACTION->addAction( "search_repeat", "&Search Again", "search_again", EDIT_WINDOW, SLOT(slotRSearch()), Key_F3, "Repeat search" );
  ACTION->addAction( "gotoline", "&Go to Line...", "goto_line", EDIT_WINDOW, SLOT(slotGotoLine()), ALT + Key_G, "Go to Line" );

  // Project Menu
  ACTION->addAction( "addfile", "&Add Existing File...", "add_existing_file", PRJ_WINDOW, SLOT(addExistingFile()), SHIFT + Key_F11, "Add existing file to project" );
  ACTION->addAction( "addnewfile", "Add &New File...", "add_new_file", PRJ_WINDOW, SLOT(newFile()), 0, "Add new file to project" );
  ACTION->addAction( "compile", "&Compile", "make", PRJ_WINDOW, SLOT(slotCompile()), Key_F9, "Compile project" );
  ACTION->addAction( "run", "R&un", "run", PRJ_WINDOW, SLOT(slotRun()), Key_F10, "Run" );
  ACTION->addAction( "stop", "&Stop", "stop", STUDIO_VIEW, SLOT(slotStop()), 0, "Terminate current process" );
  ACTION->addAction( "make_install", "Make &Install", "make_install", PRJ_WINDOW, SLOT(slotMakeInstall()), 0, "Running command `make install`" );
  ACTION->addAction( "make_distrib", "Make Distri&bution", "make_distribution", PRJ_WINDOW, SLOT(slotMakeDistrib()), 0, "Running command `make distrib`" );
  ACTION->addAction( "make_uninstall", "Ma&ke Uninstall", "make_uninstall", PRJ_WINDOW, SLOT(slotMakeUnInstall()), 0, "Running command `make uninstall`" );
  ACTION->addAction( "make_clean", "Make C&lean", "make_clean", PRJ_WINDOW, SLOT(slotMakeClean()), 0, "Running command `make clean`" );
  ACTION->addAction( "make_distclean", "Make &Distclean", "make_distclean", PRJ_WINDOW, SLOT(slotMakeDistClean()), 0, "Running command `make distclean`" );
  ACTION->addAction( "reconfigure", "R&econfigure", "reconfigure", STUDIO_VIEW, SLOT(reConfigure()), 0, "Reconfigure" );
  ACTION->addAction( "perl_automoc", "Am_edi&t", "AM_edit", PRJ_WINDOW, SLOT(slotPerlAutomoc()), 0, "Running command `perl ./admin/am_edit`" );
  ACTION->addAction( "Update_makefilesAm", "Update All &Makefile.am", "update_all_makefile", PRJ_WINDOW, SLOT(slotUpdateAllMakefileAm()), 0, "Update all Makefile.am" );
  ACTION->addAction( "projectopt", "&Options...", "misc", PRJ_WINDOW, SLOT(projectOptions()), CTRL + SHIFT + Key_F11, "Project options" );
  
  // Options Menu
  ACTION->addAction( "studio_setup", "Studio &Setup", "KDEStudio_setup", this, SLOT(slotSetup()), 0, "Setup Studio options" );
  ACTION->addAction( "accel_setup", "Configure &Keys", "configure_keys", this, SLOT(slotAccelSetup()), 0, "Configure keys" );
  ACTION->addAction( "editor_options", "Editor &Options", "editor_options", EDITOR_MANAGER, SLOT(slotOptionsDlg()), 0, "Editor options" );
  ACTION->addAction( "editor_color", "Editor &Color", "colorize", EDITOR_MANAGER, SLOT(slotColorDlg()), 0, "Editor color" );
  ACTION->addAction( "editor_defaults", "Editor &Defaults", "editor_defaults", EDITOR_MANAGER, SLOT(slotDefaultsDlg()), 0, "Editor defaults" );
  ACTION->addAction( "editor_highlighting", "Editor &Highlighting", "editor_highlighting", EDITOR_MANAGER, SLOT(slotHighlightDlg()), 0, "Editor highlighting" );

  // Windows Menu
  ACTION->addAction( "one_window", "Single &Window", "single", EDIT_WINDOW, SLOT(slotNform()), 0, "Single window mode" );
  ACTION->addAction( "split_h", "Split &Horizontal", "split_horizontal", EDIT_WINDOW, SLOT(slotHform()), 0, "Split horizontal" );
  ACTION->addAction( "split_v", "Split &Vertical", "split_vertical", EDIT_WINDOW, SLOT(slotVform()), 0, "Split vertical" );
  
  ACTION->addAction( "copyitem", "&Copy to...", "editcopy", PRJ_WINDOW, SLOT(slotCopyItem()), 0, "Copy to..." );
  ACTION->addAction( "moveitem", "Mo&ve to...", Ic(".xpm"), PRJ_WINDOW, SLOT(slotMoveItem()), 0, "Move to..." );
  ACTION->addAction( "renameitem", "&Rename", Ic(".xpm"), PRJ_WINDOW, SLOT(slotRenameItem()), 0, "Rename" );
  ACTION->addAction( "removefile", "R&emove from Project...", "editdelete", PRJ_WINDOW, SLOT(removeFile()), 0, "Remove file from project" );
  ACTION->addAction( "deletefile", "Delete &File", "remove", PRJ_WINDOW, SLOT(deleteFile()), 0, "Delete file from project" );
  
  ACTION->addAction( "removeprj", "Delete Pro&ject", "remove", PRJ_WINDOW, SLOT(removeSubProject()), 0, "Remove project" );
  ACTION->addAction( "showmakefile", "Sho&w Makefile.am", "shellscript2", PRJ_WINDOW, SLOT(slotShowMakefile()), 0, "Show Makefile.am" );

  ACTION->addAction( "set_second", "Set Second Window", "edit", EDIT_WINDOW, SLOT(slotCppH()), Key_F12, "Switch header/source" );
  ACTION->addAction( "gotoNextError", "Show Next Error", Ic(".xpm"), STUDIO_VIEW, SLOT(gotoNextError()), CTRL + ALT + Key_Next, "Show next error" );
  ACTION->addAction( "gotoPrevError", "Show Prev Error", Ic(".xpm"), STUDIO_VIEW, SLOT(gotoPrevError()), CTRL + ALT + Key_Prior, "Show prev error" );
}

void StudioApp::initToolBars()
{
  KToolBar* toolbar;
  toolbar = new KToolBar(this);
  toolbar->setFullSize(false);
  ACTION->setToolbar( toolbar, "newworkspace openworkspace openfile save save_all" );
  ACTION->setDelayedPopup( toolbar, "openworkspace", openMenu );
  addToolBar(toolbar);

  toolbar = new KToolBar(this);
  toolbar->setFullSize(false);
  ACTION->setToolbar( toolbar, "compile run stop" );
  ACTION->setDelayedPopup( toolbar, "run", runMenu );
  addToolBar(toolbar);
  
/*
  toolbar = new KToolBar(this);
  toolbar->setFullSize(false);
  ACTION->setToolbar( toolbar, "make_distrib | reconfigure | make_clean make_distclean" );
  addToolBar(toolbar);
*/  
}

void StudioApp::initView()
{
  WORKSPACE     = new MainWorkspace();
  PRJ_WINDOW    = new SProjectWindow();
  EDIT_WINDOW   = new SEditWindow( STUDIO_VIEW, "EditWindow" );
}

void StudioApp::initStatusBar()
{
  status_bar = STUDIO_VIEW->statusBar();

  status_bar->insertItem(" Col: 00000 ",2,0,true);
  status_bar->insertItem(" Line: 00000 ",1,0,true);
  status_bar->insertItem("  INS  ",0,0,true);
  status_bar->insertItem(" Modified ",4,0,true);

  status_bar->changeItem("Col:", 2);
  status_bar->changeItem("Line:", 1);
  status_bar->changeItem(" ", 4);

  status_bar->setItemFixed(0,100);
  status_bar->setItemFixed(1,100);
  status_bar->setItemFixed(2,100);
  status_bar->setItemFixed(4,100);

  connect(status_bar,SIGNAL(pressed(int)),SLOT(slotStatusBarPressed(int)));
}

void StudioApp::slotStatusBarPressed( int id )
{
  switch (id) {
    case 1:
    case 2:
      EDIT_WINDOW->slotGotoLine();
    case 4:
      EDIT_WINDOW->slotSave();
      break;
    default:
      break;
  }
}

void StudioApp::slotWorkspaceUpdateUI()
{
  ACTION->setActionsEnabled( "addfile addnewfile removefile removeprj compile run newproject projectopt deletefile make_install make_uninstall make_distrib make_clean Update_makefilesAm make_distclean perl_automoc reconfigure showmakefile", WORKSPACE->Ok() );
  ACTION->setActionsEnabled( "run", !WORKSPACE->getDefaultRunPath().isEmpty() );
}

void StudioApp::processEndUpdateUI()
{
  ACTION->setActionsEnabled( "copyitem moveitem renameitem filesearch newworkspace openworkspace", true );
  ACTION->setActionsEnabled( "stop", false );

  slotWorkspaceUpdateUI();
}


void StudioApp::processRunUpdateUI()
{
  ACTION->setActionsEnabled( "copyitem moveitem renameitem filesearch newworkspace openworkspace addfile addnewfile removefile removeprj compile run newproject projectopt deletefile make_install make_uninstall make_distrib make_clean Update_makefilesAm make_distclean perl_automoc reconfigure showmakefile", false );
  ACTION->setActionsEnabled( "run", false );
  ACTION->setActionsEnabled( "stop", true );
}

void StudioApp::slotAboutToShowRunMenu()
{
  runMenu->clear();

  QString defRunPath = WORKSPACE->getDefaultRunPath();
  QStrList list;
  WORKSPACE->getAllExecutablePath( list );

  for ( uint k = 0; k < list.count(); k++ ){
    runMenu->insertItem( list.at(k), k );
    if ( list.at(k) == defRunPath )
      runMenu->setItemChecked( k, true );
  }
}

void StudioApp::slotActivatedRunMenu( int id )
{
  QStrList list;
  WORKSPACE->getAllExecutablePath( list );
  if ( id >= 0  ){
    WORKSPACE->setDefaulRunPath( list.at(id) );
  }
}

void StudioApp::slotAboutToShowOpenMenu()
{
  openMenu->clear();

  QStrList fileList;
  kapp->config()->setGroup("MainData");
  kapp->config()->readListEntry( "resentProjectPath", fileList );

  for ( uint k = 0; k < fileList.count(); k++ )
    openMenu->insertItem( BarIcon("open_workspace"), fileList.at(k) );

  openMenu->insertSeparator();
  openMenu->insertItem( Ic("folder-trash.xpm"), "Clear history", 1000 );
}

void StudioApp::slotActivatedOpenMenu( int id )
{
  if ( id == 1000 ){
    QStrList fileList;
    fileList.clear();
    if ( WORKSPACE->Ok() ) fileList.append( WORKSPACE->getDir() + WORKSPACE->getName() + ".studio" );
    kapp->config()->setGroup("MainData");
    kapp->config()->writeEntry( "resentProjectPath", fileList );
    return;
  }

  STUDIO_VIEW->readWorkspace( openMenu->text(id) );
}

void StudioApp::slotSetup()
{
  SetupDlg* setup = new SetupDlg();
  setup->exec();
  delete setup;
}

void StudioApp::slotAccelSetup()
{
  KKeyDialog::configureKeys( accel );

  kapp->config()->setGroup("Accel Setting");
  accel->writeSettings(kapp->config());
  kapp->config()->sync();

  ACTION->resetEnableFlagForAction( accel );
  ACTION->changeAllMenuAccel( accel );
}

void StudioApp::setDefaultActionState()
{
  ACTION->setActionsEnabled( "openfile", true );
  ACTION->setActionsEnabled( "editor_options editor_color editor_defaults editor_highlighting", true );
  ACTION->setActionsEnabled( "stop", false );
  ACTION->setActionsEnabled( "showmakefile selectAll indent unindent undo redo cut copy paste save close search search_repeat replace save_all one_window split_h split_v set_second", false );
}

void StudioApp::slotExit()
{
  close( true );
}

bool StudioApp::queryClose()
{
  EDIT_WINDOW->slotCloseAll();
  view->writeDockConfig();
  return true;
}

QPixmap StudioApp::loadIcon( const QString& icon )
{
  #include "null_pix.xpm"
  QString fname = KGlobal::dirs()->findResource("data", "studio/pix/" + icon );
  if ( fname == QString::null ){
    return QPixmap(null_pix);
  }
  return QPixmap(fname);
}
