#ifndef TOOLS_H
#define TOOLS_H

#include <qstring.h>
#include <qpixmap.h>

#include "workspace.h"

class QFileInfo;

void splitPathToNameAndProjectName( QString path, QString& name, QString& projectName );

bool isSourceFile( QString file );
bool isHeaderFile( QString file );
QString getSwitchFile( QString file );
QString getFileNameFromFilePath( QString path );
QPixmap getPixmapForFileType( QString filePath );
QPixmap getPixmapForWorkspaceType( Workspace::wType type );
QPixmap findFlagPixmap(QString code);

#endif


