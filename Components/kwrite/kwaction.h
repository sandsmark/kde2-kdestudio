#ifndef KWACTION_H
#define KWACTION_H

#include <qstring.h>
#include "kwtypes.h"

class KWAction
{
public:
  enum Action {replace, newLine, delLine, insLine, killLine};

  KWAction(Action, PointStruc &cursor, int len = 0, const QString &text = QString::null);
  ~KWAction(){};

  Action action;
  PointStruc cursor;
  int len;
  QString text;
  KWAction *next;
};

class KWActionGroup
{
public:
  KWActionGroup(PointStruc &aStart);
  ~KWActionGroup();
  void insertAction(KWAction *);

  PointStruc start;
  PointStruc end;
  KWAction *action;
};

#endif


