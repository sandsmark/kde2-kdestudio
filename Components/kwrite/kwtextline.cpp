#include "kwtextline.h"
#include "kwtypes.h"

TextLine::TextLine(int attribute, int context):
len(0), size(0), text(0L), attribs(0L), attr(attribute), ctx(context), gutter(0L)
{
  hide = false;
  bookmarkData = new QIntDict<int>;
  bookmarkData->setAutoDelete( true );
}

TextLine::~TextLine()
{
  if ( gutter != 0L ) delete gutter;
  delete text;
  delete attribs;
  delete bookmarkData;
}

void TextLine::replace(int pos, int delLen, const QChar *insText, int insLen, uchar *insAttribs)
{
  int newLen, i, z;
  uchar newAttr;
  bool realloc;
  QChar *newText;
  uchar *newAttribs;

  //find new length
  newLen = len - delLen;
  if (newLen < pos) newLen = pos;
  newLen += insLen;
  newAttr = (pos < len) ? attribs[pos] : attr;

  if (newLen > size) {
    //reallocate data
    realloc = true;
    size = size*3 >> 1;
    if (size < newLen) size = newLen;
    size = (size + 15) & (~15);

    newText = new QChar[size];
    newAttribs = new uchar[size];

    ASSERT(newText);
    ASSERT(newAttribs);

    i = QMIN(len, pos);
    for (z = 0; z < i; z++) {
      newText[z] = text[z];
      newAttribs[z] = attribs[z];
    }
  } else {
    realloc = false;
    newText = text;
    newAttribs = attribs;
  }

  //fill up with spaces and attribute
  for (z = len; z < pos; z++) {
    newText[z] = ' ';
    newAttribs[z] = attr;
  }

  i = (insLen - delLen);
  if (i != 0 || realloc) {
    if (i <= 0) {
      //text to replace longer than new text
      for (z = pos + delLen; z < len; z++) {
        newText[z + i] = text[z];
        newAttribs[z + i] = attribs[z];
      }
    } else {
      //text to replace shorter than new text
      for (z = len -1; z >= pos + delLen; z--) {
        newText[z + i] = text[z];
        newAttribs[z + i] = attribs[z];
      }
    }
  }

  if (realloc) {
    delete text;
    delete attribs;
    text = newText;
    attribs = newAttribs;
  }

  if (insAttribs == 0L) {
    for (z = 0; z < insLen; z++) {
      text[pos + z] = insText[z];
      attribs[pos + z] = newAttr;
    }
  } else {
    for (z = 0; z < insLen; z++) {
      text[pos + z] = insText[z];
      attribs[pos + z] = insAttribs[z];
    }
  }
  len = newLen;
}

void TextLine::wrap(TextLine *nextLine, int pos)
{
  if ( pos == 0 )
  {
    QIntDict<int>* bd = nextLine->bookmarkData;
    nextLine->bookmarkData = bookmarkData;
    bookmarkData = bd;

    if ( gutter ){
      nextLine->setGutter( gutter->id, gutter->begin, gutter->gutterName );
      removeGutter();
    }
  }

  int l = len - pos;
  if (l > 0) {
    nextLine->replace(0, 0, &text[pos], l, &attribs[pos]);
    attr = attribs[pos];
    len = pos;
  }
}

void TextLine::unWrap(int pos, TextLine *nextLine, int len)
{
  // merge bookmark data
  QIntDict<int> *bd = nextLine->bookmarkData;
  QIntDictIterator<int> it( *bd );

  while ( it.current() ){
    bookmarkData->insert( it.currentKey(), new int( *it.current() ) );
    ++it;
  }
  bd->clear();

  gutterData* gd = nextLine->getGutter();
  if ( gd ){
    setGutter( gd->id, gd->begin, gd->gutterName );
    nextLine->removeGutter();
  }

  replace(pos, 0, nextLine->text, len, nextLine->attribs);
  attr = nextLine->getRawAttr(len);
  nextLine->replace(0, len, 0L, 0);
}

void TextLine::removeSpaces()
{
  while (len > 0 && text[len - 1] == ' ') len--;
}

int TextLine::firstChar() const
{
  int z = 0;
  while (z < len && text[z].isSpace()) z++;
  return (z < len) ? z : -1;
}

QChar TextLine::getChar(int pos) const
{
  if (pos < len) return text[pos];
  return ' ';
}

void TextLine::setAttribs(int attribute, int start, int end)
{
  if (end > len) end = len;
  for (int z = start; z < end; z++) attribs[z] = (attribs[z] & taSelectMask) | attribute;
}

void TextLine::setAttr(int attribute)
{
  attr = (attr & taSelectMask) | attribute;
}

int TextLine::getAttr(int pos) const
{
  if (pos < len) return attribs[pos] & taAttrMask;
  return attr & taAttrMask;
}

int TextLine::getAttr() const
{
  return attr & taAttrMask;
}

int TextLine::getRawAttr(int pos) const
{
  if (pos < len) return attribs[pos];
  return (attr & taSelectMask) ? attr : attr | 256;
}

int TextLine::getRawAttr() const
{
  return attr;
}

void TextLine::setContext(int context)
{
  ctx = context;
}

int TextLine::getContext() const
{
  return ctx;
}

const QChar* TextLine::getString()
{
  QChar ch = QChar('\0');
  replace(len, 0, &ch, 1);
  len--;
  return text;
}

const QChar* TextLine::getText() const
{
  return text;
}


void TextLine::select(bool sel, int start, int end)
{
  if (end > len) end = len;
  if (sel) {
    for (int z = start; z < end; z++) attribs[z] |= taSelected;
  } else {
    for (int z = start; z < end; z++) attribs[z] &= ~taSelected;
  }
}

void TextLine::selectEol(bool sel, int pos)
{
  if (sel) {
    for (int z = pos; z < len; z++) attribs[z] |= taSelected;
    attr |= taSelected;
  } else {
    for (int z = pos; z < len; z++) attribs[z] &= ~taSelected;
    attr &= ~taSelected;
  }
}


void TextLine::toggleSelect(int start, int end)
{
  if (end > len) end = len;
  for (int z = start; z < end; z++) attribs[z] = attribs[z] ^ taSelected;
}


void TextLine::toggleSelectEol(int pos)
{
  for (int z = pos; z < len; z++) attribs[z] = attribs[z] ^ taSelected;
  attr = attr ^ taSelected;
}


int TextLine::numSelected() const {
  int n = 0;
  for (int z = 0; z < len; z++) if (attribs[z] & taSelected) n++;
  return n;
}

bool TextLine::isSelected(int pos) const
{
  if (pos < len) return (attribs[pos] & taSelected);
  return (attr & taSelected);
}

bool TextLine::isSelected() const
{
  return (attr & taSelected);
}

int TextLine::findSelected(int pos) const
{
  while (pos < len && attribs[pos] & taSelected) pos++;
  return pos;
}

int TextLine::findUnselected(int pos) const
{
  while (pos < len && !(attribs[pos] & taSelected)) pos++;
  return pos;
}

int TextLine::findRevSelected(int pos) const
{
  while (pos > 0 && attribs[pos - 1] & taSelected) pos--;
  return pos;
}

int TextLine::findRevUnselected(int pos) const
{
  while (pos > 0 && !(attribs[pos - 1] & taSelected)) pos--;
  return pos;
}


int TextLine::cursorX(int pos, int tabChars) const
{
  int l = (pos < len) ? pos : len;
  int x = 0;
  for (int z = 0; z < l; z++) {
    if (text[z] == '\t') x += tabChars - (x % tabChars); else x++;
  }
  if (pos > len) x += pos - len;
  return x;
}

void TextLine::markFound(int pos, int l)
{
  l += pos;
  if (l > len) l = len;
  for (int z = pos; z < l; z++) attribs[z] |= taFound;
}

void TextLine::unmarkFound()
{
  for (int z = 0; z < len; z++) attribs[z] &= ~taFound;
}

void TextLine::setGutter( void* id, bool begin, QString name )
{
  gutter = new gutterData( id, begin, name );
}

gutterData* TextLine::getGutter()
{
  return gutter;
}

void TextLine::removeGutter()
{
  if ( gutter != 0L )
  {
    delete gutter;
    gutter = 0L;
  }
}
