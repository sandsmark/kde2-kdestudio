#ifndef KWMANAGER_H
#define KWMANAGER_H

#include <qobject.h>
#include <qpixmap.h>
#include <qstrlist.h>
#include <qobjectlist.h>
#include <qlist.h>

class KConfig;
class QPopupMenu;

class KWrite;
class TextLine;

#include "kwrite.h"
#include "kwtextline.h"
#include "kwdoc.h"
#include "highlight.h"

class KWriteManager : public QObject
{ Q_OBJECT
friend class KWrite;
friend class KWriteView;
friend class KWriteDoc;
public:
  KWriteManager();
  ~KWriteManager();

  KWrite* createKWrite( QWidget* parent, const char* name = 0, KWriteDoc* doc = 0L );

  int reservedBookmarks( const QPixmap& pix );
  QPixmap* getPixmapForBookmarks( int id );
  QPixmap* getPixmapForBookmarks( TextLine* );

  void delBookmark ( KWrite*, int groupId, int id );
  void setBookmark ( KWrite*, int groupId, int line, int id );
  void gotoBookmark( KWrite*, int groupId, int id );
  int  getBookmark ( KWrite*, int groupId, int line );

  void installRBPopup( QPopupMenu* );

  int searchFlags;
  QStrList searchForList;
  QStrList replaceWithList;
  QPopupMenu *popup;

  void setConfig( KConfig* );

public slots:
  void setCurrentKWrite( KWrite* );

  void slotOptionsDlg();
  void slotColorDlg();
  void slotDefaultsDlg();
  void slotHighlightDlg();

protected:
  KConfig* config( bool forRead = false );
  void readConfig( KWrite* );

protected slots:
  void configChanges( bool );
  void slotKWriteDestroyed();

private:
  KConfig* m_pConfig;
  KWrite* current;
  QObjectList writes;
  HlManager* hlManager;

  struct BookmarksData
  {
    int id;
    QPixmap* pix;
  };
  QList<BookmarksData> gdata;
  int idCounter;

signals:
  void activate( KWrite* );
  void deactivate( KWrite* );
};

#endif


