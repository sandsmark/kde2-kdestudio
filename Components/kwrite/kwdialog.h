#ifndef _KWDIALOG_H_
#define _KWDIALOG_H_

class QComboBox;
class QCheckBox;
class QLineEdit;
class QColor;
class KColorButton;
class KIntNumInput;

#include <qdialog.h>
#include <qstrlist.h>
#include <kdialogbase.h>

class SearchDialog : public QDialog
{Q_OBJECT
public:
  SearchDialog(QStrList *searchFor, QStrList *replaceWith, int flags, const QString& markedText, QWidget *parent, const char *name = 0L);
  const char *getSearchFor();
  const char *getReplaceWith();
  int getFlags();

protected slots:
  void okSlot();

protected:
  QComboBox *search;
  QComboBox *replace;

  QCheckBox *opt1;
  QCheckBox *opt2;
  QCheckBox *opt3;
  QCheckBox *opt4;
  QCheckBox *opt5;
  QCheckBox *opt6;
};

class ReplacePrompt : public QDialog
{ Q_OBJECT
public:
  ReplacePrompt(QWidget *parent, const char *name = 0);

signals:
  void clicked();

protected slots:
  void no();
  void all();
  virtual void done(int);

protected:
  void closeEvent(QCloseEvent *);
};

class GotoLineDialog : public KDialogBase
{Q_OBJECT
public:
  GotoLineDialog(QWidget *parent, int line, int max);
  int getLine();

protected:
  KIntNumInput *e1;
  QPushButton *btnOK;
};

class SettingsDialog : public QDialog
{Q_OBJECT
public:
  SettingsDialog(int flags, int tabWidth, int undoSteps, QWidget *parent, const char *name = 0);
  int getFlags();
  int getTabWidth();
  int getUndoSteps();

protected:
  QCheckBox *opt1;
  QCheckBox *opt2;
  QCheckBox *opt3;
  QCheckBox *opt4;
  QCheckBox *opt5;
  QCheckBox *opt7;
  QCheckBox *opt8;
  QCheckBox *opt9;
  QCheckBox *opt10;
  QCheckBox *opt11;
  QCheckBox *opt12;
  QLineEdit *e2;
  QLineEdit *e3;
};

class ColorDialog : public QDialog
{Q_OBJECT
public:
  ColorDialog( QColor*, QWidget* parent, const char* name = 0 );
  void getColors( QColor* );

protected:
  KColorButton *back;
  KColorButton *textBack;
  KColorButton *selected;
  KColorButton *found;
  KColorButton *selFound;
};

#endif

