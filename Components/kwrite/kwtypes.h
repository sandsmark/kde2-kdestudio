#ifndef KWTYPES_H
#define KWTYPES_H

#include <qdialog.h>

//text attribute constants
const int taSelected = 0x40;
const int taFound = 0x80;

const int taSelectMask = taSelected | taFound;
const int taAttrMask = ~taSelectMask & 0xFF;

const int taShift = 6;

//search flags
const int sfCaseSensitive   = 1;
const int sfWholeWords      = 2;
const int sfFromCursor      = 4;
const int sfBackward        = 8;
const int sfSelected        = 16;
const int sfPrompt          = 32;
const int sfReplace         = 64;
const int sfAgain           = 128;
const int sfWrapped         = 256;
const int sfFinished        = 512;
//dialog results
const int srYes             = QDialog::Accepted;
const int srNo              = 10;
const int srAll             = 11;
const int srCancel          = QDialog::Rejected;

//config flags
const int cfAutoIndent      = 1;
const int cfBackspaceIndent = 2;
const int cfReplaceTabs     = 8;
const int cfRemoveSpaces    = 16;
const int cfAutoBrackets    = 64;

const int cfPersistent      = 128;
const int cfKeepSelection   = 256;
const int cfVerticalSelect  = 512;
const int cfDelOnInput      = 1024;
const int cfXorSelect       = 2048;

const int cfOvr             = 4096;
const int cfMark            = 8192;

const int cfWrapCursor      = 16384;

//update flags
const int ufDocGeometry     = 1;
const int ufUpdateOnScroll  = 2;
const int ufPos             = 4;

//load flags
const int lfInsert          = 1;
const int lfNewFile         = 2;
const int lfNoAutoHl        = 4;

void resizeBuffer(void *user, int w, int h);

struct PointStruc {
  int x;
  int y;
};

struct VConfig {
  PointStruc cursor;
  int flags;
};

struct SConfig {
  PointStruc cursor;
  PointStruc startCursor;
  int flags;
};

struct gutterData
{
  gutterData( void* dataId, bool isBegin, QString name )
  {
    id = dataId;
    begin = isBegin;
    gutterName = name;
  }
  void* id;
  bool begin;
  QString gutterName;
};

class Attribute {
public:
  Attribute(): font(), fm(font) {};
  QColor col;
  QColor selCol;

  void setFont(const QFont& f)
  {
    font = f;
    fm = QFontMetrics(f);
    //workaround for slow QFontMetrics::width(), QFont::fixedPitch() doesn't seem to work
    if ((fontWidth = fm.width('W')) != fm.width('i')) fontWidth = -1;
  }

  QFont font;
  QFontMetrics fm;
  int width(QChar c) {return (fontWidth < 0) ? fm.width(c) : fontWidth;}
  int width(QString s) {return (fontWidth < 0) ? fm.width(s) : s.length()*fontWidth;}

protected:
  int fontWidth;
};

#endif


