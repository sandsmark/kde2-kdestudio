#include "kwrite.h"
#include "kwmanager.h"

#include <qmessagebox.h>
#include <qfileinfo.h>

QString KWrite::internalClipboard = QString::null;

void addToStrList(QStrList &list, const char *str)
{
  if (list.find(str) != -1) list.remove();
  if (list.count() >= 16) list.removeLast();
  list.insert(0, str);
}

KWrite::KWrite( KWriteManager* writeManager, KWriteDoc *doc, QWidget *parent, const char *name )
: QWidget(parent, name)
{
  stepLine = -1;

  manager = writeManager;

  kWriteDoc = doc;
  kWriteView = new KWriteView(this,doc);
  setFocusProxy( kWriteView );

  configFlags = 0;
  replacePrompt = 0L;

  kWriteView->setFocus();
}

KWrite::~KWrite()
{
  delete kWriteView;
}


int KWrite::currentLine()
{
  return kWriteView->cursor.y;
}

int KWrite::currentColumn()
{
  return kWriteDoc->currentColumn(kWriteView->cursor);
}

void KWrite::setCursorPosition(int line, int col)
{
  PointStruc cursor;
  cursor.x = col;
  cursor.y = line;
  kWriteView->updateCursor(cursor);
  kWriteDoc->unmarkFound();
  kWriteView->updateView(ufPos, 0, line*kWriteDoc->fontHeight - height()/2);
  kWriteDoc->updateViews(kWriteView); //uptade all other views except this one
}

int KWrite::config()
{
  return configFlags;
}

void KWrite::setConfig(int flags)
{
  if (flags != configFlags) {
    configFlags = flags;
    emit newStatus();
  }
}

bool KWrite::isModified()
{
  return kWriteDoc->modified;
}

void KWrite::setModified(bool m)
{
  kWriteDoc->setModified(m);
}

bool KWrite::isLastView()
{
  return kWriteDoc->isLastView(1);
}

KWriteDoc *KWrite::doc()
{
  return kWriteDoc;
}

int KWrite::undoState()
{
  return kWriteDoc->undoState;
}

void KWrite::copySettings(KWrite *w)
{
  configFlags = w->configFlags;
}

void KWrite::toggleVertical()
{
  setConfig(configFlags ^ cfVerticalSelect);
  emit statusMsg(i18n(configFlags & cfVerticalSelect ? "Vertical Selections On" : "Vertical Selections Off"));
}

void KWrite::toggleOverwrite()
{
  setConfig(configFlags ^ cfOvr);
}

QString KWrite::text()
{
  return kWriteDoc->text();
}

QString KWrite::markedText()
{
  return kWriteDoc->markedText(configFlags);
}

QString KWrite::currentWord()
{
  return kWriteDoc->currentWord(kWriteView->cursor);
}

QString KWrite::word(int x, int y)
{
  PointStruc cursor;
  cursor.y = (kWriteView->yPos + y)/kWriteDoc->fontHeight;
  if (cursor.y < 0 || cursor.y > kWriteDoc->lastLine()) return QString();
  cursor.x = kWriteDoc->textPos(kWriteDoc->textLine(cursor.y), kWriteView->xPos-2 + x);
  return kWriteDoc->currentWord(cursor);
}

void KWrite::setText(const char *s)
{
  kWriteDoc->setText(s);
  kWriteDoc->updateViews();
}

void KWrite::writeFile(QIODevice &dev)
{
  kWriteDoc->writeFile(dev);
  kWriteDoc->updateViews();
}

bool KWrite::loadFile(const char *name)
{
  QFileInfo info(name);
  if (!info.exists()) {
    QMessageBox::warning(this,
      i18n("Sorry"),
      i18n("The specified File does not exist"),
      i18n("OK"),
      "",
      "",
      0,0);
    return false;
  }
  if (info.isDir()){
    QMessageBox::warning(this,
      i18n("Sorry"),
      i18n("You have specified a directory"),
      i18n("OK"),
      "",
      "",
      0,0);
    return false;
  }
  if (!info.isReadable()){
    QMessageBox::warning(this,
      i18n("Sorry"),
      i18n("You do not have read permission to this file"),
      i18n("OK"),
      "",
      "",
      0,0);
    return false;
  }

  QFile f(name);
  if (f.open(IO_ReadOnly)){
    kWriteDoc->loadFile(f);
    kWriteDoc->setFileName( name );
    f.close();
    return true;
  }
  QMessageBox::warning(this,
    i18n("Sorry"),
    i18n("An Error occured while trying to open this Document"),
    i18n("OK"),
    "",
    "",
    0,0);
  return false;
}

bool KWrite::writeFile(const char *name)
{
  QFileInfo info(name);
  if(info.exists() && !info.isWritable()){
    QMessageBox::warning(this,
      i18n("Sorry"),
      i18n("You do not have write permission to this file"),
      i18n("OK"),
      "",
      "",
      0,0);
    return false;
  }

  QFile f(name);
  if (f.open(IO_WriteOnly | IO_Truncate)){
    writeFile(f);
    f.close();
    return true;
  }
  QMessageBox::warning(this,
    i18n("Sorry"),
    i18n("An Error occured while trying to open this Document"),
    i18n("OK"),
    "",
    "",
    0,0);
  return false;
}

const char *KWrite::fileName()
{
  return kWriteDoc->fileName();
}

void KWrite::setFileName(const char *s)
{
  kWriteDoc->setFileName(s);
}

void KWrite::newDoc()
{
  clear();
}

void KWrite::save()
{
  if (isModified())
    writeFile(kWriteDoc->fileName());
  else
    emit statusMsg(i18n("No changes need to be saved"));
}

void KWrite::clear()
{
  kWriteDoc->clear();
  kWriteDoc->setFileName(0);
  kWriteDoc->updateViews();
}

void KWrite::cut()
{
  VConfig c;
  kWriteView->getVConfig(c);
  kWriteDoc->cut(kWriteView,c);
  kWriteDoc->updateViews();
}

void KWrite::copy()
{
  kWriteDoc->copy(configFlags);
}

void KWrite::paste()
{
  VConfig c;
  kWriteView->getVConfig(c);
  kWriteDoc->paste(kWriteView,c);
  kWriteDoc->updateViews();
}

void KWrite::delMarkedText()
{
  VConfig c;
  kWriteView->getVConfig(c);
  kWriteDoc->delMarkedText(kWriteView,c);
  kWriteDoc->updateViews();
}

void KWrite::pasteStr(QString s)
{
  VConfig c;
  kWriteView->getVConfig(c);
  kWriteDoc->pasteStr(kWriteView,c,s);
  kWriteDoc->updateViews();
}

void KWrite::undo()
{
  kWriteDoc->undo(kWriteView,configFlags);
  kWriteDoc->updateViews();
}

void KWrite::redo()
{
  kWriteDoc->redo(kWriteView,configFlags);
  kWriteDoc->updateViews();
}

void KWrite::indent()
{
  VConfig c;
  kWriteView->getVConfig(c);
  kWriteDoc->indent(kWriteView,c);
  kWriteDoc->updateViews();
}

void KWrite::unIndent() {
  VConfig c;
  kWriteView->getVConfig(c);
  kWriteDoc->unIndent(kWriteView,c);
  kWriteDoc->updateViews();
}

void KWrite::selectAll()
{
  kWriteDoc->selectAll();
  kWriteDoc->updateViews();
}

void KWrite::deselectAll()
{
  kWriteDoc->deselectAll();
  kWriteDoc->updateViews();
}

void KWrite::invertSelection()
{
  kWriteDoc->invertSelection();
  kWriteDoc->updateViews();
}

void KWrite::search()
{
  SearchDialog *searchDialog;
  searchDialog = new SearchDialog(&(manager->searchForList), 0L, manager->searchFlags & ~sfReplace, markedText(), topLevelWidget());

  if (searchDialog->exec() == QDialog::Accepted) {
    addToStrList(manager->searchForList, searchDialog->getSearchFor());
    manager->searchFlags = searchDialog->getFlags() | (manager->searchFlags & sfPrompt);
    initSearch(s, manager->searchFlags);
    searchAgain(s);
  }
  delete searchDialog;
}

void KWrite::replace()
{
  SearchDialog *searchDialog;
  searchDialog = new SearchDialog(&(manager->searchForList), &(manager->replaceWithList), manager->searchFlags | sfReplace, markedText(), topLevelWidget());

  if (searchDialog->exec() == QDialog::Accepted) {
    addToStrList(manager->searchForList, searchDialog->getSearchFor());
    addToStrList(manager->replaceWithList, searchDialog->getReplaceWith());
    manager->searchFlags = searchDialog->getFlags();
    initSearch(s, manager->searchFlags);
    replaceAgain();
  }
  delete searchDialog;
}

void KWrite::searchAgain()
{
  initSearch(s,manager->searchFlags | sfFromCursor | sfPrompt | sfAgain);
  if (s.flags & sfReplace) replaceAgain(); else searchAgain(s);
}

void KWrite::gotoLine()
{
  GotoLineDialog *dlg;
  PointStruc cursor;

  dlg = new GotoLineDialog(this, kWriteView->cursor.y + 1, (int)kWriteDoc->contents.count());
  if (dlg->exec() == QDialog::Accepted) {
    cursor.x = 0;
    cursor.y = dlg->getLine() - 1;
    kWriteView->updateCursor(cursor);
    kWriteView->updateView(ufUpdateOnScroll);
  }
  delete dlg;
}


void KWrite::initSearch(SConfig &s, int flags)
{
  const char *searchFor = manager->searchForList.getFirst();
  s.flags = flags;
  if (s.flags & sfFromCursor) {
    s.cursor = kWriteView->cursor;
  } else {
    if (!(s.flags & sfBackward)) {
      s.cursor.x = 0;
      s.cursor.y = 0;
    } else {
      s.cursor.x = -1;
      s.cursor.y = kWriteDoc->lastLine();
    }
    s.flags |= sfFinished;
  }
  if (!(s.flags & sfBackward)) {
    if (!(s.cursor.x || s.cursor.y)) s.flags |= sfFinished;
  } else {
    s.startCursor.x -= strlen(searchFor);
  }
  s.startCursor = s.cursor;
}

void KWrite::continueSearch(SConfig &s)
{
  if (!(s.flags & sfBackward)) {
    s.cursor.x = 0;
    s.cursor.y = 0;
  } else {
    s.cursor.x = -1;
    s.cursor.y = kWriteDoc->lastLine();
  }
  s.flags |= sfFinished;
  s.flags &= ~sfAgain;
}

void KWrite::searchAgain(SConfig &s)
{
  int query;
  PointStruc cursor;
  int slen;
  QString str;

  const char *searchFor = manager->searchForList.getFirst();

  slen = strlen(searchFor);
  do {
    query = 1;
    if (kWriteDoc->doSearch(s,searchFor)) {
      cursor = s.cursor;
      if (!(s.flags & sfBackward)) s.cursor.x += slen;
      kWriteView->updateCursor(s.cursor);
      exposeFound(cursor,slen,(s.flags & sfAgain) ? 0 : ufUpdateOnScroll,false);
    } else {
      if (!(s.flags & sfFinished)) {
        // ask for continue
        if (!(s.flags & sfBackward)) {
          // forward search
          str = i18n("End of document reached\n Continue from the beginning?");
          query = QMessageBox::information(this,
            i18n("Find"),
            str,
            i18n("Yes"),
            i18n("No"),
            "",0,1);
        } else {
          // backward search
          str = i18n("Beginning of document reached\n Continue from the end?");
          query = QMessageBox::information(this,
            i18n("Find"),
            str,
            i18n("Yes"),
            i18n("No"),
            "",0,1);
        }
        continueSearch(s);
      } else {
        // wrapped
        QMessageBox::information(this,
          i18n("Find"),
          i18n("Search string not found!"),
          i18n("OK"),
          "",
          "",0,0);
      }
    }
  } while (query == 0);
}

void KWrite::replaceAgain()
{
  replaces = 0;
  if (s.flags & sfPrompt) {
    doReplaceAction(-1);
  } else {
    doReplaceAction(srAll);
  }
}

void KWrite::doReplaceAction(int result, bool found)
{
  int slen, rlen;
  PointStruc cursor;
  bool started;

  QString searchFor = manager->searchForList.getFirst();
  QString replaceWith = manager->replaceWithList.getFirst();
  slen = searchFor.length();
  rlen = replaceWith.length();

  switch (result) {
    case srYes: //yes
      kWriteDoc->recordStart(s.cursor,true);
      kWriteDoc->recordReplace(s.cursor,slen,replaceWith,rlen);
      replaces++;
      if (s.cursor.y == s.startCursor.y && s.cursor.x < s.startCursor.x)
        s.startCursor.x += rlen - slen;
      if (!(s.flags & sfBackward)) s.cursor.x += rlen;
      kWriteDoc->recordEnd(kWriteView,s.cursor,configFlags);
      break;
    case srNo: //no
      if (!(s.flags & sfBackward)) s.cursor.x += slen;
      break;
    case srAll: //replace all
      do {
        started = false;
        while (found || kWriteDoc->doSearch(s,searchFor)) {
          if (!started) {
            found = false;
            kWriteDoc->recordStart(s.cursor);
            started = true;
          }
          kWriteDoc->recordReplace(s.cursor,slen,replaceWith,rlen);
          replaces++;
          if (s.cursor.y == s.startCursor.y && s.cursor.x < s.startCursor.x)
            s.startCursor.x += rlen - slen;
          if (!(s.flags & sfBackward)) s.cursor.x += rlen;
        }
        if (started) kWriteDoc->recordEnd(kWriteView,s.cursor,configFlags);
      } while (!askReplaceEnd());
      return;
    case srCancel: //cancel
      deleteReplacePrompt();
      return;
    default:
      replacePrompt = 0L;
  }

  do {
    if (kWriteDoc->doSearch(s,searchFor)) {
      cursor = s.cursor;
      if (!(s.flags & sfBackward)) cursor.x += slen;
      kWriteView->updateCursor(cursor);
      exposeFound(s.cursor,slen,(s.flags & sfAgain) ? 0 : ufUpdateOnScroll,true);
      if (!replacePrompt) {
        replacePrompt = new ReplacePrompt(this);
        kWriteDoc->setPseudoModal(replacePrompt);
        connect(replacePrompt,SIGNAL(clicked()),this,SLOT(replaceSlot()));
        replacePrompt->show();
      }
      return;
    }
  } while (!askReplaceEnd());
  deleteReplacePrompt();
}

void KWrite::exposeFound(PointStruc &cursor, int slen, int flags, bool replace)
{
  TextLine *textLine;
  int x1, x2, y1, y2, xPos, yPos;

  kWriteDoc->markFound(cursor,slen);

  textLine = kWriteDoc->textLine(cursor.y);
  x1 = kWriteDoc->textWidth(textLine,cursor.x)        -10;
  x2 = kWriteDoc->textWidth(textLine,cursor.x + slen) +20;
  y1 = kWriteDoc->fontHeight*cursor.y                 -10;
  y2 = y1 + kWriteDoc->fontHeight                     +30;

  xPos = kWriteView->xPos;
  yPos = kWriteView->yPos;

  if (x1 < 0) x1 = 0;
  if (replace) y2 += 90;

  if (x1 < xPos || x2 > xPos + kWriteView->width()) {
    xPos = x2 - kWriteView->width();
  }
  if (y1 < yPos || y2 > yPos + kWriteView->height()) {
    xPos = x2 - kWriteView->width();
    yPos = kWriteDoc->fontHeight*cursor.y - height()/3;
  }
  kWriteView->updateView(flags | ufPos,xPos,yPos);
  kWriteDoc->updateViews(kWriteView);
}

void KWrite::deleteReplacePrompt()
{
  kWriteDoc->setPseudoModal(0L);
}

bool KWrite::askReplaceEnd()
{
  QString str;
  int query;

  kWriteDoc->updateViews();
  if (s.flags & sfFinished) {
    // replace finished
    str = i18n("%1 replace(s) made").arg(replaces);
    QMessageBox::information(this,i18n("Replace"),str);
    return true;
  }

  // ask for continue
  if (!(s.flags & sfBackward)) {
    // forward search
    str = i18n("%1 replace(s) made.\n"
	       "End of document reached.\n"
	       "Continue from the beginning?").arg(replaces);
  query = QMessageBox::information(this,
      i18n("Replace"),
      str,
      i18n("Yes"),
      i18n("No"),
      "",0,1);
  } else {
    // backward search
    str = i18n("%1 replace(s) made.\n"
		"Beginning of document reached.\n"
		"Continue from the end?").arg(replaces);
    query = QMessageBox::information(this,
      i18n("Replace"),
      str,
      i18n("Yes"),
      i18n("No"),
      "",0,1);
  }
  replaces = 0;
  continueSearch(s);
  return query;
}

void KWrite::replaceSlot()
{
  doReplaceAction(replacePrompt->result(),true);
}

void KWrite::setHl( int n )
{
  kWriteDoc->setHighlight(n);
  kWriteDoc->updateViews();
}

void KWrite::paintEvent(QPaintEvent *e)
{
  int x, y;

  QRect updateR = e->rect();

  int ux1 = updateR.x();
  int uy1 = updateR.y();
  int ux2 = ux1 + updateR.width();
  int uy2 = uy1 + updateR.height();

  QPainter paint;
  paint.begin(this);

  QColorGroup g = colorGroup();
  x = width();
  y = height();

  paint.setPen(g.dark());
  if (uy1 <= 0) paint.drawLine(0,0,x-2,0);
  if (ux1 <= 0) paint.drawLine(0,1,0,y-2);

  paint.setPen(black);
  if (uy1 <= 1) paint.drawLine(1,1,x-3,1);
  if (ux1 <= 1) paint.drawLine(1,2,1,y-3);

  paint.setPen(g.midlight());
  if (uy2 >= y-1) paint.drawLine(1,y-2,x-3,y-2);
  if (ux2 >= x-1) paint.drawLine(x-2,1,x-2,y-2);

  paint.setPen(g.light());
  if (uy2 >= y) paint.drawLine(0,y-1,x-2,y-1);
  if (ux2 >= x) paint.drawLine(x-1,0,x-1,y-1);

  x -= 2 + 16;
  y -= 2 + 16;
  if (ux2 > x && uy2 > y) {
    paint.fillRect(x,y,16,16,g.background());
  }
  paint.end();
}

void KWrite::resizeEvent(QResizeEvent *)
{
  kWriteView->tagAll();
  kWriteView->updateView(0);
}

/******************************************   GUTTER   *******************/
void KWrite::setGutterLine(int line, void* id, bool begin , QString name )
{
  TextLine *tx = kWriteDoc->textLine( line );
  if ( tx == 0L ) return;
  tx->setGutter( id, begin, name );
  kWriteDoc->tagLines( line, line );
  kWriteDoc->updateViews();
}

void KWrite::removeAllGutter()
{
  for ( int k = 0; k < kWriteDoc->lastLine() + 1; k++ )
  {
    kWriteDoc->textLine(k)->removeGutter();
  }

  kWriteDoc->tagAll();
  kWriteDoc->updateViews();
}

gutterData* KWrite::getGutter( int line )
{
  TextLine *tx = kWriteDoc->textLine( line );
  if ( tx == 0L ) return 0;
  return tx->getGutter();
}

void KWrite::gotoGutter( void* id )
{
  int y = 0;
  for ( int k = 0; k < kWriteDoc->lastLine() + 1; k++ ){
    gutterData* gData = kWriteDoc->textLine(k)->getGutter();
    if ( gData != 0L ){
      if ( gData->id == id ){
        y = k;
        break;
      }
    }
  }
  setCursorPosition( y, 0 );
}
/******************************************   STEPLINE   *******************/
void KWrite::setStepLine( int line )
{
  PointStruc cursor;
  cursor.x = 0;
  cursor.y = line;
  stepLine = line;
  kWriteView->updateCursor(cursor);
  kWriteDoc->unmarkFound();
  kWriteDoc->tagLines( line, line );
  kWriteDoc->updateViews();
}

void KWrite::clearStepLine()
{
  kWriteDoc->tagLines( stepLine, stepLine );
  stepLine = -1;
  kWriteDoc->updateViews();
}
/*****************************************************************/
QPoint KWrite::getTextCursorPosition() const
{
  int h,x,y;

  h = kWriteDoc->fontHeight;
  y = h*kWriteView->cursor.y - kWriteView->yPos;
  x = kWriteView->cXPos - (kWriteView->xPos-2) + 27;
  return QPoint(x,y);
}

void KWrite::updateView()
{
  kWriteDoc->tagAll();
  kWriteDoc->updateViews();
}
