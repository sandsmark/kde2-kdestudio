#include <qstring.h>
#include <qwidget.h>
#include <qfont.h>
#include <qpainter.h>
#include <qkeycode.h>
#include <qmessagebox.h>
#include <qpixmap.h>
#include <qfileinfo.h>
#include <qfile.h>
#include <qdir.h>
#include <qintdict.h>

#include <kapp.h>
#include <klocale.h>
#include <kconfig.h>
#include "kwview.h"
#include "kwmanager.h"

#include <X11/Xlib.h>


struct BufferInfo {
  void *user;
  int w;
  int h;
};

QList<BufferInfo> bufferInfoList;
QPixmap *buffer = 0;

QPixmap *getBuffer(void *user)
{
  BufferInfo *info;

  if (!buffer) buffer = new QPixmap;
  info = new BufferInfo;
  info->user = user;
  info->w = 0;
  info->h = 0;
  bufferInfoList.append(info);
  return buffer;
}

void resizeBuffer(void *user, int w, int h) {
  int z;
  BufferInfo *info;
  int maxW, maxH;

  maxW = w;
  maxH = h;
  for (z = 0; z < (int) bufferInfoList.count(); z++) {
    info = bufferInfoList.at(z);
    if (info->user == user) {
      info->w = w;
      info->h = h;
    } else {
      if (info->w > maxW) maxW = info->w;
      if (info->h > maxH) maxH = info->h;
    }
  }
  if (maxW != buffer->width() || maxH != buffer->height()) {
    buffer->resize(maxW,maxH);
  }
}

void releaseBuffer(void *user)
{
  int z;
  BufferInfo *info;

  for (z = (int) bufferInfoList.count() -1; z >= 0 ; z--) {
    info = bufferInfoList.at(z);
    if (info->user == user) bufferInfoList.remove(z);
  }
  resizeBuffer(0,0,0);
}

KWriteView::KWriteView(KWrite *write, KWriteDoc *doc)
: QWidget(write)
{
  kWrite = write;
  kWriteDoc = doc;

  setCursor(ibeamCursor);
  setMouseTracking(true);
  setBackgroundMode( NoBackground );
  setFocusPolicy(StrongFocus);
  move(2,2);

  xScroll = new QScrollBar(QScrollBar::Horizontal,write);
  yScroll = new QScrollBar(QScrollBar::Vertical,write);
  connect(xScroll,SIGNAL(valueChanged(int)),SLOT(changeXPos(int)));
  connect(yScroll,SIGNAL(valueChanged(int)),SLOT(changeYPos(int)));

  xPos = 0;
  yPos = 0;

  scrollTimer = 0;

  cursor.x = 0;
  cursor.y = 0;
  cursorOn = false;
  cursorTimer = 0;
  cXPos = 0;
  cOldXPos = 0;
  exposeCursor = false;

  startLine = 0;
  endLine = 0;
  updateState = 0;

  drawBuffer = getBuffer(this);

  doc->registerView(this);
}

KWriteView::~KWriteView()
{
  kWriteDoc->removeView(this);
  releaseBuffer(this);
}


bool KWriteView::event( QEvent *e )
{
  #undef KeyPress
  #undef KeyRelease

  if ( e->type() == QEvent::KeyPress ){
    QKeyEvent* k = (QKeyEvent*)e;
    if ( k->key() == Key_Tab ){
      keyPressEvent( k );
      return true;
    }
  }
  return QWidget::event( e );
}

void KWriteView::cursorLeft(VConfig &c)
{
  cursor.x--;
  if ( cursor.x < 0 && cursor.y > 0 ){
    cursor.y--;
    cursor.x = kWriteDoc->textLength(cursor.y);
  }
  cOldXPos = cXPos = kWriteDoc->textWidth(cursor);
  update(c);
}

void KWriteView::cursorRight(VConfig &c)
{
  if (c.flags & cfWrapCursor) {
    if (cursor.x >= kWriteDoc->textLength(cursor.y)) {
      if (cursor.y == kWriteDoc->lastLine()) return;
      cursor.y++;
      cursor.x = -1;
    }
  }
  cursor.x++;
  cOldXPos = cXPos = kWriteDoc->textWidth(cursor);

  update(c);
}

void KWriteView::cursorLeftWord(VConfig &c)
{
  TextLine *textLine;
  Highlight *highlight;

  highlight = kWriteDoc->highlight;
  textLine = kWriteDoc->textLine(cursor.y);
  do {
    if (cursor.x <= 0) {
      if ( cursor.y > 0) {
        cursor.y--;
        textLine = kWriteDoc->textLine(cursor.y);
        cursor.x = textLine->length() -1;
      } else break;
    } else cursor.x--;
  } while (cursor.x < 0 || !highlight->isInWord(textLine->getChar(cursor.x)));
  while (cursor.x > 0 && highlight->isInWord(textLine->getChar(cursor.x -1)))
  cursor.x--;

  cOldXPos = cXPos = kWriteDoc->textWidth(cursor);
  update(c);
}

void KWriteView::cursorRightWord(VConfig &c)
{
  TextLine *textLine;
  Highlight *highlight;
  int len;

  highlight = kWriteDoc->highlight;
  textLine = kWriteDoc->textLine(cursor.y);
  len = textLine->length();
  while (cursor.x < len && highlight->isInWord(textLine->getChar(cursor.x)))
    cursor.x++;
  do {
    if (cursor.x >= len) {
      if (cursor.y < kWriteDoc->lastLine()) {
        cursor.y++;
        textLine = kWriteDoc->textLine(cursor.y);
        len = textLine->length();
        cursor.x = 0;
      } else break;
    } else cursor.x++;
  } while (cursor.x >= len || !highlight->isInWord(textLine->getChar(cursor.x)));

  cOldXPos = cXPos = kWriteDoc->textWidth(cursor);
  update(c);
}

void KWriteView::cursorUp(VConfig &c)
{
  cursor.y--;
  cXPos = kWriteDoc->textWidth(cursor, cOldXPos);
  update(c);
}

void KWriteView::cursorDown(VConfig &c)
{
  int x;
  if ( cursor.y == kWriteDoc->lastLine() ) {
    x = kWriteDoc->textLength( cursor.y );
    if (cursor.x >= x) return;
    cursor.x = x;
    cXPos = kWriteDoc->textWidth( cursor );
  } else {
    cursor.y++;
    cXPos = kWriteDoc->textWidth(cursor,cOldXPos);
  }  
  update(c);
}

void KWriteView::home(VConfig &c)
{
  cursor.x = 0;
  cOldXPos = cXPos = 0;
  update(c);
}

void KWriteView::end(VConfig &c)
{
  cursor.x = kWriteDoc->textLength(cursor.y);
  cOldXPos = cXPos = kWriteDoc->textWidth(cursor);
  update(c);
}

void KWriteView::pageUp(VConfig &c)
{
  cursor.y -= endLine - startLine;
  cXPos = kWriteDoc->textWidth(cursor,cOldXPos);
  update(c);
}

void KWriteView::pageDown(VConfig &c)
{
  cursor.y += endLine - startLine;
  cXPos = kWriteDoc->textWidth(cursor,cOldXPos);
  update(c);
}

void KWriteView::top(VConfig &c)
{
  cursor.x = 0;
  cursor.y = 0;
  cOldXPos = cXPos = 0;
  update(c);
}

void KWriteView::bottom(VConfig &c)
{
  cursor.x = 0;
  cursor.y = kWriteDoc->lastLine();
  cOldXPos = cXPos = 0;
  update(c);
}


void KWriteView::changeXPos(int p)
{
  int dx;
  dx = xPos - p;
  xPos = p;
  if (QABS(dx) < width()-27)
  scrollW(dx,0);
  else
  repaint(27, 0, width()-27, height());
}

void KWriteView::changeYPos(int p)
{
  int dy;
  dy = yPos - p;
  yPos = p;
  startLine = yPos / kWriteDoc->fontHeight;
  endLine = (yPos + height() -1) / kWriteDoc->fontHeight;
  if (QABS(dy) < height()) scrollW(0,dy);
    else repaint(27, 0, width()-27, height());

}

void KWriteView::getVConfig(VConfig &c)
{
  c.cursor = cursor;
  c.flags = kWrite->configFlags;
}

void KWriteView::update(VConfig &c)
{
  if (cursor.x == c.cursor.x && cursor.y == c.cursor.y) return;
  exposeCursor = true;

  kWriteDoc->unmarkFound();
  
  if (cursorOn) {
    tagLines(c.cursor.y,c.cursor.y);
    cursorOn = false;
  }

  if (c.flags & cfMark) {
    kWriteDoc->selectTo(c.cursor,cursor,c.flags);
  } else {
    if (!(c.flags & cfPersistent)) kWriteDoc->deselectAll();
  }
}

void KWriteView::insLine(int line)
{
  if (line <= cursor.y) {
    cursor.y++;
  }
  if (line < startLine) {
    startLine++;
    endLine++;
    yPos += kWriteDoc->fontHeight;
  } else {
    if (line <= endLine)
      tagAll();
  }  
}

void KWriteView::delLine( int line )
{
  emit kWrite->deleteLine( line );

  if (line <= cursor.y && cursor.y > 0) {
    cursor.y--;
  }
  if (line < startLine) {
    startLine--;
    endLine--;
    yPos -= kWriteDoc->fontHeight;
  } else if (line <= endLine) {
    tagAll();
  }  
}

void KWriteView::updateCursor()
{
  cOldXPos = cXPos = kWriteDoc->textWidth(cursor);
}

void KWriteView::updateCursor(PointStruc &newCursor)
{
  exposeCursor = true;
  if (cursorOn) {
    tagLines(cursor.y,cursor.y);
    cursorOn = false;
  }
  cursor = newCursor;
  cOldXPos = cXPos = kWriteDoc->textWidth(cursor);
}

void KWriteView::updateView(int flags, int newXPos, int newYPos)
{
  int fontHeight;
  int oldXPos, oldYPos;
  int w, h;
  int z;
  bool b;
  int xMax, yMax;
  int cYPos;
  int cXPosMin, cXPosMax, cYPosMin, cYPosMax;
  int dx, dy;

  if (exposeCursor || flags & ufDocGeometry)
  {
    emit kWrite->newCurPos();
  } else {
   if (updateState == 0)
     return;
  }

  if (cursorTimer){
    killTimer(cursorTimer);
    cursorTimer = startTimer(500);
    cursorOn = true;
  }

  oldXPos = xPos;
  oldYPos = yPos;
  if (flags & ufPos){
    xPos = newXPos;
    yPos = newYPos;
    exposeCursor = true;
  }

  fontHeight = kWriteDoc->fontHeight;
  cYPos = cursor.y*fontHeight;

  z = 0;
  do {
    w = kWrite->width() - 4;
    h = kWrite->height() - 4;

    xMax = kWriteDoc->textWidth() - w + 27;
    b = (xPos > 0 || xMax > 0);
    if (b) h -= 16;
    yMax = kWriteDoc->textHeight() - h;
    if (yPos > 0 || yMax > 0) {
      w -= 16;
      xMax += 16;
      if (!b && xMax > 0) {
        h -= 16;
        yMax += 16;
      }
    }

    if (!exposeCursor) break;

    cXPosMin = xPos + 4;
    cXPosMax = xPos + w - 8 - 27;
    cYPosMin = yPos;
    cYPosMax = yPos + (h - fontHeight);

    if (cXPos < cXPosMin) {
      xPos -= cXPosMin - cXPos;
    }
    if (xPos < 0) xPos = 0;
    if (cXPos > cXPosMax) {
      xPos += cXPos - cXPosMax;
    }
    if (cYPos < cYPosMin) {
      yPos -= cYPosMin - cYPos;
    }
    if (yPos < 0) yPos = 0;
    if (cYPos > cYPosMax) {
      yPos += cYPos - cYPosMax;
    }
    z++;
  } while (z < 2);

  if (xMax < xPos) xMax = xPos;
  if (yMax < yPos) yMax = yPos;

  if (xMax > 0){
    xScroll->blockSignals(true);
    xScroll->setGeometry(2,h + 2,w,16);
    xScroll->setRange(0,xMax);
    xScroll->setValue(xPos);
    xScroll->setSteps(fontHeight,w - 4 - 16);
    xScroll->blockSignals(false);
    xScroll->show();
  } else xScroll->hide();

  if (yMax > 0) {
    yScroll->blockSignals(true);
    yScroll->setGeometry(w + 2,2,16,h);
    yScroll->setRange(0,yMax);
    yScroll->setValue(yPos);
    yScroll->setSteps(fontHeight,h - 4 - 16);
    yScroll->blockSignals(false);
    yScroll->show();
  } else yScroll->hide();

  startLine = yPos / fontHeight;
  endLine = (yPos + h -1) / fontHeight;

  if (w != width() || h != height()) {
    resize(w,h);
  } else {
    dx = oldXPos - xPos;
    dy = oldYPos - yPos;

    b = updateState == 3;
    if (flags & ufUpdateOnScroll) {
      b |= dx || dy;
    } else {
      b |= QABS(dx)*3 > w*2 || QABS(dy)*3 > h*2;
    }

    if (b) {
      repaint(27, 0, width()-27, height(), false);
    } else {
      if (updateState > 0) paintTextLines(oldXPos,oldYPos);

      if (dx || dy) {
        scrollW(dx,dy);
      } else if (cursorOn) paintCursor();
    }
  }
  exposeCursor = false;
  updateState = 0;
}

void KWriteView::tagLines(int start, int end)
{
  int line, z;
  if (updateState < 3){
    if (start < startLine) start = startLine;
    if (end > endLine) end = endLine;

    if (end - start > 1) {
      updateState = 3;
    } else {
      for (line = start; line <= end; line++) {
        for (z = 0; z < updateState && updateLines[z] != line; z++);
        if (z == updateState) {
          updateState++;
          if (updateState > 2) break;
          updateLines[z] = line;
        }
      }
    }
  }
}

void KWriteView::tagAll()
{
  updateState = 3;
}

void KWriteView::paintTextLines(int xPos, int yPos)
{
  int xStart, xEnd;
  int line, z;
  int h;

  QPainter paint;
  paint.begin(drawBuffer);

  xStart = xPos-2;
  xEnd = xStart + width();
  h = kWriteDoc->fontHeight;
  for (z = 0; z < updateState; z++) {
    line = updateLines[z];
    kWriteDoc->paintTextLine(paint,line,xStart,xEnd);
    bitBlt(this,27,line*h - yPos,drawBuffer,0,0,width(),h);
    drawGutter(paint, line, h);
    bitBlt(this,0,line*h - yPos,drawBuffer,0,0,27,h);
  }
  paint.end();
}

void KWriteView::paintCursor()
{
  int h, y, x;

  h = kWriteDoc->fontHeight;
  y = h*cursor.y - yPos;
  x = cXPos - (xPos-2) + 27;
  if ( (x-2) < 27 ) return;

  QPainter paint;
  if (cursorOn) {
    h += y - 1;

    paint.begin(this);
    QColor bg = kWriteDoc->colors[0];
    paint.setPen( QColor( ~bg.red() & 0xFF, ~bg.green() & 0xFF, ~bg.blue() & 0xFF) );
    paint.drawLine(x,y,x,h);
    paint.drawLine(x-2,y,x+2,y);
    paint.drawLine(x-2,h,x+2,h);
    paint.end();

  } else {
    if ( !drawBuffer->isNull() ){
      paint.begin(drawBuffer);
      kWriteDoc->paintTextLine(paint,cursor.y,cXPos - 2, cXPos + 3);
      bitBlt(this,x - 2,y,drawBuffer,0,0,5,h);
      paint.end();
    }
  }
}

void KWriteView::placeCursor(int x, int y, int flags)
{
  VConfig c;
  getVConfig(c);
  c.flags |= flags;
  cursor.y = (yPos + y)/kWriteDoc->fontHeight;
  cXPos = cOldXPos = kWriteDoc->textWidth(cursor,xPos-2 + x);
  update(c);
}

void KWriteView::focusInEvent(QFocusEvent *)
{
  kWrite->newCurPos();
  kWrite->newStatus();
  kWrite->newCaption();
  kWrite->newUndo();

  if (!cursorTimer) {
    cursorTimer = startTimer(500);
    cursorOn = true;
    paintCursor();
  }
  emit kWrite->manager->activate(kWrite);
}

void KWriteView::focusOutEvent(QFocusEvent *)
{
  if (cursorTimer) {
    killTimer(cursorTimer);
    cursorTimer = 0;
  }

  if (cursorOn) {
    cursorOn = false;
    paintCursor();
  }

  emit kWrite->manager->deactivate(kWrite);
}

void KWriteView::keyPressEvent(QKeyEvent *e)
{
  VConfig c;
  bool t;

  if (e->state() & AltButton){
    e->accept();
    return;
  }

  getVConfig(c);

  if ((e->ascii() >= 32 || e->ascii() == '\t')
    && e->key() != Key_Delete && e->key() != Key_Backspace) {
    if (c.flags & cfDelOnInput) {
      kWriteDoc->delMarkedText(this,c);
      getVConfig(c);
    }
    kWriteDoc->insertChar(this,c,e->ascii());
  } else {
    if (e->state() & ShiftButton) c.flags |= cfMark;

    t = false;
    if ( e->state() & ControlButton ) {
      t = true;
      switch ( e->key() ) {
        case Key_N:
            kWriteDoc->newLine(this,c);
            break;
        case Key_Y:
            kWriteDoc->killLine(this,c);
            break;
        case Key_Delete:
            kWriteDoc->cut(this,c);
            break;
        case Key_Insert:
            kWriteDoc->copy(c.flags);
            break;
       	case Key_Left:
            cursorLeftWord(c);
            break;
       	case Key_Right:
            cursorRightWord(c);
            break;
        case Key_Home:
            top(c);
            break;
        case Key_End:
            bottom(c);
            break;
        case Key_Prior:
            pageUp(c);
            break;
        case Key_Next:
            pageDown(c);
            break;
        case Key_Up:
            cursorUp(c);
            break;
        case Key_Down:
            cursorDown(c);
            break;
        default:
            t = false;
      }
    }
    if (!t) {
      if (e->state() & ControlButton) c.flags |= cfMark | cfKeepSelection;
      switch ( e->key() ) {
        case Key_Left:
            cursorLeft(c);
            break;
        case Key_Right:
            cursorRight(c);
            break;
        case Key_Up:
            cursorUp(c);
            break;
        case Key_Down:
            cursorDown(c);
            break;
        case Key_Backspace:
            if ((c.flags & cfDelOnInput) && kWriteDoc->hasMarkedText())
              kWriteDoc->delMarkedText(this,c);
            else kWriteDoc->backspace(this,c);
            break;
        case Key_Home:
            home(c);
            break;
        case Key_End:
            end(c);
            break;
        case Key_Prior:
            pageUp(c);
            break;
        case Key_Next:
            pageDown(c);
            break;
        case Key_Delete:
            if (e->state() & ShiftButton) kWriteDoc->cut(this,c);
            else if ((c.flags & cfDelOnInput) && kWriteDoc->hasMarkedText())
              kWriteDoc->delMarkedText(this,c);
            else kWriteDoc->del(this,c);
            break;
        case Key_Enter:
        case Key_Return:
            if ((c.flags & cfDelOnInput) && kWriteDoc->hasMarkedText())
              kWriteDoc->delMarkedText(this,c);
            kWriteDoc->newLine(this,c);
            break;
        case Key_Insert:
            if (e->state() & ShiftButton) kWriteDoc->paste(this,c);
              else kWrite->toggleOverwrite();
      }
    }
  }
  kWriteDoc->updateViews();
  e->accept();
}

void KWriteView::mousePressEvent(QMouseEvent *e)
{
  if (e->x() <= 27) {
    placeCursor( 0, e->y(), 0 );
    kWriteDoc->updateViews();
    emit kWrite->gutterClick( cursor.y );
    return;
  }

  QMouseEvent *ee = new QMouseEvent(QEvent::MouseButtonPress, QPoint(e->x()-27, e->y()), QPoint(e->globalX(), e->globalY()), e->button(), e->state());
  
  if (ee->button() == LeftButton) {
    int flags;

    flags = 0;
    if (ee->state() & ShiftButton) {
      flags |= cfMark;
      if (ee->state() & ControlButton) flags |= cfMark | cfKeepSelection;
    }
    placeCursor(ee->x(),ee->y(),flags);
    scrollX = 0;
    scrollY = 0;
    if (!scrollTimer) scrollTimer = startTimer(50);
    kWriteDoc->updateViews();
  }

  if (kWrite->manager->popup && ee->button() == RightButton) {
    placeCursor(ee->x(),ee->y(),0);
    kWrite->manager->popup->popup(mapToGlobal(ee->pos()));
  }

  kWrite->mousePressEvent(ee);
}

void KWriteView::wheelEvent( QWheelEvent *e )
{
  QApplication::sendEvent( yScroll, e );
}

void KWriteView::mouseDoubleClickEvent(QMouseEvent *e)
{
  if (e->button() == LeftButton) {
    VConfig c;
    getVConfig(c);
    kWriteDoc->selectWord(c.cursor, c.flags);
    kWriteDoc->updateViews();
  }
}

void KWriteView::mouseReleaseEvent(QMouseEvent *e)
{
  if (e->button() == LeftButton){
    kWrite->copy();
    killTimer(scrollTimer);
    scrollTimer = 0;
  }
}

void KWriteView::mouseMoveEvent(QMouseEvent *e)
{
  if (e->x() <= 27) {
    return;
  }
  
  QMouseEvent *ee = new QMouseEvent(QEvent::MouseMove, QPoint(e->x()-27, e->y()), QPoint(e->globalX(), e->globalY()), e->button(), e->state());

  if (ee->state() & LeftButton) {
    int flags;
    int d;

    mouseX = ee->x();
    mouseY = ee->y();
    scrollX = 0;
    scrollY = 0;
    d = kWriteDoc->fontHeight;
    if (mouseX < 0) {
      mouseX = 0;
      scrollX = -d;
    }
    if (mouseX > width()) {
      mouseX = width();
      scrollX = d;
    }
    if (mouseY < 0) {
      mouseY = 0;
      scrollY = -d;
    }
    if (mouseY > height()) {
      mouseY = height();
      scrollY = d;
    }

    flags = cfMark;
    if (ee->state() & ControlButton) flags |= cfKeepSelection;
    placeCursor(mouseX,mouseY,flags);
    kWriteDoc->updateViews();
  }
}

void KWriteView::paintEvent(QPaintEvent *e)
{
  int xStart, xEnd;
  int h;
  int line, y, yEnd;

  QRect updateR = e->rect();

  QPainter paint;
  paint.begin(drawBuffer);

  xStart = xPos-2 + updateR.x() - 27;
  xEnd = xStart + updateR.width();

  h = kWriteDoc->fontHeight;
  line = (yPos + updateR.y()) / h;
  y = line*h - yPos;
  yEnd = updateR.y() + updateR.height();

  while (y < yEnd) {
    kWriteDoc->paintTextLine(paint,line,xStart,xEnd);
    bitBlt(this,updateR.x(),y,drawBuffer,0,0,updateR.width(),h);
    drawGutter(paint, line, h);
    bitBlt(this,0,y,drawBuffer,0,0,27,h);
    line++;
    y += h;
  }
  paint.end();
  if (cursorOn) paintCursor();
}

void KWriteView::resizeEvent(QResizeEvent *)
{
  resizeBuffer(this,width(),kWriteDoc->fontHeight);
  repaint(27, 0, width()-27, height(), false);
}

void KWriteView::timerEvent(QTimerEvent *e)
{
  if (e->timerId() == cursorTimer) {
    cursorOn = !cursorOn;
    paintCursor();
  }
  if (e->timerId() == scrollTimer && (scrollX | scrollY)) {
    xScroll->setValue(xPos + scrollX);
    yScroll->setValue(yPos + scrollY);

    placeCursor(mouseX,mouseY,cfMark);
    kWriteDoc->updateViews();
  }
}

void KWriteView::scrollW( int dx, int dy )
{
  int x1, y1, x2, y2;
  int w = width();
  int h = height();

  if ( dx > 0 ){
    x1 = 0 + 27;
    x2 = dx + 27;
    w -= dx + 27;
  } else {
    x1 = -dx + 27;
    x2 = 27;
    w += dx - 27;
  }

  if ( dy > 0 ){
    y1 = 0;
    y2 = dy;
    h -= dy;
  } else {
    y1 = -dy;
    y2 = 0;
    h += dy;
  }

  if ( dx == 0 && dy == 0 )
    return;

  Display *dpy = x11Display();
  GC gc = qt_xget_readonly_gc();

  XSetGraphicsExposures( dpy, gc, TRUE );
  XCopyArea( dpy, winId(), winId(), gc, x1, y1, w, h, x2, y2 );
  if ( dy ) XCopyArea( dpy, winId(), winId(), gc, 0, y1, 27-1, h, 0, y2 );
  XSetGraphicsExposures( dpy, gc, FALSE );

  if ( dx ){
    x1 = (x2 == 27) ? w : 27;
    repaint( x1, 0, width()-w, height(), TRUE );
  }

  if ( dy ){
    y1 = (y2 == 0) ? h : 0;
    repaint( 27, y1, width()-27, height()-h, TRUE );
  }
}

void KWriteView::drawGutter(QPainter &paint, int line, int h)
{
//  #include "pix/breakpoint.xpm"
//  #include "pix/breakpoint_gr.xpm"
  #include "pix/ddd.xpm"

  #include "pix/blue_b.xpm"
  #include "pix/blue_b_end.xpm"
  #include "pix/red_b.xpm"
  #include "pix/red_b_end.xpm"
  #include "pix/green_b.xpm"

  paint.setBrushOrigin(0,-line*h);
  paint.fillRect( 0, 0, 25, h, QBrush( colorGroup().brush( QColorGroup::Background ) ));
  paint.setPen(white);
  paint.drawLine(25, 0, 25, h);
  paint.setPen( colorGroup().dark() );
  paint.drawLine(26, 0, 26, h);

  if (line < (int) kWriteDoc->contents.count()) {
  	gutterData* gData = kWriteDoc->textLine(line)->getGutter();
    if ( gData != 0L ){
      QString gname = gData->gutterName;
      if (gname == "attrib") {
        paint.drawPixmap( 6, h-12 , green_b );
      }
      if (gname == "decl_begin") {
        paint.drawPixmap( 6, h-12 , blue_b );
      }
      if (gname == "decl_end") {
        paint.drawPixmap( 4, h-10 , blue_b_end );
      }
      if (gname == "def_begin") {
        paint.drawPixmap( 6, h-12 , red_b );
      }
      if (gname == "def_end") {
        paint.drawPixmap( 4, h-10 , red_b_end );
      }

    }

    if ( kWrite->stepLine == line )
      paint.drawPixmap( 0, h-10 , ddd_xpm );

    QPixmap* pix = kWrite->manager->getPixmapForBookmarks( kWriteDoc->textLine(line) );
    if ( pix != 0L )
      paint.drawPixmap( 12, h-14 , *pix );
  }
}

