#ifndef KWRITE_H
#define KWRITE_H

class KConfig;
#include <qwidget.h>
#include <qstring.h>
#include <qiodevice.h>
#include <qpoint.h>

#include "kwtypes.h"

class KWriteManager;
class KWriteView;
class KWriteDoc;

class KWrite : public QWidget
{Q_OBJECT
friend class KWriteView;
friend class KWriteDoc;
friend class KWriteManager;
public:
  KWrite( KWriteManager*, KWriteDoc*, QWidget *parent = 0L, const char *name = 0L );
  ~KWrite();

  void updateView();

  QPoint getTextCursorPosition() const;

  void setBreakpoint( int line, int id, bool enabled = true );
  void delBreakpoint( int line );
  void gotoBreakpoint( int id );
  int  getBreakpoint( int line );

  void setStepLine( int line );
  void clearStepLine();
  int getStepLine(){ return stepLine; }

  void setGutterLine(int line, void* id, bool begin, QString name);
  gutterData* getGutter( int line );
  void removeAllGutter();
  void gotoGutter( void* id );

  int currentLine();
  int currentColumn();
  void setCursorPosition(int line, int col);
  int config();
  void setConfig(int);
  bool isModified();
  void setModified(bool);
  bool isLastView();
  KWriteDoc *doc();

  int undoState();
  void copySettings(KWrite *);

  QString text();
  QString currentWord();
  QString word(int x, int y);
  void setText(const char *);
  QString markedText();

  void clear();

  void writeFile(QIODevice &);
  bool loadFile(const char *name);
  bool writeFile(const char *name);

  const char *fileName();
  void setFileName(const char *);
  bool canDiscard();

  static QString internalClipboard;

public slots:
  void toggleVertical();
  void toggleOverwrite();
  void newDoc();
  void save();

  void cut();
  void copy();
  void paste();
  void pasteStr(QString s);
  void delMarkedText();
  void undo();
  void redo();
  void indent();
  void unIndent();
  void selectAll();
  void deselectAll();
  void invertSelection();

  void search();
  void replace();
  void searchAgain();
  void gotoLine();

  void setHl(int n);

signals:
  void newCurPos();
  void newStatus();
  void statusMsg(const char *);
  void newCaption();
  void newUndo();

  void gutterClick( int );
  void deleteLine( int );

protected slots:
  void replaceSlot();

protected:
  void initSearch(SConfig &, int flags);
  void continueSearch(SConfig &);
  void searchAgain(SConfig &);
  void replaceAgain();
  void doReplaceAction(int result, bool found = false);
  void exposeFound(PointStruc &cursor, int slen, int flags, bool replace);
  void deleteReplacePrompt();
  bool askReplaceEnd();

  virtual void paintEvent(QPaintEvent *);
  virtual void resizeEvent(QResizeEvent *);

private:
  KWriteManager* manager;
  KWriteView *kWriteView;
  KWriteDoc *kWriteDoc;

  int replaces;
  int configFlags;
  SConfig s;
  QDialog *replacePrompt;
  int stepLine;
};

#endif


