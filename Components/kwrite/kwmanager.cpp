#include "kwmanager.h"

#include <kapp.h>
#include <kconfig.h>
#include <qpopupmenu.h>

KWriteManager::KWriteManager()
{
  idCounter = 0;
  gdata.setAutoDelete( true );
  writes.setAutoDelete( false );
  popup = 0L;
  hlManager = new HlManager();
  searchFlags = 0;
  current = 0L;
  connect(this,SIGNAL(activate(KWrite*)),SLOT(setCurrentKWrite(KWrite*)));

  setConfig(0L);
}

void KWriteManager::setConfig( KConfig* c )
{
  if (c)
    m_pConfig = c;
  else
    m_pConfig = kapp->config();
}

KConfig* KWriteManager::config( bool forRead )
{
  KConfig* c = m_pConfig;
  if (!c->hasGroup("Editor Options") && forRead )
    c = kapp->config();

  c->setGroup("Editor Options");
  return c;
}

KWriteManager::~KWriteManager()
{
}

void KWriteManager::installRBPopup( QPopupMenu *p ){
  popup = p;
}

KWrite* KWriteManager::createKWrite( QWidget* parent, const char* name, KWriteDoc* doc )
{
  KWrite* w = new KWrite( this, doc ? doc:new KWriteDoc(hlManager), parent, name );
  readConfig(w);
  writes.append(w);
  connect(w,SIGNAL(destroyed()),SLOT(slotKWriteDestroyed()));
  return w;
}

void KWriteManager::slotKWriteDestroyed()
{
  writes.remove(sender());
}

void KWriteManager::configChanges( bool needUpdate )
{
  for ( QObject* o = writes.first(); o; o = writes.next() ) {
    KWrite* w = (KWrite*)o;
    readConfig(w);
    if (needUpdate) {
      w->doc()->tagAll();
      w->doc()->updateViews();
    }
  }
}

void KWriteManager::readConfig( KWrite* w )
{
  KConfig* c = config(true);
  w->configFlags = c->readNumEntry("ConfigFlags",cfReplaceTabs | cfDelOnInput) & ~cfMark;

  w->doc()->setTabWidth(c->readNumEntry("TabWidth",2));
  w->doc()->setUndoSteps(c->readNumEntry("UndoSteps",50));
  for (int z = 0; z < 5; z++) {
    QString s = QString("Color%1").arg(z);
    w->doc()->colors[z] = c->readColorEntry(s,&w->doc()->colors[z]);
  }
}

int KWriteManager::reservedBookmarks( const QPixmap& pix )
{
  int id  = idCounter++;
  BookmarksData* data = new BookmarksData;
  data->id = id;
  data->pix = new QPixmap( pix );
  gdata.append( data );
  return id;
}

QPixmap* KWriteManager::getPixmapForBookmarks( int id )
{
  for ( BookmarksData* data = gdata.first(); data; data = gdata.next() )
    if ( data->id == id ) return data->pix;
  return 0L;
}

QPixmap* KWriteManager::getPixmapForBookmarks( TextLine* line )
{
  QIntDict<int> *dict = line->bookmarkData;
  QIntDictIterator<int> it( *dict );
  return it.isEmpty() ? 0L:getPixmapForBookmarks( it.currentKey() );
}

/******************************************   BOOKMARK   *******************/
void KWriteManager::delBookmark( KWrite* write, int groupId, int id )
{
  for ( int k = 0; k < write->kWriteDoc->lastLine() + 1; k++ )
  {
    if ( getBookmark( write, groupId, k ) == id )
    {
      write->kWriteDoc->textLine( k )->bookmarkData->remove( groupId );
      write->kWriteDoc->tagLines( k, k );
      write->kWriteDoc->updateViews();
      break;
    }
  }
}

void KWriteManager::gotoBookmark( KWrite* write, int groupId, int id )
{
  int y = -1;
  for ( int k = 0; k < write->kWriteDoc->lastLine() + 1; k++ ){
    TextLine *tx = write->kWriteDoc->textLine( k );
    if ( tx ){
      QIntDict<int> *dict = tx->bookmarkData;
      QIntDictIterator<int> it( *dict );
      bool found = false;
      while ( it.current() ){
        if ( it.currentKey() == groupId && *it.current() == id ){
          found = true;
          break;
        }
        ++it;
      }
      if ( found ){
        y = k;
        break;
      }
    }
  }
  if ( y != -1 )
    write->setCursorPosition( y, 0 );
}

/* get first bookmark in group */
int KWriteManager::getBookmark( KWrite* write, int groupId, int line )
{
  TextLine *tx = write->kWriteDoc->textLine( line );
  if ( tx == 0L ) return 0;
  int *id = tx->bookmarkData->find( groupId );
  return id == 0L ? 0:*id;
}

void KWriteManager::setBookmark( KWrite* write, int groupId, int line, int id )
{
  TextLine *tx = write->kWriteDoc->textLine( line );
  if ( tx == 0L ) return;
  tx->bookmarkData->insert( groupId, new int(id) );
  write->kWriteDoc->tagLines( line, line );
  write->kWriteDoc->updateViews();
}

void KWriteManager::slotOptionsDlg()
{
  SettingsDialog *dlg;
  KConfig* c = config(true);

  int configFlags = c->readNumEntry("ConfigFlags",cfReplaceTabs | cfDelOnInput) & ~cfMark;
  int tabChars = c->readNumEntry("TabWidth",2);
  int undoSteps = c->readNumEntry("UndoSteps",50);

  dlg = new SettingsDialog(configFlags,tabChars,undoSteps,0L);
  if (dlg->exec() == QDialog::Accepted){
    KConfig* c = config();

    c->writeEntry("TabWidth",dlg->getTabWidth());
    c->writeEntry("UndoSteps",dlg->getUndoSteps());
    c->writeEntry("ConfigFlags",dlg->getFlags() | (configFlags & cfOvr));
    c->sync();

    configChanges(false);
  }
  delete dlg;
}

void KWriteManager::slotColorDlg()
{
  ColorDialog *dlg;

  QColor colors[5];
  KConfig* c = config(true);
  for (int z = 0; z < 5; z++) {
    QString s = QString("Color%1").arg(z);
    colors[z] = c->readColorEntry(s,&colors[z]);
  }

  dlg = new ColorDialog(colors,0L);
  if (dlg->exec() == QDialog::Accepted) {
    dlg->getColors(colors);
    KConfig* c = config();
    for (int z = 0; z < 5; z++) {
      QString s = QString("Color%1").arg(z);
      c->writeEntry(s,colors[z]);
    }
    configChanges(true);
  }
  delete dlg;
}

void KWriteManager::slotDefaultsDlg()
{
  DefaultsDialog *dlg;
  ItemFont defaultFont;
  ItemStyleList defaultStyleList;
  defaultStyleList.setAutoDelete(true);


  hlManager->getDefaults(defaultStyleList,defaultFont);
  dlg = new DefaultsDialog(hlManager,&defaultStyleList,&defaultFont,0L);
  if (dlg->exec() == QDialog::Accepted) {
    hlManager->setDefaults(defaultStyleList,defaultFont);
    configChanges(false);
  }
  delete dlg;
}

void KWriteManager::slotHighlightDlg()
{
  HighlightDialog *dlg;
  HlDataList hlDataList;
  hlDataList.setAutoDelete(true);

  hlManager->getHlDataList(hlDataList);
  dlg = new HighlightDialog(hlManager,&hlDataList,current ? current->doc()->getHighlight():0,0L);
  if (dlg->exec() == QDialog::Accepted) {
    hlManager->setHlDataList(hlDataList);
    configChanges(false);
  }
  delete dlg;
}

void KWriteManager::setCurrentKWrite( KWrite* cur )
{
  current = cur;
}