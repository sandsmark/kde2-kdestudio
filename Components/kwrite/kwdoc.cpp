#include <stdio.h>
#include <qobject.h>
#include <qapplication.h>
#include <qclipboard.h>
#include <qfont.h>
#include <qpainter.h>

#include <kcharsets.h>

#include "kwdoc.h"
#include "kwview.h"
#include "kwrite.h"

KWriteDoc::KWriteDoc(HlManager *hlManager)
: QObject(0L), hlManager(hlManager)
{
  updates = true;
  contents.setAutoDelete(true);

  colors[0] = white;
  colors[1] = darkBlue;
  colors[2] = black;
  colors[3] = black;
  colors[4] = white;

  highlight = 0L;
  tabChars = 2;

  newDocGeometry = false;
  modified = false;

  undoList.setAutoDelete(true);
  undoState = 0;
  undoSteps = 50;

  pseudoModal = 0L;
  clear();

  setHighlight(0);
  connect(hlManager,SIGNAL(changed()),SLOT(hlChanged()));

  parserCalbackFunc = 0;
  parserCalbackObject = 0L;
}

KWriteDoc::~KWriteDoc()
{
  highlight->release();
}

void KWriteDoc::selectWord(PointStruc &cursor, int flags)
{
  TextLine *textLine;
  int start, end, len;


  textLine = contents.at(cursor.y);
  len = textLine->length();
  start = end = cursor.x;
  while (start > 0 && highlight->isInWord(textLine->getChar(start - 1))) start--;
  while (end < len && highlight->isInWord(textLine->getChar(end))) end++;
  if (end <= start) return;
  if (!(flags & cfKeepSelection)) deselectAll();
  textLine->select(true, start, end);

  anchor.x = start;
  select.x = end;
  anchor.y = select.y = cursor.y;
  tagLines(cursor.y, cursor.y);
  if (cursor.y < selectStart) selectStart = cursor.y;
  if (cursor.y > selectEnd) selectEnd = cursor.y;
}

int KWriteDoc::lastLine() const
{
  return (int) contents.count() - 1;
}

TextLine *KWriteDoc::textLine(int line)
{
  return contents.at(line);
}

int KWriteDoc::textLength(int line)
{
  return contents.at(line)->length();
}

void KWriteDoc::tagLines(int start, int end)
{
  int z;

  for (z = 0; z < (int) views.count(); z++) {
    views.at(z)->tagLines(start,end);
  }
}

void KWriteDoc::tagAll() {
  int z;

  for (z = 0; z < (int) views.count(); z++) {
    views.at(z)->tagAll();
  }
}

void KWriteDoc::registerView(KWriteView *view) {
  views.append(view);
}

void KWriteDoc::removeView(KWriteView *view)
{
  views.remove(view);
}


int KWriteDoc::currentColumn(PointStruc &cursor)
{
  return contents.at(cursor.y)->cursorX(cursor.x,tabChars);
}

void KWriteDoc::insert(KWriteView *view, VConfig &c, const char *s)
{
  char b[256];
  int pos;

  if (!s || !*s) return;
  recordStart(c.cursor);
  pos = 0;
  if (!(c.flags & cfVerticalSelect)) {
    while (*s != 0) {
      if ((unsigned char) *s >= 32 || *s == '\t') {
        b[pos] = *s;
        pos++;
      } else if (*s == '\n') {
        recordAction(KWAction::newLine,c.cursor);
        recordReplace(c.cursor,0,b,pos);
        c.cursor.y++;
        c.cursor.x = 0;
        pos = 0;
      }
      if (pos >= 256) {
        recordReplace(c.cursor,0,b,pos);
        c.cursor.x += pos;
        pos = 0;
      }
      s++;
    }
    if (pos > 0) {
      recordReplace(c.cursor,0,b,pos);
      c.cursor.x += pos;
    }
  } else {
    int xPos;

    xPos = textWidth(c.cursor);
    while (*s != 0) {
      if ((unsigned char) *s >= 32 || *s == '\t') {
        b[pos] = *s;
        pos++;
      } else if (*s == '\n') {
        recordReplace(c.cursor,0,b,pos);
        c.cursor.y++;
        if (c.cursor.y >= (int) contents.count()) {
          recordAction(KWAction::insLine,c.cursor);
        }
        c.cursor.x = textPos(contents.at(c.cursor.y),xPos);
        pos = 0;
      }
      s++;
      if (pos >= 256 || *s == 0) {
        recordReplace(c.cursor,0,b,pos);
        c.cursor.x += pos;
        pos = 0;
      }
    }
  }
  recordEnd(view,c);
}

void KWriteDoc::insertFile(KWriteView *view, VConfig &c, QIODevice &dev)
{
  char buf[256];
  int len;
  char b[256];
  int pos;
  char *s;

  recordStart(c.cursor);
  pos = 0;
  do {
    len = dev.readBlock(buf,256);
    s = buf;
    while (len > 0) {
      if ((unsigned char) *s >= 32 || *s == '\t') {
        b[pos] = *s;
        pos++;
      } else if (*s == '\n') {
        recordAction(KWAction::newLine,c.cursor);
        recordReplace(c.cursor,0,b,pos);
        c.cursor.y++;
        c.cursor.x = 0;
        pos = 0;
      }
      if (pos >= 256) {
        recordReplace(c.cursor,0,b,pos);
        c.cursor.x += pos;
        pos = 0;
      }
      s++;
      len--;
    }
  } while (s != buf);
  if (pos > 0) {
    recordReplace(c.cursor,0,b,pos);
    c.cursor.x += pos;
  }
  recordEnd(view,c);
}

void KWriteDoc::loadFile(QIODevice &dev)
{
  TextLine *textLine;
  char block[256];
  int len;
  char *s;
  QChar ch;
  char last = '\0';

  clear();

  textLine = contents.getFirst();
  do {
    len = dev.readBlock(block, 256);
    s = block;
    while (len > 0) {
      ch = *s;
      if (ch.isPrint() || *s == '\t') {
        textLine->append(&ch, 1);
      } else if (*s == '\n' || *s == '\r') {
        if (last != '\r' || *s != '\n') {
          textLine = new TextLine();
          contents.append(textLine);
         // if (*s == '\r') eolMode = eolMacintosh;
        }// else eolMode = eolDos;
        last = *s;
      }
      s++;
      len--;
    }
  } while (s != block);
}

void KWriteDoc::writeFile(QIODevice &dev)
{
  TextLine *textLine;

  textLine = contents.first();
  do {
    QConstString str((QChar *) textLine->getText(), textLine->length());
    dev.writeBlock(str.string().latin1(), textLine->length());
    textLine = contents.next();
    if (!textLine) break;
    dev.putch('\n');
  } while (true);
  setModified(false);
}

void KWriteDoc::insertChar(KWriteView *view, VConfig &c, char ch)
{
  TextLine *textLine;
  int l, z;
  char buf[20];

  textLine = contents.at(c.cursor.y);

  if (ch == '\t' && c.flags & cfReplaceTabs) {
    l = tabChars - (textLine->cursorX(c.cursor.x,tabChars) % tabChars);
    for (z = 0; z < l; z++) buf[z] = ' ';
  } else {
    z = l = 1;
    buf[0] = ch;
    if (c.flags & cfAutoBrackets) {
      if (ch == '(') buf[l++] = ')';
      if (ch == '[') buf[l++] = ']';
      if (ch == '{') buf[l++] = '}';
    }
  }

  //do nothing if spaces will be removed
  if (buf[0] == ' ' && c.flags & cfRemoveSpaces && c.cursor.x >= textLine->length()) {
    c.cursor.x += z;
    view->updateCursor(c.cursor);
    return;
  }
  recordStart(c.cursor);

  recordReplace(c.cursor,(c.flags & cfOvr) ? l : 0,buf,l);
  c.cursor.x += z;
  recordEnd(view,c);
}

void KWriteDoc::newLine(KWriteView *view, VConfig &c)
{
  recordStart(c.cursor);

  if (!(c.flags & cfAutoIndent)) {
    recordAction(KWAction::newLine,c.cursor);
    c.cursor.y++;
    c.cursor.x = 0;
  } else {
    TextLine *textLine;
    int pos;

    textLine = contents.at(c.cursor.y);
    pos = textLine->firstChar();
    if (pos > c.cursor.x) c.cursor.x = pos;

    do {
      pos = textLine->firstChar();
      if (pos >= 0) break;
      textLine = contents.prev();
    } while (textLine);
    recordAction(KWAction::newLine,c.cursor);
    c.cursor.y++;
    c.cursor.x = 0;
    if (pos > 0) {
      recordReplace(c.cursor, 0, QString(textLine->getText(),pos), pos);
      c.cursor.x = pos;
    }
  }
  recordEnd(view,c);
}

void KWriteDoc::killLine(KWriteView *view, VConfig &c)
{
  recordStart(c.cursor);
  c.cursor.x = 0;
  recordReplace(c.cursor,0xffffff);
  if (c.cursor.y < (int) contents.count() - 1) {
    recordAction(KWAction::killLine,c.cursor);
  }
  recordEnd(view,c);
}

void KWriteDoc::backspace(KWriteView *view, VConfig &c)
{
  if (c.cursor.x <= 0 && c.cursor.y <= 0) return;
  recordStart(c.cursor);
  if (c.cursor.x > 0) {
    if (!(c.flags & cfBackspaceIndent)) {
      c.cursor.x--;
      recordReplace(c.cursor,1);
    } else {
      TextLine *textLine;
      int pos, l;

      textLine = contents.at(c.cursor.y);
      pos = textLine->firstChar();
      if (pos >= 0 && pos < c.cursor.x) pos = 0;
      while ((textLine = contents.prev()) && pos != 0) {
        pos = textLine->firstChar();
        if (pos >= 0 && pos < c.cursor.x) {
          l = c.cursor.x - pos;
          goto found;
        }
      }
      l = 1;//del one char
      found:
      c.cursor.x -= l;
      recordReplace(c.cursor,l);
    }
  } else {
    c.cursor.y--;
    c.cursor.x = contents.at(c.cursor.y)->length();
    recordAction(KWAction::delLine,c.cursor);
  }
  recordEnd(view,c);
}


void KWriteDoc::del(KWriteView *view, VConfig &c)
{
  if (c.cursor.x < contents.at(c.cursor.y)->length()) {
    recordStart(c.cursor);
    recordReplace(c.cursor,1);
    recordEnd(view,c);
  } else {
    if (c.cursor.y < (int) contents.count() -1) {
      recordStart(c.cursor);
      recordAction(KWAction::delLine,c.cursor);
      recordEnd(view,c);
    }
  }
}

void KWriteDoc::hlChanged()
{
  makeAttribs();
  updateViews();
}

void KWriteDoc::setHighlight(int n)
{
  Highlight *h;

  h = hlManager->getHl(n);
  if (h == highlight) {
    updateLines();
  } else {
    if (highlight) highlight->release();
    h->use();
    highlight = h;
    makeAttribs();
  }
}

void KWriteDoc::makeAttribs()
{

  hlManager->makeAttribs(highlight,attribs,nAttribs);
  updateFontData();
  updateLines();
}

void KWriteDoc::updateFontData()
{
  int maxAscent, maxDescent;
  int minTabWidth, maxTabWidth;
  int i, z;
  KWriteView *view;

  maxAscent = 0;
  maxDescent = 0;
  minTabWidth = 0xffffff;
  maxTabWidth = 0;

  for (z = 0; z < nAttribs; z++) {
    i = attribs[z].fm.ascent();
    if (i > maxAscent) maxAscent = i;
    i = attribs[z].fm.descent();
    if (i > maxDescent) maxDescent = i;
    i = attribs[z].fm.width('x');
    if (i < minTabWidth) minTabWidth = i;
    if (i > maxTabWidth) maxTabWidth = i;
  }

  fontHeight = maxAscent + maxDescent + 1;
  fontAscent = maxAscent;
  tabWidth = tabChars*(maxTabWidth + minTabWidth)/2;

  for (view = views.first(); view != 0L; view = views.next()) {
    resizeBuffer(view,view->width(),fontHeight);
    view->tagAll();
    view->updateCursor();
  }
}

void KWriteDoc::setTabWidth(int chars)
{
  TextLine *textLine;
  int len;

  if (tabChars == chars) return;
  if (chars < 1) chars = 1;
  if (chars > 16) chars = 16;
  tabChars = chars;
  updateFontData();

  maxLength = -1;
  for (textLine = contents.first(); textLine != 0L; textLine = contents.next()) {
    len = textWidth(textLine,textLine->length());
    if (len > maxLength) {
      maxLength = len;
      longestLine = textLine;
    }
  }
}

void KWriteDoc::updateLines(int startLine, int endLine, int flags)
{
  TextLine *textLine;
  int line, lastLine;
  int ctxNum, endCtx;

  lastLine = (int) contents.count() -1;
  if (endLine >= lastLine) endLine = lastLine;

  line = startLine;
  ctxNum = 0;
  if (line > 0) ctxNum = contents.at(line - 1)->getContext();
  do {
    textLine = contents.at(line);
    if (line <= endLine) {
      if (flags & cfRemoveSpaces) textLine->removeSpaces();
      updateMaxLength(textLine);
    }
    endCtx = textLine->getContext();
    ctxNum = highlight->doHighlight(ctxNum,textLine);
    textLine->setContext(ctxNum);
    line++;
  } while (line <= lastLine && (line <= endLine || endCtx != ctxNum));
  tagLines(startLine, line - 1);
}

void KWriteDoc::updateMaxLength(TextLine *textLine)
{
  int len = textWidth(textLine,textLine->length());
  if (len > maxLength)
  {
    longestLine = textLine;
    maxLength = len;
    newDocGeometry = true;
  } else {
    if (!longestLine || (textLine == longestLine && len <= maxLength*3/4))
    {
      maxLength = -1;
      for (textLine = contents.first(); textLine != 0L; textLine = contents.next())
      {
        len = textWidth(textLine,textLine->length());
        if (len > maxLength)
        {
          maxLength = len;
          longestLine = textLine;
        }
      }
      newDocGeometry = true;
    }
  }
}

void KWriteDoc::updateViews(KWriteView *exclude)
{
  KWriteView *view;
  int flags;

  if ( updates ){
    flags = (newDocGeometry) ? ufDocGeometry : 0;
    for (view = views.first(); view != 0L; view = views.next()) {
      if (view != exclude) view->updateView(flags);
    }
    newDocGeometry = false;
  }
}

int KWriteDoc::textWidth(TextLine *textLine, int cursorX)
{
  int x;
  int z;
  char ch;
  Attribute *a;

  x = 0;
  for (z = 0; z < cursorX; z++) {
    ch = textLine->getChar(z);
    a = &attribs[textLine->getAttr(z)];
    x += (ch == '\t') ? tabWidth - (x % tabWidth) : a->width(ch);
  }
  return x;
}

int KWriteDoc::textWidth(PointStruc &cursor)
{
  if (cursor.x < 0) cursor.x = 0;
  if (cursor.y < 0) cursor.y = 0;
  if (cursor.y >= (int) contents.count()) cursor.y = (int) contents.count() -1;
  return textWidth(contents.at(cursor.y),cursor.x);
}

int KWriteDoc::textWidth( PointStruc &cursor, int xPos )
{
  TextLine *textLine;
  int len;
  int x, oldX;
  int z;
  char ch;
  Attribute *a;

  if (cursor.y < 0) cursor.y = 0;
  if (cursor.y >= (int) contents.count()) cursor.y = (int) contents.count() -1;
  textLine = contents.at(cursor.y);
  len = textLine->length();

  x = oldX = z = 0;
  while ( x < xPos ){
    oldX = x;
    ch = textLine->getChar(z);
    a = &attribs[textLine->getAttr(z)];
    x += (ch == '\t') ? tabWidth - (x % tabWidth) : a->width(ch);
    z++;
  }
  if (xPos - oldX < x - xPos && z > 0) {
    z--;
    x = oldX;
  }
  cursor.x = z;
  return x;
}

int KWriteDoc::textPos(TextLine *textLine, int xPos)
{
  int x, oldX;
  int z;
  char ch;
  Attribute *a;

  x = oldX = z = 0;
  while (x < xPos) {
    oldX = x;
    ch = textLine->getChar(z);
    a = &attribs[textLine->getAttr(z)];
    x += (ch == '\t') ? tabWidth - (x % tabWidth) : a->width(ch);
    z++;
  }
  if (xPos - oldX < x - xPos && z > 0) z--;
  return z;
}

int KWriteDoc::textWidth()
{
  return maxLength + 8;
}

int KWriteDoc::textHeight()
{
  return contents.count()*fontHeight;
}

void KWriteDoc::toggleRect(int x1, int y1, int x2, int y2)
{
  int z;
  bool t;
  int start, end;
  TextLine *textLine;

  if (x1 > x2) {
    z = x1;
    x1 = x2;
    x2 = z;
  }
  if (y1 > y2) {
    z = y1;
    y1 = y2;
    y2 = z;
  }

  t = false;
  for (z = y1; z < y2; z++) {
    textLine = contents.at(z);
    start = textPos(textLine,x1);
    end = textPos(textLine,x2);
    if (end > start) {
      textLine->toggleSelect(start,end);
      t = true;
    }
  }
  if (t) {
    y2--;
    tagLines(y1,y2);

    if (y1 < selectStart) selectStart = y1;
    if (y2 > selectEnd) selectEnd = y2;
  }
}

void KWriteDoc::selectTo(PointStruc &start, PointStruc &end, int flags)
{
  if (start.x != select.x || start.y != select.y) {
    /* new selection */
    if (!(flags & cfKeepSelection)) deselectAll();
    anchor = start;
  }

  if (!(flags & cfVerticalSelect)) {
    //horizontal selections
    TextLine *textLine;
    int x, y;
    int xe, ye;
    bool sel;

    if (end.y > start.y || (end.y == start.y && end.x > start.x)) {
      x = start.x;
      y = start.y;
      xe = end.x;
      ye = end.y;
      sel = true;
    } else {
      x = end.x;
      y = end.y;
      xe = start.x;
      ye = start.y;
      sel = false;
    }
    tagLines(y,ye);

    if (y < selectStart) selectStart = y;
    if (ye > selectEnd) selectEnd = ye;

    textLine = contents.at(y);

    if (flags & cfXorSelect) {
      while (y < ye) {
        textLine->toggleSelectEol(x);
        x = 0;
        y++;
        textLine = contents.at(y);
      }
      textLine->toggleSelect(x,xe);
    } else {
      if (anchor.y > y || (anchor.y == y && anchor.x > x)) {
        if (anchor.y < ye || (anchor.y == ye && anchor.x < xe)) {
          sel = !sel;
          while (y < anchor.y) {
            textLine->selectEol(sel,x);
            x = 0;
            y++;
            textLine = contents.at(y);
          }
          textLine->select(sel,x,anchor.x);
          x = anchor.x;
        }
        sel = !sel;
      }
      while (y < ye) {
        textLine->selectEol(sel,x);
        x = 0;
        y++;
        textLine = contents.at(y);
      }
      textLine->select(sel,x,xe);
    }
  } else {

    int ax, sx, ex;

    ax = textWidth(anchor);
    sx = textWidth(start);
    ex = textWidth(end);

    toggleRect(ax,start.y + 1,sx,end.y + 1);
    toggleRect(sx,anchor.y,ex,end.y + 1);
  }
  select = end;
  optimizeSelection();
}

void KWriteDoc::clear()
{
  PointStruc cursor;
  KWriteView *view;

  setPseudoModal(0L);
  cursor.x = cursor.y = 0;
  for (view = views.first(); view != 0L; view = views.next()) {
    view->updateCursor(cursor);
    view->tagAll();
  }

  contents.clear();

  contents.append(longestLine = new TextLine());
  maxLength = 0;

  select.x = -1;

  selectStart = 0xffffff;
  selectEnd = 0;

  foundLine = -1;

  setModified(false);

  undoList.clear();
  currentUndo = 0;
  newUndo();
}


void KWriteDoc::copy(int flags)
{
  if (selectEnd < selectStart) return;

  QString s = markedText(flags);
  if (!s.isEmpty())
    QApplication::clipboard()->setText(s);
}

void KWriteDoc::paste(KWriteView *view, VConfig &c)
{
  QString s = QApplication::clipboard()->text();

  if (!s.isEmpty())
    insert(view,c,s);
}

void KWriteDoc::pasteStr(KWriteView *view, VConfig &c, QString s)
{
    insert(view,c,s);
}

void KWriteDoc::cut(KWriteView *view, VConfig &c)
{
  if (selectEnd < selectStart) return;

  copy(c.flags);
  delMarkedText(view,c);
}

void KWriteDoc::selectAll()
{
  int z;
  TextLine *textLine;

  select.x = -1;

  unmarkFound();
  selectStart = 0;
  selectEnd = contents.count() -1;

  tagLines(selectStart,selectEnd);

  for (z = selectStart; z < selectEnd; z++) {
    textLine = contents.at(z);
    textLine->selectEol(true,0);
  }
  textLine = contents.at(z);
  textLine->select(true,0,textLine->length());
}

void KWriteDoc::deselectAll()
{
  int z;
  TextLine *textLine;

  select.x = -1;
  if (selectEnd < selectStart) return;

  unmarkFound();
  tagLines(selectStart,selectEnd);

  for (z = selectStart; z <= selectEnd; z++) {
    textLine = contents.at(z);
    textLine->selectEol(false,0);
  }
  selectEnd = -1;
}

void KWriteDoc::invertSelection()
{
  int z;
  TextLine *textLine;

  select.x = -1;

  unmarkFound();
  selectStart = 0;
  selectEnd = contents.count() -1;

  tagLines(selectStart,selectEnd);

  for (z = selectStart; z < selectEnd; z++) {
    textLine = contents.at(z);
    textLine->toggleSelectEol(0);
  }
  textLine = contents.at(z);
  textLine->toggleSelect(0,textLine->length());
}

QString KWriteDoc::text()
{
  TextLine *textLine;
  QString s;

  textLine = contents.first();
  do {
    s.insert(s.length(), textLine->getText(), textLine->length());
    textLine = contents.next();
    if (!textLine) break;
    s.append('\n');
  } while (true);
  return s;
}

QString KWriteDoc::currentWord(PointStruc &cursor)
{
  TextLine *textLine;
  int start, end, len;

  textLine = contents.at(cursor.y);
  len = textLine->length();
  start = end = cursor.x;
  while (start > 0 && highlight->isInWord(textLine->getChar(start - 1))) start--;
  while (end < len && highlight->isInWord(textLine->getChar(end))) end++;
  len = end - start;
  return QString(&textLine->getText()[start], len);
}

void KWriteDoc::setText( const QString& s )
{
  TextLine *textLine;
  int pos;
  QChar ch;

  clear();

  textLine = contents.getFirst();
  for (pos = 0; pos <= (int) s.length(); pos++) {
    ch = s[pos];
    if (ch.isPrint() || ch == '\t') {
      textLine->append(&ch, 1);
    } else if (ch == '\n') {
      textLine = new TextLine();
      contents.append(textLine);
    }
  }
  updateLines();
}


QString KWriteDoc::markedText( int flags )
{
  TextLine *textLine;
  int z, start, end, i;
  QString s;

  if (!(flags & cfVerticalSelect)) {
    for (z = selectStart; z <= selectEnd; z++) {
      textLine = contents.at(z);
      end = 0;
      do {
        start = textLine->findUnselected(end);
        end = textLine->findSelected(start);
        for (i = start; i < end; i++) {
          s += textLine->getChar(i);
        }
      } while (start < end);
      if (textLine->isSelected()) {
        s += '\n';
      }
    }
  } else {
    for (z = selectStart; z <= selectEnd; z++) {
      textLine = contents.at(z);
      end = 0;
      do {
        start = textLine->findUnselected(end);
        end = textLine->findSelected(start);
        for (i = start; i < end; i++) {
          s += textLine->getChar(i);
        }
      } while (start < end);
      if ( z < selectEnd )
        s +='\n';
    }
  }
  return s;
}

void KWriteDoc::delMarkedText(KWriteView *view, VConfig &c)
{
  TextLine *textLine;
  int end = 0;

  if (selectEnd < selectStart) return;

  recordStart(c.cursor);

  for (c.cursor.y = selectEnd; c.cursor.y >= selectStart; c.cursor.y--) {
    textLine = contents.at(c.cursor.y);

    c.cursor.x = textLine->length();
    do {
      end = textLine->findRevUnselected(c.cursor.x);
      if (end == 0) break;
      c.cursor.x = textLine->findRevSelected(end);
      recordReplace(c.cursor,end - c.cursor.x);
    } while (true);
    end = c.cursor.x;
    c.cursor.x = textLine->length();
    if (textLine->isSelected()) recordAction(KWAction::delLine,c.cursor);
  }
  c.cursor.y++;
  if (end < c.cursor.x) c.cursor.x = end;

  selectEnd = -1;
  select.x = -1;

  recordEnd(view,c);
}

void KWriteDoc::paintTextLine(QPainter &paint, int line, int xStart, int xEnd)
{
  int y;
  TextLine *textLine;
  int len;
  const QChar *s;
  int z, x;
  char ch;
  Attribute *a = 0L;
  int attr, nextAttr;
  int xs;
  int xc, zc;

  y = 0;
  if (line >= (int) contents.count()) {
    paint.fillRect(0, y, xEnd - xStart,fontHeight, colors[0]);
    return;
  }

  textLine = contents.at(line);
  len = textLine->length();
  s = textLine->getText();

  // skip to first visible character
  x = 0;
  z = 0;
  do {
    xc = x;
    zc = z;
    if (z == len) break;
    ch = s[z];
    if (ch == '\t') {
      x += tabWidth - (x % tabWidth);
    } else {
      a = &attribs[textLine->getAttr(z)];
      x += a->width(ch);
    }
    z++;
  } while (x <= xStart);

  // draw background
  xs = xStart;
  attr = textLine->getRawAttr(zc);
  while (x < xEnd) {
    nextAttr = textLine->getRawAttr(z);
    if ((nextAttr ^ attr) & (taSelectMask | 256)) {
      paint.fillRect(xs - xStart, y, x - xs, fontHeight, colors[attr >> taShift]);
      xs = x;
      attr = nextAttr;
    }
    if (z == len) break;
    ch = s[z];
    if (ch == '\t') {
      x += tabWidth - (x % tabWidth);
    } else {
      a = &attribs[attr & taAttrMask];
      x += a->width(ch);
    }
    z++;
  }
  paint.fillRect(xs - xStart, y, xEnd - xs, fontHeight, colors[attr >> taShift]);
  len = z; //reduce length to visible length

  // draw text
  x = xc;
  z = zc;
  y += fontAscent -1;
  attr = -1;
  while (z < len) {
    ch = s[z];
    if (ch == '\t') {
      if (z > zc) {
        //this should cause no copy at all
        QConstString str((QChar *) &s[zc], z - zc);
        QString s = str.string();
        paint.drawText(x - xStart, y, s);
        x += a->width(s);
      }
      zc = z +1;
      x += tabWidth - (x % tabWidth);
    } else {
      nextAttr = textLine->getRawAttr(z);
      if (nextAttr != attr) {
        if (z > zc) {
          QConstString str((QChar *) &s[zc], z - zc);
          QString s = str.string();
          paint.drawText(x - xStart, y, s);
          x += a->width(s);
          zc = z;
        }
        attr = nextAttr;
        a = &attribs[attr & taAttrMask];

        if (attr & taSelectMask) paint.setPen(a->selCol);
          else paint.setPen(a->col);
        paint.setFont(a->font);
      }
    }
    z++;
  }
  if (z > zc) {
    QConstString str((QChar *) &s[zc], z - zc );
    paint.drawText(x - xStart, y, str.string());
  }
}

void KWriteDoc::setCalbackFunc( QObject* object, parserCalbackFuncType f )
{
  parserCalbackFunc = f;
  parserCalbackObject = object;
}

void KWriteDoc::setModified(bool m)
{
  KWriteView *view;

  if (parserCalbackObject && m){
    QString all = text();
    (parserCalbackObject->*parserCalbackFunc)(fileName(),(char*)all.latin1(),all.length());
  }
  if (m != modified) {
    modified = m;
    for (view = views.first(); view != 0L; view = views.next()) {
      emit view->kWrite->newStatus();
    }
  }
}

bool KWriteDoc::isLastView(int numViews)
{
  return ((int) views.count() == numViews);
}

void KWriteDoc::setFileName(const char *s)
{
  int z;

  fName = s;
  for (z = 0; z < (int) views.count(); z++) {
    emit views.at(z)->kWrite->newCaption();
  }

  //highlight detection
  QString sName = QString().fromLatin1( s );
  int pos = sName.findRev('/') +1;
  if (pos == 0) return; //no filename
  int hl = hlManager->wildcardFind(sName.right(sName.length()-pos));
  setHighlight(hl);
  updateViews();
}

const char *KWriteDoc::fileName()
{
  return fName;
}

bool KWriteDoc::doSearch(SConfig &sc, const QString &searchFor)
{
  int line, col;
  int searchEnd;
  QString str;
  int slen, smlen, bufLen, tlen;
  const QChar *s;
  QChar *t;
  TextLine *textLine;
  int z, pos, newPos;

  if (searchFor.isEmpty()) return false;
  str = (sc.flags & sfCaseSensitive) ? searchFor : searchFor.lower();

  s = str.unicode();
  slen = str.length();
  smlen = slen*sizeof(QChar);
  bufLen = 0;
  t = 0L;

  line = sc.cursor.y;
  col = sc.cursor.x;
  if (!(sc.flags & sfBackward)) {
    //forward search
    if (sc.flags & sfSelected) {
      if (line < selectStart) {
        line = selectStart;
        col = 0;
      }
      searchEnd = selectEnd;
    } else searchEnd = lastLine();

    while (line <= searchEnd) {
      textLine = contents.at(line);
      tlen = textLine->length();
      if (tlen > bufLen) {
        delete t;
        bufLen = (tlen + 255) & (~255);
        t = new QChar[bufLen];
      }
      memcpy(t, textLine->getText(), tlen*sizeof(QChar));
      if (sc.flags & sfSelected) {
        pos = 0;
        do {
          pos = textLine->findSelected(pos);
          newPos = textLine->findUnselected(pos);
          memset(&t[pos], 0, (newPos - pos)*sizeof(QChar));
          pos = newPos;
        } while (pos < tlen);
      }
      if (!(sc.flags & sfCaseSensitive)) for (z = 0; z < tlen; z++) t[z] = t[z].lower();

      tlen -= slen;
      if (sc.flags & sfWholeWords && tlen > 0) {
        //whole word search
        if (col == 0) {
          if (!highlight->isInWord(t[slen])) {
            if (memcmp(t, s, smlen) == 0) goto found;
          }
          col++;
        }
        while (col < tlen) {
          if (!highlight->isInWord(t[col -1]) && !highlight->isInWord(t[col + slen])) {
            if (memcmp(&t[col], s, smlen) == 0) goto found;
          }
          col++;
        }
        if (!highlight->isInWord(t[col -1]) && memcmp(&t[col], s, smlen) == 0)
          goto found;
      } else {
        //normal search
        while (col <= tlen) {
          if (memcmp(&t[col], s, smlen) == 0) goto found;
          col++;
        }
      }
      col = 0;
      line++;
    }
  } else {
    // backward search
    if (sc.flags & sfSelected) {
      if (line > selectEnd) {
        line = selectEnd;
        col = -1;
      }
      searchEnd = selectStart;
    } else searchEnd = 0;;

    while (line >= searchEnd) {
      textLine = contents.at(line);
      tlen = textLine->length();
      if (tlen > bufLen) {
        delete t;
        bufLen = (tlen + 255) & (~255);
        t = new QChar[bufLen];
      }
      memcpy(t, textLine->getText(), tlen*sizeof(QChar));
      if (sc.flags & sfSelected) {
        pos = 0;
        do {
          pos = textLine->findSelected(pos);
          newPos = textLine->findUnselected(pos);
          memset(&t[pos], 0, (newPos - pos)*sizeof(QChar));
          pos = newPos;
        } while (pos < tlen);
      }
      if (!(sc.flags & sfCaseSensitive)) for (z = 0; z < tlen; z++) t[z] = t[z].lower();

      if (col < 0 || col > tlen) col = tlen;
      col -= slen;
      if (sc.flags & sfWholeWords && tlen > slen) {
        //whole word search
        if (col + slen == tlen) {
          if (!highlight->isInWord(t[col -1])) {
            if (memcmp(&t[col], s, smlen) == 0) goto found;
          }
          col--;
        }
        while (col > 0) {
          if (!highlight->isInWord(t[col -1]) && !highlight->isInWord(t[col+slen])) {
            if (memcmp(&t[col],s,smlen) == 0) goto found;
          }
          col--;
        }
        if (!highlight->isInWord(t[slen]) && memcmp(t, s, smlen) == 0)
          goto found;
      } else {
        //normal search
        while (col >= 0) {
          if (memcmp(&t[col],s,slen) == 0) goto found;
          col--;
        }
      }
      line--;
    }
  }
  sc.flags |= sfWrapped;
  return false;

found:
  if (sc.flags & sfWrapped) {
    if ((line > sc.startCursor.y || (line == sc.startCursor.y && col >= sc.startCursor.x))
      ^ ((sc.flags & sfBackward) != 0)) return false;
  }
  sc.cursor.x = col;
  sc.cursor.y = line;
  return true;
}

void KWriteDoc::unmarkFound()
{
  if (pseudoModal) return;
  if (foundLine != -1) {
    contents.at(foundLine)->unmarkFound();
    tagLines(foundLine,foundLine);
    foundLine = -1;
  }
}

void KWriteDoc::markFound(PointStruc &cursor, int len)
{
  if (foundLine != -1) {
    contents.at(foundLine)->unmarkFound();
    tagLines(foundLine,foundLine);
  }
  contents.at(cursor.y)->markFound(cursor.x,len);
  foundLine = cursor.y;
  tagLines(foundLine,foundLine);
}


void KWriteDoc::tagLine(int line)
{
  if (tagStart > line) tagStart = line;
  if (tagEnd < line) tagEnd = line;
}

void KWriteDoc::insLine(int line)
{
  KWriteView *view;
  
  if (selectStart >= line) selectStart++;
  if (selectEnd >= line) selectEnd++;
  if (tagStart >= line) tagStart++;
  if (tagEnd >= line) tagEnd++;

  newDocGeometry = true;
  for (view = views.first(); view != 0L; view = views.next()) {
    view->insLine(line);
  }
}

void KWriteDoc::delLine(int line)
{
  KWriteView *view;
  
  if (selectStart >= line && selectStart > 0) selectStart--;
  if (selectEnd >= line) selectEnd--;
  if (tagStart >= line && tagStart > 0) tagStart--;
  if (tagEnd >= line) tagEnd--;
  
  newDocGeometry = true;

  for (view = views.first(); view != 0L; view = views.next()) {
    view->delLine(line);
  }
}

void KWriteDoc::optimizeSelection()
{
  TextLine *textLine;
  
  while (selectStart <= selectEnd) {
    textLine = contents.at(selectStart);
    if (textLine->isSelected() || textLine->numSelected() > 0) break;
    selectStart++;
  }
  while (selectEnd >= selectStart) {
    textLine = contents.at(selectEnd);
    if (textLine->isSelected() || textLine->numSelected() > 0) break;
    selectEnd--;
  }
  if (selectStart > selectEnd) {
    selectStart = 0xffffff;
    selectEnd = 0;
  }
}

void KWriteDoc::doAction(KWAction *a)
{
  switch (a->action) {
    case KWAction::replace:
      doReplace(a);
      break;
    case KWAction::newLine:
      doNewLine(a);
      break;
    case KWAction::delLine:
      doDelLine(a);
      break;
    case KWAction::insLine:
      doInsLine(a);
      break;
    case KWAction::killLine:
      doKillLine(a);
      break;
  }
}

void KWriteDoc::doReplace(KWAction *a)
{
  TextLine *textLine;
  int l;

  textLine = contents.at(a->cursor.y);
  l = textLine->length() - a->cursor.x;
  if (l > a->len) l = a->len;

  QString oldText(&textLine->getText()[a->cursor.x], (l < 0) ? 0 : l);
  textLine->replace(a->cursor.x, a->len, a->text.unicode(), a->text.length());

  a->len = a->text.length();
  a->text = oldText;

  tagLine(a->cursor.y);
}

void KWriteDoc::doNewLine(KWAction *a)
{
  TextLine *textLine, *newLine;

  textLine = contents.at(a->cursor.y);
  newLine = new TextLine(textLine->getRawAttr(), textLine->getContext());

  textLine->wrap(newLine,a->cursor.x);
  contents.insert(a->cursor.y + 1,newLine);

  insLine(a->cursor.y + 1);
  tagLine(a->cursor.y);
  tagLine(a->cursor.y + 1);
  if (selectEnd == a->cursor.y) selectEnd++;

  a->action = KWAction::delLine;
}

void KWriteDoc::doDelLine(KWAction *a)
{
  TextLine *textLine, *nextLine;

  textLine = contents.at(a->cursor.y);
  nextLine = contents.next();
  textLine->unWrap(a->cursor.x, nextLine,nextLine->length());
  textLine->setContext(nextLine->getContext());

  if (longestLine == nextLine) longestLine = 0L;
  contents.remove();

  tagLine(a->cursor.y);
  delLine(a->cursor.y + 1);

  a->action = KWAction::newLine;
}

void KWriteDoc::doInsLine(KWAction *a)
{
  contents.insert(a->cursor.y,new TextLine());

  insLine(a->cursor.y);

  a->action = KWAction::killLine;
}

void KWriteDoc::doKillLine(KWAction *a)
{
  TextLine *textLine;

  textLine = contents.at(a->cursor.y);
  if (longestLine == textLine) longestLine = 0L;

  delLine(a->cursor.y);
  contents.remove();
  tagLine(a->cursor.y);

  a->action = KWAction::insLine;
}

void KWriteDoc::newUndo()
{
  KWriteView *view;
  int state;

  state = 0;
  if (currentUndo > 0) state |= 1;
  if (currentUndo < (int) undoList.count()) state |= 2;
  if (state != undoState) {
    undoState = state;
    for (view = views.first(); view != 0L; view = views.next()) {
      emit view->kWrite->newUndo();
    }
  }
}

void KWriteDoc::recordStart(PointStruc &cursor, bool keepModal)
{
  KWActionGroup *g;

  if (!keepModal) setPseudoModal(0L);
  while ((int) undoList.count() > currentUndo) undoList.removeLast();
  while ((int) undoList.count() > undoSteps) {
    undoList.removeFirst();
    currentUndo--;
  }
  g = new KWActionGroup(cursor);
  undoList.append(g);
  currentUndo++;

  unmarkFound();
  tagEnd = 0;
  tagStart = 0xffffff;
}

void KWriteDoc::recordAction(KWAction::Action action, PointStruc &cursor)
{
  KWAction *a;

  a = new KWAction(action,cursor);
  doAction(a);
  undoList.getLast()->insertAction(a);
}

void KWriteDoc::recordReplace(PointStruc &cursor, int len, const char *text, int textLen)
{
  KWAction *a;

  a = new KWAction(KWAction::replace,cursor);
  a->len = len;
  a->text = QString().fromLatin1( text, textLen) ;
  doReplace(a);
  undoList.getLast()->insertAction(a);
}

void KWriteDoc::recordEnd(KWriteView *view, VConfig &c)
{
  recordEnd(view,c.cursor,c.flags);
}

void KWriteDoc::recordEnd(KWriteView *view, PointStruc &cursor, int flags)
{

  if (!(flags & cfPersistent)) deselectAll();

  undoList.getLast()->end = cursor;
  view->updateCursor(cursor);

  optimizeSelection();
  if (tagStart <= tagEnd) updateLines(tagStart,tagEnd,flags);
  setModified(true);
  newUndo();
}

void KWriteDoc::doActionGroup(KWActionGroup *g, int flags)
{
  KWAction *a, *next;

  setPseudoModal(0L);
  if (!(flags & cfPersistent)) deselectAll();
  unmarkFound();
  tagEnd = 0;
  tagStart = 0xffffff;

  a = g->action;
  g->action = 0L;
  while (a) {
    doAction(a);
    next = a->next;
    g->insertAction(a);
    a = next;
  }
  optimizeSelection();
  if (tagStart <= tagEnd) updateLines(tagStart,tagEnd,flags);
  setModified(true);
  newUndo();
}

void KWriteDoc::undo(KWriteView *view, int flags)
{
  KWActionGroup *g;

  if (currentUndo <= 0) return;
  currentUndo--;
  g = undoList.at(currentUndo);
  doActionGroup(g,flags);
  view->updateCursor(g->start);
}

void KWriteDoc::redo(KWriteView *view, int flags)
{
  KWActionGroup *g;

  g = undoList.at(currentUndo);
  if (!g) return;
  currentUndo++;
  doActionGroup(g,flags);
  view->updateCursor(g->end);
}

void KWriteDoc::setUndoSteps(int steps)
{
  if (steps < 5) steps = 5;
  undoSteps = steps;
}

void KWriteDoc::setPseudoModal(QWidget *w)
{
  delete pseudoModal;
  pseudoModal = w;
}

void KWriteDoc::indent(KWriteView *view, VConfig &c)
{
  TextLine *textLine;

  c.flags |= cfPersistent;
  recordStart(c.cursor);
  c.cursor.x = 0;
  if (selectEnd < selectStart) {
    //indent single line
    textLine = contents.at(c.cursor.y);
    recordReplace(c.cursor,0," ",1);
  } else {
    //indent selection
    for (c.cursor.y = selectStart; c.cursor.y <= selectEnd; c.cursor.y++) {
      textLine = contents.at(c.cursor.y);
      if (textLine->isSelected() || textLine->numSelected()) recordReplace(c.cursor,0," ",1);
    }
    c.cursor.y--;
  }
  recordEnd(view,c);
}

void KWriteDoc::unIndent(KWriteView *view, VConfig &c)
{
  char s[16];
  PointStruc cursor;
  TextLine *textLine;
  int l;
  bool started;

  c.flags |= cfPersistent;
  memset(s,' ',16);
  cursor = c.cursor;
  c.cursor.x = 0;
  if (selectEnd < selectStart) {
    //unindent single line
    textLine = contents.at(c.cursor.y);
    if (textLine->firstChar() == 0) return;
    recordStart(cursor);
    l = (textLine->getChar(0) == '\t') ? tabChars - 1 : 0;
    recordReplace(c.cursor,1,s,l);
  } else {
    started = false;
    for (c.cursor.y = selectStart; c.cursor.y <= selectEnd; c.cursor.y++) {
      textLine = contents.at(c.cursor.y);
      if ((textLine->isSelected() || textLine->numSelected())
        && textLine->firstChar() > 0) {
        if (!started) {
          recordStart(cursor);
          started = true;
        }
        l = (textLine->getChar(0) == '\t') ? tabChars - 1 : 0;
        recordReplace(c.cursor,1,s,l);
      }
    }
    c.cursor.y--;
  }
  recordEnd(view,c);
}
