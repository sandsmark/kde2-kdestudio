#ifndef _KWDOC_H_
#define _KWDOC_H_

#include <qobject.h>
#include <qlist.h>
#include <qcolor.h>
#include <qfont.h>
#include <qfontmet.h>
#include <qintdict.h>

#include "kwview.h"
#include "kwtextline.h"
#include "kwaction.h"
#include "kwtypes.h"
#include "highlight.h"

class KConfig;

const int nAttribs = 32;

typedef void(QObject::*parserCalbackFuncType)(const char*, char*, uint);

class KWriteDoc : QObject
{Q_OBJECT
friend class KWriteView;
friend class KWrite;
friend class KWriteManager;
friend class HlManager;

public:
  KWriteDoc(HlManager*);
  ~KWriteDoc();

  void selectTo(PointStruc &start, PointStruc &end, int flags);

  void pasteStr(QString s);
  int lastLine() const;
  TextLine *textLine(int line);
  int textLength(int line);
  void tagLines(int start, int end);
  void tagAll();

  void setUpdatesEnabled( bool isUpdate ){ updates = isUpdate; }

  int getHighlight() {return hlManager->findHl(highlight);}

  void setCalbackFunc( QObject* = 0L, parserCalbackFuncType = 0 );

protected slots:
  void hlChanged();

protected:
  void registerView(KWriteView *);
  void removeView(KWriteView *);

  int currentColumn(PointStruc &cursor);

  void insert(KWriteView *, VConfig &, const char *);
  void insertFile(KWriteView *, VConfig &, QIODevice &);
  void loadFile(QIODevice &);
  void writeFile(QIODevice &);

  void insertChar(KWriteView *, VConfig &, char);
  void newLine(KWriteView *, VConfig &);
  void killLine(KWriteView *, VConfig &);
  void backspace(KWriteView *, VConfig &);
  void del(KWriteView *, VConfig &);

  void setHighlight(int n);
  void makeAttribs();
  void updateFontData();
  void setTabWidth(int);
  void updateLines(int startLine = 0, int endLine = 0xffffff, int flags = 0);
  void updateMaxLength(TextLine *);
  void updateViews(KWriteView *exclude = 0L);

  int textWidth(TextLine *, int cursorX);
  int textWidth(PointStruc &cursor);
  int textWidth(PointStruc &cursor, int xPos);
  int textPos(TextLine *, int xPos);

  int textWidth();
  int textHeight();

  void toggleRect(int, int, int, int);
  void clear();
  void copy(int flags);
  void paste(KWriteView *,VConfig &);
  void pasteStr(KWriteView *,VConfig &, QString s);
  void cut(KWriteView *, VConfig &);
  void selectAll();
  void deselectAll();
  void invertSelection();
  void selectWord(PointStruc &cursor, int flags);
  QString text();
  QString currentWord(PointStruc &cursor);
  void setText( const QString & );
  bool hasMarkedText() {return (selectEnd >= selectStart);}
  QString markedText(int flags);
  void delMarkedText(KWriteView *, VConfig &);

  void paintTextLine(QPainter &, int line, int xStart, int xEnd);

  void setModified(bool);
  bool isLastView(int numViews);

  void setFileName(const char *);
  const char *fileName();

  bool doSearch(SConfig &sc, const QString &searchFor);
  void unmarkFound();
  void markFound(PointStruc &cursor, int len);

  void tagLine(int line);
  void insLine(int line);
  void delLine(int line);
  void optimizeSelection();

  void doAction(KWAction *);
  void doReplace(KWAction *);
  void doNewLine(KWAction *);
  void doDelLine(KWAction *);
  void doInsLine(KWAction *);
  void doKillLine(KWAction *);
  void newUndo();
  void recordStart(PointStruc &, bool keepModal = false);
  void recordAction(KWAction::Action, PointStruc &);
  void recordReplace(PointStruc &, int len, const char *text = 0L, int textLen = 0);
  void recordEnd(KWriteView *, VConfig &);
  void recordEnd(KWriteView *, PointStruc &, int flags);
  void doActionGroup(KWActionGroup *, int flags);
  void undo(KWriteView *, int flags);
  void redo(KWriteView *, int flags);
  void setUndoSteps(int steps);

  void setPseudoModal(QWidget *);

  void indent(KWriteView *, VConfig &);
  void unIndent(KWriteView *, VConfig &);

  QList<TextLine> contents;
  QColor colors[5];
  HlManager *hlManager;
  Highlight *highlight;
  Attribute attribs[nAttribs];

  int tabChars;
  int tabWidth;
  int fontHeight;
  int fontAscent;

  QList<KWriteView> views;
  bool newDocGeometry;

  TextLine *longestLine;
  int maxLength;

  PointStruc select;
  PointStruc anchor;
  int selectStart;
  int selectEnd;

  bool modified;
  QString fName;

  int foundLine;

  QList<KWActionGroup> undoList;
  int currentUndo;
  int undoState;
  int undoSteps;
  int tagStart;
  int tagEnd;
  bool updates;
  QWidget *pseudoModal;

  parserCalbackFuncType parserCalbackFunc;
  QObject* parserCalbackObject;
};

#endif
