#ifndef _KWVIEV_H_
#define _KWVIEV_H_

#include <qwidget.h>
#include <qscrollbar.h>
#include <qpopupmenu.h>

#include <kconfig.h>
#include "kwdialog.h"
#include "kwtypes.h"

class KWriteDoc;
class Highlight;
class Breakpoint;
class KWriteManager;
class HlManager;
class TextLine;
class KWrite;

class KWriteView : public QWidget
{Q_OBJECT
  friend class KWriteDoc;
  friend class KWrite;
public:
  KWriteView(KWrite *, KWriteDoc *);
  ~KWriteView();

  
protected:
  virtual bool event ( QEvent * );
	void drawGutter(QPainter&, int, int);
	void scrollW(int, int);

  void cursorLeft(VConfig &);
  void cursorRight(VConfig &);
  void cursorLeftWord(VConfig &);
  void cursorRightWord(VConfig &);
  void cursorUp(VConfig &);
  void cursorDown(VConfig &);
  void home(VConfig &);
  void end(VConfig &);
  void pageUp(VConfig &);
  void pageDown(VConfig &);
  void top(VConfig &);
  void bottom(VConfig &);

protected:
  void getVConfig(VConfig &);
  void update(VConfig &);
  void insLine(int line);
  void delLine(int line);
  void updateCursor();
  void updateCursor(PointStruc &newCursor);
  void updateView(int flags, int newXPos = 0, int newYPos = 0);
  void tagLines(int start, int end);
  void tagAll();

  void paintTextLines(int xPos, int yPos);
  void paintCursor();

  void placeCursor(int x, int y, int flags);

  virtual void keyPressEvent(QKeyEvent *);
  virtual void focusInEvent(QFocusEvent *);
  virtual void focusOutEvent(QFocusEvent *);
  virtual void wheelEvent( QWheelEvent * );
  virtual void mousePressEvent(QMouseEvent *);
  virtual void mouseDoubleClickEvent(QMouseEvent *);
  virtual void mouseReleaseEvent(QMouseEvent *);
  virtual void mouseMoveEvent(QMouseEvent *);
  virtual void paintEvent(QPaintEvent *);
  virtual void resizeEvent(QResizeEvent *);
  virtual void timerEvent(QTimerEvent *);

protected slots:
  void changeXPos(int);
  void changeYPos(int);

private:
  KWrite *kWrite;
  KWriteDoc *kWriteDoc;
  QScrollBar *xScroll;
  QScrollBar *yScroll;

  QPixmap *drawBuffer;
  int xPos;
  int yPos;

  int mouseX;
  int mouseY;
  int scrollX;
  int scrollY;
  int scrollTimer;

  PointStruc cursor;
  bool cursorOn;
  int cursorTimer;
  int cXPos;
  int cOldXPos;
  bool exposeCursor;

  int startLine;
  int endLine;
  int updateState;
  int updateLines[2];
};


#endif
