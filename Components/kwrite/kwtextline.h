#ifndef KWTEXTLINE_H
#define KWTEXTLINE_H

#include <qstring.h>
#include <qintdict.h>

#include "kwtypes.h"

class TextLine
{
public:
  TextLine(int attribute = 0, int context = 0);
  ~TextLine();

  bool hide;

  int length() const {return len;}
  void replace(int pos, int delLen, const QChar *insText, int insLen, uchar *insAttribs = 0L);
  void append(const QChar *s, int l) {replace(len, 0, s, l);}
  void wrap(TextLine *nextLine, int pos);
  void unWrap(int pos, TextLine *nextLine, int len);
  void removeSpaces();
  int firstChar() const;
  QChar getChar(int pos) const;

  void setAttribs(int attribute, int start, int end);
  void setAttr(int attribute);
  int getAttr(int pos) const;
  int getAttr() const;
  int getRawAttr(int pos) const;
  int getRawAttr() const;

  void setContext(int context);
  int getContext() const;
  const QChar *getString();
  const QChar *getText() const;

  void select(bool sel, int start, int end);
  void selectEol(bool sel, int pos);
  void toggleSelect(int start, int end);
  void toggleSelectEol(int pos);
  int numSelected() const;
  bool isSelected(int pos) const;
  bool isSelected() const;
  int findSelected(int pos) const;
  int findUnselected(int pos) const;
  int findRevSelected(int pos) const;
  int findRevUnselected(int pos) const;

  int cursorX(int pos, int tabChars) const;

  void markFound(int pos, int l);
  void unmarkFound();

protected:
  int len;
  int size;
  QChar *text;
  uchar *attribs;
  uchar attr;
  int ctx;

public:
  void setGutter( void* id, bool begin, QString name );
  void removeGutter();
  gutterData* getGutter();

  QIntDict<int> *bookmarkData;

private:
  gutterData* gutter;
};

#endif
