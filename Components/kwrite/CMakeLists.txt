qt2_wrap_moc(libwrite_MOC
    SOURCES
        highlight.h
        kwdialog.h
        kwdoc.h
        kwmanager.h
        kwrite.h
        kwview.h
    )
add_library(libkwrite OBJECT
    highlight.cpp
    kwdialog.cpp
    kwdoc.cpp
    kwview.cpp
    kwmanager.cpp
    kwaction.cpp
    kwrite.cpp
    kwtextline.cpp
    ${libwrite_MOC}
)
target_link_libraries(libkwrite kde2::kfile)

