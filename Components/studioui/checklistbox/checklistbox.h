#ifndef CHECKLISTBOX_H
#define CHECKLISTBOX_H

#include <qframe.h>
#include <qlistbox.h>
#include <qdict.h>
#include <qstring.h>
#include <qlineedit.h>
#include <qlabel.h>

typedef QDict<QPixmap> CBImageList;

class QPainter;
class CheckListBox;
class QToolButton;
class QFrame;
class QHBoxLayout;
class QVBoxLayout;


#define CALLBACK_ACTION(FUNC) setActionFunction((CLBcallBackFunc)&FUNC)

class CBHeader : public QFrame
{Q_OBJECT
friend class CheckListBox;
private slots:
  void slotNewItem();
  void slotDelItem();
  void slotItemUp();
  void slotItemDown();

private:
  CBHeader( CheckListBox* parentListBox, const char * name=0 );
  ~CBHeader();

  QHBoxLayout* l;
  CheckListBox* lb;
  QLabel* label;
  QToolButton* button_up;
  QToolButton* button_down;
  QToolButton* button_new;
  QToolButton* button_del;
};
/*========================================================*/

class CBLineEdit : public QLineEdit
{Q_OBJECT
friend class CheckListBox;

  CBLineEdit( QWidget * parent=0, const char * name=0 );
  ~CBLineEdit();

  void keyPressEvent( QKeyEvent * );
  void focusOutEvent ( QFocusEvent * );

signals:
  void ignore();
};
/*========================================================*/

class CheckListBoxItem : public QListBoxText
{
  friend class CheckListBox;
public:
  CheckListBoxItem( const char*, bool, CheckListBox* );
  ~CheckListBoxItem();

  void setCheck( bool isChecked = true );
  bool isChecked();

  void allowChecked( bool allow, bool def = false );
  bool isAllowChecked();

  void allowEdit( bool allow );
  bool isAllowEdit();

  void allowAction( bool allow );
  bool isAllowAction();

  QString getActionText(){ return actionText; }
  bool    isActionShowText() { return actionShowText; }
  bool    isActionShowPix () { return actionShowPix;  }

  void setActionText( QString text ){ actionText = text; }
  void setActionShowText( bool f) { actionShowText = f; }
  void setActionShowPix ( bool f) { actionShowPix  = f; }

  void enableDeleted ( bool f) { enableDelete  = f; }
  bool isEnableDeleted () { return enableDelete; }

  // any user definity data
  int userData;

  QPixmap* pixmap(){ return pix; }
  void setPixmap( const QPixmap& );

  void update();

protected:
  virtual int width( const QListBox* )  const;
  virtual int height( const QListBox* ) const;

private:
  QPixmap* pix;

  QString actionText;
  bool    actionShowText;
  bool    actionShowPix;
  bool    enableDelete;

  CheckListBox* lb;
  bool ch;
  bool _allowEdit;
  bool _allowCheck;
  bool _allowAction;
  void paint( QPainter* );
  void CLBsetText( const char* t ){ setText(t); }
};
/*========================================================*/

class _QListBox : public QListBox
{Q_OBJECT
public:
  _QListBox( QWidget * parent=0, const char * name=0, WFlags f=0 )
  :QListBox(parent, name,f){};

  void updateItem( QListBoxItem* i ){ QListBox::updateItem(i); }

protected:
  virtual void viewportMousePressEvent( QMouseEvent* e )
  {
    emit signalMousePressEvent(e);
    QListBox::viewportMousePressEvent(e);
  }

signals:
  void signalMousePressEvent( QMouseEvent * );
};

/*========================================================*/
class CheckListBox : public QWidget
{Q_OBJECT
  friend class CheckListBoxItem;
  friend class CBHeader;
public:
  CheckListBox( QWidget * parent=0, const char * name=0, WFlags f=0 );
  ~CheckListBox();

  void clear(){ listBox->clear(); }
  uint count(){ return listBox->count(); }
  void removeItem( int id ){ listBox->removeItem(id); }
  void updateList();
  
  /* DO NO DELETE ITSELF THIS ITEM
     this is only for set any property
  */
  CheckListBoxItem* insertItem ( const char * text, bool checked = false, int index=-1 );
  CheckListBoxItem* item( int index );

  typedef QString(QObject::*CLBcallBackFunc)(int);
  void setActionFunction( CLBcallBackFunc f ){ getItemText = f; };

  bool isAllowChecked(){ return allowCheck;  }
  bool isAllowEdit()   { return allowEdit;   }
  bool isAllowAction() { return allowAction; }
  bool isIconShow()    { return showIcon;    }
  void allowChecked ( bool );
  void allowEditing ( bool allow ){ allowEdit   = allow; }
  void allowActions ( bool allow ){ allowAction = allow; }
  void allowIconShow( bool );

  void showHeader( bool );
  bool isShowHeader(){ return showH; }
  void setHeaderCaption( const QString& str ){ header->label->setText(str); }
  QString getHeaderCaption(){ return header->label->text(); }


protected:
  int getXstartTextPaint();

private:
  _QListBox* listBox;
  QVBoxLayout* layout;

  bool allowCheck;
  bool allowEdit;
  bool allowAction;
  bool showIcon;

  bool showH;

  QPixmap* checked_pix;
  QPixmap* unchecked_pix;
  QPixmap* notchecked_pix;
  QPixmap* action_pix;

  CBHeader* header;
  CBLineEdit* edit;
  QToolButton* button;
  QFrame* frame;
  QString lastDir;

  CLBcallBackFunc getItemText;
  CBImageList imList;

  void insertCBItem ( CheckListBoxItem*, int );

private slots:
  void slotMousePressEvent( QMouseEvent * );

  void slotClicked( QListBoxItem* );
  void slotSelected(int);
  void slotEditIgnore();
  void slotEditAccept();
  void slotButtonClick();

signals:
  void itemTextChange( int, const char*, const char* );
  void itemCheckChange( int, bool );
};

#endif
