#include "checklistbox.h"

#include <qapp.h>
#include <qpainter.h>
#include <qkeycode.h>
#include <qscrollbar.h>
#include <qtoolbutton.h>
#include <qdir.h>
#include <qlabel.h>
#include <qlayout.h>

#include <qfiledialog.h>

#include "checked.xpm"
#include "unchecked.xpm"
#include "notchecked.xpm"
#include "open.xpm"
#include "new_up.xpm"
#include "new_down.xpm"
#include "new_new.xpm"
#include "new_del.xpm"

CBHeader::CBHeader( CheckListBox* parentListBox, const char * name )
: QFrame( parentListBox, name ),
  lb(parentListBox)
{
  setFrameStyle( Panel | Sunken );
  setLineWidth( 1);

  label = new QLabel( this );
  label->setText("Header");
  label->setMinimumWidth(18);

  button_up = new QToolButton( this);
  button_up->setPixmap( new_up_xpm );
  button_up->setFixedSize( 18, 18 );
  connect( button_up, SIGNAL(clicked()), SLOT(slotItemUp()) );

  button_down = new QToolButton( this);
  button_down->setPixmap( new_down_xpm );
  button_down->setFixedSize( 18, 18 );
  connect( button_down, SIGNAL(clicked()), SLOT(slotItemDown()) );

  button_new = new QToolButton( this);
  button_new->setPixmap( new_new_xpm );
  button_new->setFixedSize( 18, 18 );
  connect( button_new, SIGNAL(clicked()), SLOT(slotNewItem()) );

  button_del = new QToolButton( this);
  button_del->setPixmap( new_del_xpm );
  button_del->setFixedSize( 18, 18 );
  connect( button_del, SIGNAL(clicked()), SLOT(slotDelItem()) );

  l = new QHBoxLayout( this, 2 );
  l->addWidget( label );
  l->addWidget( button_new );
  l->addWidget( button_up );
  l->addWidget( button_down );
  l->addWidget( button_del );
}

CBHeader::~CBHeader()
{
}

void CBHeader::slotNewItem()
{
  if ( lb->isAllowEdit() ){
    int index = lb->listBox->count();
    lb->insertItem( "", false, -1 );
    lb->listBox->setCurrentItem( index );
    lb->listBox->setBottomItem( index );
    lb->slotSelected( index );
  }
}

void CBHeader::slotDelItem()
{
  if ( lb->listBox->currentItem() == -1 ) return;
  if ( lb->item(lb->listBox->currentItem())->isEnableDeleted() )
    lb->listBox->removeItem( lb->listBox->currentItem() );
}

void CBHeader::slotItemUp()
{
  int c  = lb->listBox->currentItem();
  if ( c == -1 || c == 0 ) return;
  CheckListBoxItem* tempItem = lb->item(c);
  lb->listBox->takeItem( tempItem );
  lb->insertCBItem( tempItem, c - 1);
  lb->listBox->setCurrentItem( c - 1 );
}

void CBHeader::slotItemDown()
{
  int c  = lb->listBox->currentItem();
  if ( c == -1 || c == (int)lb->listBox->count()-1 ) return;
  CheckListBoxItem* tempItem = lb->item(c);
  lb->listBox->takeItem( tempItem );
  lb->insertCBItem( tempItem, c + 1);
  lb->listBox->setCurrentItem( c + 1 );
}

CBLineEdit::CBLineEdit( QWidget * parent, const char * name )
:QLineEdit( parent, name )
{
}

CBLineEdit::~CBLineEdit()
{
}

void CBLineEdit::focusOutEvent( QFocusEvent * )
{
  emit ignore();
}

void CBLineEdit::keyPressEvent( QKeyEvent *e )
{
  if ( e->key() == Key_Escape ) emit ignore();
  QLineEdit::keyPressEvent( e );
  e->accept();
}

CheckListBoxItem::CheckListBoxItem( const char * text, bool checked, CheckListBox* parent )
: QListBoxText(),
  pix( 0 ),
  enableDelete( true ),
  lb( parent ),
  ch( checked ),
  _allowEdit( parent->allowEdit ),
  _allowCheck( parent->allowCheck ),
  _allowAction( parent->allowAction )
{
  setText( text );
}

int CheckListBox::getXstartTextPaint()
{
  int x1 = allowCheck ? 16:0;
  int x2 = showIcon   ? 16:0;
  return  2+ x1 + x2;
}

void CheckListBoxItem::paint ( QPainter* p )
{
  int d = lb->getXstartTextPaint();

	QFontMetrics fm = p->fontMetrics();
	p->drawText( d ,  fm.ascent() + fm.leading()/2 + 1, text() );

  if ( lb->allowCheck ){
    if ( _allowCheck ){
      if ( ch ){
        int y1 = ( height(0L) - lb->checked_pix->height() ) /2;
        p->drawPixmap(2, y1, *lb->checked_pix );
      } else {
        int y2 = ( height(0L) - lb->unchecked_pix->height() ) /2;
        p->drawPixmap(2, y2, *lb->unchecked_pix );
      }
    } else {
        int y3 = ( height(0L) - lb->notchecked_pix->height() ) /2;
        p->drawPixmap(2, y3, *lb->notchecked_pix );
    }
  }

  if ( (lb->showIcon) && (pix != 0L) )
  {
    int y4 = ( height(0L) - pix->height() )/2;
    p->drawPixmap( lb->allowCheck ? 16:2 , y4, *pix );
  }
}

void CheckListBoxItem::setPixmap( const QPixmap& p )
{
  if ( pix ) delete pix;
  pix = 0L;
  if ( !p.isNull() ){
    pix = new QPixmap( p );
  }
  update();
}

void CheckListBoxItem::update()
{
  lb->listBox->updateItem(this);
}

int CheckListBoxItem::width( const QListBox* ) const
{
  return QListBoxText::width(lb->listBox) + lb->getXstartTextPaint();
}

int CheckListBoxItem::height( const QListBox* ) const
{
  int h = lb->fontMetrics().lineSpacing() + 2;
  return QMAX( 16, h );
}

CheckListBoxItem::~CheckListBoxItem()
{
}

/////////////////////////////////////////////////////////////////////////////////

CheckListBox::CheckListBox( QWidget * parent, const char * name, WFlags f )
:QWidget( parent,name,f )
{
  layout = new QVBoxLayout( this );

  listBox = new _QListBox( this, "ListBox" );
  listBox->setFocusPolicy( NoFocus );
  connect( listBox, SIGNAL(selected(int)), SLOT(slotSelected(int)) );
  connect( listBox, SIGNAL(clicked(QListBoxItem*)), SLOT(slotClicked(QListBoxItem*)) );
  connect( listBox, SIGNAL(signalMousePressEvent(QMouseEvent*)), SLOT(slotMousePressEvent(QMouseEvent*)) );

  header = new CBHeader(this, "CBHeader");

  layout->addWidget( header );
  layout->addWidget( listBox );

  showH = true;

  allowCheck  = true;
  allowEdit   = true;
  allowAction = true;
  showIcon    = false;

  getItemText = 0L;

  checked_pix = new QPixmap( checked_xpm );
  unchecked_pix = new QPixmap( unchecked_xpm );
  notchecked_pix = new QPixmap( notchecked_xpm );
  action_pix = new QPixmap( open_xpm );

  frame = new QFrame( listBox );
  frame->setFrameStyle( QFrame::Box | QFrame::Sunken );
  frame->setLineWidth( 1);
  frame->hide();

  edit = new CBLineEdit( frame );
  edit->setFrame(QFrame::NoFrame);

  button = new QToolButton( frame );
  button->setPixmap( *action_pix );
  connect( button, SIGNAL(clicked()), SLOT(slotButtonClick()) );

  QHBoxLayout* l = new QHBoxLayout( frame, 2 );
  l->addWidget( edit );
  l->addWidget( button );

  connect( edit, SIGNAL(ignore()), SLOT(slotEditIgnore()) );
  connect( edit, SIGNAL(returnPressed()), SLOT(slotEditAccept()) );

  connect( (QObject*)listBox->verticalScrollBar(), SIGNAL(sliderPressed()), SLOT(slotEditIgnore()) );
  connect( (QObject*)listBox->verticalScrollBar(), SIGNAL(valueChanged(int)), SLOT(slotEditIgnore()) );

  lastDir = "/";
}

CheckListBox::~CheckListBox()
{
  delete listBox;
}

CheckListBoxItem* CheckListBox::insertItem ( const char * text, bool checked, int index )
{
  CheckListBoxItem* item = new CheckListBoxItem( text, checked, this );
  listBox->insertItem( item, index );
  return item;
}

void CheckListBox::allowChecked( bool ch )
{
  if ( ch == allowCheck ) return;
  allowCheck = ch;
  updateList();
}

void CheckListBox::slotSelected( int id )
{
  if ( !(item(id)->_allowEdit) ) return;

  QRect ir = listBox->itemRect( item(id) );
  frame->setGeometry(listBox->contentsRect().left(), ir.top(),
                     ir.width(), listBox->cellHeight(id)+4);

  QRect r = frame->contentsRect();

  if ( item(id)->_allowAction ){
    button->show();
    button->setFixedSize( r.height(), r.height() );
  } else {
    button->hide();
  }

  edit->setText( listBox->text(id) );
  edit->setFont(font());
  edit->selectAll();
  frame->show();
  edit->setFocus();
}

void CheckListBox::slotEditIgnore()
{
  listBox->setFocus();
  frame->hide();
}

void CheckListBox::slotEditAccept()
{
  int c = listBox->currentItem();
  QString oldText = listBox->text(c);
  item(c)->CLBsetText( edit->text() );
  listBox->setFocus();
  frame->hide();
  emit itemTextChange( c , edit->text(), oldText);
  updateList();
}

void CheckListBox::slotButtonClick()
{
  QString newText = "";
  if ( getItemText == 0L ){
    QString curDir = listBox->text( listBox->currentItem() );
    if ( !QDir(curDir).exists() ) curDir = lastDir.data();
    newText = QFileDialog::getExistingDirectory( curDir );
  } else {
    newText =(this->*getItemText)( listBox->currentItem() );
  }
  if ( newText.isEmpty() ) return;
  edit->setText( newText);
  slotEditAccept();
  lastDir = newText;
}

CheckListBoxItem* CheckListBox::item( int index )
{
  return (CheckListBoxItem*)(listBox->item(index));
}

bool CheckListBoxItem::isChecked()
{
  return ch;
}

void CheckListBoxItem::setCheck( bool isChecked )
{
  if ( ch == isChecked ) return;
  ch = isChecked;
  emit lb->itemCheckChange( lb->listBox->index(this), ch );
  update();
}

void CheckListBoxItem::allowChecked( bool allow, bool def = false )
{
  ch = def;
  _allowCheck = allow;
  update();
}

bool CheckListBoxItem::isAllowChecked()
{
  return _allowCheck;
}

void CheckListBoxItem::allowEdit( bool allow )
{
  _allowEdit = allow;
}

bool CheckListBoxItem::isAllowEdit()
{
  return _allowEdit;
}

void CheckListBoxItem::allowAction( bool allow )
{
  _allowAction = allow;
}

bool CheckListBoxItem::isAllowAction()
{
  return _allowAction;
}

void CheckListBox::allowIconShow( bool allow )
{
  showIcon = allow;
  updateList();
}

void CheckListBox::showHeader( bool show )
{
  showH = show;
  if ( showH ){
    header->show();
  } else {
    header->hide();
  }
}

void CheckListBox::insertCBItem( CheckListBoxItem* item , int index )
{
  listBox->insertItem( item, index );
}

void CheckListBox::slotMousePressEvent( QMouseEvent* evt )
{
  if ( evt->x() < 20 && allowCheck ){
   int id = listBox->index( listBox->itemAt( ((QMouseEvent*)evt)->pos() ) );
   if ( id != -1 )
     listBox->setCurrentItem( id );
     if ( item(id)->_allowCheck ){
       item(id)->ch = !(item(id)->ch);
       item(id)->update();
       updateList();
       emit itemCheckChange( id, item(id)->ch );
     }
  }
}

void CheckListBox::slotClicked( QListBoxItem* )
{
  slotEditIgnore();
}

void CheckListBox::updateList()
{
  if ( isUpdatesEnabled() ){
    int f = listBox->index( listBox->firstItem() );
    int c = listBox->currentItem();
    if ( f == c ){
      QListBoxText* t = new QListBoxText( listBox, "" );
      delete t;
    } else {
      listBox->setCurrentItem( f );
    }
    if ( c != -1 ){
      listBox->setCurrentItem( c );
    }
  }
}
