#ifndef SACTION_H
#define SACTION_H

#include <qobject.h>
#include <qdict.h>
#include <qpixmap.h>
#include <qiconset.h>
#include <qbitmap.h>
#include <qimage.h>
#include <qlist.h>
#include <ktoolbar.h>
#include <kpopupmenu.h>
#include <kaccel.h>
#include <kstdaccel.h>

struct ActionData
{
  int id;
  int accel;
  QPopupMenu* popup;
  QPopupMenu* popupForMenu;
  QPopupMenu* delayedPopup;
  const char* label;
  QPixmap* pix;
  QString icon;
  bool enabled;
  const QObject* receiver;
  const char* slot;
  const char* tooltiptext;
  QList<QObject> controls;
  QList<KAccel> accelControls;
};

class SAction : public QObject
{ Q_OBJECT
public:
  SAction( const char* );
  ~SAction();

  void addAction( const char* name, const char* label,
                  const QPixmap& pixmap,
                  const QObject *receiver, const char *slot,
                  int accel = 0, const char *tooltiptext = 0L,
                  QPopupMenu* delayedPopup = 0L
                );

  void addAction( const char* name, const char* label,
                  const QPixmap& pixmap,
                  const QObject *receiver, const char *slot,
                  KStdAccel::StdAccel stdAccel , const char *tooltiptext = 0L,
                  QPopupMenu* delayedPopup = 0L
                );

  void addAction( const char* name, const char* label,
                  const QPixmap& pixmap,
                  QPopupMenu *popup, const char *tooltiptext = 0L
                );

  void addAction( const char* name, const char* label,
                  const QString& pixmap,
                  const QObject *receiver, const char *slot,
                  int accel = 0, const char *tooltiptext = 0L,
                  QPopupMenu* delayedPopup = 0L
                );

  void addAction( const char* name, const char* label,
                  const QString& pixmap,
                  const QObject *receiver, const char *slot,
                  KStdAccel::StdAccel stdAccel , const char *tooltiptext = 0L,
                  QPopupMenu* delayedPopup = 0L
                );

  void addAction( const char* name, const char* label,
                  const QString& pixmap,
                  QPopupMenu *popup, const char *tooltiptext = 0L
                );

  void setActionPopupMenuForMenu( QString name, QPopupMenu* );

  void setToolbar( KToolBar*, const char* );
  void setMenu( KPopupMenu*, const char* , const char* );
  void setMenu( QPopupMenu*, const char* );
  void setAllAccel( KAccel* );

  KPopupMenu* createKPopupMenu( const char* , const char*  );
  QPopupMenu* createQPopupMenu( const char* );

  void setActionsEnabled( const char* , bool );
  bool isActionEnabled( const char* );
  
  void setDelayedPopup( KToolBar*, const char* , QPopupMenu* );
  void changeAllMenuAccel( KAccel* );
  void resetEnableFlagForAction( KAccel* );

private slots:
  void slotDestroyControls();

private:
  QPixmap pixmap(ActionData*);
  ActionData* findData( QString );
  void setActionEnabled( const char* , bool );
  void manageControls( QObject* obj );
  void changeMenuAccel( KAccel* , const char* );

  QDict<ActionData> *data;
  int globalId;
};
#endif


