#include "saction.h"

#include <kiconloader.h>

SAction::SAction( const char* name ) : QObject( 0L, name )
{
  data = new QDict<ActionData>;
  data->setAutoDelete( true );
  globalId = 0;
};

SAction::~SAction()
{
  delete data;
}

void SAction::addAction( const char* name, const char* label,
                         const QPixmap& pixmap,
                         const QObject *receiver, const char *slot,
                         int accel, const char *tooltiptext, QPopupMenu* delayedPopup )
{
  ActionData* tdata = new ActionData;
  tdata->id  = globalId++;
  tdata->accel = accel;
  tdata->pix = ( pixmap.isNull() ) ? 0L : new QPixmap(pixmap);
  tdata->icon = "";
  tdata->label = label;
  tdata->enabled  = true;
  tdata->tooltiptext = tooltiptext;
  tdata->receiver = receiver;
  tdata->slot = slot;
  tdata->popup = 0L;
  tdata->delayedPopup = delayedPopup;
  tdata->popupForMenu = 0L;

  data->insert( name, tdata );
}

void SAction::addAction( const char* name, const char* label,
                         const QPixmap& pixmap,
                         QPopupMenu *popup, const char *tooltiptext )
{
  ActionData* tdata = new ActionData;
  tdata->id  = globalId++;
  tdata->accel = 0;
  tdata->pix = ( pixmap.isNull() ) ? 0L : new QPixmap(pixmap);
  tdata->icon = "";
  tdata->label = label;
  tdata->enabled  = true;
  tdata->tooltiptext = tooltiptext;
  tdata->popup = popup;
  tdata->popupForMenu = 0L;

  data->insert( name, tdata );
}

void SAction::addAction( const char* name, const char* label,
                         const QPixmap& pixmap,
                         const QObject *receiver, const char *slot,
                         KStdAccel::StdAccel stdAccel, const char *tooltiptext, QPopupMenu* delayedPopup )
{
  ActionData* tdata = new ActionData;
  tdata->id  = globalId++;
  tdata->accel = KStdAccel::key( stdAccel );
  tdata->pix = ( pixmap.isNull() ) ? 0L : new QPixmap(pixmap);
  tdata->icon = "";
  tdata->label = label;
  tdata->enabled  = true;
  tdata->tooltiptext = tooltiptext;
  tdata->receiver = receiver;
  tdata->slot = slot;
  tdata->popup = 0L;
  tdata->delayedPopup = delayedPopup;
  tdata->popupForMenu = 0L;

  data->insert( name, tdata );
}

void SAction::addAction( const char* name, const char* label,
                         const QString& pixmap,
                         const QObject *receiver, const char *slot,
                         int accel, const char *tooltiptext, QPopupMenu* delayedPopup )
{
  ActionData* tdata = new ActionData;
  tdata->id  = globalId++;
  tdata->accel = accel;
  tdata->pix = 0L;
  tdata->icon = pixmap;
  tdata->label = label;
  tdata->enabled  = true;
  tdata->tooltiptext = tooltiptext;
  tdata->receiver = receiver;
  tdata->slot = slot;
  tdata->popup = 0L;
  tdata->delayedPopup = delayedPopup;
  tdata->popupForMenu = 0L;

  data->insert( name, tdata );
}

void SAction::addAction( const char* name, const char* label,
                         const QString& pixmap,
                         QPopupMenu *popup, const char *tooltiptext )
{
  ActionData* tdata = new ActionData;
  tdata->id  = globalId++;
  tdata->accel = 0;
  tdata->pix = 0L;
  tdata->icon = pixmap;
  tdata->label = label;
  tdata->enabled  = true;
  tdata->tooltiptext = tooltiptext;
  tdata->popup = popup;
  tdata->popupForMenu = 0L;

  data->insert( name, tdata );
}

void SAction::addAction( const char* name, const char* label,
                         const QString& pixmap,
                         const QObject *receiver, const char *slot,
                         KStdAccel::StdAccel stdAccel, const char *tooltiptext, QPopupMenu* delayedPopup )
{
  ActionData* tdata = new ActionData;
  tdata->id  = globalId++;
  tdata->accel = KStdAccel::key( stdAccel );
  tdata->pix = 0L;
  tdata->icon = pixmap;
  tdata->label = label;
  tdata->enabled  = true;
  tdata->tooltiptext = tooltiptext;
  tdata->receiver = receiver;
  tdata->slot = slot;
  tdata->popup = 0L;
  tdata->delayedPopup = delayedPopup;
  tdata->popupForMenu = 0L;

  data->insert( name, tdata );
}

void SAction::setToolbar( KToolBar* bar , const char* sdata )
{
  manageControls( bar );

  QString names = sdata;
  names = names.stripWhiteSpace();
  names += " ";
  while ( names.find(" ") != -1 ){
    QString name = names.left( names.find(" ") );
    names.remove( 0, names.find(" ") + 1 );
    if ( name == "|" )
      bar->insertSeparator();
    else {
      ActionData* tdata = findData( name );
      if ( tdata != 0L ){
        if ( tdata->popup != 0L ) {
          if (tdata->pix)
          bar->insertButton( *tdata->pix, tdata->id, tdata->popup, tdata->enabled, tdata->tooltiptext );
            else
          bar->insertButton( tdata->icon, tdata->id, tdata->popup, tdata->enabled, tdata->tooltiptext );
         } else {
          if (tdata->pix)
          bar->insertButton( *tdata->pix, tdata->id, SIGNAL(clicked()), tdata->receiver, tdata->slot, tdata->enabled, tdata->tooltiptext );
            else
          bar->insertButton( tdata->icon, tdata->id, SIGNAL(clicked()), tdata->receiver, tdata->slot, tdata->enabled, tdata->tooltiptext );
          if ( tdata->delayedPopup != 0L )
            bar->setDelayedPopup( tdata->id, tdata->delayedPopup );
        }
        tdata->controls.append( bar );
      }
    }
  }
}

void SAction::setActionsEnabled( const char* sdata, bool enabled )
{
  QString names = sdata;
  names = names.stripWhiteSpace();
  names += " ";
  while ( names.find(" ") != -1 ){
    QString name = names.left( names.find(" ") );
    names.remove( 0, names.find(" ") + 1 );
    if ( name != "|" ) setActionEnabled( name, enabled );
  }
}

void SAction::setActionEnabled( const char* name, bool enabled )
{
  ActionData* tdata = findData( name );
  if ( tdata == 0L ){
    debug("Cannot find action :%s", name);
    return;
  }
  tdata->enabled = enabled;

  for ( uint k = 0; k < tdata->controls.count(); k++ ){
    QObject* c = tdata->controls.at(k);
    if ( c->isA("KToolBar") ) ((KToolBar*)c)->setItemEnabled( tdata->id, enabled );
    if ( c->inherits("QPopupMenu") ) ((QPopupMenu*)c)->setItemEnabled( tdata->id, enabled );
  }

  for ( uint k = 0; k < tdata->accelControls.count(); k++ ){
    KAccel* a = tdata->accelControls.at(k);
    if ( enabled ){
      a->connectItem( tdata->tooltiptext, tdata->receiver, tdata->slot );
    } else {
      a->disconnectItem( tdata->tooltiptext, tdata->receiver, tdata->slot );
    }
  }
}

bool SAction::isActionEnabled( const char* name )
{
  ActionData* tdata = findData( name );
  if ( tdata == 0L ) return false;
  return tdata->enabled;
}

void SAction::setAllAccel( KAccel* accel )
{
  QDictIterator<ActionData> it(*data);
  while ( it.current() ) {
    ActionData* tdata = it.current();
    accel->insertItem( tdata->tooltiptext, tdata->accel );
    if ( tdata->enabled ) accel->connectItem( tdata->tooltiptext, tdata->receiver, tdata->slot );
    tdata->accelControls.append( accel );
    ++it;
  }
}

void SAction::changeMenuAccel( KAccel* accel, const char* name )
{
  ActionData* tdata = findData( name );
  for ( uint k = 0; k < tdata->controls.count(); k++ ){
    QObject* c = tdata->controls.at(k);
    if ( c->inherits("QPopupMenu") )
      accel->changeMenuAccel( (QPopupMenu*)c, tdata->id, tdata->tooltiptext );
  }
}

void SAction::setMenu( KPopupMenu* menu, const char* title, const char* sdata )
{
  menu->setTitle( title );
  setMenu( menu, sdata);
}

void SAction::setMenu( QPopupMenu* menu, const char* sdata )
{
  manageControls( menu );

  QString names = sdata;
  names = names.stripWhiteSpace();
  names += " ";
  while ( names.find(" ") != -1 ){
    QString name = names.left( names.find(" ") );
    names.remove( 0, names.find(" ") + 1 );
    if ( name == "|" )
      menu->insertSeparator();
    else {
      ActionData* tdata = findData( name );
      if ( tdata != 0L ){
        if ( tdata->popupForMenu == 0L ){
          menu->insertItem( pixmap(tdata), tdata->label, tdata->receiver, tdata->slot, tdata->accel, tdata->id );
        } else {
          menu->insertItem( pixmap(tdata), tdata->label, tdata->popupForMenu, tdata->id );
          menu->setAccel( tdata->accel, tdata->id );
        }
        menu->setItemEnabled( tdata->id, tdata->enabled );
        tdata->controls.append( menu );
      }
    }
  }
}

KPopupMenu* SAction::createKPopupMenu( const char* title , const char* sdata )
{
  KPopupMenu* menu = new KPopupMenu();
  setMenu( menu, title, sdata );
  return menu;
}

QPopupMenu* SAction::createQPopupMenu( const char* sdata )
{
  QPopupMenu* menu = new QPopupMenu();
  setMenu( menu, sdata );
  return menu;
}

void SAction::manageControls( QObject* obj )
{
  disconnect( obj, 0, this, 0 );
  connect( obj, SIGNAL(destroyed()), SLOT(slotDestroyControls()) );
}

void SAction::slotDestroyControls()
{
  const QObject* c = sender();
  if ( c == 0L ) return;

  QDictIterator<ActionData> it(*data);
  while ( it.current() ) {
    ActionData* tdata = it.current();
    for ( uint k = 0; k < tdata->controls.count(); k++ ){
      QObject* a = tdata->controls.at(k);
      if ( c == a ) tdata->controls.remove(c);
    }
    ++it;
  }
}

void SAction::setDelayedPopup( KToolBar* toolbar , const char* name , QPopupMenu* menu )
{
  ActionData* tdata = findData( name );
  if ( tdata != 0L ){
    toolbar->setDelayedPopup ( tdata->id, menu );
  }
}

void SAction::changeAllMenuAccel( KAccel* accel )
{
  QDictIterator<ActionData> it(*data);
  while ( it.current() ) {
    changeMenuAccel( accel, it.currentKey() );
    ++it;
  }
}

void SAction::resetEnableFlagForAction( KAccel* accel )
{
  QDictIterator<ActionData> it(*data);
  while ( it.current() ) {
    ActionData* tdata = it.current();
    setActionsEnabled( it.currentKey(), tdata->enabled );
    ++it;
  }
}

ActionData* SAction::findData( QString name )
{
  ActionData* tdata = data->find( name );
  if ( tdata == 0L ) debug("SAction:: cannot find action name %s", name.data() );
  return tdata;
}

void SAction::setActionPopupMenuForMenu( QString name, QPopupMenu*  poupForMenu )
{
  ActionData* tdata = findData( name );
  if ( tdata != 0L ){
    tdata->popupForMenu = poupForMenu;
  }
}

QPixmap SAction::pixmap( ActionData* d )
{
  /* XPM */
  const char*null_pix[]={
  "16 16 1 1",
  "# c None",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################",
  "################"};

  if (d->pix)
    return *d->pix;
  if (d->icon.isEmpty())
    return QPixmap(null_pix);

  return BarIcon(d->icon);
}
