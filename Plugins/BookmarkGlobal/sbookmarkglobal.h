#ifndef SBOOKMARKGLOBAL_H
#define SBOOKMARKGLOBAL_H

#include <qobject.h>
#include <qdom.h>
#include <qlist.h>
#include <qstrlist.h>
#include <ksimpleconfig.h>
#include <kconfig.h>

class KDockWidget;
class QPixmap;
class KWrite;
class QPopupMenu;
class KPopupMenu;
class BookmarkListView;
class QListViewItem;

struct BookMarkGlobalData
{
  QString name;
  QString groupName;
  QString projectName;
  QString fileName;
  int line;
  int id;
};

class SBookmarkGlobal : public QObject
{ Q_OBJECT
public:
  SBookmarkGlobal( const char* name = 0 );
  ~SBookmarkGlobal();

  void init();

private slots:
  void closeWorkspace();
  void openWorkspace();
  void editorOpenFile( KWrite*, const QString& );
  void editorSaveFile( KWrite*, const QString& );

  void beforeShowPopupMenu();

  void slotAddBookmark();
  void slotGotoBookmark();

  void slotSetMenus();

  void slotGotoMenuActivate( int );
  void slotDelBookmarkOnCurrentLine();

  void slotTreePopup( QListViewItem*, const QPoint& ,int );
  void slotElementSelected( QDomElement& );

  void slotDelTreePopup();
  void slotGotoTreePopup();
  void slotGotoKPopup( int );

private:
  void popupOnCurrentEditor( KPopupMenu* );

  void addToEditorMenu();
  void addToAction();
  void addToMainMenu();

  void setMenu( QPopupMenu* );
  void createListView( BookmarkListView*, bool onlyGroup = false);

  void writeBookmarkData();
  void readBookmarkData();

  void deleteBookmark( int );
  void gotoBookmark( QDomElement& );

  void getElementFromId( int, QDomElement& result, const QDomElement& base );
  void trySetBookmarkHere( KWrite* editor, const QString& pname, const QString& fname, const QDomElement& base );

  BookMarkGlobalData* getDataFromName( QString );

  int findFreeId();

  int bID;

  QList<BookMarkGlobalData> *data;

  KDockWidget* bookmarkDock;
  BookmarkListView* bv;
  QPixmap **book_icons;
  QPopupMenu* gotoQPopup;
  QPopupMenu* gotoQPopupForAction;
  QPopupMenu* treePopup;
  KPopupMenu* kPopup;

  QDomDocument* document;
};
#endif
