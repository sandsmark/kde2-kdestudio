#include "addbookmarkdialog.h"
#include "bookmarklistview.h"

#include <qlabel.h>
#include <qapplication.h>
#include <qpushbutton.h>
#include <qlineedit.h>

AddBookmarkDialog::AddBookmarkDialog( BookmarkListView* parentList, const char* name )
: QDialog( 0L, name, true )
{
  m_listView = parentList;
  setCaption("Create new Bookmark");

  QPushButton* create = new QPushButton( this );
  create->setGeometry( 250, 222, 100, 24 );
  create->setText( "Create" );
  create->setDefault( true );
  create->setAutoDefault( true );
  connect( create, SIGNAL(clicked()), SLOT(accept()) );

  QPushButton* cancel = new QPushButton( this );
  cancel->setGeometry( 250, 256, 100, 24 );
  cancel->setText( "Cancel" );
  connect( cancel, SIGNAL(clicked()), SLOT(reject()) );

  QPushButton* newgroup = new QPushButton( this );
  newgroup->setGeometry( 250, 50, 100, 24 );
  newgroup->setText( "New Group" );
  connect( newgroup, SIGNAL(clicked()), SLOT(slotNewGroup()) );

  list = new BookmarkListView(this,0,true);
  list->setGeometry( 10, 50, 230, 230 );

  nameEdit = new QLineEdit( this );
  nameEdit->setGeometry( 120, 10, 230, 20 );
  nameEdit->setFocus();
  connect( nameEdit, SIGNAL(returnPressed()), SLOT(accept()) );

  QLabel* label = new QLabel( this );
  label->setGeometry( 10, 20, 100, 10 );
  label->setText( "Name:" );

  setFixedSize( 360,290 );
  move( (qApp->desktop()->width() - width())/2, (qApp->desktop()->height() - height())/2 );
}


AddBookmarkDialog::~AddBookmarkDialog()
{
}

void AddBookmarkDialog::slotNewGroup()
{
  QDialog* newGroupNameDialog = new QDialog( parentWidget(), "NewGroupDialog", true, 0 );
  newGroupNameDialog->setCaption("Enter new group name");
  newGroupNameDialog->setIcon( *icon() );

  QPushButton* ok = new QPushButton( newGroupNameDialog );
  ok->setGeometry( 144, 40, 96, 24 );
  ok->setText( "Ok" );
  ok->setDefault( true );
  ok->setAutoDefault( true );
  connect( ok, SIGNAL(clicked()), newGroupNameDialog, SLOT(accept()) );

  QPushButton* fcancel = new QPushButton( newGroupNameDialog );
  fcancel->setGeometry( 248, 40, 96, 24 );
  fcancel->setText( "Cancel" );
  connect( fcancel, SIGNAL(clicked()), newGroupNameDialog, SLOT(reject()) );

  QLineEdit* newGroupName = new QLineEdit( newGroupNameDialog );
  newGroupName->setGeometry( 120, 10, 224, 20 );
  newGroupName->setText( "" );
  newGroupName->setFocus();
  connect( newGroupName, SIGNAL(returnPressed()), newGroupNameDialog, SLOT(accept()) );

  QLabel* flabel = new QLabel( newGroupNameDialog );
  flabel->setGeometry( 8, 16, 100, 10 );
  flabel->setText( "New group name:" );

  newGroupNameDialog->setFixedSize( 350, 70 );

  newGroupNameDialog->move( (qApp->desktop()->width() - width())/2, (qApp->desktop()->height() - height())/2 );

  if ( newGroupNameDialog->exec() == QDialog::Accepted ){
    QString newname = newGroupName->text();
    if ( !newname.isEmpty() ){
      BookmarkListViewItem* item = parentForCreate();
      list->createGroup( item->element(), newname );
      item->setOpen(true);
    }
  }
  delete newGroupNameDialog;
}

BookmarkListViewItem* AddBookmarkDialog::parentForCreate()
{
  return (BookmarkListViewItem*)( list->currentItem()?list->currentItem():list->firstChild() );
}
