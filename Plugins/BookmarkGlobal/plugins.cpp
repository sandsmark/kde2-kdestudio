#include "plugins.h"
#include "sbookmarkglobal.h"

class BookMarkGlobalPlugins : public Plugin
{
public:
  BookMarkGlobalPlugins()
  {
    plgModul = new SBookmarkGlobal( "GlobalBookmark" );
  }
  ~BookMarkGlobalPlugins()
  {
    delete plgModul;
  }

  void init()
  {
    plgModul->init();
  }

private:
  SBookmarkGlobal* plgModul;
};

extern "C"{
  Plugin* Register()
  {
    return new BookMarkGlobalPlugins;
  }
}
