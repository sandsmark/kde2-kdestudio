#include "sbookmarkglobal.h"
#include "studio.h"
#include "studioview.h"
#include "seditwindow.h"
#include "saction.h"
#include "kwmanager.h"
#include "tools.h"
#include "workspace.h"

#include "bookmarklistview.h"

#include <kdockwidget.h>
#include <kmenubar.h>
#include <qapp.h>
#include <qpixmap.h>
#include <qimage.h>
#include <qmessagebox.h>
#include <qlineedit.h>
#include <qtextstream.h>

SBookmarkGlobal::SBookmarkGlobal( const char* name )
:QObject( 0L, name )
{
  #include "bookmark16x16.xpm"
  book_icons = new QPixmap *[56];

  QImage* allpix = new QImage( bookmark16x16 );
  for ( int x = 0; x < 14; x++ )
    for ( int y = 0; y < 4; y++ ){
      book_icons[ x + y*14 ] = new QPixmap();
      book_icons[ x + y*14 ]->convertFromImage( allpix->copy( x*16, y*16, 16, 16 ) );
    }
  delete allpix;
  data = new QList<BookMarkGlobalData>;
  data->setAutoDelete( true );

  treePopup = 0L;
  gotoQPopup = new QPopupMenu();                                        
  gotoQPopupForAction = new QPopupMenu();                               
  kPopup = 0L;                                                          

  document = new QDomDocument();
}

SBookmarkGlobal::~SBookmarkGlobal()
{
  if ( document )
    delete document;
}

void SBookmarkGlobal::init()
{
  addToAction();
  addToMainMenu();
  addToEditorMenu();

  connect( WORKSPACE, SIGNAL(closeMainWorkspace()), SLOT(closeWorkspace()) );
  connect( WORKSPACE, SIGNAL(openMainWorkspace()), SLOT(openWorkspace()) );

  connect( EDIT_WINDOW, SIGNAL(EditorOpenFile(KWrite*,const QString&)), SLOT(editorOpenFile(KWrite*,const QString&)) );
  connect( EDIT_WINDOW, SIGNAL(EditorSaveFile(KWrite*,const QString&)), SLOT(editorSaveFile(KWrite*,const QString&)) );

  connect( gotoQPopup, SIGNAL(aboutToShow()), SLOT(slotSetMenus()) );
  connect( gotoQPopup, SIGNAL(activated(int)), SLOT(slotGotoMenuActivate(int)) );

  connect( gotoQPopupForAction, SIGNAL(aboutToShow()), SLOT(slotSetMenus()) );
  connect( gotoQPopupForAction, SIGNAL(activated(int)), SLOT(slotGotoMenuActivate(int)) );

  bookmarkDock = STUDIO_VIEW->createDockWidget( "Bookmarks", *book_icons[15] );
  bv = new BookmarkListView( bookmarkDock );
  bookmarkDock->setWidget( bv );
  bv->setFocusPolicy( QWidget::NoFocus );

  connect( bv, SIGNAL(rightButtonPressed(QListViewItem*,const QPoint&,int)), SLOT(slotTreePopup(QListViewItem*,const QPoint&,int)) );
  connect( bv, SIGNAL(elementSelected(QDomElement&)), SLOT(slotElementSelected(QDomElement&)) );

  #include "globalbookmark.xpm"
  bID = EDITOR_MANAGER->reservedBookmarks( globalbookmark_xpm );
}

void SBookmarkGlobal::addToAction()
{
  ACTION->addAction( "addbookmarkglobal", "Add Global &Bookmark", *book_icons[3], this, SLOT(slotAddBookmark()), 0, ("Bookmark Global Add") );
//  ACTION->addAction( "addbookmarkglobal", ("Add Global &Bookmark"), *book_icons[3], this, SLOT(slotAddBookmark()), 0, ("Bookmark Global Add") );
//  ACTION->addAction( "delbookmarkglobal", ("Delete Global Bookmark"), *book_icons[3], this, SLOT(slotDelBookmarkOnCurrentLine()), 0, ("Bookmark Global Delete") );
//  ACTION->addAction( "gotobookmarkglobal", ("Goto Global Bookmark"), *book_icons[3], this, SLOT(slotGotoBookmark()), Key_Q+ALT, ("Bookmark Global Goto") );
//  ACTION->setActionPopupMenuForMenu( "gotobookmarkglobal", gotoQPopupForAction );
}

void SBookmarkGlobal::addToEditorMenu()
{
//  ACTION->setMenu( EDITOR_POPUPMENU, "| addbookmarkglobal gotobookmarkglobal delbookmarkglobal");
  ACTION->setMenu( EDITOR_POPUPMENU, "| addbookmarkglobal");
  connect( EDITOR_POPUPMENU, SIGNAL(aboutToShow()), SLOT(beforeShowPopupMenu()) );
}

void SBookmarkGlobal::slotAddBookmark()
{
  if (!WORKSPACE->Ok())
    return;
    
  KWrite* editor = EDIT_WINDOW->getActiveEditor();
  if ( !editor )
    return;

  QString fileName;
  QString projectName;
  splitPathToNameAndProjectName( editor->fileName(), fileName, projectName );
  int line = editor->currentLine();
  QString bname = projectName + ":" + fileName + ":" + QString().setNum(line+1);

  QDomElement element;
  bv->addNewBookmark( bname,element );
  if ( !element.isNull() ){
    int id = findFreeId();
    element.setAttribute("projectName",projectName);
    element.setAttribute("fileName",fileName);
    element.setAttribute("line",line);
    element.setAttribute("id",id);

    EDITOR_MANAGER->setBookmark( editor, bID, line, id );
  }
  writeBookmarkData();
}

void SBookmarkGlobal::slotGotoBookmark()
{
  if ( kPopup != 0L )
    delete kPopup;
    
  kPopup = new KPopupMenu();
  kPopup->setTitle("Goto Global Bookmark");
  connect( kPopup, SIGNAL(activated(int)), SLOT(slotGotoKPopup(int)) );
  setMenu( kPopup );
  kPopup->setActiveItem( QMIN(kPopup->count()-1, 1) );

  popupOnCurrentEditor( kPopup );
}

void SBookmarkGlobal::popupOnCurrentEditor( KPopupMenu* kPopup )
{
  KWrite* editor = EDIT_WINDOW->getActiveEditor();
  QPoint p = ( editor ) ? editor->mapToGlobal( editor->getTextCursorPosition() ) : QCursor::pos();

  kPopup->exec(p);
}

void SBookmarkGlobal::slotSetMenus()
{
  ((QPopupMenu*)sender())->clear();
  setMenu( (QPopupMenu*)sender() );
}

void SBookmarkGlobal::setMenu( QPopupMenu* menu )
{
//  if ( config == 0L ) return;

  // build simple tree menu for bookmarks
  QDict<QPopupMenu> dict;
//  config->setGroup( "BookmarkGlobal" );
  QStrList l;
//  config->readListEntry( "TreeListData", l );
  l.first();
  while ( l.count() != 0 ){
    QString  s = l.current();
    s.remove( 0, s.find("/") + 1 );
    QString s1 = "";
    QString s2 = "";
    while ( s.find("/") != -1 ){
      s2 = s1;
      s1 = s.left( s.find("/") );
      s.remove( 0, s.find("/") + 1 );
    }
    if ( s2.isEmpty() ){
      if ( !s1.isEmpty() ){
        QPopupMenu* m = new QPopupMenu();
        connect( m, SIGNAL(activated(int)), SLOT(slotGotoMenuActivate(int)) );
        menu->insertItem( *book_icons[5], s1, m );
        dict.insert( s1 , m );
      }
      l.remove();
    } else {
      QPopupMenu* m = dict.find( s2 );
      if ( m != 0L ){
        QPopupMenu* m1 = new QPopupMenu();
        connect( m1, SIGNAL(activated(int)), SLOT(slotGotoMenuActivate(int)) );
        m->insertItem( *book_icons[5], s1, m1 );
        dict.insert( s1 , m1 );
        l.remove();
      } else {
        if ( l.next() == 0L ) l.first(); else l.next();
      }
    }
  }
  for ( uint i = 0; i < data->count(); i++ ){
    BookMarkGlobalData* d = data->at(i);
    QPopupMenu* m = dict.find( d->groupName );
    if ( m != 0L )
      m->insertItem( *book_icons[15], d->name );
    else
      menu->insertItem( *book_icons[15], d->name );
  }
}

void SBookmarkGlobal::slotGotoMenuActivate( int id )
{
//  BookMarkGlobalData* d = getDataFromName( ((QPopupMenu*)sender())->text( id ) );
//  gotoBookmark( d );
}

void SBookmarkGlobal::closeWorkspace()
{
  bv->clear();
  writeBookmarkData();
}

void SBookmarkGlobal::openWorkspace()
{
  readBookmarkData();
}

void SBookmarkGlobal::readBookmarkData()
{
  KSimpleConfig* config = WORKSPACE->getConfig();
  config->setGroup( "BookmarkGlobal" );
  QString data = config->readEntry("Data");

  if ( document )
    delete document;

  if ( data.isEmpty() ){
    document = new QDomDocument("Bookmarks");
    QDomElement element = document->createElement("Bookmarks");
    element.setAttribute("type","root");
    document->appendChild(element);
  } else {
    document = new QDomDocument();
    document->setContent( data );
  }
  QDomElement element = document->documentElement();
  bv->setData( element );
}

void SBookmarkGlobal::writeBookmarkData()
{
  if (!WORKSPACE->Ok())
    return;
    
  QString data;
  QTextStream stream( &data, IO_WriteOnly );
  document->save(stream,2);

  KSimpleConfig* config = WORKSPACE->getConfig();
  config->setGroup( "BookmarkGlobal" );
  config->writeEntry( "Data", data );
  config->sync();
}

void SBookmarkGlobal::beforeShowPopupMenu()
{
  if (!WORKSPACE->Ok())
    return;
    
//  ACTION->setActionsEnabled( "addbookmarkglobal delbookmarkglobal", false );
  ACTION->setActionsEnabled( "addbookmarkglobal", false );
  KWrite* editor = EDIT_WINDOW->getActiveEditor();
  if ( editor != 0L ){
    int i = EDITOR_MANAGER->getBookmark( editor, bID, editor->currentLine() );
    ACTION->setActionsEnabled( "addbookmarkglobal", (i == 0) );
//    ACTION->setActionsEnabled( "delbookmarkglobal", (i > 0) );
  }
}

void SBookmarkGlobal::slotDelBookmarkOnCurrentLine()
{
  if (!WORKSPACE->Ok())
    return;
    
  KWrite* editor = EDIT_WINDOW->getActiveEditor();
  if ( editor != 0L ){
    int i = EDITOR_MANAGER->getBookmark( editor, bID, editor->currentLine() );
    deleteBookmark( i );
  }
}

int SBookmarkGlobal::findFreeId()
{
  int id = 1;
  for (;;id++){
    QDomElement element;
    getElementFromId(id,element,document->documentElement());
    if (element.isNull())
      break;
  }
  return id;
}

void SBookmarkGlobal::editorSaveFile( KWrite* editor , const QString& name )
{
  if (!WORKSPACE->Ok())
    return;
    
  // fixed bookmark->line for this file and save it;
  for ( int k = 0; k < editor->doc()->lastLine() + 1; k++ ){
    int id = EDITOR_MANAGER->getBookmark( editor, bID, k );
    if ( id > 0 ){
      QDomElement element;
      getElementFromId(id,element,document->documentElement());
      if (!element.isNull())
        element.setAttribute("line",k);
    }
  }
  writeBookmarkData();
}

void SBookmarkGlobal::getElementFromId( int id, QDomElement& result, const QDomElement& base )
{
  if ( base.isNull() )
    return;

  if ( base.attribute("type") == "item" && base.attribute("id").toInt() == id ){
    result = base;
    return;
  }

  QDomElement s = base.firstChild().toElement();
  for( ; !s.isNull(); s = s.nextSibling().toElement() ){
      getElementFromId(id,result,s);
  }
}

void SBookmarkGlobal::editorOpenFile( KWrite* editor , const QString& name )
{
  if (!WORKSPACE->Ok())
    return;
    
  // set bookmark for this file
  QString fileName;
  QString projectName;
  splitPathToNameAndProjectName( name, fileName, projectName );

  trySetBookmarkHere(editor,projectName,fileName,document->documentElement());

  for ( uint i = 0; i < data->count(); i++ ){
    BookMarkGlobalData* d = data->at(i);
    if ( d->fileName == fileName && d->projectName == projectName )
      EDITOR_MANAGER->setBookmark( editor, bID, d->line, d->id );
  }
}

void SBookmarkGlobal::trySetBookmarkHere( KWrite* editor, const QString& pname, const QString& fname, const QDomElement& base )
{
  if ( base.isNull() )
    return;

  if ( base.attribute("type") == "item" &&
       base.attribute("projectName") == pname &&
       base.attribute("fileName") == fname )
  {
    EDITOR_MANAGER->setBookmark( editor, bID, base.attribute("line").toInt(), base.attribute("id").toInt() );
  }

  QDomElement s = base.firstChild().toElement();
  for( ; !s.isNull(); s = s.nextSibling().toElement() ){
      trySetBookmarkHere(editor,pname,fname,s);
  }
}


void SBookmarkGlobal::deleteBookmark( int id )
{
/*
  BookMarkGlobalData* d = getDataFromId( id );
  if ( d != 0L ){
    Workspace* w = WORKSPACE->getWorkspaceFromName( d->projectName );
    KWrite* editor = EDIT_WINDOW->findKWriteFromFileName( w->getDir() + d->fileName );
    WORKSPACE->freeSubWorkspace( w );
    if ( editor != 0L ) EDITOR_MANAGER->delBookmark( editor, bID, d->id );
    data->remove( d );
    writeBookmarkData();
    createListView( bv );
  }
*/
}

BookMarkGlobalData* SBookmarkGlobal::getDataFromName( QString name )
{
  for ( BookMarkGlobalData* d = data->first(); d != 0L; d = data->next() )
    if ( d->name == name ) return d;
  return 0L;
}

void SBookmarkGlobal::slotElementSelected( QDomElement& element )
{
  if ( element.attribute("type") == "item" )
    gotoBookmark(element);
}

void SBookmarkGlobal::gotoBookmark( QDomElement& d )
{
  if ( !d.isNull() ){
    Workspace* w = WORKSPACE->getWorkspaceFromName( d.attribute("projectName") );
    EDIT_WINDOW->slotViewTreeListItem( w->getDir() + d.attribute("fileName") );
    KWrite* editor = EDIT_WINDOW->findKWriteFromFileName( w->getDir() + d.attribute("fileName") );
    WORKSPACE->freeSubWorkspace( w );
    if ( editor != 0L ) EDITOR_MANAGER->gotoBookmark( editor, bID, d.attribute("id").toInt() );
  }
}

void SBookmarkGlobal::slotTreePopup( QListViewItem* item, const QPoint &pos , int )
{
/*
  if ( item == 0L ) return;

  bv->blockSignals( true );

  bv->setSelected( item, true );
  bv->setCurrentItem( item );
  bv->ensureItemVisible( item );

  if ( !((BookmarkListViewItem*)item)->isTop ){
    if ( treePopup != 0L ) delete treePopup;
    treePopup = new QPopupMenu();

    if ( !((BookmarkListViewItem*)item)->isGroup ){
      treePopup->insertItem( *book_icons[15], "Goto bookmark", this, SLOT(slotGotoTreePopup()) );
      treePopup->insertItem( *book_icons[54], "Delete bookmark", this, SLOT(slotDelTreePopup()) );
    } else {
      treePopup->insertItem( *book_icons[54], "Delete group", this, SLOT(slotDelTreePopup()) );
    }
    treePopup->popup( pos );
  }
  bv->blockSignals( false );
*/
}

void SBookmarkGlobal::slotDelTreePopup()
{
/*
  QListViewItem* item = bv->currentItem();
  if ( item != 0L ){
    if ( ((BookmarkListViewItem*)item)->isGroup ){
      // delete tree part (GROUP)
      delete item;
      // and save changes
      QStrList l;
      l = bv->getTreeAsList();
//      config->setGroup( "BookmarkGlobal" );
//      config->writeEntry( "TreeListData", l );
//      config->sync();
      // delete all child bookmarks ( changes will be automatic saveed )
      for ( uint i = 0; i < data->count(); i++ ){
        BookMarkGlobalData* d = data->at(i);
        if ( bv->getGroupItem( d->groupName ) == 0L ) deleteBookmark( d->id );
      }
    } else {
      BookMarkGlobalData* d = getDataFromName( item->text(0) );
      if ( d != 0L ) deleteBookmark( d->id );
    }
  }
*/
}

void SBookmarkGlobal::slotGotoTreePopup()
{
/*
  QListViewItem* item = bv->currentItem();
  if ( item != 0L && !((BookmarkListViewItem*)item)->isGroup ){
    BookMarkGlobalData* d = getDataFromName( item->text(0) );
    gotoBookmark( d );
  }
*/
}

void SBookmarkGlobal::slotGotoKPopup( int id )
{
//  BookMarkGlobalData* d = getDataFromName( kPopup->text( id ) );
//  gotoBookmark( d );
}

void SBookmarkGlobal::addToMainMenu()
{
//  MENU_BAR->insertItem( "Boo&kmark", gotoQPopup );
}
