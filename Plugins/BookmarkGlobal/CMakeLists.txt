qt2_wrap_moc(libBookmarkGlobal_MOC
    SOURCES
        addbookmarkdialog.h
        bookmarklistview.h
        sbookmarkglobal.h
)
add_library(BookmarkGlobal MODULE
    sbookmarkglobal.cpp
    bookmarklistview.cpp
    addbookmarkdialog.cpp
    plugins.cpp
    ${libBookmarkGlobal_MOC}
)
target_link_libraries(BookmarkGlobal
    kde2::kdeui
    kinit_kdestudio
)


install(TARGETS BookmarkGlobal DESTINATION lib/kdestudio/plugins)
