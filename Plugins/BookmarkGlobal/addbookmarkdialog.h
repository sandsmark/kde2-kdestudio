#ifndef ADDBOOKMARKDIALOG_H
#define ADDBOOKMARKDIALOG_H

#include <qdialog.h>
#include <qdom.h>

class QLineEdit;
class BookmarkListView;
class BookmarkListViewItem;

class AddBookmarkDialog : public QDialog
{ Q_OBJECT
public:
  AddBookmarkDialog( BookmarkListView*, const char* name = 0 );
  ~AddBookmarkDialog();

  BookmarkListViewItem* parentForCreate();

  BookmarkListView* list;
  QLineEdit* nameEdit;
  BookmarkListView* m_listView;

private slots:
  void slotNewGroup();
};

#endif


