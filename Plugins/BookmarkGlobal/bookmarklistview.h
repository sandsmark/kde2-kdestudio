#ifndef BOOKMARKLISTVIEW_H
#define BOOKMARKLISTVIEW_H

#include <qlistview.h>
#include <qdom.h>

class BookmarkListViewItem;

class BookmarkListView : public QListView
{ Q_OBJECT
friend class BookmarkListViewItem;
public:
	BookmarkListView( QWidget* parent=0, const char *name=0, bool onlyGroups = false );
	~BookmarkListView();

  void setData( QDomElement& );
  void update();

  QPixmap* pixmap( QDomElement&, bool open = false );

  void addNewBookmark( const QString&, QDomElement& );
  BookmarkListViewItem* createGroup( QDomElement&, const QString& );

protected slots:
  void slotItemSelected( QListViewItem* );

signals:
  void elementSelected( QDomElement& );

private:
  void createItemsFromElement( QDomElement&, BookmarkListViewItem* );

  QPixmap **book_icons;

  QDomElement m_null_element;
  QDomElement rootElement;

  bool m_onlyGroups;
};

class BookmarkListViewItem : public QListViewItem
{
public:
	BookmarkListViewItem( BookmarkListView* parent, QDomElement& );
	BookmarkListViewItem( BookmarkListViewItem* parent, QDomElement& );

	~BookmarkListViewItem();

  virtual void paintCell( QPainter *, const QColorGroup & cg, int column, int width, int alignment );
  virtual int width ( const QFontMetrics &, const QListView *, int column ) const;

  BookmarkListViewItem* tryCreateGroupHere( QDomElement& , const QString& );

  QDomElement& element(){ return m_element; }
//  BookmarkListViewItem* after;
//  QPixmap *openPix;
//  bool isTop;
//  bool isGroup;

protected:
  QDomElement m_element;
};
#endif
