#include "bookmarklistview.h"
#include "addbookmarkdialog.h"
#include <qapplication.h>
#include <qlineedit.h>
#include <qpixmap.h>
#include <qheader.h>
#include <qimage.h>

BookmarkListView::BookmarkListView( QWidget* parent, const char *name, bool onlyGroups )
: QListView(parent,name)
{
  m_onlyGroups = onlyGroups;

  #include "bookmark16x16.xpm"
  book_icons = new QPixmap *[56];

  QImage* allpix = new QImage( bookmark16x16 );
  for ( int x = 0; x < 14; x++ )
    for ( int y = 0; y < 4; y++ ){
      book_icons[ x + y*14 ] = new QPixmap();
      book_icons[ x + y*14 ]->convertFromImage( allpix->copy( x*16, y*16, 16, 16 ) );
    }
  delete allpix;

  setSorting( -1 );                        
  setRootIsDecorated(true);
  addColumn("");
  header()->hide();
  setFrameStyle( QFrame::Panel | QFrame::Sunken );
  setLineWidth(2);

  connect( this, SIGNAL(clicked(QListViewItem*)), SLOT(slotItemSelected(QListViewItem*)) );
}

void BookmarkListView::slotItemSelected( QListViewItem* i )
{
  if (!i)
    return;

  emit elementSelected(((BookmarkListViewItem*)i)->element());
}

void BookmarkListView::setData( QDomElement& element )
{
  blockSignals( true );
  rootElement = element;
  BookmarkListViewItem* i = new BookmarkListViewItem( this , element );
  createItemsFromElement( element, i );
  blockSignals( false );
}

void BookmarkListView::createItemsFromElement( QDomElement& element, BookmarkListViewItem* item )
{
  if ( element.isNull() )
    return;

  QDomElement s = element.firstChild().toElement();
  for( ; !s.isNull(); s = s.nextSibling().toElement() ){
    if ( !m_onlyGroups || s.attribute("type")=="group" ){
      BookmarkListViewItem* i = new BookmarkListViewItem( item, s );
      createItemsFromElement( s, i );
    }
  }
}

void BookmarkListView::update()
{
  clear();
  setData(rootElement);
}

void BookmarkListView::addNewBookmark( const QString& text, QDomElement& element )
{
  AddBookmarkDialog* dialog = new AddBookmarkDialog( this );

  dialog->setIcon( *book_icons[14] );
  dialog->nameEdit->setText( text );
  dialog->list->setData( ((BookmarkListViewItem*)firstChild())->element() );

  if ( dialog->exec() == QDialog::Accepted ){
    QDomElement p = dialog->parentForCreate()->element();
    element = p.ownerDocument().createElement("Item");
    element.setAttribute("type","item");
    element.setAttribute("name",dialog->nameEdit->text());
    p.appendChild(element);
  }
  delete dialog;
  update();
}

BookmarkListViewItem* BookmarkListView::createGroup( QDomElement& parent, const QString& text )
{
  BookmarkListViewItem* item = (BookmarkListViewItem*)firstChild();
  return item->tryCreateGroupHere( parent, text );
}

BookmarkListView::~BookmarkListView()
{
}
/*********************************************************************************************/

BookmarkListViewItem::BookmarkListViewItem( BookmarkListView* parent, QDomElement& element )
: QListViewItem( parent )
{
  m_element = element;
  setText(0,element.tagName());
}

BookmarkListViewItem::BookmarkListViewItem( BookmarkListViewItem* parent, QDomElement& element )
: QListViewItem( parent )
{
  m_element = element;
  setText(0,element.attribute("name"));
}

BookmarkListViewItem::~BookmarkListViewItem()
{
}

BookmarkListViewItem* BookmarkListViewItem::tryCreateGroupHere( QDomElement& parent, const QString& text )
{
  if ( m_element == parent ){
    QDomElement element = m_element.ownerDocument().createElement("Group");
    element.setAttribute("type","group");
    element.setAttribute("name",text);
    m_element.appendChild(element);
    return new BookmarkListViewItem(this,element);
  }

  BookmarkListViewItem* item = (BookmarkListViewItem*)firstChild();
  BookmarkListViewItem* result = 0L;
  while ( item && !result ){
    result = item->tryCreateGroupHere(parent,text);
    item = (BookmarkListViewItem*)item->nextSibling();
  }
  return result;
}

QPixmap* BookmarkListView::pixmap( QDomElement& element, bool open )
{
  if ( element == rootElement )
    return book_icons[13];

  if ( element.attribute("type") == "group" )
    return open ? book_icons[0]:book_icons[1];

  // default for item
  return book_icons[39];
}

int BookmarkListViewItem::width(const QFontMetrics& fm, const QListView* lv, int ) const
{
  return  -(fm.minLeftBearing()+fm.minRightBearing()) + fm.width(text(0)) + 2*lv->itemMargin() + 20;
}

void BookmarkListViewItem::paintCell( QPainter * p, const QColorGroup & cg, int , int width, int align )
{
  if ( !p ) return;

  BookmarkListView* lv = (BookmarkListView*)listView();
  int r = lv->itemMargin();
  int marg = lv->itemMargin();

  QPixmap* cp = lv->pixmap( m_element, isOpen() );

  p->fillRect( 0, 0, width, height(), cg.base() );

  if ( isSelected() )
  {
    p->fillRect( r - marg, 0, width - r + marg, height(), QApplication::winStyleHighlightColor() );
    p->setPen( white );
  } else {
    p->setPen( cg.text() );
  }

  p->drawPixmap( r, (height()-cp->height())/2, *cp );
  r += cp->width() + marg + 3;

  p->drawText( r, 0, width-marg-r, height(), align | AlignVCenter, text(0) );
}
#if 0
/*
QStrList BookmarkListView::getTreeAsList()
{
  QStrList list;
  QDictIterator<BookmarkListViewItem> it(data);
  while ( it.current() != 0L )
  {
    BookmarkListViewItem* item = it.current();
    QString path = QString(item->text(0)) + "/";
    while ( item->parent() != 0L ){
      item = (BookmarkListViewItem*)item->parent();
      path = path.prepend( QString(item->text(0)) + "/" );
    }
    list.append( path );
    ++it;
  }
  return list;
}
*/
/*
void BookmarkListView::applyTreeFromList( QStrList list )
{
  clear();
  for ( uint k = 0; k < list.count(); k++ )
  {
    QString path = list.at(k);
    QString parentSub = "";
    while ( path.find("/") != -1 ){
      QString sub = path.left( path.find("/") );
      path.remove( 0, path.find("/") + 1 );
      if ( data.find( sub ) == 0L )
        if ( parentSub.isEmpty() ) insertGroup( sub ); else insertGroup( sub, parentSub );
      parentSub = sub;
    }
  }
}
*/
void BookmarkListView::selectGroup( QString name )
{
  if ( name.isEmpty() ) return;

  BookmarkListViewItem* item = data.find( name );
  if ( item != 0L ){
    QListViewItem* parent  = item->parent();
    while ( parent != 0L ){
      parent->setOpen( true );
      parent = parent->parent();
    }
    setSelected( item, true );
    setCurrentItem( item );
    ensureItemVisible( item );
  }
}
/*
void BookmarkListView::clear()
{
  data.clear();
  QListView::clear();

  BookmarkListViewItem* topItem = new BookmarkListViewItem( this , "Bookmarks" );
  topItem->after = 0L;
  data.insert( "Bookmarks", topItem );
  topItem->setOpen( true );
  selectGroup( "Bookmarks" );
}
*/
void BookmarkListView::saveData()
{
  sdata.clear();
  QDictIterator<BookmarkListViewItem> it(data);
  while ( it.current() != 0L )
  {
    BookmarkListViewItem* item = it.current();
    if ( item->isOpen() ) sdata.append( item->text(0) );
    ++it;
  }
  BookmarkListViewItem* item = (BookmarkListViewItem*)currentItem();
  if ( item == 0L ) sdataCurrentItem = "";
  else
//    if ( item->isGroup )
      sdataCurrentItem = item->text(0);
//    else
//      sdataCurrentItem = item->parent()->text(0);
}

void BookmarkListView::restoreData()
{
  for ( QString name = sdata.first(); name != 0L; name = sdata.next() )
  {
    BookmarkListViewItem* item = data.find( name );
    if ( item != 0L ) item->setOpen( true );
  }
  if ( !sdataCurrentItem.isEmpty() ){
    BookmarkListViewItem* item = data.find( sdataCurrentItem );
    if ( item != 0L ){
      setSelected( item, true );
      setCurrentItem( item );
      ensureItemVisible( item );
    }
  }
  sdata.clear();
  sdataCurrentItem = "";
}

BookmarkListViewItem* BookmarkListView::getGroupItem( QString name )
{
  return ( name.isEmpty() ) ? 0L : data.find( name );
}
/*
529-02-86, 902-6789719
*/
#endif