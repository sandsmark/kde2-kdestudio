#include "plugins.h"
#include "sbookmark.h"

class BookMarkPlugins : public Plugin
{
public:
  BookMarkPlugins()
  {
    plgModul = new SBookmark( "Bookmark" );
  }
  ~BookMarkPlugins()
  {
    delete plgModul;
  }

  void init()
  {
    plgModul->init();
  }

private:
  SBookmark* plgModul;
};

extern "C"{
  Plugin* Register()
  {
    return new BookMarkPlugins;
  }
}


