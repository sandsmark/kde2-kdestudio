#ifndef SBOOKMARK_H
#define SBOOKMARK_H

#include <qobject.h>
#include <qlist.h>

class QPixmap;
class KWrite;
class QPopupMenu;
class KPopupMenu;

struct BookMarkData
{
  BookMarkData() { set = false; }
  ~BookMarkData() {};
  QString projectName;
  QString fileName;
  int line;
  bool set;
  KWrite* editor; // only for temporary bookmark
};

class SBookmark : public QObject
{ Q_OBJECT
public:
  SBookmark( const char* name = 0 );
  ~SBookmark();

  void init();

private slots:
  void beforeShowPopupMenu();
  void editorCloseFile( KWrite*, QString );

  void slotSetBookmark();
  void slotGotoBookmark();
  void slotDeleteBookmark();

  void slotSetMenus();

  void slotSetMenuActivate( int );
  void slotGotoMenuActivate( int );
  void slotDeleteMenuActivate( int );

  void slotB0();
  void slotB1();
  void slotB2();
  void slotB3();
  void slotB4();
  void slotB5();
  void slotB6();
  void slotB7();
  void slotB8();
  void slotB9();

private:
  void popupOnCurrentEditor( KPopupMenu* );

  int bID;
  void addToEditorMenu();
  void addToAction();
  void setMenu( QPopupMenu* );

  QList<BookMarkData> *data;

  QPopupMenu* setQPopup;
  QPopupMenu* gotoQPopup;
  QPopupMenu* delQPopup;
  KPopupMenu* kPopup;
};


#endif
