#include "sbookmark.h"
#include "studio.h"
#include "seditwindow.h"
#include "saction.h"
#include "kwmanager.h"
#include "tools.h"

#include <klocale.h>
#include <qpixmap.h>
#include <qimage.h>

SBookmark::SBookmark( const char* name )
:QObject( 0L, name )
{
  data = new QList<BookMarkData>;
  data->setAutoDelete( true );
  for ( int i = 0; i < 10 + 1; i++ ) data->append( new BookMarkData );

  setQPopup = new QPopupMenu();
  gotoQPopup = new QPopupMenu();
  delQPopup = new QPopupMenu();
  kPopup = 0L;
}

SBookmark::~SBookmark()
{
}

void SBookmark::init()
{
  addToAction();

  connect( EDIT_WINDOW, SIGNAL(EditorBeforeClosingFile(KWrite*,QString)), SLOT(editorCloseFile(KWrite*,QString)) );

  addToEditorMenu();

  connect( setQPopup, SIGNAL(aboutToShow()), SLOT(slotSetMenus()) );
  connect( gotoQPopup, SIGNAL(aboutToShow()), SLOT(slotSetMenus()) );
  connect( delQPopup, SIGNAL(aboutToShow()), SLOT(slotSetMenus()) );

  connect( setQPopup, SIGNAL(activated(int)), SLOT(slotSetMenuActivate(int)) );
  connect( gotoQPopup, SIGNAL(activated(int)), SLOT(slotGotoMenuActivate(int)) );
  connect( delQPopup, SIGNAL(activated(int)), SLOT(slotDeleteMenuActivate(int)) );

  #include "bookmark.xpm"
  bID = EDITOR_MANAGER->reservedBookmarks( bookmark_xpm );
}

void SBookmark::addToAction()
{
  ACTION->addAction( "bookmark0", ("Goto bookmark0"), Ic(".xpm"), this, SLOT(slotB0()), CTRL + Key_0, ("Goto bookmark0") );
  ACTION->addAction( "bookmark1", ("Goto bookmark1"), Ic(".xpm"), this, SLOT(slotB1()), CTRL + Key_1, ("Goto bookmark1") );
  ACTION->addAction( "bookmark2", ("Goto bookmark2"), Ic(".xpm"), this, SLOT(slotB2()), CTRL + Key_2, ("Goto bookmark2") );
  ACTION->addAction( "bookmark3", ("Goto bookmark3"), Ic(".xpm"), this, SLOT(slotB3()), CTRL + Key_3, ("Goto bookmark3") );
  ACTION->addAction( "bookmark4", ("Goto bookmark4"), Ic(".xpm"), this, SLOT(slotB4()), CTRL + Key_4, ("Goto bookmark4") );
  ACTION->addAction( "bookmark5", ("Goto bookmark5"), Ic(".xpm"), this, SLOT(slotB5()), CTRL + Key_5, ("Goto bookmark5") );
  ACTION->addAction( "bookmark6", ("Goto bookmark6"), Ic(".xpm"), this, SLOT(slotB6()), CTRL + Key_6, ("Goto bookmark6") );
  ACTION->addAction( "bookmark7", ("Goto bookmark7"), Ic(".xpm"), this, SLOT(slotB7()), CTRL + Key_7, ("Goto bookmark7") );
  ACTION->addAction( "bookmark8", ("Goto bookmark8"), Ic(".xpm"), this, SLOT(slotB8()), CTRL + Key_8, ("Goto bookmark8") );
  ACTION->addAction( "bookmark9", ("Goto bookmark9"), Ic(".xpm"), this, SLOT(slotB9()), CTRL + Key_9, ("Goto bookmark9") );

  ACTION->addAction( "setbookmark", "&Set Bookmark", Ic("bookmarkmenu.xpm"), this, SLOT(slotSetBookmark()), CTRL + Key_K, ("Bookmark Set") );
  ACTION->addAction( "gotobookmark", "G&oto Bookmark", Ic("bookmarkmenu.xpm"), this, SLOT(slotGotoBookmark()), CTRL + Key_Q, ("Bookmark Goto") );
  ACTION->addAction( "delbookmark", "&Delete Bookmark", Ic("bookmarkmenu.xpm"), this, SLOT(slotDeleteBookmark()), CTRL + Key_D, ("Bookmark Delete") );

  ACTION->setActionPopupMenuForMenu( "setbookmark", setQPopup );
  ACTION->setActionPopupMenuForMenu( "gotobookmark", gotoQPopup );
  ACTION->setActionPopupMenuForMenu( "delbookmark", delQPopup );
}

void SBookmark::addToEditorMenu()
{
  ACTION->setMenu( EDITOR_POPUPMENU, "| setbookmark gotobookmark delbookmark");
  connect( EDITOR_POPUPMENU, SIGNAL(aboutToShow()), SLOT(beforeShowPopupMenu()) );
}

void SBookmark::slotSetBookmark()
{
 if ( kPopup != 0L ) delete kPopup;
  kPopup = new KPopupMenu();
  kPopup->setTitle("Set Bookmark");
  connect( kPopup, SIGNAL(activated(int)), SLOT(slotSetMenuActivate(int)) );
  setMenu( kPopup );
  kPopup->setActiveItem(1);

  popupOnCurrentEditor( kPopup );
}

void SBookmark::popupOnCurrentEditor( KPopupMenu* kPopup )
{
  KWrite* editor = EDIT_WINDOW->getActiveEditor();
  QPoint p = ( editor ) ? editor->mapToGlobal( editor->getTextCursorPosition() ) : QCursor::pos();

  kPopup->exec(p);
}

void SBookmark::slotGotoBookmark()
{
 if ( kPopup != 0L ) delete kPopup;
  kPopup = new KPopupMenu();
  kPopup->setTitle("Goto Bookmark");
  connect( kPopup, SIGNAL(activated(int)), SLOT(slotGotoMenuActivate(int)) );
  setMenu( kPopup );
  kPopup->setActiveItem(1);

  popupOnCurrentEditor( kPopup );
}

void SBookmark::slotDeleteBookmark()
{
 if ( kPopup != 0L ) delete kPopup;
  kPopup = new KPopupMenu();
  kPopup->setTitle("Delete Bookmark");
  connect( kPopup, SIGNAL(activated(int)), SLOT(slotDeleteMenuActivate(int)) );
  setMenu( kPopup );
  kPopup->setActiveItem(1);

  popupOnCurrentEditor( kPopup );
}

void SBookmark::slotSetMenus()
{
  setQPopup->clear();
  gotoQPopup->clear();
  delQPopup->clear();
  setMenu( setQPopup );
  setMenu( gotoQPopup );
  setMenu( delQPopup );
}

void SBookmark::setMenu( QPopupMenu* menu )
{
  menu->setCheckable( true );
  for ( int i = 1; i <= 10; i++ ){
    QString num = QString().setNum(i-1);
    num.prepend("&");
    num.append(" ");
    BookMarkData* d = data->at( i );
    menu->insertItem( num + ( d->set ? d->fileName:QString("empty")), i );
    menu->setItemChecked( i, d->set );
  }
}

void SBookmark::beforeShowPopupMenu()
{
  ACTION->setActionsEnabled( "setbookmark gotobookmark delbookmark", false );
  KWrite* editor = EDIT_WINDOW->getActiveEditor();
  if ( editor != 0L ){
    ACTION->setActionsEnabled( "gotobookmark delbookmark", true );
    if (  EDITOR_MANAGER->getBookmark( editor, bID, editor->currentLine() ) == 0 )
      ACTION->setActionsEnabled( "setbookmark", true );
  }
}

void SBookmark::editorCloseFile( KWrite* editor , QString )
{
  for ( int i = 1; i <= 10; i++ ){
    BookMarkData* d = data->at( i );
    if ( d->set && d->editor == editor )
      slotDeleteMenuActivate( i );
  }
}

void SBookmark::slotSetMenuActivate( int id )
{
  KWrite* editor = EDIT_WINDOW->getActiveEditor();
  if ( editor != 0L ){
    BookMarkData* d = data->at( id );
    if ( d->set ) return;
    d->editor = editor;
    d->line = editor->currentLine();
    splitPathToNameAndProjectName( editor->fileName(), d->fileName, d->projectName );
    EDITOR_MANAGER->setBookmark( editor, bID, d->line, id );
    d->set = true;
  }
}

void SBookmark::slotGotoMenuActivate( int id )
{
  BookMarkData* d = data->at( id );
  if ( d->set ){
    if ( EDIT_WINDOW->selectEditor( d->editor ) ){
      EDITOR_MANAGER->gotoBookmark( d->editor, bID, id );
    } else
      slotDeleteMenuActivate( id );
  }
}

void SBookmark::slotDeleteMenuActivate( int id )
{
  BookMarkData* d = data->at( id );
  if ( d->set ){
    EDITOR_MANAGER->delBookmark( d->editor, bID, id );
    d->set = false;
  }
}

void SBookmark::slotB0()
{
  slotGotoMenuActivate(1);
}

void SBookmark::slotB1()
{
  slotGotoMenuActivate(2);
}

void SBookmark::slotB2()
{
  slotGotoMenuActivate(3);
}

void SBookmark::slotB3()
{
  slotGotoMenuActivate(4);
}

void SBookmark::slotB4()
{
  slotGotoMenuActivate(5);
}

void SBookmark::slotB5()
{
  slotGotoMenuActivate(6);
}

void SBookmark::slotB6()
{
  slotGotoMenuActivate(7);
}

void SBookmark::slotB7()
{
  slotGotoMenuActivate(8);
}

void SBookmark::slotB8()
{
  slotGotoMenuActivate(9);
}

void SBookmark::slotB9()
{
  slotGotoMenuActivate(10);
}

