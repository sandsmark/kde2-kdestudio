#include "selectMethod.h"
#include "classlistview.h"
#include "sparser.h"
#include <studio.h>

#include <checklistbox.h>

#include <qdom.h>
#include <qsizegrip.h>
#include <qheader.h>
#include <qlayout.h>

SelectMethod::SelectMethod( QWidget * parent, const char * name, WFlags f )
: QWidget( parent, name, f )
{
  QHBoxLayout* l = new QHBoxLayout(this,3,3);

  list = new ClassListView(this);
  parents = new CheckListBox(this);
  l->addWidget(list,3);
  l->addWidget(parents,1);

  list->setVScrollBarMode(QScrollView::AlwaysOn);
  list->setHScrollBarMode(QScrollView::AlwaysOn);
  list->setCornerWidget(new QSizeGrip(list));

  parents->allowEditing( false );
  parents->allowActions( false );
  parents->allowIconShow( true );
  parents->showHeader( false );

  connect( list, SIGNAL(returnPressed(QListViewItem*)), SLOT(slotSelected(QListViewItem*)) );
  connect( list, SIGNAL(doubleClicked(QListViewItem*)), SLOT(slotSelected(QListViewItem*)) );
  connect( parents, SIGNAL(itemCheckChange(int,bool)), SLOT(parentChanges(int,bool)) );
}

SelectMethod::~SelectMethod()
{
}

void SelectMethod::slotSelected( QListViewItem* i )
{
  if (i){
    QString method = i->text(0);
    emit methodSelected( method );
    parentWidget()->hide();
  }
}

void SelectMethod::updateClass()
{
  list->clear();
  ClassListViewItem* rootItem = list->setData(parents->item(0)->text());

  for ( uint k = 0; k < parents->count(); k++ )
  {
    uint kk = parents->count()-1-k;
    if ( parents->item(kk)->isChecked() ){
      QDomElement cl = getClassByName( parents->item(kk)->text() );
      if ( !cl.isNull() ) {
        QDomElement f = cl.firstChild().toElement();
        for( ; !f.isNull(); f = f.nextSibling().toElement() ){
          rootItem->processMemberNodes(f);
        }
      }
    }
  }

  list->setFocus();
  if ( list->childCount() != 0 ){
    list->setCurrentItem( list->firstChild() );
    list->setSelected( list->firstChild(), true );
    list->ensureItemVisible( list->firstChild() );
    list->setFocus();
  }
}

QDomElement SelectMethod::getClassByName( const QString& className )
{
  QDomElement f = main_element.firstChild().toElement();
  for( ; !f.isNull(); f = f.nextSibling().toElement() ){
    if ( f.attribute("t").toInt()==(int)TAG_CLASS && f.tagName() == className ) {
      return f;
    }
  }
  return nullElement;
}

bool SelectMethod::init( QDomElement& element, QString className )
{
  main_element = element;
  QDomElement fclass = getClassByName(className);

  if (fclass.isNull())
    return false;

  parents->clear();
  // add self
  CheckListBoxItem* cur = parents->insertItem( fclass.tagName(), true );
  cur->setPixmap( *PARSER->getPixmapFor(fclass) );

  // setup list of parent class
  addParentsForClass( fclass, ACCESS_PUBLIC );
  parents->updateList();

  updateClass();

  return true;
}

void SelectMethod::addParentsForClass( QDomElement& aClass, accessType parentAccess )
{
  QStringList pc = QStringList::split(",",aClass.attribute("p"));
  for ( QStringList::Iterator it = pc.begin(); it != pc.end(); ++it )
  {
    QString cn = *it;
    int naccess = cn.left(1).toInt();
    cn.remove(0,1);

    accessType access = ACCESS_PUBLIC;
    if ( naccess == 1 )
      access = ACCESS_PROTECTED;
    if ( naccess == 2 )
      access = ACCESS_PRIVATE;

    if ( parentAccess == ACCESS_PRIVATE )
      access = ACCESS_PRIVATE;

    if ( parentAccess == ACCESS_PROTECTED )
      if ( access == ACCESS_PUBLIC )
        access = ACCESS_PROTECTED;

    CheckListBoxItem* ct = parents->insertItem( cn, false );
    ct->setPixmap( *PARSER->getPixmapFor(TAG_MEMBER,access) );

    QDomElement pClass = getClassByName(cn);
    if (!pClass.isNull())
      addParentsForClass( pClass, access );
  }
}

void SelectMethod::parentChanges(int,bool)
{
  updateClass();
}
