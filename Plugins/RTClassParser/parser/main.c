typedef enum { FALSE, TRUE } bool;

#include <stdlib.h>		/* to declare malloc(), realloc() */

#include "ctags.h"
#include "main.h"

extern void error()
{
	exit(1);
}

extern void* eMalloc( size )
  const size_t size;
{
  void *buffer = malloc(size);
  if (buffer == NULL)
    error(FATAL, "out of memory");
  return buffer;
}

extern void* eRealloc( ptr, size )
  void *const ptr;
  const size_t size;
{
  void *buffer = realloc(ptr, size);
  if (buffer == NULL)
    error(FATAL, "out of memory");
  return buffer;
}

extern void eFree( ptr )
  void *const ptr;
{
  free(ptr);
}
