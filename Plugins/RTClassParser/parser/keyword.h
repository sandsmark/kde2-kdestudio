#ifndef _KEYWORD_H
#define _KEYWORD_H

extern void addKeyword(const char *const string, int value);
extern int lookupKeyword(const char *const string);
extern void freeKeywordTable();

#endif
