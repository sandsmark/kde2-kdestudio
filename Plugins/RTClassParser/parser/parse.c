typedef enum { FALSE, TRUE } bool;

#include <string.h>
#include <setjmp.h>

#include "ctags.h"
#include "entry.h"
#include "get.h"
#include "keyword.h"
#include "main.h"
#include "parse.h"

/*============================================================================
=   Macros
============================================================================*/
#define isspacetab(c)       (c==' ' || c=='\t')
#define activeToken(st)     (st->token[(int)st->tokenIndex])
#define parentDecl(st)      (st->parent == NULL ? DECL_NONE : st->parent->declaration)
#define isType(token,t)     (bool)((token)->type == t)
#define insideEnumBody(st)  (bool)(st->parent == NULL ? FALSE : (st->parent->declaration == DECL_ENUM))
#define isExternCDecl(st,c) (bool)(c == STRING_SYMBOL  && !st->haveQualifyingName && st->scope == SCOPE_EXTERN)
#define isOneOf(c,s)        (bool)(strchr((s),(c)) != NULL)

/*============================================================================
=   Data declarations
============================================================================*/

enum { NumTokens = 3 };

typedef enum eException {
    ExceptionNone, ExceptionEOF, ExceptionFormattingError,
    ExceptionBraceFormattingError
} exception_t;

//Used to specify type of keyword.
typedef enum eKeywordId {
    KEYWORD_NONE,
    KEYWORD_ATTRIBUTE, KEYWORD_ABSTRACT,
    KEYWORD_bool, KEYWORD_BYTE,
    KEYWORD_CATCH, KEYWORD_CHAR, KEYWORD_CLASS, KEYWORD_CONST,
    KEYWORD_DOUBLE,
    KEYWORD_ENUM, KEYWORD_EXPLICIT, KEYWORD_EXTERN, KEYWORD_EXTENDS,
    KEYWORD_FINAL, KEYWORD_FLOAT, KEYWORD_FRIEND,
    KEYWORD_IMPLEMENTS, KEYWORD_IMPORT, KEYWORD_INLINE, KEYWORD_INT,
    KEYWORD_INTERFACE,
    KEYWORD_LONG,
    KEYWORD_MUTABLE,
    KEYWORD_NAMESPACE, KEYWORD_NEW, KEYWORD_NATIVE,
    KEYWORD_OPERATOR, KEYWORD_OVERLOAD,
    KEYWORD_PRIVATE, KEYWORD_PROTECTED, KEYWORD_PUBLIC,
    KEYWORD_REGISTER,
    KEYWORD_SHORT, KEYWORD_SIGNED, KEYWORD_STATIC, KEYWORD_STRUCT,
    KEYWORD_SYNCHRONIZED,
    KEYWORD_TEMPLATE, KEYWORD_THROW, KEYWORD_THROWS, KEYWORD_TRANSIENT,
    KEYWORD_TRY, KEYWORD_TYPEDEF, KEYWORD_TYPENAME,
    KEYWORD_UNION, KEYWORD_UNSIGNED, KEYWORD_USING,
    KEYWORD_VIRTUAL, KEYWORD_VOID, KEYWORD_VOLATILE,
    KEYWORD_WCHAR_T
} keywordId;

// Used to determine whether keyword is valid for the current language and what its ID is.
typedef struct sKeywordDesc {
  const char *name;
  keywordId id;
} keywordDesc;

// Used for reporting the type of object parsed by nextToken().
typedef enum eTokenType {
    TOKEN_NONE,		/* none */
    TOKEN_ARGS,		/* a parenthetical pair and its contents */
    TOKEN_BRACE_CLOSE,
    TOKEN_BRACE_OPEN,
    TOKEN_COMMA,		/* the comma character */
    TOKEN_DOUBLE_COLON,	/* double colon indicates nested-name-specifier */
    TOKEN_KEYWORD,
    TOKEN_NAME,		/* an unknown name */
    TOKEN_PAREN_NAME,	/* a single name in parentheses */
    TOKEN_SEMICOLON,	/* the semicolon character */
    TOKEN_SPEC,		/* a storage class specifier, qualifier, type, etc. */
    TOKEN_COUNT
} tokenType;

// This describes the scoping of the current statement.
typedef enum eTagScope {
    SCOPE_GLOBAL,		/* no storage class specified */
    SCOPE_STATIC,		/* static storage class */
    SCOPE_EXTERN,		/* external storage class */
    SCOPE_FRIEND,		/* declares access only */
    SCOPE_TYPEDEF,		/* scoping depends upon context */
    SCOPE_COUNT
} tagScope;

typedef enum eDeclaration {
    DECL_NONE,
    DECL_BASE,			/* base type (default) */
    DECL_CLASS,
    DECL_ENUM,
    DECL_FUNCTION,
    DECL_IGNORE,		/* non-taggable "declaration" */
    DECL_INTERFACE,
    DECL_NAMESPACE,
    DECL_NOMANGLE,		/* C++ name demangling block */
    DECL_STRUCT,
    DECL_UNION,
    DECL_COUNT
} declType;

/*  Information about the parent class of a member (if any).
 */
typedef struct sMemberInfo {
    accessType	access;		/* access of current statement */
    accessType	accessDefault;	/* access default for current statement */
} memberInfo;

typedef struct sTokenInfo {
    tokenType     type;
    keywordId     keyword;
    vString*      name;   /* the name of the token */
    vString*      scope;  /* scope name of the token */
    uint lineNumber;	/* line number of tag */
} tokenInfo;

/*  Describes the statement currently undergoing analysis.
 */
typedef struct sStatementInfo {
    tagScope	scope;
    declType	declaration;	/* specifier associated with TOKEN_SPEC */
    bool	gotName;	/* was a name parsed yet? */
    bool	haveQualifyingName;  /* do we have a name we are considering? */
    bool	gotParenName;	/* was a name inside parentheses parsed yet? */
    bool	gotArgs;	/* was a list of parameters parsed yet? */
    bool	isPointer;	/* is 'name' a pointer? */
    impType	implementation;	/* abstract or concrete implementation? */
    unsigned int tokenIndex;	/* currently active token */
    tokenInfo*	token[(int)NumTokens];
    tokenInfo*	context;	/* accumulated scope of current statement */
    tokenInfo*	blockName;	/* name of current block */
    memberInfo	member;		/* information regarding parent class/struct */
    vString*	parentClasses;	/* parent classes */
    vString*  usingNamespace; /* for directive "using namespace" */
    struct sStatementInfo *parent;  /* statement we are nested within */
    const char* funcArg;
} statementInfo;

typedef struct sParenInfo {
    bool isPointer;
    bool isParamList;
    bool isKnrParamList;
    bool isNameCandidate;
    bool invalidContents;
    bool nestedArgs;
    unsigned int parameterCount;
} parenInfo;

/*============================================================================
=   Data definitions
============================================================================*/

static jmp_buf Exception;

static const keywordDesc KeywordTable[] = {
    /* keyword		keyword ID		 */
    { "__attribute__",	KEYWORD_ATTRIBUTE },
    { "catch",		KEYWORD_CATCH },
    { "char",		KEYWORD_CHAR },
    { "class",		KEYWORD_CLASS },
    { "const",		KEYWORD_CONST },
    { "double",		KEYWORD_DOUBLE },
    { "enum",		KEYWORD_ENUM },
    { "explicit",	KEYWORD_EXPLICIT },
    { "extern",		KEYWORD_EXTERN },
    { "float",		KEYWORD_FLOAT },
    { "friend",		KEYWORD_FRIEND },
    { "inline",		KEYWORD_INLINE },
    { "int",		KEYWORD_INT },
    { "long",		KEYWORD_LONG },
    { "mutable",	KEYWORD_MUTABLE },
    { "namespace",	KEYWORD_NAMESPACE },
    { "new",		KEYWORD_NEW },
    { "operator",	KEYWORD_OPERATOR },
    { "overload",	KEYWORD_OVERLOAD },
    { "private",	KEYWORD_PRIVATE },
    { "protected",	KEYWORD_PROTECTED },
    { "public",		KEYWORD_PUBLIC },
    { "register",	KEYWORD_REGISTER },
    { "short",		KEYWORD_SHORT },
    { "signed",		KEYWORD_SIGNED },
    { "static",		KEYWORD_STATIC },
    { "struct",		KEYWORD_STRUCT },
    { "template",	KEYWORD_TEMPLATE },
    { "throw",		KEYWORD_THROW },
    { "try",		KEYWORD_TRY },
    { "typedef",	KEYWORD_TYPEDEF },
    { "typename",	KEYWORD_TYPENAME },
    { "union",		KEYWORD_UNION },
    { "unsigned",	KEYWORD_UNSIGNED },
    { "using",		KEYWORD_USING },
    { "virtual",	KEYWORD_VIRTUAL },
    { "void",		KEYWORD_VOID },
    { "volatile",	KEYWORD_VOLATILE },
    { "wchar_t",	KEYWORD_WCHAR_T }
};

/*============================================================================
=   Function prototypes
============================================================================*/
static void initToken(tokenInfo *const token);
static void advanceToken(statementInfo *const st);
static void retardToken(statementInfo *const st);
static tokenInfo *prevToken(const statementInfo *const st, unsigned int n);
static void setToken(statementInfo *const st, const tokenType type);
static tokenInfo *newToken(void);
static void deleteToken(tokenInfo *const token);
static bool isContextualKeyword(const tokenInfo *const token);
static bool isContextualStatement(const statementInfo *const st);
static bool isMember(const statementInfo *const st);
static void initMemberInfo(statementInfo *const st);
static void reinitStatement(statementInfo *const st, const bool comma);
static void initStatement(statementInfo *const st, statementInfo *const parent);
static const char *tagName(const tagType type);
static int tagLetter(const tagType type);
static tagType declToTagType(const declType declaration);
static void findScopeHierarchy(vString *const string, const statementInfo *const st);
static void makeTag(const tokenInfo *const token, const statementInfo *const st, const tagType type);
static bool isValidTypeSpecifier(const declType declaration);
static void qualifyEnumeratorTag(const statementInfo *const st, const tokenInfo *const nameToken);
static void qualifyFunctionTag(const statementInfo *const st, const tokenInfo *const nameToken);
static void qualifyFunctionDeclTag(const statementInfo *const st, const tokenInfo *const nameToken);
static void qualifyCompoundTag(const statementInfo *const st, const tokenInfo *const nameToken);
static void qualifyBlockTag(statementInfo *const st, const tokenInfo *const nameToken);
static void qualifyVariableTag(const statementInfo *const st, const tokenInfo *const nameToken);
static int skipToNonWhite(void);
static void skipToFormattedBraceMatch(void);
static void skipToMatch(const char *const pair);
static void skipParens(void);
static void skipBraces(void);
static keywordId analyzeKeyword(const char *const name);
static void analyzeIdentifier(tokenInfo *const token);
static void readCustomIdentifier(tokenInfo *const token, const int firstChar);
static void readIdentifier(tokenInfo *const token, const int firstChar);
static void readOperator(statementInfo *const st);
static void copyToken(tokenInfo *const dest, const tokenInfo *const src);
static void setAccess(statementInfo *const st, const accessType access);
static void discardTypeList(tokenInfo *const token);
static void addParentClass(statementInfo *const st, tokenInfo *const token, accessType access);
static void readParents(statementInfo *const st, const int qualifier);
static void processName(statementInfo *const st);
static void processToken(tokenInfo *const token, statementInfo *const st);
static void restartStatement(statementInfo *const st);
static void skipMacro(statementInfo *const st);
static bool skipPostArgumentStuff(statementInfo *const st);
static void analyzePostParens(statementInfo *const st);
static void analyzeParens(statementInfo *const st);
static void addContext(statementInfo *const st, const tokenInfo* const token);
static void processColon(statementInfo *const st);
static int skipInitializer(statementInfo *const st);
static void processInitializer(statementInfo *const st);
static void parseIdentifier(statementInfo *const st, const int c);
static void parseGeneralToken(statementInfo *const st, const int c);
static void nextToken(statementInfo *const st);
static statementInfo *newStatement(statementInfo *const parent);
static void deleteStatement(void);
static void deleteAllStatements(void);
static bool isStatementEnd(const statementInfo *const st);
static void checkStatementEnd(statementInfo *const st);
static void nest(statementInfo *const st, const unsigned int nestLevel);
static void tagCheck(statementInfo *const st);
static void createTags(const unsigned int nestLevel, statementInfo *const parent);
static void buildCKeywordHash(void);
static void init(void);

/*----------------------------------------------------------------------------
*-	Token management
----------------------------------------------------------------------------*/

static void initToken( token )
  tokenInfo *const token;
{
  token->type       = TOKEN_NONE;
  token->keyword    = KEYWORD_NONE;
  token->lineNumber = getFileLine();
  vStringClear(token->name);
  vStringClear(token->scope);
}

static void advanceToken( st )
  statementInfo *const st;
{
  if (st->tokenIndex >= (unsigned int)NumTokens - 1)
    st->tokenIndex = 0;
  else
    ++st->tokenIndex;

  initToken(st->token[st->tokenIndex]);
}

static void retardToken( st )
  statementInfo *const st;
{
  if (st->tokenIndex == 0)
    st->tokenIndex = (unsigned int)NumTokens - 1;
  else
    --st->tokenIndex;
  setToken(st, TOKEN_NONE);
}

static tokenInfo *prevToken( st, n )
  const statementInfo *const st;
  unsigned int n;
{
  unsigned int tokenIndex;
  unsigned int num = (unsigned int)NumTokens;

  tokenIndex = (st->tokenIndex + num - n) % num;

  return st->token[tokenIndex];
}

static void setToken( st, type )
  statementInfo *const st;
  const tokenType type;
{
  tokenInfo *token;

  token = activeToken(st);
  initToken(token);
  token->type = type;
}

static tokenInfo *newToken()
{
  tokenInfo *const token = (tokenInfo *)eMalloc(sizeof(tokenInfo));

  token->name = vStringNew();
  token->scope = vStringNew();
  initToken(token);

  return token;
}

static void deleteToken( token )
  tokenInfo *const token;
{
  if (token != NULL) {
    vStringDelete(token->name);
    vStringDelete(token->scope);
    eFree(token);
  }
}
/*----------------------------------------------------------------------------
*-	Statement management
----------------------------------------------------------------------------*/

static bool isContextualKeyword( token )
    const tokenInfo *const token;
{
    bool result;
    switch (token->keyword)
    {
	case KEYWORD_CLASS:
	case KEYWORD_ENUM:
	case KEYWORD_INTERFACE:
	case KEYWORD_NAMESPACE:
	case KEYWORD_STRUCT:
	case KEYWORD_UNION:
	    result = TRUE;
	    break;

	default:
	    result = FALSE;
	    break;
    }
    return result;
}

static bool isContextualStatement( st )
    const statementInfo *const st;
{
    bool result = FALSE;
    if (st != NULL) switch (st->declaration)
    {
	case DECL_CLASS:
	case DECL_ENUM:
	case DECL_INTERFACE:
	case DECL_NAMESPACE:
	case DECL_STRUCT:
	case DECL_UNION:
	    result = TRUE;
	    break;

	default:
	    result = FALSE;
	    break;
    }
    return result;
}

static bool isMember( st )
  const statementInfo *const st;
{
  bool result;
  if (isType(st->context, TOKEN_NAME))
    result = TRUE;
  else
    result = isContextualStatement(st->parent);
  return result;
}

static void initMemberInfo( st )
    statementInfo *const st;
{
  accessType accessDefault = ACCESS_UNDEFINED;

  if (st->parent != NULL)
    switch (st->parent->declaration) {
      case DECL_ENUM:
      case DECL_NAMESPACE:
      case DECL_UNION:
        accessDefault = ACCESS_UNDEFINED;
        break;
      case DECL_CLASS:
        accessDefault = ACCESS_PRIVATE;
        break;
      case DECL_INTERFACE:
      case DECL_STRUCT:
        accessDefault = ACCESS_PUBLIC;
        break;
      default:
        break;
    }
  st->member.accessDefault = accessDefault;
  st->member.access        = accessDefault;
}

static void reinitStatement( st, partial )
  statementInfo *const st;
  const bool partial;
{
  unsigned int i;
  if (!partial) {
    st->scope = SCOPE_GLOBAL;
    if (isContextualStatement(st->parent))
      st->declaration = DECL_BASE;
    else
      st->declaration = DECL_NONE;
  }
  st->gotParenName = FALSE;
  st->isPointer = FALSE;
  st->implementation = IMP_DEFAULT;
  st->gotArgs = FALSE;
  st->gotName = FALSE;
  st->haveQualifyingName = FALSE;
  st->tokenIndex = 0;

  for (i = 0; i < (unsigned int)NumTokens; ++i)
    initToken(st->token[i]);

  initToken(st->context);
  initToken(st->blockName);
  vStringClear(st->parentClasses);

  // Init member info.
  if (!partial)
    st->member.access = st->member.accessDefault;
}

static void initStatement( st, parent )
    statementInfo *const st;
    statementInfo *const parent;
{
    st->parent = parent;
    initMemberInfo(st);
    reinitStatement(st, FALSE);
}

/*----------------------------------------------------------------------------
*-	Tag generation functions
----------------------------------------------------------------------------*/

static const char *tagName( type )
    const tagType type;
{
    /*  Note that the strings in this array must correspond to the types in
     *  the tagType enumeration.
     */
    static const char *const names[] = {
	"undefined", "class", "enum", "enumerator", "field", "function",
	"interface", "member", "method", "namespace", "prototype",
	"struct", "typedef", "union", "variable", "externvar"
    };

    return names[(int)type];
}

static int tagLetter( type )
    const tagType type;
{
    /*  Note that the characters in this list must correspond to the types in
     *  the tagType enumeration.
     */
    const char *const chars = "?cgeffimmnppstuvx";

    return chars[(int)type];
}

static tagType declToTagType( declaration )
    const declType declaration;
{
    tagType type = TAG_UNDEFINED;

    switch (declaration)
    {
	case DECL_CLASS:	type = TAG_CLASS;	break;
	case DECL_ENUM:		type = TAG_ENUM;	break;
	case DECL_FUNCTION:	type = TAG_FUNCTION;	break;
	case DECL_INTERFACE:	type = TAG_INTERFACE;	break;
	case DECL_NAMESPACE:	type = TAG_NAMESPACE;	break;
	case DECL_STRUCT:	type = TAG_STRUCT;	break;
	case DECL_UNION:	type = TAG_UNION;	break;

	default: break;
    }
    return type;
}

static void findScopeHierarchy( string, st )
    vString *const string;
    const statementInfo *const st;
{
  const char* const anon = "";
  bool nonAnonPresent = FALSE;
  vString *temp = vStringNew();

  vStringClear(string);

  if (isType(st->context, TOKEN_NAME)) {
    vStringCopy(string, st->context->name);
    nonAnonPresent = TRUE;
  }

  if (st->parent != NULL) {
    const statementInfo *s;
    unsigned depth = 0;

    for (s = st->parent  ;  s != NULL  ;  s = s->parent, ++depth)
    {
      if (isContextualStatement(s)) {
        vStringCopy(temp, string);
        vStringClear(string);
        if (isType(s->blockName, TOKEN_NAME)) {
          if (isType(s->context, TOKEN_NAME) && vStringLength(s->context->name) > 0) {
            vStringCat(string, s->context->name);
            vStringCatS(string,"::");
          }
          vStringCat(string, s->blockName->name);
          nonAnonPresent = TRUE;
        } else {
          vStringCopyS(string, anon);
        }
        if (vStringLength(temp) > 0)
          vStringCatS(string,"::");
        vStringCat(string, temp);
      }
    }

    if (!nonAnonPresent)
      vStringClear(string);
  }

  // add "using namespace" directive
  if (vStringLength(st->usingNamespace)!=0) {
    vStringCopy(temp,string);
    vStringCopy(string,st->usingNamespace);
    vStringCatS(string,"::");
    vStringCat(string,temp);
  }
  vStringDelete(temp);
}

static void makeTag( token, st, type )
  const tokenInfo *const token;
  const statementInfo *const st;
  const tagType type;
{
  if (isType(token, TOKEN_NAME)  &&  vStringLength(token->name) > 0 )
  {
    tagEntryInfo e;

    findScopeHierarchy(token->scope, st);
    initTagEntry(&e,vStringValue(token->name));

    e.lineNumber	= token->lineNumber;
    e.kind = type;
    e.parents = vStringValue(st->parentClasses);
    e.scope = vStringValue(token->scope);
    e.funcArg = st->funcArg;
    e.access = st->member.access;
    e.implementation = st->implementation;

    makeTagEntry(&e);
  }
}

static bool isValidTypeSpecifier( declaration )
  const declType declaration;
{
  bool result;
  switch (declaration) {
  case DECL_BASE:
  case DECL_CLASS:
  case DECL_ENUM:
  case DECL_STRUCT:
  case DECL_UNION:
    result = TRUE;
    break;
  default:
    result = FALSE;
    break;
  }
  return result;
}

static void qualifyEnumeratorTag( st, nameToken )
    const statementInfo *const st;
    const tokenInfo *const nameToken;
{
  if (isType(nameToken, TOKEN_NAME))
    makeTag(nameToken, st, TAG_ENUMERATOR);
}

static void qualifyFunctionTag( st, nameToken )
    const statementInfo *const st;
    const tokenInfo *const nameToken;
{
  if (isType(nameToken, TOKEN_NAME)) {
    makeTag(nameToken,st,TAG_FUNCTION);
  }
}

static void qualifyFunctionDeclTag( st, nameToken )
    const statementInfo *const st;
    const tokenInfo *const nameToken;
{
  if (isType(nameToken, TOKEN_NAME))
    if (st->scope == SCOPE_TYPEDEF)
      makeTag(nameToken, st, TAG_TYPEDEF);
    else
      if (isValidTypeSpecifier(st->declaration))
        makeTag(nameToken, st, TAG_PROTOTYPE);
}

static void qualifyCompoundTag( st, nameToken )
    const statementInfo *const st;
    const tokenInfo *const nameToken;
{
  if (isType(nameToken, TOKEN_NAME)) {
    const tagType type = declToTagType(st->declaration);

    if (type != TAG_UNDEFINED)
      makeTag(nameToken, st, type);
  }
}

static void qualifyBlockTag( st, nameToken )
    statementInfo *const st;
    const tokenInfo *const nameToken;
{
  switch (st->declaration) {
    case DECL_CLASS:
    case DECL_ENUM:
    case DECL_INTERFACE:
    case DECL_NAMESPACE:
    case DECL_STRUCT:
    case DECL_UNION:
      qualifyCompoundTag(st, nameToken);
      break;
    default: break;
  }
}

static void qualifyVariableTag( st, nameToken )
  const statementInfo *const st;
  const tokenInfo *const nameToken;
{
  /* We have to watch that we do not interpret a declaration of the
   * form "struct tag;" as a variable definition. In such a case, the
   * token preceding the name will be a keyword.
   */
  if ( isType(nameToken, TOKEN_NAME) && st->declaration != DECL_IGNORE )
    if (st->scope == SCOPE_TYPEDEF)
      makeTag(nameToken, st, TAG_TYPEDEF);
    else
      if (isValidTypeSpecifier(st->declaration)) {
        if (isMember(st)) {
          if (st->scope == SCOPE_GLOBAL || st->scope == SCOPE_STATIC)
            makeTag(nameToken, st, TAG_MEMBER);
        } else {
          if (st->scope == SCOPE_EXTERN || !st->haveQualifyingName)
            makeTag(nameToken, st, TAG_EXTERN_VAR);
          else
            makeTag(nameToken, st, TAG_VARIABLE);
        }
      }
}

/*----------------------------------------------------------------------------
*-	Parsing functions
----------------------------------------------------------------------------*/

/*  Skip to the next non-white character.
 */
static int skipToNonWhite()
{
  int c;
  do
    c = cppGetc();
  while (isspacetab(c) || c == NEWLINE );

  return c;
}

/*  Skips to the next brace in column 1. This is intended for cases where
 *  preprocessor constructs result in unbalanced braces.
 */
static void skipToFormattedBraceMatch()
{
  int c, next;

  c = cppGetc();
  next = cppGetc();
  while (c != EOF  &&  (c != '\n'  ||  next != '}'))
  {
    c = next;
    next = cppGetc();
  }
}

/*  Skip to the matching character indicated by the pair string. If skipping
 *  to a matching brace and any brace is found within a different level of a
 *  #if conditional statement while brace formatting is in effect, we skip to
 *  the brace matched by its formatting. It is assumed that we have already
 *  read the character which starts the group (i.e. the first character of
 *  "pair").
 */
static void skipToMatch( pair )
    const char *const pair;
{
    const bool braceMatching = (bool)(strcmp("{}", pair) == 0);
    const bool braceFormatting = (bool)(isBraceFormat() && braceMatching);
    const unsigned int initialLevel = getDirectiveNestLevel();
    const int begin = pair[0], end = pair[1];
    const uint lineNumber = getFileLine();
    int matchLevel = 1;
    int c = '\0';
    while (matchLevel > 0  &&  (c = cppGetc()) != EOF)
    {
	if (c == begin)
	{
	    ++matchLevel;
	    if (braceFormatting  &&  getDirectiveNestLevel() != initialLevel)
	    {
		skipToFormattedBraceMatch();
		break;
	    }
	}
	else if (c == end)
	{
	    --matchLevel;
	    if (braceFormatting  &&  getDirectiveNestLevel() != initialLevel)
	    {
		skipToFormattedBraceMatch();
		break;
	    }
	}
    }
    if (c == EOF)
    {
	if (braceMatching)
	    longjmp(Exception, (int)ExceptionBraceFormattingError);
	else
	    longjmp(Exception, (int)ExceptionFormattingError);
    }
}

static void skipParens()
{
  const int c = skipToNonWhite();

  if (c == '(')
    skipToMatch("()");
  else
    cppUngetc(c);
}

static void skipBraces()
{
  const int c = skipToNonWhite();

  if (c == '{')
    skipToMatch("{}");
  else
    cppUngetc(c);
}

static keywordId analyzeKeyword( name )
    const char *const name;
{
  const keywordId id = (keywordId)lookupKeyword(name);
  return id;
}

static void analyzeIdentifier( token )
  tokenInfo *const token;
{
  char *const name = vStringValue(token->name);
  const char *replacement = NULL;
  bool parensToo = FALSE;

  if ( !isIgnoreToken(name, &parensToo, &replacement)) {
    if (replacement != NULL)
      token->keyword = analyzeKeyword(replacement);
    else
      token->keyword = analyzeKeyword(vStringValue(token->name));

    if (token->keyword == KEYWORD_NONE)
      token->type = TOKEN_NAME;
    else
      token->type = TOKEN_KEYWORD;
  } else {
    initToken(token);
    if (parensToo) {
      int c = skipToNonWhite();
      if (c == '(')
        skipToMatch("()");
    }
  }
}

static void readCustomIdentifier( token, firstChar )
    tokenInfo *const token;
    const int firstChar;
{
  vString *const name = token->name;
  int c = firstChar;

  initToken(token);

  do {
    vStringPut(name, c);
    c = cppGetc();
  } while (isident(c) || c == ':' );
  vStringTerminate(name);
  cppUngetc(c);		/* unget non-identifier character */

  analyzeIdentifier(token);
}

static void readIdentifier( token, firstChar )
    tokenInfo *const token;
    const int firstChar;
{
  vString *const name = token->name;
  int c = firstChar;

  initToken(token);

  do {
    vStringPut(name, c);
    c = cppGetc();
  } while (isident(c));
  vStringTerminate(name);
  cppUngetc(c);		/* unget non-identifier character */

  analyzeIdentifier(token);
}

static void readOperator( st )
    statementInfo *const st;
{
  const char *const acceptable = "+-*/%^&|~!=<>,[]";
  const tokenInfo* const prev = prevToken(st,1);
  tokenInfo *const token = activeToken(st);
  int c = skipToNonWhite();
;//  vString *const name = token->name;

    /*  When we arrive here, we have the keyword "operator" in 'name'. */
  if (isType(prev, TOKEN_KEYWORD) &&
      (prev->keyword == KEYWORD_ENUM ||prev->keyword == KEYWORD_STRUCT || prev->keyword == KEYWORD_UNION))
    ;	/* ignore "operator" keyword if preceded by these keywords */
  else {
    if (c == '(') { /*  Verify whether this is a valid function call (i.e. "()") operator. */
      if (cppGetc() == ')') {
;//        vStringPut(name, ' ');  /* always separate operator from keyword */
        c = skipToNonWhite();
        if (c == '(')
;//          vStringCatS(name, "()");
      } else {
        skipToMatch("()");
        c = cppGetc();
      }
    } else {
      if (isident1(c)) {
        /* Handle "new" and "delete" operators, and conversion functions
         * (per 13.3.1.1.2[2] of the C++ spec).
         */
        bool whiteSpace = TRUE;	/* default causes insertion of space */
        do {
          if (isspacetab(c))
            whiteSpace = TRUE;
          else {
            if (whiteSpace) {
;//              vStringPut(name, ' ');
              whiteSpace = FALSE;
            }
;//            vStringPut(name, c);
          }
          c = cppGetc();
        } while (! isOneOf(c, "(;")  &&  c != EOF);
;//        vStringTerminate(name);
      } else {
        if (isOneOf(c, acceptable)) {
;//          vStringPut(name, ' ');	/* always separate operator from keyword */
          do {
;//            vStringPut(name, c);
            c = cppGetc();
          } while (isOneOf(c, acceptable));
;//          vStringTerminate(name);
        }
      }
    }
  }
  cppUngetc(c);

  token->type	= TOKEN_NAME;
  token->keyword = KEYWORD_NONE;
  processName(st);
}

static void copyToken( dest, src )
    tokenInfo *const dest;
    const tokenInfo *const src;
{
    dest->type         = src->type;
    dest->keyword      = src->keyword;
    dest->lineNumber   = src->lineNumber;
    vStringCopy(dest->name, src->name);
}

static void setAccess( st, access )
    statementInfo *const st;
    const accessType access;
{
  if (isMember(st)) {
    int c = skipToNonWhite();
    if (c == ':')
      reinitStatement(st, FALSE);
    else
      cppUngetc(c);
    st->member.accessDefault = access;
    st->member.access = access;
  }
}

static void discardTypeList( token )
    tokenInfo *const token;
{
  int c = skipToNonWhite();
  while (isident1(c)) {
    readIdentifier(token, c);
    c = skipToNonWhite();
    if (c == '.'  ||  c == ',')
      c = skipToNonWhite();
  }
  cppUngetc(c);
}

static void addParentClass( st, token, access )
    statementInfo *const st;
    tokenInfo *const token;
    accessType access;
{
  if (vStringLength(st->parentClasses) > 0)
    vStringPut(st->parentClasses, ',');

  switch (access)	{
    case ACCESS_PRIVATE:
      vStringPut(st->parentClasses, '2');
      break;
    case ACCESS_PUBLIC:
      vStringPut(st->parentClasses, '0');
      break;
    case ACCESS_PROTECTED:
      vStringPut(st->parentClasses, '1');
      break;
    default:
      break;
  }

  vStringCat(st->parentClasses, token->name);
}

static void readParents ( st, qualifier )
    statementInfo *const st;
    const int qualifier;
{
  tokenInfo *const token = newToken();
  tokenInfo *const parent = newToken();
  int c;
  accessType access = ACCESS_PRIVATE;

  do {
    c = skipToNonWhite();
    if (c == EOF) {
      longjmp(Exception, (int)ExceptionFormattingError);
    }
    if (isident1(c)) {
      readIdentifier(token, c);
      if (isType(token, TOKEN_NAME))
        vStringCat(parent->name, token->name);
    }
    else
      if (c == qualifier)
        vStringPut(parent->name, c);
      else
        if (isType(token, TOKEN_NAME)) {
          addParentClass(st, parent, access);
          initToken(parent);
          access = ACCESS_PRIVATE;
        }
        if (isType(token, TOKEN_KEYWORD)) {
          switch (token->keyword)	{
            case KEYWORD_PRIVATE:   access = ACCESS_PRIVATE;   break;
            case KEYWORD_PROTECTED: access = ACCESS_PROTECTED; break;
            case KEYWORD_PUBLIC:    access = ACCESS_PUBLIC;    break;
            default:
              break;
          }
        }
  } while (c != '{');
  cppUngetc(c);
  deleteToken(parent);
  deleteToken(token);
}

static void processName( st )
    statementInfo *const st;
{
  if (st->gotName  &&  st->declaration == DECL_NONE)
    st->declaration = DECL_BASE;
  st->gotName = TRUE;
  st->haveQualifyingName = TRUE;
}

static void processToken( token, st )
    tokenInfo *const token;
    statementInfo *const st;
{
    switch (token->keyword)		/* is it a reserved word? */
    {
	default: break;

	case KEYWORD_NONE:	processName(st);			break;
	case KEYWORD_ABSTRACT:	st->implementation = IMP_ABSTRACT;	break;
	case KEYWORD_ATTRIBUTE:	skipParens(); initToken(token);		break;
	case KEYWORD_CATCH:	skipParens(); skipBraces();		break;
	case KEYWORD_CHAR:	st->declaration = DECL_BASE;		break;
	case KEYWORD_CLASS:	st->declaration = DECL_CLASS;		break;
	case KEYWORD_CONST:	st->declaration = DECL_BASE;		break;
	case KEYWORD_DOUBLE:	st->declaration = DECL_BASE;		break;
	case KEYWORD_ENUM:	st->declaration = DECL_ENUM;		break;
	case KEYWORD_EXTENDS:	readParents(st, '.');
				setToken(st, TOKEN_NONE);		break;
	case KEYWORD_FLOAT:	st->declaration = DECL_BASE;		break;
	case KEYWORD_FRIEND:	st->scope	= SCOPE_FRIEND;		break;
	case KEYWORD_IMPLEMENTS:readParents(st, '.');
				setToken(st, TOKEN_NONE);		break;
	case KEYWORD_IMPORT:	st->declaration = DECL_IGNORE;		break;
	case KEYWORD_INT:	st->declaration = DECL_BASE;		break;
	case KEYWORD_INTERFACE: st->declaration = DECL_INTERFACE;	break;
	case KEYWORD_LONG:	st->declaration = DECL_BASE;		break;
	case KEYWORD_NAMESPACE: st->declaration = DECL_NAMESPACE;	break;
	case KEYWORD_OPERATOR:	readOperator(st);			break;
	case KEYWORD_PRIVATE:	setAccess(st, ACCESS_PRIVATE);		break;
	case KEYWORD_PROTECTED:	setAccess(st, ACCESS_PROTECTED);	break;
	case KEYWORD_PUBLIC:	setAccess(st, ACCESS_PUBLIC);		break;
	case KEYWORD_SHORT:	st->declaration = DECL_BASE;		break;
	case KEYWORD_SIGNED:	st->declaration = DECL_BASE;		break;
	case KEYWORD_STRUCT:	st->declaration = DECL_STRUCT;		break;
	case KEYWORD_THROWS:	discardTypeList(token);			break;
	case KEYWORD_TYPEDEF:	st->scope	= SCOPE_TYPEDEF;	break;
	case KEYWORD_UNION:	st->declaration = DECL_UNION;		break;
	case KEYWORD_UNSIGNED:	st->declaration = DECL_BASE;		break;
	case KEYWORD_USING:	st->declaration = DECL_IGNORE;		break;
	case KEYWORD_VOID:	st->declaration = DECL_BASE;		break;
	case KEYWORD_VOLATILE:	st->declaration = DECL_BASE;		break;
	case KEYWORD_VIRTUAL:	st->implementation = IMP_VIRTUAL;	break;

	case KEYWORD_EXTERN:
	    st->scope = SCOPE_EXTERN;
	    st->declaration = DECL_BASE;
	    break;

	case KEYWORD_STATIC:
		st->scope = SCOPE_STATIC;
	    st->declaration = DECL_BASE;
	    break;
    }
}

/*----------------------------------------------------------------------------
*-	Parenthesis handling functions
----------------------------------------------------------------------------*/

static void restartStatement( st )
    statementInfo *const st;
{
    tokenInfo *const save = newToken();
    tokenInfo *token = activeToken(st);

    copyToken(save, token);
    reinitStatement(st, FALSE);
    token = activeToken(st);
    copyToken(token, save);
    deleteToken(save);
    processToken(token, st);
}

static void skipMacro( st )
    statementInfo *const st;
{
    tokenInfo *const prev2 = prevToken(st, 2);

    if (isType(prev2, TOKEN_NAME))
	retardToken(st);
    skipToMatch("()");
}

// Skips over characters following the parameter list.
static bool skipPostArgumentStuff( st )
    statementInfo *const st;
{
  tokenInfo *const token = activeToken(st);
  bool end = FALSE;
  int c = skipToNonWhite();

  do {
    switch (c) {
      case '{': cppUngetc(c); end = TRUE; break;
      case ';': cppUngetc(c); end = TRUE; break;
      case '(': skipToMatch("()");        break;
      case '[': skipToMatch("[]");        break;
    	default:
          break;
    }

  	if (!end)
	  {
      c = skipToNonWhite();
      if (c == EOF)
        end = TRUE;
  	}
  } while (!end);

	setToken(st, TOKEN_NONE);
  return (bool)(c != EOF);
}

static void analyzePostParens( st )
    statementInfo *const st;
{
  int c = skipToNonWhite();

  cppUngetc(c);
  if (isOneOf(c, "{;,="))
	;
  else
    if (! skipPostArgumentStuff(st)) {
      longjmp(Exception, (int)ExceptionFormattingError);
    }
}

static void analyzeParens( st )
    statementInfo *const st;
{
  int matchLevel = 1;
  int c = '\0';
  tokenInfo* arg = newToken();
  tokenInfo* const prev = prevToken(st, 1);
  tokenInfo* token = 0;
  vString*   name = 0;
  bool skip = FALSE;

  if (! isType(prev, TOKEN_NONE)) {   /* in case of ignored enclosing macros */
    st->gotArgs = TRUE;
    setToken(st, TOKEN_ARGS);

    token  = activeToken(st);
    name = token->name;

    while ( matchLevel > 0  &&  c  != EOF && (c = skipToNonWhite()) != EOF )
    {
      switch (c) {
        case '(': ++matchLevel;
          break;
        case ')': --matchLevel;
          break;
      }
      if ( matchLevel == 1 ){
        switch (c) {
          case '&':
          case '*':
            vStringPut(name,c);
            skip = TRUE;
            break;
          case ',':
            vStringPut(name,c);
            vStringPut(name,' ');
            skip = FALSE;
            break;
          default:
            if ( !skip ){
              initToken(arg);
              readCustomIdentifier(arg,c);
              if ( arg->keyword == KEYWORD_CONST ){
                vStringCat(name,arg->name);
                vStringPut(name,' ');
                c = skipToNonWhite();
                initToken(arg);
                readCustomIdentifier(arg,c);
              }

              vStringCat(name,arg->name);
              vStringPut(name,' ');
              skip = TRUE;
          }
        }
      }
    }
    deleteToken(arg);

    if (c == EOF) {
      longjmp(Exception, (int)ExceptionFormattingError);
    }

    vStringTerminate(name);

    c = skipToNonWhite();
    cppUngetc(c);

    advanceToken(st);
    analyzePostParens(st);
  }
}

/*----------------------------------------------------------------------------
*-	Token parsing functions
----------------------------------------------------------------------------*/

static void addContext( st, token )
    statementInfo *const st;
    const tokenInfo *const token;
{
    if (isType(token, TOKEN_NAME))
    {
	if (vStringLength(st->context->name) > 0)
	{
		vStringCatS(st->context->name, "::");
	}
	vStringCat(st->context->name, token->name);
	st->context->type = TOKEN_NAME;
    }
}

static void processColon( st )
    statementInfo *const st;
{
    const int c = skipToNonWhite();
    const bool doubleColon = (bool)(c == ':');

    if (doubleColon)
    {
	setToken(st, TOKEN_DOUBLE_COLON);
	st->haveQualifyingName = FALSE;
    }
    else
    {
	cppUngetc(c);
	if (st->declaration == DECL_CLASS  ||  st->declaration == DECL_STRUCT )
	{
	    readParents(st, ':');
	}
    }
}

/*  Skips over any initializing value which may follow an '=' character in a
 *  variable definition.
 */
static int skipInitializer( st )
    statementInfo *const st;
{
    bool done = FALSE;
    int c;

    while (! done)
    {
	c = skipToNonWhite();

	if (c == EOF)
	    longjmp(Exception, (int)ExceptionFormattingError);
	else switch (c)
	{
	    case ',':
	    case ';': done = TRUE; break;

	    case '0':
		if (st->implementation == IMP_VIRTUAL)
		    st->implementation = IMP_PURE_VIRTUAL;
		break;

	    case '[': skipToMatch("[]"); break;
	    case '(': skipToMatch("()"); break;
	    case '{': skipToMatch("{}"); break;

	    case '}':
		if (insideEnumBody(st))
		    done = TRUE;
		else if (! isBraceFormat())
		{
		    longjmp(Exception, (int)ExceptionBraceFormattingError);
		}
		break;

	    default: break;
	}
    }
    return c;
}

static void processInitializer( st )
  statementInfo *const st;
{
  const bool inEnumBody = insideEnumBody(st);
  const int c = skipInitializer(st);

  if (c == ';')
    setToken(st, TOKEN_SEMICOLON);
  else
    if (c == ',')
      setToken(st, TOKEN_COMMA);
    else
      if (c == '}'  &&  inEnumBody) {
        cppUngetc(c);
        setToken(st, TOKEN_COMMA);
      }

  if (st->scope == SCOPE_EXTERN)
    st->scope = SCOPE_GLOBAL;
}

static void parseIdentifier( st, c )
    statementInfo *const st;
    const int c;
{
  tokenInfo *const token = activeToken(st);

  readIdentifier(token, c);
  if (! isType(token, TOKEN_NONE))
    processToken(token, st);
}

static void parseGeneralToken( st, c )
    statementInfo *const st;
    const int c;
{
  const tokenInfo *const prev = prevToken(st, 1);

  if (isident1(c)) {
    parseIdentifier(st, c);
    if (isType(st->context, TOKEN_NAME) && isType(activeToken(st), TOKEN_NAME) && isType(prev, TOKEN_NAME)) {
      initToken(st->context);
    }
  } else
    if (isExternCDecl(st, c)) {
      st->declaration = DECL_NOMANGLE;
      st->scope = SCOPE_GLOBAL;
    }
}

/*  Reads characters from the pre-processor and assembles tokens, setting
 *  the current statement state.
 */
static void nextToken( st )
    statementInfo *const st;
{
  tokenInfo *token = activeToken(st);

  do {
    int c = skipToNonWhite();
    switch (c) {
      case EOF: longjmp(Exception, (int)ExceptionEOF);        break;
      case '(': analyzeParens(st);  token = activeToken(st);  break;
      case '*': st->haveQualifyingName = FALSE;               break;
      case ',': setToken(st, TOKEN_COMMA);                    break;
      case ':': processColon(st);                             break;
      case ';': setToken(st, TOKEN_SEMICOLON);                break;
      case '<': skipToMatch("<>");                            break;
      case '=': processInitializer(st);                       break;
      case '[': skipToMatch("[]");                            break;
      case '{': setToken(st, TOKEN_BRACE_OPEN);               break;
      case '}': setToken(st, TOKEN_BRACE_CLOSE);              break;
      default:  parseGeneralToken(st, c);                     break;
    }
  } while (isType(token, TOKEN_NONE));
}

/*----------------------------------------------------------------------------
*-	Scanning support functions
----------------------------------------------------------------------------*/

static statementInfo *CurrentStatement = NULL;

static statementInfo *newStatement( parent )
    statementInfo *const parent;
{
  statementInfo *const st = (statementInfo *)eMalloc(sizeof(statementInfo));
  unsigned int i;

  for (i = 0  ;  i < (unsigned int)NumTokens  ;  ++i)
    st->token[i] = newToken();

  st->context = newToken();
  st->blockName = newToken();
  st->parentClasses = vStringNew();

  st->usingNamespace = vStringNew();
  if (parent != NULL)
    vStringCopy(st->usingNamespace,parent->usingNamespace);

  initStatement(st, parent);
  CurrentStatement = st;

  return st;
}

static void deleteStatement()
{
  statementInfo *const st = CurrentStatement;
  statementInfo *const parent = st->parent;
  unsigned int i;

  for (i = 0 ; i < (unsigned int)NumTokens; ++i) {
    deleteToken(st->token[i]);
    st->token[i] = NULL;
  }
  deleteToken(st->blockName);           st->blockName = NULL;
  deleteToken(st->context);             st->context = NULL;
  vStringDelete(st->parentClasses);     st->parentClasses = NULL;
  vStringDelete(st->usingNamespace);    st->usingNamespace = NULL;

  eFree(st);
  CurrentStatement = parent;
}

static void deleteAllStatements()
{
  while (CurrentStatement != NULL)
    deleteStatement();
}

static bool isStatementEnd( st )
  const statementInfo *const st;
{
  const tokenInfo *const token = activeToken(st);
  bool isEnd;

  if (isType(token, TOKEN_SEMICOLON))
    isEnd = TRUE;
  else
    if (isType(token, TOKEN_BRACE_CLOSE))
      isEnd = (bool)(! isContextualStatement(st));
    else
      isEnd = FALSE;

  return isEnd;
}

static void checkStatementEnd( st )
    statementInfo *const st;
{
  const tokenInfo *const token = activeToken(st);

  if (isType(token, TOKEN_COMMA))
    reinitStatement(st, TRUE);
  else
    if (isStatementEnd(st)) {
      reinitStatement(st, FALSE);
      cppEndStatement();
    } else {
      cppBeginStatement();
      advanceToken(st);
    }
}

static void nest( st, nestLevel )
    statementInfo *const st;
    const unsigned int nestLevel;
{
  switch (st->declaration) {
    case DECL_CLASS:
    case DECL_ENUM:
    case DECL_INTERFACE:
    case DECL_NAMESPACE:
    case DECL_NOMANGLE:
    case DECL_STRUCT:
    case DECL_UNION:
      createTags(nestLevel, st);
      break;
    default:
      skipToMatch("{}");
    break;
  }
  advanceToken(st);
  setToken(st, TOKEN_BRACE_CLOSE);
}

static void tagCheck( st )
    statementInfo *const st;
{
  const tokenInfo *const token = activeToken(st);
  const tokenInfo *const prev  = prevToken(st, 1);
  const tokenInfo *const prev2 = prevToken(st, 2);

  switch (token->type) {
  case TOKEN_NAME:
    if (insideEnumBody(st))
      qualifyEnumeratorTag(st, token);
    break;

  case TOKEN_BRACE_OPEN:
    if (isType(prev, TOKEN_ARGS)) {
      if (st->haveQualifyingName) {
        st->declaration = DECL_FUNCTION;
        st->funcArg = vStringValue(prev->name);
        if (isType (prev, TOKEN_NAME))
          copyToken(st->blockName, prev2);
        qualifyFunctionTag(st, prev2);
      }
    } else
      if (isContextualStatement(st)) {
        if (isType (prev, TOKEN_NAME))
          copyToken(st->blockName, prev);
        qualifyBlockTag(st, prev);
      }
    break;

  case TOKEN_SEMICOLON:
  case TOKEN_COMMA:
    if (insideEnumBody(st))
    ;
    else
      if (isType(prev, TOKEN_NAME)) {
        if (prev2->keyword == KEYWORD_NAMESPACE) {
          vStringCopy(st->usingNamespace,prev->name);
        } else {
          if (isContextualKeyword(prev2))
            st->scope = SCOPE_EXTERN;
          qualifyVariableTag(st, prev);
        }
      } else
        if (isType(prev, TOKEN_ARGS)  &&  isType(prev2, TOKEN_NAME)) {
          if (st->isPointer)
            qualifyVariableTag(st, prev2);
          else
            st->funcArg = vStringValue(prev->name);
            qualifyFunctionDeclTag(st, prev2);
        }
    break;

  default: break;
  }
}

/*  Parses the current file and decides whether to write out and tags that
 *  are discovered.
 */
static void createTags( nestLevel, parent )
    const unsigned int nestLevel;
    statementInfo *const parent;
{
  statementInfo *const st = newStatement(parent);

  while (TRUE) {
    tokenInfo *token;
    nextToken(st);
    token = activeToken(st);
    if (isType(token, TOKEN_BRACE_CLOSE)) {
      if (nestLevel > 0)
        break;
      else {
        longjmp(Exception, (int)ExceptionBraceFormattingError);
      }
    } else {
      if (isType(token, TOKEN_DOUBLE_COLON)) {
        addContext(st, prevToken(st, 1));
        advanceToken(st);
      } else {
        tagCheck(st);
        if (isType(token, TOKEN_BRACE_OPEN))
          nest(st, nestLevel + 1);
        checkStatementEnd(st);
      }
    }
  }
  deleteStatement();
}

static void buildCKeywordHash()
{
  const size_t count = sizeof(KeywordTable) / sizeof(KeywordTable[0]);
  size_t i;

  for (i=0;i<count;++i) {
    const keywordDesc *p = &KeywordTable[i];
    addKeyword(p->name, (int)p->id);
  }
}

static void init()
{
  static bool isInitialized = FALSE;

  if (! isInitialized) {
    buildCKeywordHash();
    isInitialized = TRUE;
  }
}

extern void createCTags()
{
  exception_t exception;

  init();
  cppInit();

  exception = (exception_t)setjmp(Exception);
  if (exception == ExceptionNone)
    createTags(0, NULL);
  else
    deleteAllStatements();

  cppTerminate();
}
