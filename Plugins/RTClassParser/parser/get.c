typedef enum { FALSE, TRUE } bool;
#include <string.h>

#include "entry.h"
#include "get.h"
#include "main.h"
#include "vstring.h"

/*============================================================================
=   Macros
============================================================================*/
#define stringMatch(s1,s2)	(strcmp(s1,s2) == 0)
#define isspacetab(c)		((c) == ' ' || (c) == '\t')

/*============================================================================
=   Data declarations
============================================================================*/
typedef enum { COMMENT_NONE, COMMENT_C, COMMENT_CPLUS } Comment;

enum eCppLimits {
    MaxCppNestingLevel = 20,
    MaxDirectiveName = 10
};

/*============================================================================
=   Data declarations
============================================================================*/

/*  Defines the one nesting level of a preprocessor conditional.
 */
typedef struct sConditionalInfo {
    bool ignoreAllBranches;	/* ignoring parent conditional branch */
    bool singleBranch;	/* choose only one branch */
    bool branchChosen;	/* branch already selected */
    bool ignoring;		/* current ignore state */
} conditionalInfo;

/*  Defines the current state of the pre-processor.
 */
typedef struct sCppState {
  int ungetch, ungetch2;	/* ungotten characters, if any */
  bool resolveRequired;	/* must resolve if/else/elif/endif branch */
  struct sDirective {
    enum eState {
      DRCTV_NONE,   /* no known directive - ignore to end of line */
      DRCTV_DEFINE, /* "#define" encountered */
      DRCTV_HASH,   /* initial '#' read; determine directive */
      DRCTV_IF,     /* "#if" or "#ifdef" encountered */
      DRCTV_LINE,   /* "#line" encountered */
      DRCTV_UNDEF   /* "#undef" encountered */
    } state;
    bool	accept;   /* is a directive syntatically permitted? */
    vString * name; /* macro name */
    unsigned int nestLevel; /* level 0 is not used */
    conditionalInfo ifdef[MaxCppNestingLevel];
  } directive;
} cppState;

/*============================================================================
=   Data definitions
============================================================================*/

/*  Use brace formatting to detect end of block.
 */
static bool BraceFormat = FALSE;

static cppState Cpp = {
  '\0', '\0',   /* ungetch characters */
    FALSE,      /* resolveRequired */
  {
    DRCTV_NONE, /* state */
    FALSE,      /* accept */
    NULL,       /* tag name */
    0,          /* nestLevel */
    { {FALSE,FALSE,FALSE,FALSE} } /* ifdef array */
  }  /* directive */
};

/*============================================================================
=   Function prototypes
============================================================================*/
static bool readDirective(int c, char *const name, unsigned int maxLength);
static bool readDefineTag(int c, vString *const name, bool *const parameterized);
static unsigned long readLineNumber(int c);
static conditionalInfo *currentConditional(void);
static bool isIgnore(void);
static bool setIgnore(const bool ignore);
static bool isIgnoreBranch(void);
static void chooseBranch(void);
static bool pushConditional(const bool firstBranchChosen);
static bool popConditional(void);
static bool directiveHash(const int c);
static bool directiveIf(const int c);
static void makeDefineTag(const char *const name);
static void directiveDefine(const int c);
static vString *readFileName(int c);
static void directiveLine(int c);
static bool handleDirective(const int c);
static Comment isComment(void);
static int skipOverCComment(void);
static int skipOverCplusComment(void);
static int skipToEndOfString(void);
static int skipToEndOfChar(void);

/*============================================================================
=   Function definitions
============================================================================*/

extern bool isBraceFormat()
{
  return BraceFormat;
}

extern unsigned int getDirectiveNestLevel ()
{
  return Cpp.directive.nestLevel;
}

extern void cppInit()
{
  BraceFormat = FALSE;

  Cpp.ungetch         = '\0';
  Cpp.ungetch2        = '\0';
  Cpp.resolveRequired = FALSE;

  Cpp.directive.state     = DRCTV_NONE;
  Cpp.directive.accept    = TRUE;
  Cpp.directive.nestLevel = 0;

  if (Cpp.directive.name == NULL)
    Cpp.directive.name = vStringNew();
  else
    vStringClear(Cpp.directive.name);
}

extern void cppTerminate()
{
  if (Cpp.directive.name != NULL) {
    vStringDelete(Cpp.directive.name);
    Cpp.directive.name = NULL;
  }
}

extern void cppBeginStatement()
{
  Cpp.resolveRequired = TRUE;
}

extern void cppEndStatement()
{
  Cpp.resolveRequired = FALSE;
}

/*----------------------------------------------------------------------------
*   Scanning functions
*
*   This section handles preprocessor directives.  It strips out all
*   directives and may emit a tag for #define directives.
*--------------------------------------------------------------------------*/

/*  This puts a character back into the input queue for the source File.
 *  Up to two characters may be ungotten.
 */
extern void cppUngetc( c )
  const int c;
{
  Cpp.ungetch2 = Cpp.ungetch;
  Cpp.ungetch = c;
}

/*  Reads a directive, whose first character is given by "c", into "name".
 */
static bool readDirective( c, name, maxLength )
  int c;
  char *const name;
  unsigned int maxLength;
{
  unsigned int i;

  for (i = 0  ;  i < maxLength - 1  ;  ++i) {
    if (i > 0) {
      c = fileGetc();
      if (c == EOF  ||  ! isalpha(c)) {
        fileUngetc();
        break;
      }
    }
    name[i] = c;
  }
  name[i] = '\0';					/* null terminate */

  return (bool)isspacetab(c);
}

/*  Reads an identifier, whose first character is given by "c", into "tag",
 *  together with the file location and corresponding line number.
 */
static bool readDefineTag( c, name, parameterized )
  int c;
  vString *const name;
  bool *const parameterized;
{
  vStringClear(name);
  do {
    vStringPut(name, c);
  } while (c = fileGetc(), (c != EOF  &&  isident(c)));
  fileUngetc();
  vStringPut(name, '\0');

  *parameterized = (bool)(c == '(');
  return (bool)(isspacetab(c)  ||  c == '(');
}

/*  Reads a line number.
 */
static unsigned long readLineNumber( c )
  int c;
{
  unsigned long lNum = 0;

  while (c != EOF  &&  isdigit(c)) {
    lNum = (lNum * 10) + (c - '0');
    c = fileGetc();
  }
  fileUngetc();

  return lNum;
}

static conditionalInfo *currentConditional()
{
  return &Cpp.directive.ifdef[Cpp.directive.nestLevel];
}

static bool isIgnore()
{
  return Cpp.directive.ifdef[Cpp.directive.nestLevel].ignoring;
}

static bool setIgnore( ignore )
    const bool ignore;
{
  return Cpp.directive.ifdef[Cpp.directive.nestLevel].ignoring = ignore;
}

static bool isIgnoreBranch()
{
  conditionalInfo *const ifdef = currentConditional();

    /*  Force a single branch if an incomplete statement is discovered
     *  en route. This may have allowed earlier branches containing complete
     *  statements to be followed, but we must follow no further branches.
     */
  if (Cpp.resolveRequired  &&  ! BraceFormat)
    ifdef->singleBranch = TRUE;

    /*  We will ignore this branch in the following cases:
     *
     *  1.  We are ignoring all branches (conditional was within an ignored
     *        branch of the parent conditional)
     *  2.  A branch has already been chosen and either of:
     *      a.  A statement was incomplete upon entering the conditional
     *      b.  A statement is incomplete upon encountering a branch
     */
  return (bool)(ifdef->ignoreAllBranches || (ifdef->branchChosen  &&  ifdef->singleBranch));
}

static void chooseBranch()
{
  if (! BraceFormat) {
    conditionalInfo *const ifdef = currentConditional();

    ifdef->branchChosen = (bool)(ifdef->singleBranch || Cpp.resolveRequired);
  }
}

/*  Pushes one nesting level for an #if directive, indicating whether or not
 *  the branch should be ignored and whether a branch has already been chosen.
 */
static bool pushConditional( firstBranchChosen )
    const bool firstBranchChosen;
{
    const bool ignoreAllBranches = isIgnore();	/* current ignore */
    bool ignoreBranch = FALSE;

    if (Cpp.directive.nestLevel < (unsigned int)MaxCppNestingLevel - 1)
    {
	conditionalInfo *ifdef;

	++Cpp.directive.nestLevel;
	ifdef = currentConditional();

	/*  We take a snapshot of whether there is an incomplete statement in
	 *  progress upon encountering the preprocessor conditional. If so,
	 *  then we will flag that only a single branch of the conditional
	 *  should be followed.
	 */
	ifdef->ignoreAllBranches= ignoreAllBranches;
	ifdef->singleBranch	= Cpp.resolveRequired;
	ifdef->branchChosen	= firstBranchChosen;
	ifdef->ignoring		= (bool)(ignoreAllBranches || (
				    ! firstBranchChosen  &&  ! BraceFormat  &&
				    (ifdef->singleBranch)));
	ignoreBranch = ifdef->ignoring;
    }
    return ignoreBranch;
}

/*  Pops one nesting level for an #endif directive.
 */
static bool popConditional()
{
    if (Cpp.directive.nestLevel > 0)
	--Cpp.directive.nestLevel;

    return isIgnore();
}

static vString *readFileName( c )
    int c;
{
    vString *const fileName = vStringNew();
    bool quoteDelimited = FALSE;

    if (c == '"')
    {
	c = fileGetc();			/* skip double-quote */
	quoteDelimited = TRUE;
    }
    while (c != EOF  &&  c != '\n'  &&
	    (quoteDelimited ? (c != '"') : (c != ' '  &&  c != '\t')))
    {
	vStringPut(fileName, c);
	c = fileGetc();
    }
    vStringPut(fileName, '\0');

    return fileName;
}

static void directiveLine( c )
    int c;
{
    if (FALSE/*Option.lineDirectives*/)
    {
	unsigned long lNum = readLineNumber(c);
	vString *fileName;

	/*  Skip over space and tabs.
	*/
	do { c = fileGetc(); } while (c != EOF  &&  (c == ' '  ||  c == '\t'));

	fileName = readFileName(c);
	if (vStringLength(fileName) == 0)
	    vStringDelete(fileName);
	else
	    setSourceFileName(fileName);
	setSourceFileLine(lNum - 1);		/* applies to NEXT line */
    }
    Cpp.directive.state = DRCTV_NONE;
}

static void makeDefineTag( name )
    const char *const name;
{
  tagEntryInfo e;
  initTagEntry(&e, name);
  e.kind = TAG_MACRO;
  makeTagEntry(&e);
}

static void directiveDefine( c )
    const int c;
{
    bool parameterized;

    if (isident1(c))
    {
	readDefineTag(c, Cpp.directive.name, &parameterized);
	makeDefineTag(vStringValue(Cpp.directive.name));
    }
    Cpp.directive.state = DRCTV_NONE;
}

static bool directiveIf( c )
    const int c;
{
    const bool ignore = pushConditional((bool)(c != '0'));

    Cpp.directive.state = DRCTV_NONE;
    return ignore;
}

static bool directiveHash( c )
    const int c;
{
    bool ignore = FALSE;

    if (isdigit(c))
	directiveLine(c);
    else
    {
	char directive[MaxDirectiveName];

	readDirective(c, directive, MaxDirectiveName);
	if (stringMatch(directive, "define"))
	    Cpp.directive.state = DRCTV_DEFINE;
	else if (stringMatch(directive, "undef"))
	    Cpp.directive.state = DRCTV_UNDEF;
	else if (strncmp(directive, "if", (size_t)2) == 0)
	    Cpp.directive.state = DRCTV_IF;
	else if (stringMatch(directive, "elif")  ||
		stringMatch(directive, "else"))
	{
	    ignore = setIgnore(isIgnoreBranch());
	    if (! ignore  &&  stringMatch(directive, "else"))
		chooseBranch();
	    Cpp.directive.state = DRCTV_NONE;
	}
	else if (stringMatch(directive, "endif"))
	{
	    ignore = popConditional();
	    Cpp.directive.state = DRCTV_NONE;
	}
	else if (stringMatch(directive, "line"))
	    Cpp.directive.state = DRCTV_LINE;
	else	/* "pragma", etc. */
	    Cpp.directive.state = DRCTV_NONE;
    }
    return ignore;
}

/*  Handles a pre-processor directive whose first character is given by "c".
 */
static bool handleDirective( c )
    const int c;
{
    bool ignore = FALSE;

    switch (Cpp.directive.state)
    {
	case DRCTV_NONE:	ignore = isIgnore();		break;
	case DRCTV_DEFINE:	directiveDefine(c);		break;
	case DRCTV_HASH:	ignore = directiveHash(c);	break;
	case DRCTV_IF:		ignore = directiveIf(c);	break;
	case DRCTV_LINE:	directiveLine(c);		break;
	case DRCTV_UNDEF:	directiveDefine(c);		break;
    }
    return ignore;
}

/*  Called upon reading of a slash ('/') characters, determines whether a
 *  comment is encountered, and its type.
 */
static Comment isComment()
{
  Comment comment;
  const int next = fileGetc();

  if (next == '*')
    comment = COMMENT_C;
  else
    if (next == '/')
      comment = COMMENT_CPLUS;
    else {
      fileUngetc();
      comment = COMMENT_NONE;
    }

  return comment;
}

/*  Skips over a C style comment. According to ANSI specification a comment
 *  is treated as white space, so we perform this subsitution.
 */
static int skipOverCComment()
{
    int c = fileGetc();

    while (c != EOF)
    {
	if (c != '*')
	    c = fileGetc();
	else
	{
	    const int next = fileGetc();

	    if (next != '/')
		c = next;
	    else
	    {
		c = ' ';			/* replace comment with space */
		break;
	    }
	}
    }
    return c;
}

/*  Skips over a C++ style comment.
 */
static int skipOverCplusComment()
{
    int c;

    while ((c = fileGetc()) != EOF)
    {
	if (c == BACKSLASH)
	    fileGetc();			/* throw away next character, too */
	else if (c == NEWLINE)
	    break;
    }
    return c;
}

/*  Skips to the end of a string, returning a special character to
 *  symbolically represent a generic string.
 */
static int skipToEndOfString()
{
    int c;

    while ((c = fileGetc()) != EOF)
    {
	if (c == BACKSLASH)
	    fileGetc();			/* throw away next character, too */
	else if (c == DOUBLE_QUOTE)
	    break;
    }
    return STRING_SYMBOL;		/* symbolic representation of string */
}

/*  Skips to the end of the three (possibly four) 'c' sequence, returning a
 *  special character to symbolically represent a generic character.
 */
static int skipToEndOfChar()
{
    int c;

    while ((c = fileGetc()) != EOF)
    {
	if (c == BACKSLASH)
	    fileGetc();			/* throw away next character, too */
	else if (c == SINGLE_QUOTE)
	    break;
	else if (c == NEWLINE)
	{
	    fileUngetc();
	    break;
	}
    }
    return CHAR_SYMBOL;		    /* symbolic representation of character */
}

/*  This function returns the next character, stripping out comments,
 *  C pre-processor directives, and the contents of single and double
 *  quoted strings. In short, strip anything which places a burden upon
 *  the tokenizer.
 */
extern int cppGetc()
{
  bool directive = FALSE;
  bool ignore = FALSE;
  int c;

  if (Cpp.ungetch != '\0') {
    c = Cpp.ungetch;
    Cpp.ungetch = Cpp.ungetch2;
    Cpp.ungetch2 = '\0';
    return c;	    /* return here to avoid re-calling debugPutc() */
  }

  do {
    c = fileGetc();
    switch (c) {
      case EOF:
        ignore    = FALSE;
        directive = FALSE;
        break;

    case TAB:
    case SPACE:
      break; /* ignore most white space */

    case NEWLINE:
      if (directive  &&  ! ignore)
        directive = FALSE;
      Cpp.directive.accept = TRUE;
      break;

    case DOUBLE_QUOTE:
      Cpp.directive.accept = FALSE;
      c = skipToEndOfString();
      break;

    case '#':
      if (Cpp.directive.accept) {
        directive = TRUE;
        Cpp.directive.state  = DRCTV_HASH;
        Cpp.directive.accept = FALSE;
      }
      break;

    case SINGLE_QUOTE:
      Cpp.directive.accept = FALSE;
      c = skipToEndOfChar();
      break;

    case '/': {
      const Comment comment = isComment();

      if (comment == COMMENT_C)
        c = skipOverCComment();
      else
        if (comment == COMMENT_CPLUS) {
          c = skipOverCplusComment();
          if (c == NEWLINE)
          fileUngetc();
        } else
          Cpp.directive.accept = FALSE;
      break;
      }

    case '\\': {
      int next = fileGetc();
      if (next == NEWLINE)
        continue;
      else
        fileUngetc();
      break;
      }

    default:
      Cpp.directive.accept = FALSE;
      if (directive)
        ignore = handleDirective(c);
      break;
    }
  } while (directive || ignore);

  return c;
}
