#ifndef _PARSE_H
#define _PARSE_H

typedef enum eImplementation {
    IMP_DEFAULT,
    IMP_ABSTRACT,
    IMP_VIRTUAL,
    IMP_PURE_VIRTUAL,
    IMP_COUNT        /* must be last */
} impType;

typedef enum eVisibilityType {
    ACCESS_UNDEFINED,
    ACCESS_PUBLIC,
    ACCESS_PROTECTED,
    ACCESS_PRIVATE,
} accessType;

typedef enum eTagType {
    TAG_UNDEFINED,
    TAG_NAMESPACE,  /* namespace name */
    TAG_CLASS,      /* class name */
    TAG_STRUCT,     /* structure name */
    TAG_ENUM,       /* enumeration name */
    TAG_UNION,      /* union name */

    TAG_FUNCTION,   /* function definition */
    TAG_PROTOTYPE,  /* function prototype or declaration */
    TAG_ENUMERATOR, /* enumerator (enumeration value) */
    TAG_MEMBER,     /* structure, class or interface member */

    TAG_METHOD,     /* method declaration */

    TAG_INTERFACE,  /* interface declaration */
    TAG_TYPEDEF,    /* typedef name */
    TAG_VARIABLE,   /* variable definition */
    TAG_EXTERN_VAR, /* external variable declaration */
    TAG_MACRO,      /* define */

    TAG_COUNT       /* must be last */
} tagType;

extern void createCTags();

#endif

