#ifndef _ENTRY_H
#define _ENTRY_H

#include <stdio.h>

#include "ctags.h"
#include "parse.h"
#include "vstring.h"

extern uint fileLineNumber;
#define getFileLine() fileLineNumber

//Information about the current tag candidate.
typedef struct sTagEntryInfo {
    uint lineNumber;         /* line number of tag */
    const char* name;        /* name of the tag */
    const char* scope;       /* scope of the tag */
    const char* funcArg;     /* argument for function */
    const char* parents;     /* parents classes for class */
    impType	implementation;  /* abstract or concrete implementation? */
    tagType kind;
    accessType access;
} tagEntryInfo;

#endif
