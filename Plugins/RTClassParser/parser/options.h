#ifndef _OPTIONS_H
#define _OPTIONS_H

#include "vstring.h"

extern stringList* OptionIgnore;

extern bool isIgnoreToken(const char *const name, bool *const pIgnoreParens, const char **const replacement);

extern void initParser();
extern void resetParser();
extern void endParser();

#endif
