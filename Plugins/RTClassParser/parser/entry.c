typedef enum { FALSE, TRUE } bool;

#include <string.h>
#include "entry.h"

extern void initTagEntry( e, name )
  tagEntryInfo *const e;
  const char *const name;
{
  e->lineNumber  = getFileLine();
  e->name        = name;
  e->scope       = 0L;
  e->kind        = TAG_UNDEFINED;
  e->funcArg     = 0L;
  e->parents     = 0L;
}
