#ifndef _GET_H
#define _GET_H

extern bool isBraceFormat(void);
extern unsigned int getDirectiveNestLevel();
extern void cppInit();
extern void cppTerminate();
extern void cppBeginStatement();
extern void cppEndStatement();
extern void cppUngetc(const int c);
extern int cppGetc();

#endif
