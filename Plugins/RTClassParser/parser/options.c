typedef enum { FALSE, TRUE } bool;

#include <string.h>
#include <stdio.h>

#include "ctags.h"
#include "options.h"
#include "main.h"

stringList* OptionIgnore;

extern bool isIgnoreToken( name, pIgnoreParens, replacement )
  const char *const name;
  bool *const pIgnoreParens;
  const char **const replacement;
{
  bool result = FALSE;

  if (OptionIgnore != NULL) {
    const size_t nameLen = strlen(name);
    unsigned int i;

    if (pIgnoreParens != NULL)
      *pIgnoreParens = FALSE;

    for (i=0;i<stringListCount(OptionIgnore);++i) {
      vString *token = stringListItem(OptionIgnore,i);
      if (strncmp(vStringValue(token), name, nameLen) == 0) {
        const size_t tokenLen = vStringLength(token);
        if (nameLen == tokenLen) {
          result = TRUE;
          break;
        } else
          if (tokenLen == nameLen + 1  && vStringChar(token, tokenLen - 1) == '+') {
            result = TRUE;
            if (pIgnoreParens != NULL)
              *pIgnoreParens = TRUE;
              break;
          } else
            if (vStringChar(token, nameLen) == '=') {
              if (replacement != NULL)
                *replacement = vStringValue(token) + nameLen + 1;
              break;
            }
      }
    }
  }
  return result;
}

void saveIgnoreToken( token )
  const char *const token;
{
  vString *const ignoreToken = vStringNewInit(token);
  stringListAdd(OptionIgnore, ignoreToken);
}

void resetParser()
{
  stringListDelete(OptionIgnore);
  OptionIgnore = stringListNew();
  saveIgnoreToken("Q_OBJECT");
  saveIgnoreToken("Q_OBJECT_FAKE");
  saveIgnoreToken("Q_EXPORT");
  saveIgnoreToken("Q_CLASSINFO+");
  saveIgnoreToken("Q_PROPERTY+");
  saveIgnoreToken("Q_OVERRIDE+");
  saveIgnoreToken("Q_ENUMS+");
  saveIgnoreToken("Q_SETS+");
}

void initParser()
{
  OptionIgnore = stringListNew();
  resetParser();
}

void endParser()
{
  stringListDelete(OptionIgnore);
}
