#ifndef _CTAGS_H
#define _CTAGS_H

#include <ctype.h>	/* to define isalnum() and isalpha() */

//Is the character valid as a character of a C identifier?
#define isident(c)	(isalnum(c) || (c) == '_')

//Is the character valid as the first character of a C identifier?
#define isident1(c)	(isalpha(c) || (c) == '_' || (c) == '~')

enum eCharacters {
    SPACE	= ' ',
    NEWLINE	= '\n',
    CRETURN	= '\r',
    FORMFEED	= '\f',
    TAB		= '\t',
    VTAB	= '\v',
    DOUBLE_QUOTE  = '"',
    SINGLE_QUOTE  = '\'',
    BACKSLASH	  = '\\',
    STRING_SYMBOL = ('S' + 0x80),
    CHAR_SYMBOL	  = ('C' + 0x80)
};

#endif

