#ifndef _MAIN_H
#define _MAIN_H

#include <stdio.h>

typedef int errorSelection;
enum eErrorTypes { FATAL = 1, WARNING = 2, PERROR = 4 };

extern void* eMalloc(const size_t size);
extern void* eRealloc(void *const ptr, const size_t size);
extern void eFree(void *const ptr);
extern void error();

#endif
