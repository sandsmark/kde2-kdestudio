typedef enum { FALSE, TRUE } bool;

#include <limits.h>	/* to define INT_MAX */
#include <string.h>

#include "vstring.h"
#include "main.h"

/*============================================================================
=   Data definitions
============================================================================*/
const size_t vStringInitialSize = 32;

/*============================================================================
=   Function prototypes
============================================================================*/
static void vStringResize(vString *const string, const size_t newSize);

/*============================================================================
=   Function definitions
============================================================================*/

static void vStringResize( string, newSize )
  vString *const string;
  const size_t newSize;
{
  char *const newBuffer = (char *)eRealloc(string->buffer, newSize);
  string->size = newSize;
  string->buffer = newBuffer;
}

/*----------------------------------------------------------------------------
*-	External interface
----------------------------------------------------------------------------*/

extern bool vStringAutoResize( string )
    vString *const string;
{
    bool ok = TRUE;

    if (string->size <= INT_MAX / 2)
    {
	const size_t newSize = string->size * 2;

	vStringResize(string, newSize);
    }
    return ok;
}

extern void vStringClear( string )
    vString *const string;
{
    string->length = 0;
    string->buffer[0] = '\0';
}

extern void vStringDelete( string )
    vString *const string;
{
    if (string != NULL)
    {
	if (string->buffer != NULL)
	    eFree(string->buffer);
	eFree(string);
    }
}

extern vString *vStringNew()
{
    vString *const string = (vString *)eMalloc(sizeof(vString));

    string->length = 0;
    string->size   = vStringInitialSize;
    string->buffer = (char *)eMalloc(string->size);

    vStringClear(string);

    return string;
}

extern void vStringPut( string, c )
    vString *const string;
    const int c;
{
    if (string->length == string->size)		/*  check for buffer overflow */
	vStringAutoResize(string);

    string->buffer[string->length] = c;
    if (c != '\0')
	string->length++;
}

extern void vStringCatS( string, s )
    vString *const string;
    const char *const s;
{
    const char *p = s;

    do
	vStringPut(string, *p);
    while (*p++ != '\0');
}

extern vString *vStringNewInit( s )
    const char *const s;
{
    vString *new = vStringNew();

    vStringCatS(new, s);

    return new;
}

extern void vStringNCatS( string, s, length )
    vString *const string;
    const char *const s;
    const size_t length;
{
    const char *p = s;
    size_t remain = length;

    while (*p != '\0'  &&  remain > 0)
    {
	vStringPut(string, *p);
	--remain;
	++p;
    }
    vStringTerminate(string);
}

extern void vStringStrip( string )
    vString *const string;
{
    while (string->length > 0  &&  string->buffer[string->length - 1] == ' ')
    {
	string->length--;
	string->buffer[string->length] = '\0';
    }
}

extern void vStringCopyS( string, s )
    vString *const string;
    const char *const s;
{
    vStringClear(string);
    vStringCatS(string, s);
}

extern void vStringNCopyS( string, s, length )
    vString *const string;
    const char *const s;
    const size_t length;
{
    vStringClear(string);
    vStringNCatS(string, s, length);
}

extern void vStringCopyToLower( dest, src )
    vString *const dest;
    vString *const src;
{
    const size_t length = src->length;
    const char *s = src->buffer;
    char *d;
    size_t i;

    if (dest->size < src->size)
	vStringResize(dest, src->size);
    d = dest->buffer;
    for (i = 0  ;  i < length  ;  ++i)
    {
	int c = s[i];

	d[i] = tolower(c);
    }
    d[i] = '\0';
}

extern void vStringSetLength( string )
    vString *const string;
{
    string->length = strlen(string->buffer);
}

extern stringList* stringListNew()
{
  stringList* const result = (stringList*)eMalloc(sizeof(stringList));
  result->max   = 0;
  result->count = 0;
  result->list  = NULL;
  return result;
}

extern void stringListAdd( current, string )
  stringList *const current;
  vString *string;
{
  enum { incrementalIncrease = 10 };
  if (current->list == NULL) {
    current->count = 0;
    current->max   = incrementalIncrease;
    current->list  = (vString **)eMalloc((size_t)current->max * sizeof(vString *));
  } else
    if (current->count == current->max) {
      current->max += incrementalIncrease;
      current->list = (vString **)eRealloc(current->list,(size_t)current->max * sizeof(vString *));
    }
  current->list[current->count++] = string;
}

extern unsigned int stringListCount( current )
  stringList *const current;
{
  return current->count;
}

extern vString* stringListItem( current, indx )
  stringList *const current;
  const unsigned int indx;
{
  return current->list[indx];
}

extern void stringListClear( current )
  stringList *const current;
{
  unsigned int i;
  for (i = 0  ;  i < current->count  ;  ++i) {
    vStringDelete(current->list[i]);
    current->list[i] = NULL;
  }
  current->count = 0;
}

extern void stringListDelete( current )
  stringList* const current;
{
  if (current != NULL) {
    if (current->list != NULL) {
      stringListClear(current);
      eFree(current->list);
      current->list = NULL;
    }
    current->max   = 0;
    current->count = 0;
    eFree(current);
  }
}

extern bool stringListHas( current, string)
  stringList *const current;
  const char *const string;
{
  bool result = FALSE;
  unsigned int i;
  for (i = 0; ! result  &&  i < current->count; ++i) {
    if (strcmp(string, vStringValue(current->list[i])) == 0)
      result = TRUE;
  }
  return result;
}
