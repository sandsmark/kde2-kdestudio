typedef enum { FALSE, TRUE } bool;

#include <string.h>
#include "keyword.h"
#include "main.h"

/*============================================================================
=   Macros
============================================================================*/
#define HASH_EXPONENT	7	/* must be less than 17 */

/*============================================================================
=   Data declarations
============================================================================*/
typedef struct sHashEntry {
  const char *string;
  struct sHashEntry *next;
  int value;
} hashEntry;

/*============================================================================
=   Data definitions
============================================================================*/
const unsigned int TableSize = 1 << HASH_EXPONENT;
static hashEntry **HashTable = NULL;

/*============================================================================
=   Function prototypes
============================================================================*/
static hashEntry **getHashTable(void);
static hashEntry *getHashTableEntry(unsigned int hashedValue);
static unsigned int hashValue(const char *const string);
static hashEntry *newEntry(const char *const string, int value);

/*============================================================================
=   Function definitions
============================================================================*/

static hashEntry **getHashTable()
{
  static bool allocated = FALSE;
  if (!allocated) {
    unsigned int i;
    HashTable =(hashEntry**)eMalloc((size_t)TableSize * sizeof(hashEntry*));
    for (i = 0; i < TableSize; ++i)
      HashTable[i] = NULL;
    allocated = TRUE;
  }
  return HashTable;
}

static hashEntry *getHashTableEntry( hashedValue )
  unsigned int hashedValue;
{
  hashEntry **const table = getHashTable();
  return table[hashedValue];
}

static unsigned int hashValue( string )
  const char *const string;
{
  unsigned long value = 0;
  const unsigned char *p;

  /*  We combine the various words of the multiword key using the method
   *  described on page 512 of Vol. 3 of "The Art of Computer Programming".
   */
  for (p = (const unsigned char *)string; *p != '\0'; ++p) {
    value <<= 1;
    if (value & 0x00000100L)
      value = (value & 0x000000ffL) + 1L;
    value ^= *p;
  }
  /*  Algorithm from page 509 of Vol. 3 of "The Art of Computer Programming"
   *  Treats "value" as a 16-bit integer plus 16-bit fraction.
   */
  value *= 40503L;		          /* = 2^16 * 0.6180339887 ("golden ratio") */
  value &= 0x0000ffffL;	        /* keep fractional part */
  value >>= 16 - HASH_EXPONENT; /* scale up by hash size and move down */

  return value;
}

static hashEntry *newEntry( string, value )
  const char *const string;
  int value;
{
  hashEntry *const entry = (hashEntry *)eMalloc(sizeof(hashEntry));
  unsigned int i;

  entry->string = string;
  entry->next = NULL;
  entry->value = value;

  return entry;
}

/*  Note that it is assumed that a "value" of zero means an undefined keyword
 *  and clients of this function should observe this. Also, all keywords added
 *  should be added in lower case. If we encounter a case-sensitive language
 *  whose keywords are in upper case, we will need to redesign this.
 */
extern void addKeyword( string, value )
  const char *const string;
  int value;
{
  const unsigned int hashedValue = hashValue(string);
  hashEntry *tableEntry = getHashTableEntry(hashedValue);
  hashEntry *entry = tableEntry;

  if (entry == NULL) {
    hashEntry **const table = getHashTable();
    hashEntry *new = newEntry(string, value);
    table[hashedValue] = new;
  } else {
    hashEntry *prev = NULL;
    while (entry != NULL) {
      if (strcmp(string, entry->string) == 0) {
        entry->value = value;
        break;
      }
      prev = entry;
      entry = entry->next;
    }
    if (entry == NULL) {
      hashEntry *new = newEntry(string, value);
      prev->next = new;
    }
  }
}

extern int lookupKeyword( string )
  const char *const string;
{
  const unsigned int hashedValue = hashValue(string);
  hashEntry *entry = getHashTableEntry(hashedValue);
  int value = 0;

  while (entry != NULL) {
    if (strcmp(string, entry->string) == 0) {
      value = entry->value;
      break;
    }
    entry = entry->next;
  }
  return value;
}

extern void freeKeywordTable()
{
  if (HashTable != NULL) {
    unsigned int i;
    for (i = 0; i < TableSize; ++i) {
      hashEntry *entry = HashTable[i];
      while (entry != NULL) {
        hashEntry *next = entry->next;
        eFree(entry);
        entry = next;
      }
    }
    eFree(HashTable);
  }
}

