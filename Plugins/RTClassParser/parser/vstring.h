#ifndef _VSTRING_H
#define _VSTRING_H

#include <stdlib.h>	/* to define size_t */

/*============================================================================
=   Macros
============================================================================*/
#define vStringValue(vs)	((vs)->buffer)
#define vStringLength(vs)	((vs)->length)
#define vStringSize(vs)		((vs)->size)
#define vStringCat(vs,s)	vStringCatS((vs), vStringValue((s)))
#define vStringNCat(vs,s,l)	vStringNCatS((vs), vStringValue((s)), (l))
#define vStringCopy(vs,s)	vStringCopyS((vs), vStringValue((s)))
#define vStringNCopy(vs,s,l)	vStringNCopyS((vs), vStringValue((s)), (l))
#define vStringChar(vs,i)	((vs)->buffer[i])
#define vStringTerminate(vs)	vStringPut(vs, '\0')

/*============================================================================
=   Data declarations
============================================================================*/
typedef struct sVString {
  size_t length;  /* size of buffer used */
  size_t size;    /* allocated size of buffer */
  char*  buffer;  /* location of buffer */
} vString;

typedef struct sStringList {
  unsigned int max;
  unsigned int count;
  vString** list;
} stringList;
/*============================================================================
=   Function prototypes
============================================================================*/
extern stringList *stringListNew();
extern void stringListAdd(stringList *const current, vString *string);
extern void stringListClear(stringList *const current);
extern unsigned int stringListCount(stringList *const current);
extern vString* stringListItem(stringList *const current, const unsigned int indx);
extern void stringListDelete(stringList *const current);
extern bool stringListHas(stringList *const current, const char *const string);

extern bool vStringAutoResize(vString *const string);
extern void vStringClear(vString *const string);
extern vString *vStringNew(void);
extern void vStringDelete(vString *const string);
extern void vStringPut(vString *const string, const int c);
extern void vStringStrip(vString *const string);
extern void vStringCatS(vString *const string, const char *const s);
extern void vStringNCatS(vString *const string, const char *const s, const size_t length);
extern vString *vStringNewInit(const char *const s);
extern void vStringCopyS(vString *const string, const char *const s);
extern void vStringNCopyS(vString *const string, const char *const s, const size_t length);
extern void vStringCopyToLower(vString *const dest, vString *const src);
extern void vStringSetLength(vString *const string);

#endif
