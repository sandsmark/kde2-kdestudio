#include "rtclassparser.h"
#include "classlistview.h"
#include "sparser.h"

#include "studio.h"
#include "studioview.h"
#include "seditwindow.h"
#include "saction.h"
#include "workspace.h"
#include "kwdoc.h"
#include "kwrite.h"

#include <kapp.h>
#include <kconfig.h>
#include <kdockwidget.h>
#include <kiconloader.h>

#include <qpixmap.h>
#include <qlayout.h>
#include <qsizegrip.h>

RTClassParser::RTClassParser( const char* name )
:QObject(0L,name)
{
  popup = new QFrame( 0L, 0L, WType_Popup );
  popup->setFrameStyle( QFrame::Box | QFrame::Raised );

  KConfig* config = kapp->config();
  config->setGroup("SourceNavigator");
  int dw = config->readNumEntry("Width",150);
  int dh = config->readNumEntry("Height",200);
  popup->resize(dw,dh);

  QVBoxLayout* ly = new QVBoxLayout(popup,2);
  poplv = new ClassListView(popup);
  poplv->setVScrollBarMode(QScrollView::AlwaysOn);
  poplv->setHScrollBarMode(QScrollView::AlwaysOn);
  poplv->setCornerWidget(new QSizeGrip(poplv));
  ly->addWidget(poplv);
}

RTClassParser::~RTClassParser()
{
  KConfig* config = kapp->config();
  config->setGroup("SourceNavigator");
  config->writeEntry("Width",popup->width());
  config->writeEntry("Height",popup->height());
  config->sync();

  delete popup;
  delete parser;
}

void RTClassParser::init()
{
  parserDock = STUDIO_VIEW->createDockWidget( "Class Explorer", BarIcon("class_explorer") );
  lv = new ClassListView( parserDock );
  parserDock->setWidget( lv );
  lv->setFocusPolicy( QWidget::NoFocus );

  parser = new SParser(lv);
  connect(lv,SIGNAL(gotoFileLine(const QString&,int)),EDIT_WINDOW,SLOT(selectLine(const QString&,int)));

  connect( WORKSPACE, SIGNAL(closeMainWorkspace()), SLOT(closeWorkspace()) );
  connect( WORKSPACE, SIGNAL(openMainWorkspace()), SLOT(openWorkspace()) );

  connect( WORKSPACE,
           SIGNAL(insertFilesInWorkspace(QStrList&)),
           parser,
           SLOT(parseFiles(QStrList&)) );

  connect( WORKSPACE,
           SIGNAL(removeFileFromWorkspace(const QString&)),
           parser,
           SLOT(removeFile(const QString&)) );

  connect( EDIT_WINDOW,
           SIGNAL(EditorOpenFile(KWrite*,const QString&)),
           SLOT(editorOpenFile(KWrite*,const QString&)) );

  connect(poplv,
          SIGNAL(gotoFileLine(const QString&,int)),
          SLOT(selectLine(const QString&,int)));

  ACTION->addAction( "sourcenavigator", "Source Nav&igator", Ic(".xpm"), this, SLOT(slotSourceNavigator()), Key_F6,"Source Navigator" );
  ACTION->setMenu( EDITOR_POPUPMENU, "| sourcenavigator");
}

void RTClassParser::closeWorkspace()
{
  parser->reset();
}

void RTClassParser::openWorkspace()
{
  QStrList list;
  WORKSPACE->getAllProjectFilePath( list );

  lv->setBasePathName(WORKSPACE->getDir());
  parser->parseFiles( list );
}

void RTClassParser::editorOpenFile( KWrite* write , const QString& path )
{
  QString name(write->fileName());
  if (SParser::needParsing(name) && name.find(WORKSPACE->getDir())==0 ) {
    typedef void(SParser::*calbackFuncType)(const char*, char*, uint);
    calbackFuncType f = &SParser::reparse;
    write->doc()->setCalbackFunc(parser,*((parserCalbackFuncType*)&f));
  }
}

void RTClassParser::slotSourceNavigator()
{
  KWrite* editor = EDIT_WINDOW->getActiveEditor();
  if ( !editor )
    return;

  QString word = editor->currentWord();
  if ( word.isEmpty() )
    return;

  qApp->setOverrideCursor(waitCursor);

  ClassListView::ElementList found;
  parser->findWord(word,found,parser->rootElement());

  qApp->restoreOverrideCursor();

  if ( found.count() == 0 )
    return;

  poplv->clear();
  ClassListViewItem* rootItem = poplv->setData("Found...");

  ClassListView::ElementListIt it;
  for (it=found.begin();it!=found.end();++it){
    QDomElement f = (*it);

    ClassListView::ElementList tree;
    while (!f.hasAttribute("f")){
      tree.prepend(f);
      f = f.parentNode().toElement();
    }
    rootItem->processList(tree);
  }

  popup->move( editor->mapToGlobal(editor->getTextCursorPosition()) );
  popup->show();
}

void RTClassParser::selectLine( const QString& name,int l )
{
  EDIT_WINDOW->selectLine(name,l);
  popup->hide();
}

