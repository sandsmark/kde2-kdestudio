#include <saction.h>
#include <studio.h>
#include "seditwindow.h"
#include <tools.h>
#include "kwmanager.h"

#include <kapp.h>
#include <kconfig.h>

#include <qdom.h>
#include <qimage.h>
#include <qdir.h>
#include <qstring.h>
#include <qregexp.h>
#include <qfileinfo.h>
#include <qtextstream.h>

#include <qlabel.h>
#include <qlayout.h>
#include <qtooltip.h>

#include "classlistview.h"
#include "sparser.h"
#include "selectMethod.h"
#include "codecompletion.h"

#define STUDIO_PREFIX "WAIT_STUDIO_WAIT"

SCodeCompletion::SCodeCompletion( const char* name )
:QObject( 0L, name )
{
  label = 0L;
  popup = new QFrame( 0L, 0L, WType_Popup );
  popup->installEventFilter( this );
  QVBoxLayout* popupLayout = new QVBoxLayout(popup,2,0);
  popup->setFrameStyle( QFrame::Box | QFrame::Raised );

  sd = new SelectMethod( popup );
  connect( sd, SIGNAL(methodSelected(QString)), this, SLOT(slotSelect(QString)) );
  popupLayout->addWidget(sd);

  KConfig* config = kapp->config();
  config->setGroup("SelectMethod");
  int dw = config->readNumEntry("Width",300);
  int dh = config->readNumEntry("Height",300);
  popup->resize(dw,dh);

  qPopupHistory = new QPopupMenu();
  connect( qPopupHistory, SIGNAL(activated(int)), this, SLOT(slotHistoryActivayed(int)) );
  connect( qPopupHistory, SIGNAL(aboutToShow()), SLOT(slotShowQHistory()) );

  history = new KPopupMenu();
  connect( history, SIGNAL(activated(int)), this, SLOT(slotHistoryActivayed(int)) );

  hlist.setAutoDelete( true );
  historyDict.setAutoDelete( true );

  parserDocument = 0L;
}

SCodeCompletion::~SCodeCompletion()
{
  KConfig* config = kapp->config();
  config->setGroup("SelectMethod");
  config->writeEntry("Width",popup->width());
  config->writeEntry("Height",popup->height());
  config->sync();

  delete popup;
  delete history;
  delete qPopupHistory;

  if (parserDocument) {
    delete parserDocument;
    parserDocument = 0L;
  }
}

void SCodeCompletion::init()
{
  ACTION->addAction( "CodeCompletion", "Code Completio&n", Ic("t_class.xpm"), this, SLOT(slotCodeCompletion()), ALT + Key_P, "Code Completion" );
  ACTION->addAction( "FunctionParameter", "&Function Parameter", Ic("t_class.xpm"), this, SLOT(slotFunctionParam()), ALT + Key_F, "Function Parameter" );
  ACTION->addAction( "HideFunParToolTip", "Hide &Function Parameter", Ic("t_class.xpm"), this, SLOT(slotHideToolTip()), Key_Escape, "Hide Function Parameter" );
  ACTION->addAction( "ShowLastFunParToolTip", "Last...", Ic("t_class.xpm"), this, SLOT(slotShowLabel()), ALT + Key_L, "Last function parameter" );

  ACTION->addAction( "FunctionParameterHistory", "Function Parameter &History", Ic("t_class.xpm"), this, SLOT(slotShowHistory()), CTRL + ALT + Key_F, "Function Parameter history" );
  ACTION->setActionPopupMenuForMenu( "FunctionParameterHistory", qPopupHistory );

  ACTION->setMenu( EDITOR_POPUPMENU, "| CodeCompletion FunctionParameter FunctionParameterHistory");
  ACTION->setActionsEnabled( "FunctionParameterHistory", false );
}

void SCodeCompletion::runMake()
{
  KWrite* editor = STUDIO->currentEditor;
  QString fname = editor->fileName();
  fname.remove( 0, fname.find(":")+1 );
  QFileInfo info( fname );

  QString fccName = info.baseName() + ".fcc";
  modifyMakefile( info.dirPath(), info.fileName(), fccName );

  QDir::setCurrent( info.dirPath() );
  make.clearArguments();
  make << "make" << fccName;

  make.start( KProcess::Block,KProcess::Stderr );

  restoreMakefile( info.dirPath() );
  // remove assembler file...
  QDir().remove( info.dirPath() + "/" + info.baseName() + ".s" );
}

void SCodeCompletion::slotCodeCompletion()
{
  make.disconnect();
  connect(&make,SIGNAL(receivedStderr(KProcess*,char*,int)), this, SLOT(slotMakeError(KProcess*,char*,int)) );

  KWrite* editor = STUDIO->currentEditor;
  if ( editor == 0L ) return;

  editor->updateView();

  QString fname = editor->fileName();
  fname.remove( 0, fname.find(":")+1 );
  QFileInfo info( fname );

  QApplication::setOverrideCursor( waitCursor );

  editor->doc()->setUpdatesEnabled( false );
  int line = editor->currentLine();
  int column = editor->currentColumn();
  TextLine* tl = editor->doc()->textLine( line );

  QString notMember = STUDIO_PREFIX;
  notMember += ";//";

  if ( column == 0 || !(tl->getChar(column-1) == '.' || tl->getChar(column-1) == '>') )
  {
    notMember.prepend( " this->" );
  }
  editor->pasteStr( notMember );
  editor->save();

  out = QString::null;
  runMake();

  editor->undo();
  editor->save();
  editor->doc()->setUpdatesEnabled( true );

  int i = out.find( STUDIO_PREFIX );
  if ( i != -1  ){
    int ie = out.find( "\n", i );
    if ( ie != -1 ){
      out.truncate(ie);
    }

    int ib = out.findRev( "\n", i );
    if ( ib != -1 ){
      out.remove(0,ib+1);
    }

    // preprocessor output file.. for C++
    QString flname = info.dirPath() + "/" + info.baseName() + ".ii";
    int len;
    QRegExp f("`class [^']+'");
    int idx = f.match( out, 0, &len );
    if ( idx != -1 ){
      QString className = out.mid( idx + 7, len - 7 - 1 );

      if ( className.find("<") ) // template
        className = className.left( className.find("<") );

      if (parserDocument)
        delete parserDocument;
      parserDocument = new QDomDocument("ClassData");
      PARSER->parseOneFile(flname,parserDocument);

      // remove preprocessor output file .... after parsing ...
      QDir().remove( flname );

      QDomElement element = parserDocument->documentElement().firstChild().toElement();
      if ( sd->init( element, className ) ){
        popup->move( editor->mapToGlobal( editor->getTextCursorPosition() ) );
        popup->show();
      }
    }
  }
  QApplication::restoreOverrideCursor();
}

void SCodeCompletion::slotMakeError( KProcess* , char* buffer, int buflen )
{
  out += QString().fromLatin1( buffer,buflen );
  if ( out.find( STUDIO_PREFIX ) != -1 )
    make.kill();
}

void SCodeCompletion::slotSelect( QString method )
{
  KWrite* editor = STUDIO->currentEditor;
  if ( editor == 0L || method == QString::null) return;
  editor->pasteStr( method );
}

void SCodeCompletion::modifyMakefile( QString path, QString fileName, QString tempName )
{
  QFile file( path + "/Makefile" );
  QTextStream stream(&file);
  QString str;

  list.clear();
  if(file.open(IO_ReadOnly)){
    while(!stream.eof()){
      list.append(stream.readLine());
    }
  }
  file.close();

  file.open(IO_WriteOnly);

  for ( uint l=0; l<list.count(); l++ ){
    stream << list.at(l);
    stream << "\n";
  }

  stream << "CXXFLAGS = \n";
  stream << tempName + ": " + fileName + "\n";
  stream << "\t$(CXXCOMPILE) $< -save-temps -S -o /dev/null\n";

  file.close();
}

void SCodeCompletion::restoreMakefile( QString path )
{
  QFile file( path + "/Makefile" );
  QTextStream stream(&file);
  QString str;

  file.open(IO_WriteOnly);

  for ( uint l=0; l<list.count(); l++ ){
    stream << list.at(l);
    stream << "\n";
  }
  file.close();
  list.clear();
}

bool SCodeCompletion::eventFilter( QObject* o, QEvent* e )
{
  if ( e->type() == QEvent::Hide ){
      if (parserDocument) {
        delete parserDocument;
        parserDocument = 0L;
      }
  }
  return QObject::eventFilter( o, e );
}

void SCodeCompletion::slotMakeErrorForFunc( KProcess*, char* buffer, int buflen )
{
  out += QString().fromLatin1( buffer,buflen );

  int k = out.find("\n");
  while ( k != -1 ){
    QString s = out.left( k );
    out.remove(0,k+1);

    if ( find_next ){
      int i = s.find(find_str);
      if ( i != -1 ){ // found next candidate
        s.remove(0,i);
        func.append(s);
      } else {
        make.kill();
      }
    } else {
      int y = s.find("candidates are:");
      if ( y != -1 ){   // found first candidate
        s.remove(0,y);
        s.remove(0,s.find(":")+1);
        func.append(s);
        find_next = true;
        find_str = s.left( s.find("::") );
      }
    }

    k = out.find("\n");
  }
}

void SCodeCompletion::slotFunctionParam()
{
  make.disconnect();
  connect(&make,SIGNAL(receivedStderr(KProcess*,char*,int)), this, SLOT(slotMakeErrorForFunc(KProcess*,char*,int)) );

  KWrite* editor = STUDIO->currentEditor;
  if ( editor == 0L ) return;

  editor->updateView();

  QString fname = editor->fileName();
  fname.remove( 0, fname.find(":")+1 );
  QFileInfo info( fname );

  QApplication::setOverrideCursor( waitCursor );

  editor->doc()->setUpdatesEnabled( false );
  int line = editor->currentLine();
  int column = editor->currentColumn();
  TextLine* tl = editor->doc()->textLine( line );

  int cmin = column;
  int needfind = 1; // need find first "("  and skip "()"
  while ( cmin >= 0 && needfind != 0 ){
    cmin--;
    if ( tl->getChar(cmin) == '(' ) needfind--;
    if ( tl->getChar(cmin) == ')' ) needfind++;
  }
  int cmax = column;
  needfind = 1; // need find first ")"  and skip "()"
  while ( cmax < tl->length() && needfind != 0 ){
    if ( tl->getChar(cmax) == ')' ) needfind--;
    if ( tl->getChar(cmax) == '(' ) needfind++;
    cmax++;
  }

  if ( cmin >= 0 && cmax <= tl->length() ){
    PointStruc start;
    PointStruc end;
    start.y  = end.y = line;
    start.x = cmin + 1;
    end.x = cmax - 1;

    bool hasMarked = ( start.x < end.x );
    if ( hasMarked ){
      editor->doc()->selectTo( start ,end ,0 );
      editor->delMarkedText(); // 1 undo
    }
    // create function with parameter (QString,QString,QString,QString,QString); // need 100% error here
    editor->pasteStr("\"KDE\",\"STUDIO\",\"FUNCTION\",\"PARAMETER\",\"DETECT\""); // 2 undo   maximum 5;
    editor->save();

    out = QString::null;
    func.clear();
    find_next = false;

    runMake();

    editor->undo();
    if ( hasMarked )
      editor->undo();

    editor->save();
    // remove preprocessor output file...
    QString flname = info.dirPath() + "/" + info.baseName() + ".ii";
    QDir().remove( flname );

    if ( func.count() != 0 ){
      QString tooltip = func.first();

      // add to history;
      QString hi = func.first();
      hi = hi.stripWhiteSpace();
      hi = hi.left(hi.find("("))+" (...)";
      hlist.append( hi );
      ACTION->setActionsEnabled( "FunctionParameterHistory", true );

      for ( QString s = func.next(); s; s = func.next() ){
        tooltip += "\n";
        tooltip += s;
      }

      historyDict.insert( hi, new QString(tooltip) );

      label = new QLabel( editor, "toolTipLabel", WStyle_Customize | WStyle_NoBorder );
      label->setFrameStyle( QFrame::Plain | QFrame::Box );
      label->setLineWidth( 1 );
      label->setMargin( 1 );
      label->setAlignment( AlignLeft | AlignTop );
      label->setAutoResize( true );
      label->setText( tooltip );
      label->setAutoMask( false );
      label->setBackgroundColor( yellow );

      slotShowLabel();
    }
    func.clear();
    out = QString::null;
  }
  editor->doc()->setUpdatesEnabled( true );
  QApplication::restoreOverrideCursor();
}

void SCodeCompletion::slotHideToolTip()
{
  if ( label ) label->hide();
}

void SCodeCompletion::slotShowLabel()
{
  KWrite* editor = STUDIO->currentEditor;
  if ( label && editor ){
    QPoint p = editor->getTextCursorPosition();
    p -= QPoint( 0, label->height() + 3 );
    label->move( p );
    label->show();
  }
}

void SCodeCompletion::initHistoryPopup( QPopupMenu* menu )
{
  menu->clear();
  if ( menu->inherits("KPopupMenu") ) ((KPopupMenu*)menu)->setTitle("Function parameter");
  lastpopup = menu->insertItem("Last..");
  lastitem = menu->indexOf( menu->insertSeparator() );
  menu->insertSeparator();
  clearitem = menu->insertItem( Ic("folder-trash.xpm"), "Clear history");
}

void SCodeCompletion::slotHistoryActivayed( int id )
{
  if ( id == lastpopup ){
    slotShowLabel();
    return;
  }

  if ( id == clearitem ){
    historyDict.clear();
    hlist.clear();
    ACTION->setActionsEnabled( "FunctionParameterHistory", false );
    return;
  }

  QPopupMenu* m = (QPopupMenu*)sender();
  QString* hs = historyDict.find( m->text(id) );
  if ( hs ){
    label->setText( *hs );
    slotShowLabel();
  }
}

void SCodeCompletion::slotShowHistory()
{
  KWrite* editor = EDIT_WINDOW->getActiveEditor();
  if ( editor ){
    setupHistory( history );
    QPoint p = editor->mapToGlobal( editor->getTextCursorPosition() );
    history->setActiveItem( history->indexOf(lastpopup) );
    history->exec(p);
  }
}

void SCodeCompletion::slotShowQHistory()
{
  setupHistory( qPopupHistory );
}

void SCodeCompletion::setupHistory( QPopupMenu* menu )
{
  #include "method_public.xpm"
  initHistoryPopup( menu );
  for ( QString s = hlist.first(); s; s = hlist.next() ){
    QPixmap pix(method_public_xpm);
    lastitem = menu->indexOf( menu->insertItem(pix,s, -1, lastitem+1) );
  }
}

#undef STUDIO_PREFIX
