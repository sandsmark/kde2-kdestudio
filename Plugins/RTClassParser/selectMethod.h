#ifndef SELECTMETHOD_H
#define SELECTMETHOD_H

#include <qwidget.h>
#include <qdom.h>

extern "C"{
  #include "parse.h"
}
class CheckListBox;
class QListViewItem;
class ClassListView;

class SelectMethod : public QWidget
{ Q_OBJECT
public:
  SelectMethod( QWidget * parent=0, const char * name=0, WFlags f=0 );
  ~SelectMethod();

  bool init( QDomElement&, QString );

protected:
  void updateClass();
  QDomElement getClassByName( const QString& );
  void addParentsForClass( QDomElement&, accessType );

protected slots:
  void parentChanges(int,bool);
  void slotSelected( QListViewItem* );

private:
  CheckListBox* parents;
  ClassListView* list;

  QDomElement nullElement;
  QDomElement main_element;

signals:
  void methodSelected( QString );
};

#endif
