#ifndef SPARSER_H
#define SPARSER_H

extern "C"{
  #include "entry.h"
  #include "options.h"
  #include "main.h"
}

#include <qdom.h>
#include <qobject.h>
#include <qstringlist.h>
#include <qstrlist.h>
#include <qasciidict.h>
#include <qpixmap.h>
#include <qvaluelist.h>

#define PARSER SParser::rtParser

class ClassListView;

class SParser : public QObject
{ Q_OBJECT
public:
  SParser( ClassListView* = 0L );
  ~SParser();

  void reset();

  static SParser* rtParser;
  static bool needParsing( QString& name );

  void reparse( const char* f, char* d, uint s );

  const QDomElement rootElement(){ return m_element;}
  void findWord( const QString&, QValueList<QDomElement>& , const QDomElement& );

  void parseOneFile( const QString& name, QDomDocument* oneDocument, ClassListView* oneBlv  = 0L );

public slots:
  void parseFiles( QStrList& filesList );
  void removeFile( const QString& name );

private:
  void initPixmaps();
  void parseFile( const QString& name, bool open );
  bool openFile( const QString& name );

  void insertInScope( const char* scope, QDomElement& element );

public:
  void makeDOMClassEntry( tagEntryInfo* tag );
  void makeDOMMethodEntry( tagEntryInfo* tag );
  void makeDOMMemberEntry( tagEntryInfo* tag );

private:
  ClassListView* blv;

  QDomDocument* document;
  QDomElement   m_element;
  QDomElement   current;
  QAsciiDict<QDomElement> pathDict;
  bool lock;

  static QPixmap** typesPixmaps;

public:
  static QPixmap* getPixmapFor( QDomElement& );
  static QPixmap* getPixmapFor( tagType, accessType = ACCESS_PUBLIC );

  char* data;
  uint  dataPos;
  uint  size;
};

extern SParser* rtParser;

#endif


