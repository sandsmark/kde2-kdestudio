#ifndef RTCLASSPARSER_H
#define RTCLASSPARSER_H

#include <qobject.h>

class QFrame;

class KDockWidget;
class ClassListView;
class SParser;
class KWrite;

class RTClassParser : public QObject
{ Q_OBJECT
public:
  RTClassParser( const char* name = 0 );
  ~RTClassParser();

  void init();

private slots:
  void closeWorkspace();
  void openWorkspace();

  void editorOpenFile( KWrite*, const QString& );

  void slotSourceNavigator();
  void selectLine( const QString& name,int l );

private:
  KDockWidget* parserDock;
  ClassListView* lv;
  SParser* parser;

  QFrame* popup;
  ClassListView* poplv;
};

#endif


