#include "plugins.h"

#include "rtclassparser.h"
#include "codecompletion.h"

class RTClassParserPlugin : public Plugin
{
public:
  RTClassParserPlugin()
  {
    plgModul1 = new RTClassParser( "Bookmark" );
    plgModul2 = new SCodeCompletion( "CodeCompletion" );
  }
  ~RTClassParserPlugin()
  {
    delete plgModul2;
    delete plgModul1;
  }

  void init()
  {
    plgModul1->init();
    plgModul2->init();
  }

private:
  RTClassParser* plgModul1;
  SCodeCompletion* plgModul2;
};

extern "C"{
  Plugin* Register()
  {
    return new RTClassParserPlugin;
  }
}


