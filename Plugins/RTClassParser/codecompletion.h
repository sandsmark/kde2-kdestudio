#ifndef CODECOMPLETION_H
#define CODECOMPLETION_H

#include <qobject.h>
#include <qstrlist.h>
#include <qdict.h>
#include <kprocess.h>

class QPopupMenu;
class KPopupMenu;
class QLabel;
class KProcess;
class QFrame;
class QDomDocument;

class SelectMethod;

class SCodeCompletion : public QObject
{Q_OBJECT
public:
  SCodeCompletion( const char* name );
  ~SCodeCompletion();

  void init();
  virtual bool eventFilter( QObject *, QEvent * );

protected slots:
  void slotMakeError( KProcess*, char*, int );
  void slotMakeErrorForFunc( KProcess*, char*, int );
  void slotHideToolTip();
  void slotHistoryActivayed( int );
  void slotShowQHistory();

public slots:
  void slotCodeCompletion();
  void slotFunctionParam();
  void slotSelect( QString );
  void slotShowLabel();
  void slotShowHistory();

protected:
  void runMake();
  void modifyMakefile( QString, QString, QString );
  void restoreMakefile( QString );
  void initHistoryPopup( QPopupMenu* );
  void setupHistory( QPopupMenu* );

  QStrList list; // for store Makefile

  QStrList func; // found function definition
  bool find_next;
  QString find_str;
  QLabel* label;

  KPopupMenu* history;
  QPopupMenu* qPopupHistory;
  KProcess make;
  QString out;
  QFrame* popup;
  SelectMethod* sd;

  int lastpopup;
  int lastitem;
  int clearitem;

  QStrList hlist;
  QDict<QString> historyDict;

  QDomDocument* parserDocument;
};

/* For functiom parameter
class max{
public:
void ii(int k){};
void ii(char k){};
};

int main(){
max* m;
m = new max();
m->ii();
};
-------------------------------- compilator error output
A.cpp: In function `int main()':
A.cpp:10: no matching function for call to `max::ii ()'
A.cpp:3: candidates are: max::ii(int)
A.cpp:4:                 max::ii(char)
*/

/* For code completion
class max{
public:
};

int main(){
max* m;
m = new max();
m->not_member_func;
};
-------------------------------- compilator error output
B.cpp: In function `int main()':
B.cpp:8: `class max' has no member named `not_member_func'
*/
#endif


