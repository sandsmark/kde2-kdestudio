#include "sparser.h"
#include "classlistview.h"

#include "studio.h"

#include <qcursor.h>
#include <qfile.h>
#include <qdir.h>
#include <qapp.h>

SParser* SParser::rtParser = 0L;
QPixmap** SParser::typesPixmaps = 0L;

SParser::SParser( ClassListView* list )
:QObject()
{
  data = 0;
  dataPos = 0;
  size = 0;

  SParser::rtParser = this;
  lock = false;

  blv = list;

  pathDict.setAutoDelete(true);

  document = new QDomDocument("ClassData");
  reset();

  initPixmaps();
  initParser();
}

void SParser::initPixmaps()
{
  if (typesPixmaps)
    return;

  #include "fclass.xpm"
  #include "namespace.xpm"
  #include "member_private.xpm"
  #include "member_protected.xpm"
  #include "member_public.xpm"
  #include "method_private.xpm"
  #include "method_protected.xpm"
  #include "method_public.xpm"
  #include "enumerator.xpm"
  #include "struct.xpm"
  #include "enum.xpm"

  typesPixmaps = new QPixmap *[11];

  typesPixmaps[0] = new QPixmap(namespace_xpm);
  typesPixmaps[1] = new QPixmap(class_xpm);
  typesPixmaps[2] = new QPixmap(structure_xpm);
  typesPixmaps[3] = new QPixmap(enum_xpm);

  typesPixmaps[4] = new QPixmap(member_private_xpm);
  typesPixmaps[5] = new QPixmap(member_protected_xpm);
  typesPixmaps[6] = new QPixmap(member_public_xpm);

  typesPixmaps[7] = new QPixmap(method_private_xpm);
  typesPixmaps[8] = new QPixmap(method_protected_xpm);
  typesPixmaps[9] = new QPixmap(method_public_xpm);

  typesPixmaps[10] = new QPixmap(enumerator_xpm);
}

QPixmap* SParser::getPixmapFor( QDomElement& e )
{
  if (!typesPixmaps)
    return 0L;

  tagType x = (tagType)e.attribute("x").toInt();

  switch (x){
    case TAG_NAMESPACE:
      return typesPixmaps[0];
    case TAG_CLASS:
      return typesPixmaps[1];
    case TAG_STRUCT:
      return typesPixmaps[2];
    case TAG_UNION:
      return typesPixmaps[1];
    case TAG_ENUM:
      return typesPixmaps[3];
    case TAG_ENUMERATOR:
      return typesPixmaps[10];
    default:
      break;
  }

  accessType a = (accessType)e.attribute("a").toInt();

  if ( x == TAG_MEMBER ) {
    switch (a){
      case ACCESS_PRIVATE:
        return typesPixmaps[4];
      case ACCESS_PROTECTED:
        return typesPixmaps[5];
      default:
        return typesPixmaps[6];
    }
  }

  tagType t = (tagType)e.attribute("t").toInt();

  if ( t == TAG_METHOD ) {
    switch (a){
      case ACCESS_PRIVATE:
        return typesPixmaps[7];
      case ACCESS_PROTECTED:
        return typesPixmaps[8];
      default:
        return typesPixmaps[9];
    }
  }

  return 0L;
}

QPixmap* SParser::getPixmapFor( tagType t, accessType a )
{
  if (!typesPixmaps)
    return 0L;

  switch (t){
    case TAG_NAMESPACE:
      return typesPixmaps[0];
    case TAG_CLASS:
      return typesPixmaps[1];
    case TAG_STRUCT:
      return typesPixmaps[2];
    case TAG_UNION:
      return typesPixmaps[1];
    case TAG_ENUM:
      return typesPixmaps[3];
    case TAG_ENUMERATOR:
      return typesPixmaps[10];
    default:
      break;
  }

  if ( t == TAG_MEMBER ) {
    switch (a){
      case ACCESS_PRIVATE:
        return typesPixmaps[4];
      case ACCESS_PROTECTED:
        return typesPixmaps[5];
      default:
        return typesPixmaps[6];
    }
  }
  if ( t == TAG_METHOD ) {
    switch (a){
      case ACCESS_PRIVATE:
        return typesPixmaps[7];
      case ACCESS_PROTECTED:
        return typesPixmaps[8];
      default:
        return typesPixmaps[9];
    }
  }

  return 0L;
}

void SParser::reset()
{
  delete document;
  document = new QDomDocument("ClassData");
  m_element = document->createElement("ClassData");
  document->appendChild(m_element);

  if (blv){
    blv->clear();
    blv->setData(m_element); // for empty data init
  }
}

SParser::~SParser()
{
  endParser();
  delete document;
}


void SParser::parseFiles( QStrList& filesList )
{
  if (lock)
    return;

  qApp->setOverrideCursor(waitCursor);
  size = 100*1024;
  data = (char*)eMalloc(size);

  for ( QString it = filesList.first();it!=0L;it = filesList.next()) {
    if (needParsing(it)){
      STATUS(QString("Parsing: ")+it+"...");
      qApp->processEvents();
      parseFile(it,true);
    }
  }

  eFree(data);
  qApp->restoreOverrideCursor();
  STATUS("Ready...");
}

bool SParser::openFile( const QString& name )
{
  QFile f(name);
  if (!f.exists()) {
    debug("File not found : %s",(const char*)name);
    return false;
  }

  uint s = f.size()+1;
  if ( s>size){
    eFree(data);
    size = s;
    data = (char*)eMalloc(size);
  }

  for (uint k=0;k<size;k++) data[k]=EOF;

  if ( f.open( IO_ReadOnly ) ){
    int i = f.readBlock(data,size-1);
    f.close();
    if ( i != -1 ){
      data[size-1] = EOF;
    } else {
      data[0] = EOF;
    }
  } else {
    data[0] = EOF;
  }
  dataPos = 0;
  return true;
}

void SParser::removeFile( const QString& name )
{
  if (lock)
    return;

  QDomElement* e = pathDict.find(name);
  if ( e && !e->isNull() ){
    if (blv)
      blv->removeFileElement(*e);
    m_element.removeChild(*e);
    pathDict.remove(name);
  }
}

void SParser::parseFile( const QString& name, bool open )
{
  if ( open )
    if (!openFile(name))
      return;

  fileLineNumber = 0;
  current = document->createElement("x");
  current.setAttribute("f",name);
  m_element.appendChild(current);

  pathDict.insert(name,new QDomElement(current));

  createCTags();

  if ( blv )
    blv->insertFileElement(current);
}

void SParser::parseOneFile( const QString& name, QDomDocument* oneDocument, ClassListView* oneBlv )
{
  qApp->setOverrideCursor(waitCursor);
  size = 100*1024;
  data = (char*)eMalloc(size);

  STATUS(QString("Parsing: ")+name+"...");
  qApp->processEvents();

  if (!openFile(name)) {
    qApp->restoreOverrideCursor();
    STATUS(QString("Error parsing: ")+name);
    return;
  }
  /******************************************************/
  lock = true;

  QDomDocument* storeDocument = document;
  QDomElement storeElement = m_element;
  ClassListView* storeBlv = blv;

  document = oneDocument;
  m_element = document->createElement("ClassData");
  blv = oneBlv;
  document->appendChild(m_element);

  if (blv){
    blv->clear();
    blv->setData(m_element); // for empty data init
  }

  fileLineNumber = 0;
  current = document->createElement("x");
  current.setAttribute("f",name);
  m_element.appendChild(current);

  createCTags();

  if ( blv )
    blv->insertFileElement(current);

  eFree(data);
  qApp->restoreOverrideCursor();
  STATUS("Ready...");

  document = storeDocument;
  m_element = storeElement;
  blv = storeBlv;

  lock = false;
}

void SParser::insertInScope( const char* scope, QDomElement& element )
{
  QString s(scope);
  QDomElement candidate = current;
  while ( !s.isEmpty() ){
    QString c(s);
    int k = c.find("::");
    if (k!=-1){
      s.remove(0,k+2);
      c.truncate(k);
    } else {
      s.truncate(0);
    }
    QDomElement t = candidate.namedItem(c).toElement();
    if ( t.isNull() ){
      t = document->createElement(c);
      t.setAttribute("t",(int)TAG_CLASS);        // group
      t.setAttribute("x",(int)TAG_CLASS);        // type
      candidate.appendChild(t);
    }
    candidate = t;
  }
  candidate.appendChild(element);
}

void SParser::makeDOMClassEntry( tagEntryInfo* tag )
{
  QDomElement cn = document->createElement(tag->name);
  cn.setAttribute("t",(int)TAG_CLASS);        // group
  cn.setAttribute("x",(int)tag->kind);        // type
  cn.setAttribute("l",tag->lineNumber);       // line
  cn.setAttribute("p",tag->parents);          // parents
  insertInScope(tag->scope,cn);
}

void SParser::makeDOMMethodEntry( tagEntryInfo* tag )
{
  QString sname(tag->name);
  QString sClassName(tag->scope);
  if (sClassName.findRev("::")!=-1)
    sClassName.remove(0,sClassName.findRev("::")+2);

  if ( tag->name[0] == '~' ) // destructor
    sname = "[destructor]";

  if ( sClassName == sname ) // constructor
    sname = "[constructor]";

  QDomElement fn = document->createElement(sname);
  fn.setAttribute("a",(int)tag->access);  // access
  fn.setAttribute("x",(int)tag->kind);    // type
  fn.setAttribute("t",(int)TAG_METHOD);   // group
  fn.setAttribute("l",tag->lineNumber);   // line
  insertInScope(tag->scope,fn);

  // arguments
  QString sarg( tag->funcArg[0]?tag->funcArg:"");

  QDomElement s = document->createElement("f");
  fn.appendChild(s);
  s.setAttribute("x",(int)tag->kind);     // type
  s.setAttribute("t",(int)TAG_METHOD);    // group
  s.setAttribute("a",(int)tag->access);   // access
  s.setAttribute("l",tag->lineNumber);    // line
  s.setAttribute("arg",sarg);
}

void SParser::makeDOMMemberEntry( tagEntryInfo* tag )
{
  QDomElement bn = document->createElement(tag->name);
  bn.setAttribute("x",(int)tag->kind);   // type
  bn.setAttribute("t",(int)TAG_MEMBER);  // group
  bn.setAttribute("a",(int)tag->access); // access
  bn.setAttribute("l",tag->lineNumber);  // line
  insertInScope(tag->scope,bn);
}

void SParser::reparse( const char* f, char* d, uint s )
{
  removeFile(f);

  data = d;
  dataPos = 0;
  size = s;

  parseFile(f,false);
}

bool SParser::needParsing( QString& name )
{
  QString ext = QFileInfo(name).extension();
  return ( ext == "cpp" ||
           ext == "c" ||
           ext == "C" ||
           ext == "cc" ||
           ext == "cxx" ||
           ext == "h" ||
           ext == "H" ||
           ext == "hxx" ||
           ext == "ii" ); // preprocessor file
}

void SParser::findWord( const QString& text, QValueList<QDomElement>& list, const QDomElement& element )
{
  if ( element.tagName() == text )
    list << element;

  QDomElement s = element.firstChild().toElement();
  for( ; !s.isNull(); s = s.nextSibling().toElement() )
      findWord( text, list, s );
}

/*******************************************************************************/
extern "C"{
  uint fileLineNumber;
  extern int fileGetc()
  {
    int c = EOF;
    if ( SParser::rtParser->dataPos < SParser::rtParser->size ){
      c = SParser::rtParser->data[SParser::rtParser->dataPos++];
      if ( c == NEWLINE )
        fileLineNumber++;
    }
    return c;
  }

  extern void fileUngetc()
  {
    if ( SParser::rtParser->dataPos < SParser::rtParser->size ){
      if ( SParser::rtParser->data[--SParser::rtParser->dataPos] == NEWLINE )
        fileLineNumber--;
    }
  }

  extern void makeTagEntry( tagEntryInfo* tag )
  {
    switch (tag->kind) {
      case TAG_NAMESPACE:
      case TAG_CLASS:
      case TAG_STRUCT:
      case TAG_UNION:
      case TAG_ENUM:
        SParser::rtParser->makeDOMClassEntry(tag);
        break;
      case TAG_FUNCTION:
      case TAG_PROTOTYPE:
        SParser::rtParser->makeDOMMethodEntry(tag);
        break;
      case TAG_ENUMERATOR:
      case TAG_MEMBER:
        SParser::rtParser->makeDOMMemberEntry(tag);
        break;
      case TAG_METHOD:
      case TAG_TYPEDEF:
        break;
      default:
        break;
    }
  }
}
