#include "classlistview.h"
#include "sparser.h"
#include <qapplication.h>
#include <qheader.h>

extern "C"{
  #include "entry.h"
}

ClassListView::ClassListView( QWidget* parent, const char *name )
: QListView(parent,name)
{
  setupPixmap();
  setSorting(0);
  setRootIsDecorated(true);
  addColumn("");
  setColumnWidthMode(0,Maximum);
  header()->hide();
  setFrameStyle( QFrame::Panel | QFrame::Sunken );
  setLineWidth(2);

  mainItem = 0L;
}

void ClassListView::contentsMousePressEvent( QMouseEvent* ev )
{
  QListView::contentsMousePressEvent(ev);

  QPoint p(contentsToViewport(ev->pos()));
  ClassListViewItem* i = (ClassListViewItem*)itemAt(p);
  if (!i)
    return;

  if ( ev->pos().x() > treeStepSize() * ( i->depth() + ( rootIsDecorated() ? 1 : 0) ) + itemMargin() )
      ; // p is not in root decoration
  else
      return; // p is in the root decoration

  QDomElement e;

  if (ev->state() == ShiftButton ){
    e = i->m_element;
    if (e.isNull())
      e = i->m_helper;
  } else {
    e = i->m_helper;
    if (e.isNull())
      e = i->m_element;
  }
  int lnum = e.attribute("l").toInt();

  while ( !e.isNull() && e.attribute("f").isEmpty() )
    e = e.parentNode().toElement();

  if ( e.isNull() )
    return;

  emit gotoFileLine(e.attribute("f"),lnum+1);
}

void ClassListView::setupPixmap()
{
  #include "fopen.xpm"
  #include "fclosed.xpm"

  p_fopen = new QPixmap(folder_open_xpm);
  p_fclose = new QPixmap(folder_closed_xpm);
}

QPixmap* ClassListView::pixmap( ClassListViewItem* item )
{
  if ( item == mainItem )
    return PARSER->getPixmapFor(TAG_CLASS);

  if ( item->m_element.isNull() && item->m_helper.isNull() ){
    return item->isOpen() ? p_fopen:p_fclose;
  }

  QDomElement e = item->m_element.isNull() ? item->m_helper:item->m_element;
  return PARSER->getPixmapFor(e);
}

void ClassListView::clear()
{
  QListView::clear();
  removedText.clear();
}

ClassListViewItem* ClassListView::setData( QDomElement& element )
{
  blockSignals( true );

  mainItem = new ClassListViewItem( this , element );
  mainItem->setText(0,element.tagName());
  mainItem->setOpen(true);

  QDomElement f = element.firstChild().toElement();
  for( ; !f.isNull(); f = f.nextSibling().toElement() ){
    insertFileElement(f);
  }

  blockSignals( false );

  return mainItem;
}

ClassListViewItem* ClassListView::setData( const QString& text )
{
  blockSignals( true );

  mainItem = new ClassListViewItem( this , m_null_element );
  mainItem->setText(0,text);
  mainItem->setOpen(true);

  blockSignals( false );

  return mainItem;
}

void ClassListView::setBasePathName( const QString& base )
{
  basePathLength = base.length();
}

void ClassListView::insertFileElement( QDomElement& element, bool remove )
{
  QString fpath = element.attribute("f");
  fpath.remove(0,basePathLength);

  QStringList dirList = QStringList::split("/",fpath);
  dirList.remove(dirList.last()); // remove file name
  if (remove)
    removedText.clear();
  mainItem->processFileNodes( element, dirList, remove );
}

void ClassListView::removeFileElement( QDomElement& element )
{
  insertFileElement(element,true);
}

ClassListView::~ClassListView()
{
  clear();

  delete p_fopen;
  delete p_fclose;
}

/*********************************************************************************************/

ClassListViewItem::ClassListViewItem( ClassListView* parent, QDomElement& element )
: QListViewItem( parent )
{
  m_element = element;
  count = 0;
}

ClassListViewItem::ClassListViewItem( ClassListViewItem* parent, QDomElement& element )
: QListViewItem( parent )
{
  m_element = element;
  count = 0;
}

ClassListViewItem::ClassListViewItem( ClassListViewItem* parent )
: QListViewItem( parent )
{
  count = 0;
}

QString ClassListViewItem::key(int,bool) const
{
  if ( text(0)=="[constructor]")
    return " 0";

  if ( text(0)=="[destructor]")
    return " 1";

  if (m_element.isNull() && m_helper.isNull())
    return QString(" ")+text(0);

  if (!m_element.isNull())
    return m_element.attribute("x")+m_element.attribute("a")+text(0);

  return m_helper.attribute("x")+m_helper.attribute("a")+text(0);
}

void ClassListViewItem::processFileNodes( QDomElement& element, QStringList& dirList, bool remove )
{
  if ( dirList.isEmpty() ){

    QDomElement f = element.firstChild().toElement();
    for( ; !f.isNull(); f = f.nextSibling().toElement() ){
      if ( f.attribute("t").toInt()==(int)TAG_CLASS)
        processClassNodes(f,remove);
      else
        processMemberNodes(f,remove);
    }
    return;
  }

  QString currentDir = dirList.first();
  dirList.remove( dirList.begin() );
  ClassListViewItem* i = dirDict.find(currentDir);
  if ( !i ){
    if ( remove )
      return;

    i = new ClassListViewItem( this );
    i->setText(0,currentDir);
    if (listView()->removedText.findIndex(currentDir)!=-1)
      i->setOpen(true);
    dirDict.insert(currentDir,i);
  }
  i->processFileNodes( element, dirList, remove );

  if ( remove && i->childCount() == 0 ){
    dirDict.remove(currentDir);
    if (i->isOpen())
      listView()->removedText << currentDir;
    delete i;
  }
}

void ClassListViewItem::processClassNodes( QDomElement& element, bool remove )
{
  QString className = element.tagName();
  ClassListViewItem* i = classDict.find(className);
  if ( !i ){
    if ( remove )
      return;

    i = new ClassListViewItem( this, element );
    i->setText(0,className);
    if (listView()->removedText.findIndex(className)!=-1)
      i->setOpen(true);
    classDict.insert(className,i);
  } else {
    if (element.hasAttribute("l"))
      i->m_element = element;
  }

  QDomElement f = element.firstChild().toElement();
  for( ; !f.isNull(); f = f.nextSibling().toElement() ){
    i->processMemberNodes(f,remove);
  }

  if ( remove ){
    i->count--;
    if ( i->count == 0 ){
      classDict.remove(className);
      if (i->isOpen())
        listView()->removedText << className;
      delete i;
    }
  } else {
    i->count++;
  }
}

void ClassListViewItem::processMemberNodes( QDomElement& element, bool remove )
{
  tagType type = (tagType)element.attribute("t").toInt();
  if ( type == TAG_CLASS ){
    processClassNodes(element,remove);
    return;
  }

  if ( type == TAG_MEMBER ){
    QString memberName = element.tagName();
    ClassListViewItem* i = memberDict.find(memberName);
    if ( !i ){
      if ( remove )
        return;
      i = new ClassListViewItem( this, element );
      i->setText(0,memberName);
      if (listView()->removedText.findIndex(memberName)!=-1)
        i->setOpen(true);
      memberDict.insert(memberName,i);
    }
    if ( remove ){
      memberDict.remove(memberName);
      if (i->isOpen())
        listView()->removedText << memberName;
      delete i;
    }
    return;
  }

  if ( type == TAG_METHOD ){
    QString methodName = element.tagName();
    ClassListViewItem* i = methodDict.find(methodName);
    if ( !i ){
      if ( remove )
        return;
      i = new ClassListViewItem(this);
      i->setText(0,methodName);
      if (listView()->removedText.findIndex(methodName)!=-1)
        i->setOpen(true);
      methodDict.insert(methodName,i);
    }
    if (element.attribute("x").toInt()!=(int)TAG_PROTOTYPE)
      i->m_helper = element;
    else
      i->m_element = element;

    // process arguments
    QDomElement f = element.firstChild().toElement();
    for( ; !f.isNull(); f = f.nextSibling().toElement() ){
      i->processArgumentNodes(f,remove);
    }
    if ( remove && i->childCount() == 0 ){
      methodDict.remove(methodName);
      if (i->isOpen())
        listView()->removedText << methodName;
      delete i;
    }
    return;
  }
}

void ClassListViewItem::processArgumentNodes( QDomElement& element, bool remove )
{
  QString argName = element.attribute("arg");
  if (argName.isNull())
    return;

  QString fullName = element.parentNode().nodeName() + "(" + argName + ")";
  ClassListViewItem* i = argDict.find(argName);
  if ( !i ){
    if ( remove )
      return;
    i = new ClassListViewItem(this);
    i->setText(0,fullName);
    if (listView()->removedText.findIndex(fullName)!=-1)
      i->setOpen(true);
    argDict.insert(argName,i);
  }
  if (element.attribute("x").toInt()!=(int)TAG_PROTOTYPE)
    i->m_helper = element;
  else
    i->m_element = element;

  if ( remove ){
    i->count--;
    if ( i->count == 0 ){
      argDict.remove(argName);
      if (i->isOpen())
        listView()->removedText << fullName;
      delete i;
    }
  } else {
    i->count++;
  }
}

int ClassListViewItem::width(const QFontMetrics& fm, const QListView* lv, int ) const
{
  return -(fm.minLeftBearing()+fm.minRightBearing()) + fm.width(text(0)) + lv->itemMargin()*2 + 20;
}

void ClassListViewItem::paintCell( QPainter * p, const QColorGroup & cg, int , int width, int align )
{
  if ( !p ) return;

  ClassListView* lv = (ClassListView*)listView();
  int r = lv->itemMargin();
  int marg = lv->itemMargin();

  QPixmap* op = lv->pixmap(this);

  p->fillRect( 0, 0, width, height(), cg.base() );

  if ( isSelected() )
  {
    p->fillRect( r - marg, 0, width - r + marg, height(), QApplication::winStyleHighlightColor() );
    p->setPen( white );
  } else {
    p->setPen( cg.text() );
  }

  if (op){
    p->drawPixmap( r, (height()-op->height())/2, *op );
    r += op->width() + marg + 3;
  }
  p->drawText( r, 0, width-marg-r, height(), align | AlignVCenter,  text(0) );
}
/**********************************************************************/
void ClassListViewItem::processList( ClassListView::ElementList& l )
{
  ClassListView::ElementListIt it;
  it=l.begin();
  QDomElement elemnt = (*it);

  if ( elemnt.attribute("t").toInt()==(int)TAG_CLASS)
    processClassList(l,it);
  else
    processMemberNodes(elemnt);
}

void ClassListViewItem::processClassList( ClassListView::ElementList& l, ClassListView::ElementListIt& it )
{
  QDomElement element = (*it);

  QString className = element.tagName();
  ClassListViewItem* i = classDict.find(className);
  if ( !i ){
    i = new ClassListViewItem( this, element );
    i->setText(0,className);
    i->setOpen(true);
    classDict.insert(className,i);
  } else {
    if (element.hasAttribute("l"))
      i->m_element = element;
  }

  if ( it == l.end() )
    return;

  ++it;
  element = (*it);

  tagType type = (tagType)element.attribute("t").toInt();
  if ( type == TAG_CLASS ){
    i->processClassList(l,it);
  } else {
    i->processMemberNodes(element);
  }
}

