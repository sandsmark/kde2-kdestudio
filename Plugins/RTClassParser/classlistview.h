#ifndef CLASSLISTVIEW_H
#define CLASSLISTVIEW_H

#include <qlistview.h>
#include <qvaluelist.h>
#include <qdict.h>
#include <qstrlist.h>
#include <qstringlist.h>
#include <qasciidict.h>
#include <qpixmap.h>
#include <qdom.h>

class ClassListViewItem;

class ClassListView : public QListView
{ Q_OBJECT
friend class ClassListViewItem;
public:
	ClassListView( QWidget* parent=0, const char *name=0 );
	~ClassListView();

  void setBasePathName( const QString& );
  ClassListViewItem* setData( QDomElement& );
  ClassListViewItem* setData( const QString& text );
  void clear();

  void insertFileElement( QDomElement& element, bool remove = false );
  void removeFileElement( QDomElement& );

  typedef QValueList<QDomElement> ElementList;
  typedef QValueList<QDomElement>::Iterator ElementListIt;

  QPixmap* pixmap( ClassListViewItem* );

protected:
  virtual void contentsMousePressEvent( QMouseEvent* );

signals:
  void gotoFileLine( const QString&, int );

private:
  void setupPixmap();
  QPixmap* p_fopen;
  QPixmap* p_fclose;

private:
  ClassListViewItem* mainItem;
  QDomElement m_null_element;

  QStringList removedText;
  int basePathLength;
};
/********************************************************************************************/
class ClassListViewItem : public QListViewItem
{
friend class ClassListView;
public:
	ClassListViewItem( ClassListView* parent, QDomElement& );
	ClassListViewItem( ClassListViewItem* parent, QDomElement& );
	ClassListViewItem( ClassListViewItem* parent );

  void processFileNodes( QDomElement& element, QStringList&, bool remove = false );
  void processClassNodes( QDomElement& element, bool remove = false );
  void processMemberNodes( QDomElement& element, bool remove = false );
  void processArgumentNodes( QDomElement& element, bool remove = false );

  void processList( ClassListView::ElementList& );
  void processClassList( ClassListView::ElementList&, ClassListView::ElementListIt& );

  virtual void paintCell( QPainter *, const QColorGroup & cg, int column, int width, int alignment );
  virtual int width ( const QFontMetrics &, const QListView *, int column ) const;

  QDomElement& element(){ return m_element; }

  ClassListView* listView(){ return (ClassListView*)QListViewItem::listView(); }
  virtual QString key(int,bool) const;

protected:
  QAsciiDict<ClassListViewItem> dirDict;
  QAsciiDict<ClassListViewItem> classDict;
  QAsciiDict<ClassListViewItem> memberDict;
  QAsciiDict<ClassListViewItem> methodDict;
  QAsciiDict<ClassListViewItem> argDict;

  QDomElement m_element;
  QDomElement m_helper;
  int count;
};
#endif
